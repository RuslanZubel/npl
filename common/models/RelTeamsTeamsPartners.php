<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rel_partners_partners_statuses".
 *
 * @property int $id
 * @property int $fk_partners Партнер
 * @property int $fk_partners_statuses Статус
 * @property int $fk_author
 * @property int $created_at
 * @property int $updated_at
 * @property string $is_active
 *
 * @property Partners $fkPartners
 * @property PartnersStatuses $fkPartnersStatuses
 * @property Users $fkAuthor
 */
class RelTeamsTeamsPartners extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_teams_teams_partners';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkTeams()
    {
        return $this->hasOne(Teams::className(), ['id' => 'fk_partners']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkTeamsPartners()
    {
        return $this->hasOne(TeamsPartners::className(), ['id' => 'fk_teams_partners']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RelTeamsTeamsPartnersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RelTeamsTeamsPartnersQuery(get_called_class());
    }
}
