<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\Races]].
 *
 * @see \common\models\Races
 */
class RacesQuery extends \common\components\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Races[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Races|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
