<?php

namespace common\models\queries;

use frontend\models\SiteGalleriesImages;
/**
 * This is the ActiveQuery class for [[\common\models\SiteGalleriesImages]].
 *
 * @see \common\models\SiteGalleriesImages
 */
class SiteGalleriesImagesQuery extends \common\components\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\SiteGalleriesImages[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\SiteGalleriesImages|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public static function countImage($galleryId) {
        $images = SiteGalleriesImages::find()->where(['fk_site_galleries' => $galleryId])->all();
        return count($images);
    }

    public static function getCover($galleryId) {
        $image = SiteGalleriesImages::find()->where(['fk_site_galleries' => $galleryId, 'is_cover' => 'yes'])->one();
        return $image;
    }

    public static function getFirstImage($galleryId) {
        $image = SiteGalleriesImages::find()->where(['fk_site_galleries' => $galleryId])->orderBy(['sort' => SORT_ASC])->one();
        return $image;
    }
}
