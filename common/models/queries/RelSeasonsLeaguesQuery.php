<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[RelSeasonsLeagues]].
 *
 * @see RelSeasonsLeagues
 */
class RelSeasonsLeaguesQuery extends \common\components\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return RelSeasonsLeagues[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RelSeasonsLeagues|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
