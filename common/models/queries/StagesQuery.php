<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\Stages]].
 *
 * @see \common\models\Stages
 */
class StagesQuery extends \common\components\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Stages[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Stages|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
