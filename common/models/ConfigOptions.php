<?php

namespace common\models;

/**
 * This is the model class for table "config_options".
 *
 * @property int $id
 * @property string $setting
 * @property string $value
 */
class ConfigOptions extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'config_options';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'setting' => 'Параметр',
            'value' => 'Значение',
        ];
    }

}
