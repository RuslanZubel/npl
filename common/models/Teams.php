<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "teams".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $is_active
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property int $agent
 * @property string $image
 * @property string $cover_image
 *
 * @property User $fkAuthor
 */
class Teams extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teams';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'backend\components\Upload',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 200, 'height' => 200],
                ],
                'zoomCrop' => false,
                'filePath' => '@imgTeamsLogo/[[pk]].jpg',
                'fileUrl' => '@imgTeamsLogoUrl/[[pk]].jpg',
                'thumbPath' => '@imgTeamsLogo/thumbs/[[pk]].jpg',
                'thumbUrl' => '@imgTeamsLogoUrl/thumbs/[[pk]].jpg'
            ],
            'cover_image' => [
                'class' => 'backend\components\Upload',
                'attribute' => 'cover_image',
                'thumbs' => [
                    'thumb' => ['width' => 1200, 'height' => 600],
                ],
                'filePath' => '@imgTeamsCover/[[pk]].jpg',
                'fileUrl' => '@imgTeamsCoverUrl/[[pk]].jpg',
                'thumbPath' => '@imgTeamsCover/thumbs/[[pk]].jpg',
                'thumbUrl' => '@imgTeamsCoverUrl/thumbs/[[pk]].jpg'
            ],

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelTeamsTeamsPartners()
    {
        return $this->hasMany(RelTeamsTeamsPartners::className(), ['fk_teams' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScores()
    {
        return $this->hasMany(Scores::className(), ['fk_teams' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\TeamsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\TeamsQuery(get_called_class());
    }

    public static function getList($params = []){
        return ArrayHelper::map(
            self::find()
                ->orderBy([
                    'title'  =>  SORT_ASC
                ])
                ->asArray()
                ->all(),
            'id',
            'title'
        ) ;
    }

}
