<?php

namespace common\models;

/**
 * This is the model class for table "rel_receipt_trainings".
 *
 * @property int $id rel_receipt_trainings id
 * @property Receipt $receipt_id referred receipt id
 * @property Training $training_id referred training id
 *
 */
class RelReceiptTraining extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_receipt_trainings';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkReceipt()
    {
        return $this->hasOne(Receipt::className(), ['id' => 'fk_rel_training_receipt']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkTraining()
    {
        return $this->hasOne(Training::className(), ['id' => 'fk_rel_receipt_training']);
    }
}