<?php

namespace common\models;
use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property int $id id страницы
 * @property string $type тип страницы
 * @property string $title Заголовок
 * @property string $shorttitle Короткий заголовок
 * @property string $text Содержимое
 * @property string $shorttext Анонс
 * @property int $sort сортировка
 * @property int $fk_author автор
 * @property int $created_at Дата создания
 * @property int $updated_at Дата редактирования
 * @property string $is_active Видимость
 *
 * @property Users $fkAuthor
 */
class Pages extends BaseModel
{
	
	const TYPE_PAGE_ABOUT = 'about';
	const TYPE_PAGE_DOCUMENTS = 'document_root';
	
	public static function getAllowedPageTypes(){
		return [ self::TYPE_PAGE_ABOUT, self::TYPE_PAGE_DOCUMENTS ];
	}
	
	public function getPageLabel() {
		return $this->shorttitle;
	}
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }


    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'backend\components\Upload',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 200, 'height' => 200],
                ],
                'zoomCrop' => false,
                'filePath' => '@imgPages/[[pk]].jpg',
                'fileUrl' => '@imgPagesUrl/[[pk]].jpg',
                'thumbPath' => '@imgPages/thumbs/[[pk]].jpg',
                'thumbUrl' => '@imgPagesUrl/thumbs/[[pk]].jpg'
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\PagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\PagesQuery(get_called_class());
    }
}
