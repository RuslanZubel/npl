<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rel_seasons_leagues".
 *
 * @property int $id
 * @property int $fk_seasons
 * @property int $fk_leagues
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property string $is_active
 *
 * @property Users $fkAuthor
 * @property Leagues $fkLeagues
 * @property Seasons $fkSeasons
 * @property RelStagesRelSeasonsLeagues[] $relStagesRelSeasonsLeagues
 */
class RelSeasonsLeagues extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_seasons_leagues';
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkLeagues()
    {
        return $this->hasOne(Leagues::className(), ['id' => 'fk_leagues']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkSeasons()
    {
        return $this->hasOne(Seasons::className(), ['id' => 'fk_seasons']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelTeamsRelSeasonsLeagues()
    {
        return $this->hasMany(RelTeamsRelSeasonsLeagues::className(), ['fk_rel_seasons_leagues' => 'id']);
    }

    public function getRelStages()
    {
        return $this->hasMany(Stages::className(), ['fk_rel_seasons_leagues' => 'id']);
    }

    public function getFkPartnersPartnersStatuses() {
        return $this->hasManyEx(RelPartnersPartnersStatuses::className(), ['fk_rel_seasons_leagues' => 'id']);
    }

    /**
     * @inheritdoc
     * @return RelSeasonsLeaguesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RelSeasonsLeaguesQuery(get_called_class());
    }
}
