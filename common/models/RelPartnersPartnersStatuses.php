<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rel_partners_partners_statuses".
 *
 * @property int $id
 * @property int $fk_partners Партнер
 * @property int $fk_partners_statuses Статус
 * @property int $fk_author
 * @property int $created_at
 * @property int $updated_at
 * @property string $is_active
 *
 * @property Partners $fkPartners
 * @property PartnersStatuses $fkPartnersStatuses
 * @property Users $fkAuthor
 */
class RelPartnersPartnersStatuses extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_partners_partners_statuses';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkPartners()
    {
        return $this->hasOne(Partners::className(), ['id' => 'fk_partners']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkPartnersStatuses()
    {
        return $this->hasOne(PartnersStatuses::className(), ['id' => 'fk_partners_statuses']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RelPartnersPartnersStatusesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RelPartnersPartnersStatusesQuery(get_called_class());
    }
}
