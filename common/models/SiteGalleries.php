<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\BaseModel;


/**
 * This is the model class for table "site_galleries".
 *
 * @property int $id id
 * @property string $title название галереи
 * @property string $subtitle подзаголовок
 * @property int $sort сортировка
 * @property string $show_on_index отображать на главной
 * @property int $custom_date своя дата у галереи
 * @property int $id_author id автора
 * @property int $updated_at дата редактирования
 * @property int $created_at дата создания
 * @property string $is_active видимость
 *
 * @property Users $author
 * @property SiteGalleriesImages[] $siteGalleriesImages
 */
class SiteGalleries extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_galleries';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteGalleriesImages()
    {
        return $this->hasMany(SiteGalleriesImages::className(), ['fk_site_galleries' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SiteGalleriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SiteGalleriesQuery(get_called_class());
    }
}
