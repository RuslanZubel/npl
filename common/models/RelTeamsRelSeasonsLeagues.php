<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rel_teams_rel_seasons_leagues".
 *
 * @property int $id
 * @property int $fk_teams
 * @property int $fk_rel_seasons_leagues
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property string $is_active
 *
 * @property Teams $fkTeams
 * @property RelSeasonsLeagues $fkRelSeasonsLeagues
 */
class RelTeamsRelSeasonsLeagues extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_teams_rel_seasons_leagues';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkTeams()
    {
        return $this->hasOne(Teams::className(), ['id' => 'fk_teams']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRelSeasonsLeagues()
    {
        return $this->hasOne(RelSeasonsLeagues::className(), ['id' => 'fk_rel_seasons_leagues']);
    }

    public function getFkRelTeamsTeamsPartners() {
        return $this->hasManyEx(RelTeamsTeamsPartners::className(), ['fk_rel_teams_rel_seasons_league' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RelTeamsRelSeasonsLeaguesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RelTeamsRelSeasonsLeaguesQuery(get_called_class());
    }
}
