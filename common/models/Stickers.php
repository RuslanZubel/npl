<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stickers".
 *
 * @property int $id
 * @property int $fk_leagues
 * @property string $description
 * @property string $color
 * @property int $sort
 * @property int $created_at
 * @property int $updated_at
 * @property string $is_active
 * @property int $fk_author
 *
 * @property Scores[] $scores
 * @property Users $fkAuthor
 * @property Leagues $fkLeagues
 */
class Stickers extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stickers';
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScores()
    {
        return $this->hasMany(Scores::className(), ['fk_stickers' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkLeagues()
    {
        return $this->hasOne(Leagues::className(), ['id' => 'fk_leagues']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\StickersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\StickersQuery(get_called_class());
    }
}
