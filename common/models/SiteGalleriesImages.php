<?php

namespace common\models;

use common\models\BaseModel;

/**
 * This is the model class for table "site_galleries_images".
 *
 * @property int $id id
 * @property int $fk_site_galleries Галерея
 * @property string $image изображение
 * @property string $description Описание
 * @property int $custom_date Использовать свою дату
 * @property int $sort Сортировка
 * @property string $is_cover является обложкой
 * @property int $id_author id автора
 * @property int $created_at дата создания
 * @property int $updated_at дата редактирования
 * @property string $is_active видимость
 *
 * @property SiteGalleries $fkSiteGalleries
 */
class SiteGalleriesImages extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_galleries_images';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkSiteGalleries()
    {
        return $this->hasOne(SiteGalleries::className(), ['id' => 'fk_site_galleries']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SiteGalleriesImagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SiteGalleriesImagesQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'backend\components\Upload',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 200, 'height' => 200],
                ],
                'zoomCrop' => false,
                'filePath' => '@imgSiteGalleriesImages/[[pk]].jpg',
                'fileUrl' => '@imgSiteGalleriesImagesUrl/[[pk]].jpg',
                'thumbPath' => '@imgSiteGalleriesImages/thumbs/[[pk]].jpg',
                'thumbUrl' => '@imgSiteGalleriesImagesUrl/thumbs/[[pk]].jpg'
            ],

        ];
    }

}
