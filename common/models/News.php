<?php

namespace common\models;

use yii\helpers\ArrayHelper;

class News extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\NewsQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'backend\components\Upload',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 200, 'height' => 200],
                ],
                'zoomCrop' => false,
                'filePath' => '@imgNews/[[pk]].jpg',
                'fileUrl' => '@imgNewsUrl/[[pk]].jpg',
                'thumbPath' => '@imgNews/thumbs/[[pk]].jpg',
                'thumbUrl' => '@imgNewsUrl/thumbs/[[pk]].jpg'
            ],

        ];
    }

}
