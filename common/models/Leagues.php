<?php

namespace common\models;

class Leagues extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leagues';
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelSeasonsLeagues()
    {
        return $this->hasMany(RelSeasonsLeagues::className(), ['fk_leagues' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStickers()
    {
        return $this->hasMany(Stickers::className(), ['fk_leagues' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\LeaguesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\LeaguesQuery(get_called_class());
    }
}
