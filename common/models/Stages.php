<?php

namespace common\models;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "stages".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property int $fk_seasons
 * @property int $cost
 * @property int $main_trainings_date
 *
 * @property Races[] $races
 * @property Users $fkAuthor
 * @property Seasons $fkSeasons
 */
class Stages extends BaseModel
{

    private $start_date_copy = null;
    private $end_date_copy = null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stages';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRelSeasonsLeagues()
    {
        return $this->hasOne(RelSeasonsLeagues::className(), ['id' => 'fk_rel_seasons_leagues']);
    }

    public function getFkRaces()
    {
        return $this->hasMany(Races::className(), ['id' => 'fk_stages']);
    }

//    public function getRelSeasonsLeagues()
//    {
//        return $this->hasOne(RelStagesRelSeasonsLeagues::className(), [ 'fk_stages' => 'id']);
//    }



    /**
     * @inheritdoc
     * @return \common\models\queries\StagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\StagesQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'backend\components\Upload',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 200, 'height' => 200],
                ],
                'zoomCrop' => false,
                'filePath' => '@imgStages/[[pk]].jpg',
                'fileUrl' => '@imgStagesUrl/[[pk]].jpg',
                'thumbPath' => '@imgStages/thumbs/[[pk]].jpg',
                'thumbUrl' => '@imgStagesUrl/thumbs/[[pk]].jpg'
            ],

        ];
    }

    public function afterFind()
    {
        $this->start_date_copy = $this->start_date;
        $this->end_date_copy = $this->end_date;
        if (empty($this->main_trainings_date)) {
            $startDateTime = new \DateTime();
            date_timestamp_set($startDateTime, $this->start_date);
            $startDayOfWeek = (int)date('N', $startDateTime->getTimestamp());
            if ($startDayOfWeek === 4) {
                $this->main_trainings_date = $startDateTime->getTimestamp();
            }
            if ($startDayOfWeek < 4) {
                $this->main_trainings_date = strtotime(date('Y-m-d H:i', $startDateTime->getTimestamp()) . ' + ' . (4 - $startDayOfWeek) . ' days');
            }
            if ($startDayOfWeek > 4) {
                $this->main_trainings_date = strtotime(date('Y-m-d H:i', $startDateTime->getTimestamp()) . ' - ' . (6 - $startDayOfWeek) . ' days');
            }

        }
        parent::afterFind();
    }

    public function getStartEndDate() {
        return \Yii::$app->formatter->asDatetime($this->start_date_copy, "php: j F") . ' - ' . \Yii::$app->formatter->asDatetime($this->end_date_copy, "php: j F Y");
    }
}
