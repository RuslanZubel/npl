<?php

namespace common\models;



/**
 * This is the model class for table "seasons".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property int $fk_leagues
 *
 * @property Users $fkAuthor
 * @property Leagues $fkLeagues
 * @property Stages[] $stages
 */
class Seasons extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seasons';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	public function getFkLeague()
    {
        return $this->hasOne(Leagues::className(), ['id' => 'fk_leagues']);
    }
    
    public static function find()
    {
        return new \common\models\queries\SeasonsQuery(get_called_class());
    }

}
