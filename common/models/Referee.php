<?php

namespace common\models;


/**
 * This is the model class for table "receipt".
 *
 * @property int $id referee id
 * @property int $stage_id stage id
 * @property string $name name of referee
 * @property string $contact_info
 * @property int $created_by
 * @property int $updated_by
 *
 */
class Referee extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referee';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'contact_info' => 'Контактная информация',
            'stage_id' => 'Этапы',
        ];
    }
}