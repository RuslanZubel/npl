<?php

namespace common\models;

use Yii;


//НА УДАЛЕНИЕ
//
//
//
//
//
//
//
//
//
//                 ДАННУЮ МОДЕЛЬ МЫ МЕНЯЕМ НА ДРУГУЮ http://joxi.ru/52a6D4WtG1yl0A
//
//
//
//
//
//
//
//
//
//
//
//
//
//НА УДАЛЕНИЕ



/**
 * This is the model class for table "rel_members_teams".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property int $fk_members
 * @property int $fk_teams
 *
 * @property Users $fkAuthor
 * @property Members $fkMembers
 * @property Teams $fkTeams
 */
class RelMembersTeams extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_members_teams';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkMembers()
    {
        return $this->hasOne(Members::className(), ['id' => 'fk_members']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkTeams()
    {
        return $this->hasOne(Teams::className(), ['id' => 'fk_teams']);
    }
}