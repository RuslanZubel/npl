<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rel_stages_rel_seasons_leagues".
 *
 * @property int $id
 * @property int $fk_stages
 * @property int $fk_rel_seasons_leagues
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property string $is_active
 *
 * @property Races[] $races
 * @property Stages $fkStages
 * @property RelSeasonsLeagues $fkRelSeasonsLeagues
 */
class RelStagesRelSeasonsLeagues extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_stages_rel_seasons_leagues';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaces()
    {
        return $this->hasMany(Races::className(), ['fk_rel_stages_rel_seasons_leagues' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkStages()
    {
        return $this->hasOne(Stages::className(), ['id' => 'fk_stages']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRelSeasonsLeagues()
    {
        return $this->hasOne(RelSeasonsLeagues::className(), ['id' => 'fk_rel_seasons_leagues']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RelStagesRelSeasonsLeaguesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RelStagesRelSeasonsLeaguesQuery(get_called_class());
    }
}
