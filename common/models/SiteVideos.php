<?php

namespace common\models;

use Yii;
use common\models\BaseModel;

/**
 * This is the model class for table "site_videos".
 *
 * @property int $id id
 * @property string $title Название
 * @property string $description Описание
 * @property string $source_type Источник id видео
 * @property string $video_id id видео
 * @property string $cover_image обложка видео
 * @property int $sort Сортировка
 * @property string $show_on_index является обложкой
 * @property int $custom_date Использовать свою дату
 * @property int $id_author id автора
 * @property int $created_at дата создания
 * @property int $updated_at дата редактирования
 * @property string $is_active видимость
 */
class SiteVideos extends BaseModel
{

    /**
     * @inheritdoc
     * @return \common\models\queries\SiteVideosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SiteVideosQuery(get_called_class());
    }
}
