<?php

namespace common\models;


/**
 * This is the model class for table "receipt".
 *
 * @property int $id period id
 * @property \DateTime $start_date
 * @property \DateTime $end_date
 * @property int $stage_id
 *
 */
class AdditionalTrainingsPeriod extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'additional_trainings_period';
    }
}