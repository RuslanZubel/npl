<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 *
 * @property Roles $fkAuthor
 * @property Roles[] $roles
 */
class Roles extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'roles';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    public static function find()
    {
        return new \common\models\queries\RolesQuery(get_called_class());
    }

    public static function getList($params = []){
        return ArrayHelper::map(
            self::find()
                ->orderBy([
                    'title'  =>  SORT_ASC
                ])
                ->asArray()
                ->all(),
            'id',
            'title'
        ) ;
    }

}
