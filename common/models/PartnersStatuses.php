<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "partners_statuses".
 *
 * @property int $id
 * @property string $description
 * @property string $is_active
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 *
 * @property Partners[] $partners
 */
class PartnersStatuses extends BaseModel
{

    public static function tableName()
    {
        return 'partners_statuses';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkSeasons()
    {
        return $this->hasOne(Seasons::className(), ['id' => 'fk_seasons']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\PartnersStatusesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\PartnersStatusesQuery(get_called_class());
    }
}
