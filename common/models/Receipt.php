<?php

namespace common\models;


/**
 * This is the model class for table "receipt".
 *
 * @property int $id receipt id
 * @property Teams $team_id id of referee
 * @property string $product_type ENUM('stage', 'training')
 * @property int $created_by user
 * @property boolean $confirmed confirmed in pay system flag
 * @property string $cp_data cloudpayments data
 *
 */
class Receipt extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'receipt';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkTeam()
    {
        return $this->hasOne(Teams::className(), ['id' => 'team_id']);
    }

}