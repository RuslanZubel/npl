<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 17/04/2017
 * Time: 08:45
 */

namespace common\models;


use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{
    const ACTIVE_TRUE = 'yes';
    const ACTIVE_FALSE = 'no';

    public $rowClass;

    /**
     * Метод возвращает свойство объекта, по дефолту возвращается свойство title
     */

    public static function getById($id, $field = 'title')
    {
        return self::find()->select($field)->where(['id' => $id])->scalar();
    }

    protected function hasManyEx($class, $link) {
        $rightClassName = self::getRightClassName($class);
        $query = $this->hasMany($rightClassName, $link);
        if ($rightClassName::useRelSort()) {
            $query->orderBy(['sort' => SORT_ASC]);
        }
        return $query;
    }

    protected static function getRightClassName($passClassName) {
        $rightClassName = $passClassName;

        $selfClassName = self::className();
        $selfClassNameRoute = explode("\\", $selfClassName);
        $passClassNameRoute = explode("\\", $passClassName);
        if (count($selfClassNameRoute) > 0 && count($passClassNameRoute) >0 ) {
            $passClassNameRoute[0] = $selfClassNameRoute[0];
            $rightClassName = implode("\\", $passClassNameRoute);
        }

        return $rightClassName;
    }

    protected static function useRelSort() {
        return false;
    }
}