<?php

namespace common\models;


/**
 * This is the model class for table "scores".
 *
 * @property int $id
 * @property int $fk_races
 * @property int $fk_teams
 * @property int $fk_stickers
 * @property double $score_count
 * @property string $sticker_comment
 * @property double $sticker_score_modifier
 * @property int $created_at
 * @property int $updated_at
 * @property int $id_author
 * @property string $is_active
 *
 * @property Races $fkRaces
 * @property Stickers $fkStickers
 * @property Teams $fkTeams
 * @property Users $author
 */
class Scores extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scores';
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRaces()
    {
        return $this->hasOne(Races::className(), ['id' => 'fk_races']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkStickers()
    {
        return $this->hasOne(Stickers::className(), ['id' => 'fk_stickers']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRelTeamsRelSeasonsLeagues()
    {
        return $this->hasOne(RelTeamsRelSeasonsLeagues::className(), ['id' => 'fk_teams_rel_seasons_leagues']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\ScoresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ScoresQuery(get_called_class());
    }
}
