<?php

namespace common\models;

/**
 * This is the model class for table "rel_receipt_stages".
 *
 * @property int $id rel_receipt_stages id
 * @property Receipt $receipt_id referred receipt id
 * @property int $stage_id referred stage id
 *
 */
class RelReceiptStage extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_receipt_stages';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkReceipt()
    {
        return $this->hasOne(Receipt::className(), ['id' => 'receipt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkStage()
    {
        return $this->hasOne(Stages::className(), ['id' => 'stage_id']);
    }
}