<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "teams_structures".
 *
 * @property int $ids
 * @property int $fk_members
 * @property int $fk_rel_teams_rel_seasons_league
 * @property int $fk_roles
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property string $is_active
 */
class TeamsStructures extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teams_structures';
    }

    public function getFkMembers()
    {
        return $this->hasOne(Members::className(), ['id' => 'fk_members']);
    }

    public function getFkRoles()
    {
        return $this->hasOne(Roles::className(), ['id' => 'fk_roles']);
    }

    public function getFkRelTeamsRelSeasonsLeagues()
    {
        return $this->hasOne(RelTeamsRelSeasonsLeagues::className(), ['id' => 'fk_rel_teams_rel_seasons_league']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\TeamsStructuresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\TeamsStructuresQuery(get_called_class());
    }

    public static function getList() {

        $result = ArrayHelper::map(Members::find()
            ->all(), 'id',
            function ($data) {
                return $data['firstname'] . ' ' . $data['lastname'];
            });

        return $result;

    }
}
