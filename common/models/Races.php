<?php

namespace common\models;


class Races extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'races';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkStages()
    {
        return $this->hasOne(Stages::className(), ['id' => 'fk_stages']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScores()
    {
        return $this->hasMany(Scores::className(), ['fk_races' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RacesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RacesQuery(get_called_class());
    }
}
