<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "partners".
 *
 * @property int $id
 * @property int $sort
 * @property string $title
 * @property string $description
 * @property string $site_url
 * @property string $image
 * @property string $is_active
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 *
 * @property Users $fkAuthor
 */
class TeamsPartners extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teams_partners';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }


    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'backend\behaviors\uploader\UploaderBehaviour',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb'     =>  ['width' => 270, 'height' => 270]
                ],
                'defaultPhoto'      =>  '@imgDefaults/objects.png',
                'fileNamePattern'   =>  '[[pk]]_[[salt]].[[ext]]',
                'thumbNamePattern'  =>  '[[pk]]_[[salt]]_[[thumb]].[[ext]]',
                'filePath'          =>  '@imgTeamsPartners/',
                'fileUrl'           =>  '@imgTeamsPartnersUrl/',
                'thumbPath'         =>  '@imgTeamsPartners/thumbs/',
                'thumbUrl'          =>  '@imgTeamsPartnersUrl/thumbs/',
            ],
        ];
    }

    public static function find()    //Добавлено
    {
        return new \common\models\queries\TeamsPartnersQuery(get_called_class());
    }

    public function getFileUrl($attribute = null)
    {
        if ($attribute === null) {
            return $this->getBehavior('image')->getFileUrl('thumb');
        }
        return $this->getBehavior($attribute)->getFileUrl('thumb');
    }

    public function getThumbFileUrl($attribute)
    {
        return $this->getBehavior($attribute)->getFileUrl('thumb');
    }

    public function getImgWidth($attribute)
    {
        list($width1, $height1, $type1, $attr1) = getimagesize($this->getFilePath($attribute));
        return $width1;
    }

    public function getFilePath($attribute)
    {
        //return Yii::getAlias('@imgPartners'.$this->getFileUrl($attribute));
        return $this->getBehavior($attribute)->getFilePath('thumb');
    }

    public function getImgHeight($attribute)
    {
        list($width1, $height1, $type1, $attr1) = getimagesize($this->getFilePath($attribute));
        return $height1;
    }

    public static function getList() {

        $result = ArrayHelper::map(self::find()
            ->all(), 'id',
            function ($data) {
                return $data['title'];
            });

        return $result;

    }
}
