<?php

namespace common\models;

/**
 * This is the model class for table "training".
 *
 * @property int $id training id
 * @property Teams $team_id referred team id
 * @property int $cost cost of one training
 * @property string $start_datetime datetime of start training
 * @property string $end_datetime datetime of end training
 * @property \DateTime $updated_at
 * @property \DateTime $created_at
 * @property int $created_by
 * @property int $boat_number
 * @property int $stage_id
 *
 */
class Training extends BaseModel
{
    public static $TRAINING_DEFAULT_COST = 100;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkTeam()
    {
        return $this->hasOne(Teams::className(), ['id' => 'fk_training_to_team']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkStages()
    {
        return $this->hasOne(Stages::className(), ['id' => 'stage_id']);
    }

}