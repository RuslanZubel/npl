<?php

namespace common\models;


/**
 * This is the model class for table "receipt".
 *
 * @property int $id period id
 * @property int $team_id
 * @property int $training_id
 * @property boolean $free
 *
 */
class PayedTraining extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payed_training';
    }
}