<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "members".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $vfps_id
 * @property string $image
 * @property string $description
 * @property string $is_active
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 *
 * @property Users $fkAuthor
 * @property int $sort  // Добавлено
 */
class Members extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'members';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'backend\components\Upload',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 300, 'height' => 300],
                ],
                'zoomCropFromTheTop' => true,
                'filePath' => '@imgMembers/[[pk]].jpg',
                'fileUrl' => '@imgMembersUrl/[[pk]].jpg',
                'thumbPath' => '@imgMembers/thumbs/[[pk]].jpg',
                'thumbUrl' => '@imgMembersUrl/thumbs/[[pk]].jpg'
            ],

        ];
    }

    public static function find()  //Добавлено
    {
        return new \common\models\queries\MembersQuery(get_called_class());
    }

}
