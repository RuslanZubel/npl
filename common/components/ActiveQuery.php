<?php
// TODO: данного класса не было, добавил. Наследование quere переделать от него. У меня сделано только для News, Stages

namespace common\components;

use common\models\BaseModel;

/**
 * Created by PhpStorm.
 * User: amio
 * Date: 22/06/2017
 * Time: 18:31
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    public $isActiveStatus = null;


    public function activeOnly($value = true)
    {
        $this->isActiveStatus = $value;
        return $this;
    }


    public function prepareGlobalConditions() {
        if (!is_null($this->isActiveStatus)) {
            $this->andWhere([(new $this->modelClass)->tableName() . '.is_active' => $this->isActiveStatus ? BaseModel::ACTIVE_TRUE : BaseModel::ACTIVE_FALSE]);
        }
    }


    public function all($db = null)
    {
        $this->prepareGlobalConditions();
        return parent::all($db);
    }


    public function one($db = null)
    {
        $this->prepareGlobalConditions();
        return parent::one($db);
    }

}

