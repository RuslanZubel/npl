<?php
/**
 * Created by PhpStorm.
 * User: Виктор
 * Date: 15.05.2018
 * Time: 15:46
 */

namespace common\components;

class UniqueString
{
    public static function getUniqueString($lenght = 13, $prefix = '_', $postfix = '_') {

        /*
         * Не использовать эту функцию для генерации идентификаторов в JavaScript - это периодически
         * ломает клиентскую часть, так как в строке, возвращаемой этой функцией могу содежаться
         * символы, недопустимые для именования переменных и функций
         * \Yii::$app->security->generateRandomString(5);
         */

        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($lenght / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return $prefix . substr(bin2hex($bytes), 0, $lenght) . $postfix;
    }
}