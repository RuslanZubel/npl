<?php
namespace common\components;

use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\Palette\RGB;
use Imagine\Imagick\Imagine ;

class OnforImagine {

    public static function resizeZoomCrop($source, $w, $h, $target){
        $imagine    =   new Imagine();

        $procImg    =   $imagine->open( $source ) ;
        $size       =   $procImg->getSize() ;

        if( $size->getWidth() > $size->getHeight() ){
            $ratio          =       $size->getWidth() / $size->getHeight() ;
            $futureHeight   =       $h ;
            $futureWidth    =       $h *  $ratio ;
        }elseif( $size->getWidth()  <  $size->getHeight() ){
            $ratio          =       $size->getHeight() / $size->getWidth() ;
            $futureWidth    =       $w ;
            $futureHeight   =       $w * $ratio ;
        }else{
            $ratio  =       1 ;
            $futureWidth    =       $w ;
            $futureHeight   =       $h ;
        }

        $procImg->resize(   new Box(
            $futureWidth,
            $futureHeight
        ) ) ;

        $procImg->crop(
            new Point(
                ( ( ( $futureWidth / 2 ) - ( $w / 2 ) ) > 0 ) ? ( ( $futureWidth / 2 ) - ( $w / 2 ) ) : 0,
                0
            ),
            new Box($w, $h)
        ) ;

        $palette    =   new RGB();
        $finalImage =   $imagine->create(
            new Box( $w, $h ),
            $palette->color('#FFFFF', 100)
        );
        $finalImage->paste(
            $procImg,
            new Point(
                ( $futureWidth >= $w ) ? 0 : ( ( $w / 2 ) - ( $futureWidth / 2 ) ) ,
                0
            )
        ) ;
        $finalImage->save( $target );
    }

}
