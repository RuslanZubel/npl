<?php
/**
 * Created by PhpStorm.
 * User: Виктор
 * Date: 16.05.2018
 * Time: 18:51
 */

namespace common\components;

use common\models\Leagues;
use common\models\RelSeasonsLeagues;
use common\models\Stages;
use yii\base\ErrorException;
use yii\httpclient\Exception;


class ScoreCounter
{

    public static $EXCLUDED_RACES_IDS = [];
    public static $EXCLUDED_STAGES_IDS = [39];

    public static function getScoreData($seasonTeams, $stages, $racesClass, $scoresClass, &$stageTeamRacesScores, &$stageTeamScores, &$teamWholeScore, &$teamAllScore, &$raceRates)
    {
        /**
         * $stageTeamRacesScores
         * [stageId][teamId][raceId]['score'] = баллы, набранные командой teamId в гонке raceId этапа stageId
         * [stageId][teamId][raceId]['penalty'] = штрафные баллы, набранные командой teamId в гонке raceId этапа stageId
         * [stageId][teamId][raceId]['sticker'] = стикер
         * [stageId][teamId][raceId]['sticker_description'] = описание стикера
         *
         * $stageTeamScores
         * [stageId][teamId]['race_count'] - количество гонок, в которых участвовала команда teamId в этапе stageId
         * [stageId][teamId]['score'] = баллы, набранные командой teamId за весь этап stageId
         *                              если команда не участвовала ни в одной гонке, то ей начисляется количество баллов: количество участников + 1
         * [stageId][teamId]['score_div'] = [stageId][teamId]['score'] / [stageId][teamId]['race_count']
         * [stageId][teamId]['penalty'] = штрафные баллы, набранные командой teamId за весь этап stageId
         *
         * [stageId][teamId]['whole_score'] - баллы, набранные командой teamId за весь этап stageId
         *                                      рассчитывается как [stageId][teamId]['score'] + [stageId][teamId]['penalty']
         *
         * $teamWholeScore
         * [teamId]['score'] - количество баллов за все этапы
         */
//        $stageTeamRacesScores = array();
//        $stageTeamScores = array();
//        $teamWholeScore = array();
//
//        foreach ($seasonTeams as $team) {
//            $teamWholeScore[$team->id] = 0;
//            $teamAllScore[$team->id] = 0;
//        }
//        foreach ($stages as $stageIndex => $stageItem) {
//            $currentStage = $stageItem;
//            $stageRaces = $racesClass::find()->where(['fk_stages' => $currentStage->id])->orderBy(['sort' => SORT_ASC])->all();
//
//            foreach ($seasonTeams as $team) {
//                $stageTeamScores[$currentStage->id][$team->id]['race_count'] = 0;
//                $stageTeamScores[$currentStage->id][$team->id]['score'] = 0;
//                $stageTeamScores[$currentStage->id][$team->id]['all_score'] = 0;
//                $stageTeamScores[$currentStage->id][$team->id]['score_div'] = 0;
//                $stageTeamScores[$currentStage->id][$team->id]['penalty'] = 0;
//                $stageTeamScores[$currentStage->id][$team->id]['whole_score'] = 0;
//            }
//
//            if ($stageRaces && $currentStage) {
//                foreach ($seasonTeams as $team) {
//                    foreach ($stageRaces as $race) {
//                        $stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['score'] = '';
//                        $stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['penalty'] = '';
//                        $stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['sticker'] = '';
//                        $stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['sticker_description'] = '';
//                        $currentScore = $scoresClass::find()->where(['fk_races' => $race->id, 'fk_teams_rel_seasons_leagues' => $team->id])->one();
//                        if ($currentScore) {
//                            $stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['score'] = $currentScore->score_count;
//                            $sticker = $currentScore->fkStickers;
//                            if ($sticker !== null) {
//                                $stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['sticker_description'] = $sticker->description;
//                                $stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['sticker'] = $sticker->color;
//                            }
//
//                            if ($currentScore->score_count !== null && $currentScore->score_count != 0) {
//                                $stageTeamScores[$currentStage->id][$team->id]['score'] += $currentScore->score_count;
//                                $stageTeamScores[$currentStage->id][$team->id]['race_count'] += 1;
//                            }
//                            $stageTeamScores[$currentStage->id][$team->id]['penalty'] += $currentScore->sticker_score_modifier;
//                        }
//
//                        if ($stageTeamScores[$currentStage->id][$team->id]['race_count'] > 0) {
//                            $stageTeamScores[$currentStage->id][$team->id]['score_div'] =
//                                $stageTeamScores[$currentStage->id][$team->id]['score']
//                                /
//                                $stageTeamScores[$currentStage->id][$team->id]['race_count'];
//                        } else {
//                            //команда не участвовала ни в одной гонке
//                            //начисляем баллы из расчёта: количество участников + 1
//                            $stageTeamScores[$currentStage->id][$team->id]['score_div'] = count($seasonTeams) + 1;
//                        }
//
//                        $stageTeamScores[$currentStage->id][$team->id]['whole_score'] =
//                            $stageTeamScores[$currentStage->id][$team->id]['score_div']
//                            +
//                            $stageTeamScores[$currentStage->id][$team->id]['penalty'];
//                        $stageTeamScores[$currentStage->id][$team->id]['all_score'] =
//                            $stageTeamScores[$currentStage->id][$team->id]['score']
//                            +
//                            $stageTeamScores[$currentStage->id][$team->id]['penalty'];
//
//                    }
////                    if ($stageIndex === 5) {
////                        $teamWholeScore[$team->id] += $stageTeamScores[$currentStage->id][$team->id]['whole_score'] * 2;
////                    } else {
//                    $teamAllScore[$team->id] += $stageTeamScores[$currentStage->id][$team->id]['all_score'];
//                    $teamWholeScore[$team->id] += $stageTeamScores[$currentStage->id][$team->id]['whole_score'];
//                    if ($currentStage->id === 27) {
//                        $teamWholeScore = [
//                            2347 => 2.9001,
//                            2348 => 3.7001,
//                            2349 => 4.300,
//                            2351 => 4.500,
//                            2352 => 3.500,
//                            2353 => 5.300,
//                            2355 => 3.300,
//                            2357 => 3.700,
//                            2358 => 5.000,
//                            2359 => 3.100,
//                            2360 => 5.000,
//                            2361 => 4.5001,
//                            2362 => 3.200,
//                            2363 => 7.400,
//                            2364 => 7.100,
//                            2365 => 4.800,
//                            2366 => 3.200,
//                            2367 => 5.300,
//                            2368 => 5.300,
//                            2369 => 6.600,
//                            2371 => 4.600,
//                            2373 => 25,
//                            2375 => 3.7002,
//                            2376 => 2.900,
//                        ];
//                    }
////                    }
//                }
//            }
//        }

        $teamsIds = array_map(function ($team) {
            return $team->fk_teams;
        }, $seasonTeams);
        $stageIds = array_map(function ($stage) {
            return $stage->id;
        }, $stages);
//        $stageIds = [27];
//        $teamsIds = [1907, 1908];

        $stsSubQuery = (new \yii\db\Query())
            ->select(['rel_teams_rel_seasons_leagues.id AS team_id',
                'stages.id AS stage_id',
                'races.id AS race_id',
                'scores.id AS score_id',
                'score_count AS score',
                'sticker_score_modifier AS penalty',
                'stickers.color AS sticker',
                'stickers.description AS sticker_description',
                'races.sort AS races_sort',
            ])
            ->from('teams')
            ->where(['in', 'teams.id', $teamsIds])
            ->andWhere(['in', 'stages.id', $stageIds])
            ->leftJoin('rel_teams_rel_seasons_leagues', 'rel_teams_rel_seasons_leagues.fk_teams = teams.id')
            ->leftJoin('rel_seasons_leagues', 'rel_teams_rel_seasons_leagues.fk_rel_seasons_leagues = rel_seasons_leagues.id')
            ->leftJoin('stages', 'rel_seasons_leagues.id = stages.fk_rel_seasons_leagues')
            ->leftJoin('races', 'stages.id = races.fk_stages')
            ->leftJoin('scores', 'scores.fk_races = races.id AND rel_teams_rel_seasons_leagues.id = scores.fk_teams_rel_seasons_leagues')
            ->leftJoin('stickers', 'scores.fk_stickers = stickers.id')
            ->orderBy(['races.sort' => SORT_ASC]);

        $realStages = (new \yii\db\Query())
            ->from('stages')
            ->where(['in', 'stages.id', $stageIds])
            ->all();

        $preparedData = $stsSubQuery->all();
        $stageTeamRacesScores = [];
        $racesCountByTeams = [];
        foreach ($preparedData as $preparedRow) {

            if (!key_exists($preparedRow['stage_id'], $racesCountByTeams)) {
                $racesCountByTeams[$preparedRow['stage_id']] = [];
            }
            if (!key_exists($preparedRow['team_id'], $racesCountByTeams[$preparedRow['stage_id']])) {
                $racesCountByTeams[$preparedRow['stage_id']][$preparedRow['team_id']] = 0;
            }
            if ($preparedRow['score'] > 0) {

                $racesCountByTeams[$preparedRow['stage_id']][$preparedRow['team_id']]++;
            }

            if (!key_exists($preparedRow['stage_id'], $stageTeamRacesScores)) {
                $stageTeamRacesScores[$preparedRow['stage_id']] = [];
            }
            if (!key_exists($preparedRow['team_id'], $stageTeamRacesScores[$preparedRow['stage_id']])) {
                $stageTeamRacesScores[$preparedRow['stage_id']][$preparedRow['team_id']] = [];
            }
            $stageTeamRacesScores[$preparedRow['stage_id']][$preparedRow['team_id']][$preparedRow['race_id']] = [];
            $stageTeamRacesScores[$preparedRow['stage_id']][$preparedRow['team_id']][$preparedRow['race_id']]['score'] = !is_null($preparedRow['score']) ? $preparedRow['score'] : 0;
            $stageTeamRacesScores[$preparedRow['stage_id']][$preparedRow['team_id']][$preparedRow['race_id']]['penalty'] = $preparedRow['penalty'];
            $stageTeamRacesScores[$preparedRow['stage_id']][$preparedRow['team_id']][$preparedRow['race_id']]['sticker'] = $preparedRow['sticker'];
            $stageTeamRacesScores[$preparedRow['stage_id']][$preparedRow['team_id']][$preparedRow['race_id']]['sticker_description'] = $preparedRow['sticker_description'];
        }


        $stageTeamScores = [];

        $teamAllScore = [];
        $teamWholeScore = [];
        $raceRates = [];
        $formattedRealStages = [];
        foreach ($realStages as $realStage) {
            $formattedRealStages[$realStage['id']] = $realStage;
        }
        foreach ($stageTeamRacesScores as $stage_id => $stageTeamsRacesScore) {
            $calculatedRaceCount = 1;

            if (!empty($formattedRealStages[$stage_id]['calculation_races']) && !is_null($formattedRealStages[$stage_id]['calculation_races'])) {
                $calculatedRaceCount = $formattedRealStages[$stage_id]['calculation_races'];
                if ($stage_id === 53) {
                    $calculatedRaceCount++;
                }
            } else {
                $tempCount = array_filter($racesCountByTeams[$stage_id], function ($val) {
                    return $val > 0;
                });

                if (!empty($tempCount)) {
                    $calculatedRaceCount = min($tempCount);

                }
                if ($calculatedRaceCount < 10 && ($stage_id === 34 || $stage_id === 35)) { // TODO create some flag for stage
                    $calculatedRaceCount = 10;
                    if ($stage_id === 34) {
                        $calculatedRaceCount = 11;
                    }
                    if ($stage_id === 35) {
                        $calculatedRaceCount = 18;
                    }
                    if ($stage_id === 29) {
                        $calculatedRaceCount = 10;
                    }
                }

                if ($stage_id === 29) {
                    $calculatedRaceCount = 11;
                }
                if ($stage_id === 30) {
                    $calculatedRaceCount = 8;
                }

            }

            if (!key_exists($stage_id, $stageTeamScores)) {
                $stageTeamScores[$stage_id] = [];
            }
            $selectedStage = array_values(array_filter($realStages, function ($st) use ($stage_id) {
                return (int)$st['id'] === (int)$stage_id;
            }));
            if (count($selectedStage) > 0) {
                $selectedStage = $selectedStage[0];
            }


            foreach ($stageTeamsRacesScore as $team_id => $teamRacesScore) {

                if (!key_exists($team_id, $stageTeamScores[$stage_id])) {
                    $stageTeamScores[$stage_id][$team_id] = [];
                    $stageTeamScores[$stage_id][$team_id]['race_count'] = $calculatedRaceCount;
                }
                $stageTeamScores[$stage_id][$team_id]['real_race_count'] = 0;

                $tempRaceCounter = 0;
                if (count($selectedStage) > 0 && $selectedStage['end_date'] > time()) {
                    $calculatedRaceCount = count(array_filter($stageTeamRacesScores[$stage_id][$team_id], function ($ttt) {
                        return $ttt['score'] > 0;
                    }));
                }
                foreach ($teamRacesScore as $race_id => $raceScore) {
                    if (!in_array($race_id, self::$EXCLUDED_RACES_IDS)) {
                        if ($raceScore['score'] > 0) {
                            $stageTeamScores[$stage_id][$team_id]['real_race_count']++;
                            $tempRaceCounter++;
                        }
                        if (!key_exists('score', $stageTeamScores[$stage_id][$team_id])) {
                            $stageTeamScores[$stage_id][$team_id]['score'] = 0;
                        }
                        if (!key_exists('all_score', $stageTeamScores[$stage_id][$team_id])) {
                            $stageTeamScores[$stage_id][$team_id]['all_score'] = 0;
                        }
                        if (!key_exists('penalty', $stageTeamScores[$stage_id][$team_id])) {
                            $stageTeamScores[$stage_id][$team_id]['penalty'] = 0;
                        }
                        if (!key_exists($team_id, $raceRates)) {
                            $raceRates[$team_id] = [];
                        }
                        if ($tempRaceCounter <= $calculatedRaceCount) {
                            if (count($stages) > 3 && end($stages)->id === $stage_id && !in_array($stage_id, [81, 83])) {
                                $stageTeamScores[$stage_id][$team_id]['score'] += $raceScore['score'];

                            }
                            $stageTeamScores[$stage_id][$team_id]['score'] += $raceScore['score'];


                            $stageTeamScores[$stage_id][$team_id]['all_score'] += $raceScore['score'];


                            if (!is_null($raceScore['score']) && !key_exists((int)$raceScore['score'], $raceRates[$team_id])) {
                                $raceRates[$team_id][(int)$raceScore['score']] = 0;
                            }
                            if (!is_null($raceScore['score'])) {
                                $raceRates[$team_id][(int)$raceScore['score']]++;
                            }
                            if (!is_null($raceScore['penalty'])) {
                                $stageTeamScores[$stage_id][$team_id]['penalty'] += $raceScore['penalty'];
                            }
                        }
                    }


                }

                if (count($selectedStage) > 0 && $selectedStage['end_date'] > time()) {
                    $stageTeamScores[$stage_id][$team_id]['race_count'] = $stageTeamScores[$stage_id][$team_id]['real_race_count'];
                }

                if (!key_exists('score_div', $stageTeamScores[$stage_id][$team_id])) {
                    $stageTeamScores[$stage_id][$team_id]['score_div'] = 0;
                }

                if ($stageTeamScores[$stage_id][$team_id]['real_race_count'] > 0 && $stageTeamScores[$stage_id][$team_id]['real_race_count'] >= $calculatedRaceCount) {
                    $stageTeamScores[$stage_id][$team_id]['score_div'] =
                        $stageTeamScores[$stage_id][$team_id]['score'] /
                        $stageTeamScores[$stage_id][$team_id]['race_count'];
//                } elseif (Stages::findOne($stage_id)->end_date < time()) {
                } elseif (
                    Stages::findOne($stage_id)->end_date < time() + 60 * 60 * 24
                    && $stageTeamScores[$stage_id][$team_id]['real_race_count'] < $calculatedRaceCount
                    && $stage_id !== 53
                ) {
                    $stageTeamScores[$stage_id][$team_id]['score_div'] = count($seasonTeams) + 1;
                } elseif (
                    Stages::findOne($stage_id)->end_date < time() + 60 * 60 * 24
                    && $stageTeamScores[$stage_id][$team_id]['real_race_count'] < $calculatedRaceCount
                    && $stage_id === 53
                ) {
                    $stageTeamScores[$stage_id][$team_id]['score_div'] = count($seasonTeams) + 1
                        + ($stageTeamScores[$stage_id][$team_id]['score'] / 100000000);
                }
                if (!key_exists('whole_score', $stageTeamScores[$stage_id][$team_id])) {
                    $stageTeamScores[$stage_id][$team_id]['whole_score'] =
                        $stageTeamScores[$stage_id][$team_id]['score_div'] +
                        $stageTeamScores[$stage_id][$team_id]['penalty'];
                }
                if (!key_exists('all_score', $stageTeamScores[$stage_id][$team_id])) {
                    $stageTeamScores[$stage_id][$team_id]['all_score'] = 0;
                }
                $stageTeamScores[$stage_id][$team_id]['all_score'] =
                    $stageTeamScores[$stage_id][$team_id]['score'] +
                    $stageTeamScores[$stage_id][$team_id]['penalty'];

                if (!key_exists($team_id, $teamAllScore)) {
                    $teamAllScore[$team_id] = 0;
                }
                if (!key_exists($team_id, $teamWholeScore)) {
                    $teamWholeScore[$team_id] = 0;
                }
                if(!in_array($stage_id, self::$EXCLUDED_STAGES_IDS) || count($stages) === 1) {

                    $teamAllScore[$team_id] += $stageTeamScores[$stage_id][$team_id]['all_score'];
                    $teamWholeScore[$team_id] += $stageTeamScores[$stage_id][$team_id]['whole_score'];
                }

            }

        }
        uksort($teamWholeScore, function ($a, $b) use ($teamWholeScore, $raceRates) {
            $aVal = $teamWholeScore[$a];
            $bVal = $teamWholeScore[$b];
            if ($aVal > $bVal) {
                return true;
            }
            if ($aVal < $bVal) {
                return false;
            }
            if ($aVal == $bVal) {
                for ($i = 1; $i < 9; $i++) {
                    if (key_exists($i, $raceRates[$a])) {
                        if (key_exists($i, $raceRates[$b])) {
                            if ($raceRates[$a][$i] > $raceRates[$b][$i]) {
                                return false;
                            }
                            if ($raceRates[$a][$i] < $raceRates[$b][$i]) {
                                return true;
                            }
                        } else {
                            return false;
                        }
                    } elseif (key_exists($i, $raceRates[$b])) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
            return true;
        });

    }

    public static function getSeasonData($seasonId, $relSeasonLeaguesClass, $relTeamsRelSeasonsLeaguesClass, $stagesClass, $racesClass, $scoresClass, $useIsActive = false)
    {

        $leagues = Leagues::find()
            ->joinWith('relSeasonsLeagues')
            ->where([$relSeasonLeaguesClass::tableName() . '.fk_seasons' => $seasonId])
            ->select([Leagues::tableName() . '.id', Leagues::tableName() . '.title'])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $stageTeamRacesScoresPool = array();
        $stageTeamScoresPool = array();
        $teamWholeScorePool = array();

        foreach ($leagues as $league) {
            $relSeasonLeague = $relSeasonLeaguesClass::find()
                ->where(['fk_seasons' => $seasonId, 'fk_leagues' => $league->id])
                ->one();
            if (!$relSeasonLeague) {
                continue;
            }

            $stages = $stagesClass::find()
                ->where(['fk_rel_seasons_leagues' => $relSeasonLeague->id])
                ->orderBy($stagesClass::tableName() . '.start_date')
                ->all();
//            array_pop($stages);


            if ($useIsActive) {
                $seasonTeams = $relTeamsRelSeasonsLeaguesClass::find()
                    ->where(['fk_rel_seasons_leagues' => $relSeasonLeague->id])
                    ->joinWith('fkTeams as teams')
                    ->onCondition(['teams.is_active' => 'yes'])
                    ->all();
            } else {
                $seasonTeams = $relTeamsRelSeasonsLeaguesClass::find()
                    ->where(['fk_rel_seasons_leagues' => $relSeasonLeague->id])
                    ->joinWith('fkTeams as teams')
                    ->all();
            }

            self::getScoreData($seasonTeams, $stages, $racesClass, $scoresClass,
                $stageTeamRacesScores, $stageTeamScores, $teamWholeScore, $teamAllScore, $raceRates);

            try {
                if (count($stages) > 1 && !($seasonId === 10 && $league->id === 5)) {
                    throw new ErrorException($seasonId . ' ' . $league->id);

                    $lastStageId = end($stages)->id;
                    foreach ($stageTeamRacesScores[$lastStageId] as $teamId => $teamRaces) {
                        foreach ($teamRaces as $raceId => $teamsRaces) {

                            $stageTeamRacesScores[$lastStageId][$teamId][$raceId]['score'] = $stageTeamRacesScores[$lastStageId][$teamId][$raceId]['score'] * 2;
                            if (!is_null($stageTeamRacesScores[$lastStageId][$teamId][$raceId]['penalty'])) {
                                $stageTeamRacesScores[$lastStageId][$teamId][$raceId]['penalty'] = $stageTeamRacesScores[$lastStageId][$teamId][$raceId]['penalty'] * 2;
                            }
                        }
                    }
                }
            } catch (\ErrorException $e) {

            }

            $stageTeamRacesScoresPool[$relSeasonLeague->id] = $stageTeamRacesScores;
            $stageTeamScoresPool[$relSeasonLeague->id] = $stageTeamScores;
            $teamWholeScorePool[$relSeasonLeague->id]['scores'] = $teamWholeScore;
            $teamWholeScorePool[$relSeasonLeague->id]['teams'] = $seasonTeams;
            $teamWholeScorePool[$relSeasonLeague->id]['league'] = $relSeasonLeague;
        }

        /*$teamSeasonWholeScore = array();

        foreach($teamWholeScorePool as $relSeasonLeagueId => $teamWholeScore) {
            foreach($teamWholeScore as $teamId => $score)
            if (!array_key_exists($teamId, $teamSeasonWholeScore)) {
                $teamSeasonWholeScore[$teamId] = $score;
            } else {
                $teamSeasonWholeScore[$teamId] += $score;
            }
        }*/

        //return $teamSeasonWholeScore;
        return $teamWholeScorePool;
    }
}