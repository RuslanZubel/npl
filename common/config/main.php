<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',

        '@img' => '@frontend/web/uploads/images',
        '@file' => '@frontend/web/uploads/files',

        '@uploads' => '@frontend/web/uploads',
        '@uploadsUrl' => '/uploads',

        '@tempFiles' => '@frontend/web/uploads/temp-files',

        '@imgPartners' => '@img/partners',
        '@imgPartnersUrl' => '/uploads/images/partners',

        '@imgTeamsPartners' => '@img/teams-partners',
        '@imgTeamsPartnersUrl' => '/uploads/images/teams-partners',

        '@imgMembers' => '@img/members',
        '@imgMembersUrl' => '/uploads/images/members',

        '@imgTeamsLogo' => '@img/teams-logo',
        '@imgTeamsLogoUrl' => '/uploads/images/teams-logo',

        '@imgTeamsCover' => '@img/teams-cover',
        '@imgTeamsCoverUrl' => '/uploads/images/teams-cover',

        '@imgNews' => '@img/news',
        '@imgNewsUrl' => '/uploads/images/news',

        '@imgStages' => '@img/stages',
        '@imgStagesUrl' => '/uploads/images/stages',

        '@elfinder' => '@frontend/web/uploads/elfinder',
        '@elfinderUrl' => '/uploads/elfinder',

        '@imgSiteGalleriesImages' => '@img/site-galleries-images',
        '@imgSiteGalleriesImagesUrl' => '/uploads/images/site-galleries-images',

        '@imgPages' => '@img/pages',
        '@imgPagesUrl' => '/uploads/images/pages',

        '@home' => '/',
    ],
    'language' => 'ru',
    'sourceLanguage' => 'ru_RU',
    'timeZone' => 'Europe/Moscow',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'timeZone' => 'Europe/Moscow',
            'dateFormat' => 'dd.MM.Y',
            'timeFormat' => 'HH:mm',
            'datetimeFormat' => 'dd.MM.Y HH:mm',
            'decimalSeparator' => '.',
            'thousandSeparator' => ' ',
            'nullDisplay' => '-',
            'currencyCode'  =>  'RUB',
            'locale' => 'ru_RU'
        ],
    ],

];
