<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome.min.css',
        'css/jquery.fancybox.min.css',
        'css/jquery.formstyler.css',
        'css/jquery.mCustomScrollbar.css',
        'css/slick-theme.css',
        'css/slick.css',
        'css/app.css',
        'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic',
    ];
    public $js = [
        'js/app.js',
        'js/jquery.fancybox.min.js',
        'js/jquery.formstyler.min.js',
        'js/maskedinput.js',
        'js/slick.min.js',
        'https://widget.cloudpayments.ru/bundles/cloudpayments'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
