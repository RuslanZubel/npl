<?php
/**
 * Created by PhpStorm.
 * User: codepug
 * Date: 04/04/2018
 * Time: 16:09
 */

namespace frontend\traits;  // TODO: данного трейта вообще не было, добавил


trait  BaseFrontendTrait
{
	public static function getList($params = [])
	{
		$query = self::find();
		if (!empty($params['condition'])) {
			$query->andWhere($params['condition']);
		}
		if (!empty($params['orderBy'])) {
			$query->orderBy($params['orderBy']);
		} else {
			$query->orderBy(['sort' => SORT_ASC]);
		}
		if (!empty($params['limit'])) {
			$query->limit((int)$params['limit']);
		}
		
		return $query;
	}

    public static function find()
    {
        return parent::find()->activeOnly();
    }

    protected static function useRelSort() {
        return true;
    }
}
