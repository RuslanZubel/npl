<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 18/04/2017
 * Time: 14:20
 */

namespace frontend\behaviors;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class DateFieldBehavior extends Behavior
{
    public $attribute;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
        ];
    }

    public function beforeValidate()
    {
        //die("BEFORE VALIDATE");
        $this->owner->{$this->attribute} = strtotime( $this->owner->{$this->attribute} );
    }

    public function afterValidate() {
        //die("AFTER FIND");
        if ($this->attribute > 0) {
            $this->owner->{$this->attribute} = \Yii::$app->formatter->asDatetime($this->owner->{$this->attribute}, "php:G:i, j F Y");
        }
    }

    public function afterFind() {
        //die("AFTER FIND");
        $this->owner->{$this->attribute} = \Yii::$app->formatter->asDatetime($this->owner->{$this->attribute}, "php:G:i, j F Y");
    }

}