<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Partners */

$this->title = 'Создание Partners';
?>
<div class="partners-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
