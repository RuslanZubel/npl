function remove_gallery_image(id) {
    $.post(
        '/admin/mediafolder/site-galleries-images/delete',
        {'id': id,},
        function( content ){
            $.pjax.reload('#site-galleries-images-grid');
        }
    );
}

function make_gallery_image_main(galleryId, id) {
    $.post(
        '/admin/mediafolder/site-galleries-images/make-cover',
        {
            'galleryId': galleryId,
            'id': id,
        },
        function( content ){
            $.pjax.reload('#site-galleries-images-grid');
        }
    );
}