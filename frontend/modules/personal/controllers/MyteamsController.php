<?php

namespace frontend\modules\personal\controllers;


use backend\models\Teams;
use frontend\modules\personal\components\BasePersonalController;
use Yii;

class MyteamsController extends BasePersonalController
{
    public function actionIndex(){
        $model = new Teams();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('/personal/stages');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

}