<?php

namespace frontend\modules\personal\controllers;


use common\models\Leagues;
use common\models\Receipt;
use common\models\RelReceiptStage;
use common\models\RelSeasonsLeagues;
use common\models\RelTeamsRelSeasonsLeagues;
use common\models\Seasons;
use common\models\Stages;
use common\models\Teams;
use common\models\TeamsStructures;
use common\models\Training;
use common\models\User;
use frontend\modules\personal\components\BasePersonalController;
use linslin\yii2\curl\Curl;
use Yii;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\StaleObjectException;

class StagesController extends BasePersonalController
{
    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionIndex()
    {
        $model = new Receipt();
        $receipt = new Receipt();
        $relReceiptStage = new RelReceiptStage();
        $selectedTeam = null;
        $selectedTeam = Teams::findOne(['fk_author' => Yii::$app->user->id]);
        if (!$selectedTeam || is_null($selectedTeam) || empty($selectedTeam)) {
            $selectedTeam = Teams::findOne(['agent' => Yii::$app->user->id]);
        }
        if (!$selectedTeam || is_null($selectedTeam) || empty($selectedTeam)) {
                    $selectedTeam = Teams::findOne(['title' => User::findOne(Yii::$app->user->id)->username]);
                }
        $rel = RelTeamsRelSeasonsLeagues::find()->where(['fk_teams' => $selectedTeam->id])->all();
        $teamRelSeasonLeaguesIds = array_map(function ($relation) {
            return $relation->fk_rel_seasons_leagues;
        }, $rel);

        $seasons = Seasons::find()
            ->orderBy(Seasons::tableName() . '.sort')
            ->all();
        if (is_null($seasons)) {
            throw new \Exception("Лиги не найдены");
        }


        $stages = Stages::find()
            ->where(['>', 'start_date', strtotime("2 hours")])
            ->orderBy(Stages::tableName() . '.start_date')
            ->all();
        if (is_null($stages)) {
            throw new \Exception("Этапы не найдены");
        }
        $resultStages = [];
        foreach ($stages as $stage) {

            $relReceiptStagesIds = RelReceiptStage::find()
                ->where(['stage_id' => $stage->id])
                ->select('receipt_id')
                ->all();
            $relReceiptStagesIds = array_map(function ($relationS) {
                return $relationS->receipt_id;
            }, $relReceiptStagesIds);

            $receipts = Receipt::find()
                ->where(['in', 'id', $relReceiptStagesIds])
                ->andWhere(['created_by' => Yii::$app->user->id])
                ->all();

            if ((count($receipts) === 0 || count($relReceiptStagesIds) === 0) &&
                count($stage->getFkRelSeasonsLeagues()->all()) > 0
            ) {
                $stageRelSeasonLeague = RelSeasonsLeagues::findOne($stage->fk_rel_seasons_leagues);
                $stageLeague = $stageRelSeasonLeague->getFkLeagues()->all()[0];
                if (in_array($stageRelSeasonLeague->id, $teamRelSeasonLeaguesIds)) {
                    $stage->title = $stageLeague->title . ' ' .
                        $stage->title . ' (' . $stage->getStartEndDate() . ' )';
                    $resultStages[] = $stage;
                }
            }


        }

        $selectedStage = new Stages();
        $selectedLeague = new Leagues();
        $selectedSeason = new Seasons();


        if (Yii::$app->request->post() && !empty(Yii::$app->request->post()['RelReceiptStage']['stage_id'])) {
            $stage = Stages::findOne(Yii::$app->request->post()['RelReceiptStage']['stage_id']);
            $receipt->team_id = $selectedTeam->id;
            $receipt->product_type = 'stage';
            $receipt->confirmed = false;
            $receipt->created_by = Yii::$app->user->id;
            $stage->save();
            $receipt->save();
            $relReceiptStage->receipt_id = $receipt->id;
            $relReceiptStage->stage_id = $stage->id;
            $relReceiptStage->save();
            return $this->redirect('/personal/stages/pay-stage?receipt_id=' . $receipt->id);

        } else {

            return $this->render('index', [
                'stages' => $resultStages,
                'seasons' => $seasons,
                'selectedStage' => $selectedStage,
                'selectedLeague' => $selectedLeague,
                'selectedSeason' => $selectedSeason,
                'selectedTeam' => $selectedTeam,
                'model' => $model,
                'receipt' => $receipt,
                'relReceiptStage' => $relReceiptStage
            ]);
        }


    }

    public function actionIntro()
    {
        return $this->render('intro');
    }

    public function actionDone()
    {
        return $this->render('done');
    }

    public function actionMy()
    {
        $user = User::findOne(Yii::$app->user->id);
        $myTrainingReceipts = Receipt::find()->where([
            'created_by' => Yii::$app->user->id,
            'product_type' => 'stage'
        ])->all();
        $formattedTrainingReceipts = array();
        foreach ($myTrainingReceipts as $receipt) {
            $relRecSt = RelReceiptStage::findOne(['receipt_id' => $receipt->id]);
            $stageId = $relRecSt->stage_id;
            $stage = Stages::findOne($stageId);
            $total_cost = $stage->cost;
            $formattedTrainingReceipts[] = array(
                'receipt_id' => $receipt->id,
                'total_cost' => $total_cost,
                'confirmed' => $receipt->confirmed,
                'team_id' => $receipt->getFkTeam()->all()[0]->title,
                'user_email' => $user->email,
                'stage_title' => $stage->title . ' (' . $stage->getStartEndDate() . ' )',
                'stage_id' => $stage->id
            );

        }
        $provider = new ArrayDataProvider([
            'allModels' => $formattedTrainingReceipts,
            'sort' => [
                'attributes' => ['receipt_id'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        return $this->render('my', [
            'receipts' => $provider
        ]);
    }

    public function actionDelete($id)
    {
        $receipt = Receipt::findOne($id);
        $relReceiptStage = RelReceiptStage::findOne(['receipt_id' => $id]);
        $stage = Stages::findOne($relReceiptStage->stage_id);
        //TODO check  adding to reserve list of payed trainings
        $training = Training::findOne([
            'stage_id' => $stage->id,
            'created_by' => Yii::$app->user->id,
            'cost' => 0
        ]);
        try {
            $receipt->delete();
            $training->delete();
        } catch (StaleObjectException $e) {
        } catch (\Throwable $e) {
        }


        return $this->redirect(['my']);
    }

    public function actionConfirm()
    {
        $receipt = Receipt::findOne(\Yii::$app->request->post('id'));
        $receipt->confirmed = true;
        $receipt->save();

        try {
            $relRecStage = RelReceiptStage::findOne(['receipt_id' => $receipt->id]);
            $team = Teams::findOne($receipt->team_id);


            $relRecTr = RelReceiptStage::find()->where(['receipt_id' => $receipt->id])->all();
            $trIds = array_map(function ($rel) {
                return $rel->stage_id;
            }, $relRecTr);
            $myTrainings = Stages::find()->where(['in', 'id', $trIds])->all();

            $mapReceiptItems = function (Stages $trg) use ($team) {
                return array(
                    'label' => 'Этап «' . $trg->title . '» команды «' . $team->title . '»',
                    'price' => number_format(((int)$trg->cost / 0.97), 2, '.', ''),
                    'quantity' => 1,
                    'amount' => number_format(((int)$trg->cost / 0.97), 2, '.', ''),
                    'vat' => 0,
                    'method' => 0,
                    'object' => 0,
                    'measurementUnit' => 'шт',
                );
            };

            $items = array_values(array_map($mapReceiptItems, $myTrainings));
            $amount = 0;
            foreach ($items as $item) {
                $amount += $item['amount'];
            }

            $customerReceipt = array(
                'Items' => $items,
                'taxationSystem' => 0,
                'email' => User::findOne(Yii::$app->user->id)->email,
                'phone' => '',
                'isoBso' => false,
                'amounts' => array(
                    'electronic' => number_format($amount, 2, '.', ''),
                    'advancePayment' => 0.00,
                    'credit' => 0.00,
                    'provision' => 0.00
                )
            );

            $data = array(
                'Inn' => '7706447178',
                'InvoiceId' => $receipt->id,
                'AccountId' => User::findOne(Yii::$app->user->id)->email,
                'Type' => 'Income',
                'CustomerReceipt' => $customerReceipt
            );


            $curl = new Curl();
            $response = $curl->setRawPostData(
                json_encode($data)
            )
                ->setHeaders([
//                'Authorization' => 'Basic cGtfZDg3MmU1YjFhMjJlNTE1ODI3NTVkZGI0OGI4OWI6Y2FlMzNlZmY1ZDhjYzY4NjYxMjMxZTkzNWFmMzgwMDU=',
                    'Authorization' => 'Basic cGtfYjFjN2E0NGRhMDI5ZjgyMjgzMWI3MWUwNDE5NTU6N2ZhNzRiMzU0NGRkYjFjYTBmMDYwMjA4ZGVkOWU5Y2I=',
                    'Content-Type' => 'application/json'
                ])
                ->post('https://api.cloudpayments.ru/kkt/receipt');
//        return $this->asJson($response);
            try {

                $receipt->cp_data = json_encode($response);
                $receipt->save();
            } catch (ErrorException $e) {
                $receipt->cp_data = $e->getMessage();
                $receipt->save();
            }
        } catch (ErrorException $e) {
            $receipt->cp_data = $e->getMessage();
            $receipt->save();
        }

        return $this->redirect('/personal/stages/my');
    }

    public function actionPay()
    {
        $receipt = Receipt::findOne((int)\Yii::$app->request->post('id'));
        $receipt->confirmed = true;
        $receipt->save();

        try {
            $relRecTr = RelReceiptStage::find()->where(['receipt_id' => $receipt->id])->all();
            $stageIds = array_map(function ($rel) {
                return $rel->stage_id;
            }, $relRecTr);
            $myStages = Stages::find()->where(['in', 'id', $stageIds])->all();

            $mapReceiptItems = function (Stages $stage) {
                return array(
                    'label' => 'Этап «' . $stage->title . '»',
                    'price' => number_format((ceil((int)$stage->cost / 0.97)), 2, '.', ''),
                    'quantity' => 1,
                    'amount' => number_format((ceil((int)$stage->cost / 0.97)), 2, '.', ''),
                    'vat' => 0,
                    'method' => 0,
                    'object' => 0,
                    'measurementUnit' => 'шт',
                );
            };

            $items = array_values(array_map($mapReceiptItems, $myStages));
            $amount = 0;
            foreach ($items as $item) {
                $amount += $item['amount'];
            }

            $customerReceipt = array(
                'Items' => $items,
                'taxationSystem' => 0,
                'email' => User::findOne(Yii::$app->user->id)->email,
                'phone' => '',
                'isoBso' => false,
                'amounts' => array(
                    'electronic' => number_format($amount, 2, '.', ''),
                    'advancePayment' => 0.00,
                    'credit' => 0.00,
                    'provision' => 0.00
                )
            );

            $data = array(
                'Inn' => '7706447178',
                'InvoiceId' => $receipt->id,
                'AccountId' => User::findOne(Yii::$app->user->id)->email,
                'Type' => 'Income',
                'CustomerReceipt' => $customerReceipt
            );


            $curl = new Curl();
            $response = $curl->setRawPostData(
                json_encode($data)
            )
                ->setHeaders([
                    'Authorization' => 'Basic cGtfYjFjN2E0NGRhMDI5ZjgyMjgzMWI3MWUwNDE5NTU6N2ZhNzRiMzU0NGRkYjFjYTBmMDYwMjA4ZGVkOWU5Y2I=',
                    'Content-Type' => 'application/json'
                ])
                ->post('https://api.cloudpayments.ru/kkt/receipt');
            try {

                $receipt->cp_data = json_encode($response);
                $receipt->save();
            } catch (ErrorException $e) {
                $receipt->cp_data = $e->getMessage();
                $receipt->save();
            }
        } catch (ErrorException $e) {
            $receipt->cp_data = $e->getMessage();
            $receipt->save();
        }

        return $this->redirect(['my']);
    }

    public function actionEditCommand($team_id)
    {
        $fkRelTeamsRelSeasonsLeagues = RelTeamsRelSeasonsLeagues::findOne(['fk_teams' => (int)$team_id])->id;
        $teamId = $team_id;

        $members = TeamsStructures::find()
            ->where(['fk_rel_teams_rel_seasons_league' => $fkRelTeamsRelSeasonsLeagues])
            ->joinWith('fkMembers')
            ->joinWith('fkRoles');
        $count = $fkRelTeamsRelSeasonsLeagues;
        $providerParams = array();
        $providerParams['query'] = $members;


        $providerParams['pagination'] = false;

        $dataProvider = new ActiveDataProvider($providerParams);

        $teamTitle = Teams::getById($teamId);

        return $this->render('edit_team_struct', [
            'members' => $members,
            'teamTitle' => $teamTitle,
            'fkRelTeamsRelSeasonsLeagues' => $fkRelTeamsRelSeasonsLeagues,
            'dataProvider' => $dataProvider,
            'count' => $count
        ]);
    }

    public function actionAddTeamMember()
    {
        $fkRelTeamsRelSeasonsLeagues = (int)\Yii::$app->request->post('fkRelTeamsRelSeasonsLeagues');
        $memberUnit = strval(\Yii::$app->request->post('memberUnit'));
        $memberRole = strval(\Yii::$app->request->post('memberRole'));

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        $relTeamsRelSeasonsLeagues = RelTeamsRelSeasonsLeagues::findOne($fkRelTeamsRelSeasonsLeagues);
        if (!$relTeamsRelSeasonsLeagues) {
            $answerJSON['error'] = 'Сезон-лига не найдена';
            return $this->asJson($answerJSON);
        }

        $err = false;

        $teamStructures = new TeamsStructures();
        try {
            $teamStructures->fk_rel_teams_rel_seasons_league = $fkRelTeamsRelSeasonsLeagues;
            $teamStructures->fk_members = $memberUnit;
            $teamStructures->fk_roles = $memberRole;
            $teamStructures->created_at = date('Y-m-d H:i:s');
            $teamStructures->is_active = 'yes';
            if ($teamStructures->save() === false) {
                $answerJSON['error'] = 'Ошибка сохранения данных';
                $answerJSON['error'] .= print_r($teamStructures->getErrors(), true);
            }
        } catch (\Exception $ex) {
            $err = true;
            $exMsg = $ex->getMessage();
            if (strpos($exMsg, 'Duplicate') !== false) {
                $answerJSON['error'] = "Такая пара участник-должность уже есть в данной команде данной пары сезон-лига";
            } else {
                $answerJSON['error'] = "Ошибка сохранения данных: {$exMsg}";
            }
        }

        return $err ? $this->asJson($answerJSON) :
            $this->redirect('/personal/stages/edit-command?team_id=' . $teamStructures->getFkRelTeamsRelSeasonsLeagues()->all()[0]->fk_teams);
    }

    public function actionGetTraining($id)
    {
        $this->redirect('/personal/training?stage_id=' . $id);

    }

    public function actionRemoveTeamMember($id)
    {
        $teamStructureId = $id;

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];
        $teamStructures = TeamsStructures::findOne($teamStructureId);

        try {

            if ($teamStructures) {
                if ($teamStructures->delete() === false) {
                    $answerJSON['error'] = 'Ошибка удаления данных';
                }
                return $this->redirect('/personal/stages/edit-command?team_id=' . $teamStructures->getFkRelTeamsRelSeasonsLeagues()->all()[0]->fk_teams);

            } else {
                $answerJSON['error'] = "Запись о члене команды не найдена";
            }

        } catch (\Exception $ex) {
            $answerJSON['error'] = "Ошибка удаления данных: {$ex->getMessage()}";
        }
        return $this->redirect('/personal/stages/edit-command?team_id=' . $teamStructures->getFkRelTeamsRelSeasonsLeagues()->all()[0]->fk_teams);
    }

    public function actionMainTrainings($id)
    {
        return $this->redirect('/personal/training/main-trainings?stage_id=' . $id);

    }

    public function actionAdditionalTrainings($id)
    {
        return $this->redirect('/personal/training/additional-trainings?stage_id=' . $id);

    }

    public function actionRedirectToCredits($id)
    {
        return $this->redirect('/personal/stages/credits?id=' . $id);
    }

    public function actionCredits($id)
    {
        return $this->render('credits', ['id' => $id]);

    }

    public function actionPayStage($receipt_id)
    {
        $receipt = Receipt::findOne($receipt_id);
        $team = Teams::findOne($receipt->team_id);
        $relRecStage = RelReceiptStage::findOne(['receipt_id' => $receipt->id]);
        $stage = Stages::findOne($relRecStage->stage_id);
        $formattedPaymentData = array(
            'stage_title' => $stage->title,
            'total_cost' => $stage->cost,
            'stage_id' => $stage->id,
            'receipt_id' => $receipt_id,
            'user_email' => User::findOne(Yii::$app->user->id)->email,
            'team_title' => $team->title,
        );
        return $this->render('pay', [
            'paymentData' => json_encode($formattedPaymentData)
        ]);
    }
}
