<?php

namespace frontend\modules\personal\controllers;


use common\models\AdditionalTrainingsPeriod;
use common\models\PayedTraining;
use common\models\Receipt;
use common\models\RelReceiptStage;
use common\models\RelReceiptTraining;
use common\models\Stages;
use common\models\Teams;
use common\models\Training;
use common\models\User;
use frontend\modules\personal\components\BasePersonalController;
use linslin\yii2\curl\Curl;
use Yii;
use yii\base\ErrorException;
use yii\data\ArrayDataProvider;
use yii\httpclient\CurlTransport;
use yii\httpclient\Exception;

class TrainingController extends BasePersonalController
{
    private $mapping = array(
        1 => 'a',
        2 => 'b',
        3 => 'c',
        4 => 'd',
        5 => 'e',
        6 => 'f',
        7 => 'g',
        8 => 'h',
        9 => 'i',

    );

    public function actionIndex($stage_id)
    {
        $stage = Stages::findOne($stage_id);
        $selectedTeam = new Teams();
        $teams = Teams::find()->where(['fk_author' => Yii::$app->user->id])->all();
        if (Yii::$app->request->post()) {
            if (Yii::$app->request->post()['Teams'] && Yii::$app->request->post()['Teams']['id']) {
                $selectedTeam = Teams::findOne(['id' => Yii::$app->request->post()['Teams']['id']]);
            } else $selectedTeam = new Teams();
        }
        $trainings = Training::find()->all();

        $func = function ($tr) {
            return array(
                'title' => Teams::findOne($tr->team_id)->title,
                'id' => $tr->id,
                'start' => $tr->start_datetime,
                'end' => $tr->end_datetime,
                'resourceId' => $this->mapping[$tr->boat_number]
            );
        };


        $formattedTrainings = array_map($func, $trainings);

        $grouped = array();
        foreach ($trainings as $training) {
            $grouped[$training->start_datetime][] = $training;
        }

        return $this->render('index', [
            'model' => $selectedTeam,
            'teams' => $teams,
            'events' => json_encode($formattedTrainings),
            'grouped' => json_encode($grouped),
            'myEvents' => json_encode([]),
            'action' => 'create',
            'receipt' => null,
            'stage' => $stage
        ]);

    }


    public function actionRegisterTraining()
    {
        $receipt = new Receipt();
        $receipt->team_id = (int)\Yii::$app->request->post('trainings')[0]['team_id'];
        $receipt->confirmed = false;
        $receipt->product_type = 'training';
        $receipt->created_by = Yii::$app->user->id;
        $receipt->save();

        foreach (\Yii::$app->request->post('trainings') as $training) {
            $trainingObj = new Training();
            $trainingObj->team_id = (int)$training['team_id'];
            $trainingObj->cost = 12000;
            $trainingObj->start_datetime = $training['start'];
            $trainingObj->end_datetime = $training['end'];
            $trainingObj->boat_number = $training['boat_number'];
            $trainingObj->created_by = Yii::$app->user->id;
            $trainingObj->save();

            $relReceiptTraining = new RelReceiptTraining();
            $relReceiptTraining->receipt_id = $receipt->id;
            $relReceiptTraining->training_id = $trainingObj->id;
            $relReceiptTraining->save();
        }
        return $this->redirect(['done']);
    }

    public function actionDone()
    {
        return $this->render('done');
    }

    public function actionMy()
    {
        $trsSubQuery = (new \yii\db\Query())
            ->select(['training_id', 'confirmed', 'receipt.id AS receipt_id'])
            ->from('rel_receipt_trainings')
//            ->where(['product_type' => 'training'])
            ->rightJoin('receipt', 'receipt.id = rel_receipt_trainings.receipt_id');

        $trs = (new \yii\db\Query())
            ->select(['stages.id AS stage_id', 'stages.title AS stage_title', 'training.id', 'start_datetime', 'training.cost', 'team_id', 'created_by', 'confirmed', 'boat_number', 'receipt_id'])
            ->from('training')
            ->where(['created_by' => Yii::$app->user->id])
            ->leftJoin('teams', 'teams.id = training.team_id')
            ->leftJoin('stages', 'stages.id = training.stage_id')
            ->leftJoin(['u' => $trsSubQuery], 'u.training_id = training.id')
            ->all();

        $user = User::findOne(Yii::$app->user->id);
        $myTrainingReceipts = Receipt::find()->where([
            'created_by' => Yii::$app->user->id,
            'product_type' => 'training'
        ])->all();
        $formattedTrainingReceipts = array();
        foreach ($myTrainingReceipts as $receipt) {
            $relRecTr = RelReceiptTraining::find()->where(['receipt_id' => $receipt->id])->all();
            $trIds = array_map(function ($rel) {
                return $rel->training_id;
            }, $relRecTr);
            if (!empty($trIds) && count($trIds) > 0) {
                $trainings = Training::find()->where(['in', 'id', $trIds])->all();
                $total_cost = 0;
                foreach ($trainings as $training) {
                    $total_cost += $training->cost;
                }
                try{

                    $stage = Stages::findOne($trainings[0]->stage_id);
                } catch (\Exception $e) {
                    throw new Exception(json_encode($trIds));

                }
                $formattedTrainingReceipts[] = array(
                    'receipt_id' => $receipt->id,
                    'total_cost' => $total_cost,
                    'confirmed' => $receipt->confirmed,
                    'team_id' => $receipt->getFkTeam()->all()[0]->title,
                    'user_email' => $user->email,
                    'stage_id' => $trainings[0]->stage_id,
                    'stage_title' => $stage->title
                );
            }

        }
        $provider = new ArrayDataProvider([
            'allModels' => $trs,
            'sort' => [
                'attributes' => ['team_id'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        return $this->render('my',
            [
                'receipts' => $provider
            ]);
    }

    public function actionConfirm()
    {
        $receipt = Receipt::findOne(\Yii::$app->request->post('id'));
        $receipt->confirmed = true;
        $receipt->save();
//        return $this->redirect(['done']);
    }

    public function actionDelete($receipt_id)
    {
        $receipt = Receipt::findOne($receipt_id);
        $relRecTraining = RelReceiptTraining::findOne(['receipt_id' => $receipt_id]);
        $training = Training::findOne($relRecTraining->training_id);
        if (empty($training) || is_null($training) || !$training) {
            return $this->redirect('/personal/training/error');
        }
        if ($training->cost > 0 && $receipt->confirmed) {
            $payedTraining = PayedTraining::findOne(['training_id' => $training->id]);
            if (empty($payedTraining) || !$payedTraining || is_null($payedTraining)) {
                $payedTraining = new PayedTraining();
            }
            $payedTraining->team_id = $receipt->team_id;
            $payedTraining->free = true;
            $payedTraining->save();
        }
        $receipt->delete();
        $relRecTraining->delete();
        $training->delete();


        return $this->redirect(['my']);
    }

    public function actionView($id)
    {
        $receipt = Receipt::findOne($id);
        $relRecTr = RelReceiptTraining::find()->where(['receipt_id' => $id])->all();
        $training = Training::findOne($relRecTr[0]->training_id);
        $stage = Stages::findOne($training->stage_id);
//        return $this->render('debug', [
//            'model' => json_encode($training->team_id),
//        ]);
        $selectedTeam = Teams::findOne($receipt->team_id);
        $func = function ($tr) {
            return array(
                'title' => Teams::findOne($tr->team_id)->title,
                'id' => $tr->id,
                'start' => $tr->start_datetime,
                'end' => $tr->end_datetime,
                'cost' => $tr->cost,
            );
        };
        $trIds = array_map(function ($rel) {
            return $rel->training_id;
        }, $relRecTr);
        $myTrainings = Training::find()->where(['in', 'id', $trIds])->all();


        $trainings = Training::find()->all();

        $formattedTrainings = array_map($func, $trainings);
        $myFormattedTrainings = array_map($func, $myTrainings);

        $grouped = array();
        foreach ($trainings as $training) {
            $grouped[$training->start_datetime][] = $training;
        }

        return $this->render('index', [
            'model' => $selectedTeam,
            'teams' => Teams::find()->all(),
            'events' => json_encode($formattedTrainings),
            'grouped' => json_encode($grouped),
            'myEvents' => json_encode($myFormattedTrainings),
            'action' => 'update',
            'receipt' => $receipt,
            'stage' => $stage
        ]);
    }

    public function actionPay()
    {
        $receipt = Receipt::findOne((int)\Yii::$app->request->post('id'));
        $receipt->confirmed = true;
        $receipt->save();

        try {
            $relRecTr = RelReceiptTraining::find()->where(['receipt_id' => $receipt->id])->all();
            $trIds = array_map(function ($rel) {
                return $rel->training_id;
            }, $relRecTr);
            $myTrainings = Training::find()->where(['in', 'id', $trIds])->all();
            foreach ($myTrainings as $training) {
                $payedTraining = new PayedTraining();
                $payedTraining->training_id = $training->id;
                $payedTraining->team_id = $training->team_id;
                $payedTraining->save();
            }
            $mapReceiptItems = function (Training $trg) {
                return array(
                    'label' => 'Тренировка ' . $trg->start_datetime,
                    'price' => number_format(((int)$trg->cost / 0.97), 2, '.', ''),
                    'quantity' => 1,
                    'amount' => number_format(((int)$trg->cost / 0.97), 2, '.', ''),
                    'vat' => 0,
                    'method' => 0,
                    'object' => 0,
                    'measurementUnit' => 'шт',
                );
            };

            $items = array_values(array_map($mapReceiptItems, $myTrainings));
            $amount = 0;
            foreach ($items as $item) {
                $amount += $item['amount'];
            }

            $customerReceipt = array(
                'Items' => $items,
                'taxationSystem' => 0,
                'email' => User::findOne(Yii::$app->user->id)->email,
                'phone' => '',
                'isoBso' => false,
                'amounts' => array(
                    'electronic' => number_format($amount, 2, '.', ''),
                    'advancePayment' => 0.00,
                    'credit' => 0.00,
                    'provision' => 0.00
                )
            );

            $data = array(
                'Inn' => '7706447178',
                'InvoiceId' => $receipt->id,
                'AccountId' => User::findOne(Yii::$app->user->id)->email,
                'Type' => 'Income',
                'CustomerReceipt' => $customerReceipt
            );

            $curl = new Curl();
            $response = $curl->setRawPostData(
                json_encode($data)
            )
                ->setHeaders([
                    'Authorization' => 'Basic cGtfYjFjN2E0NGRhMDI5ZjgyMjgzMWI3MWUwNDE5NTU6N2ZhNzRiMzU0NGRkYjFjYTBmMDYwMjA4ZGVkOWU5Y2I=',
                    'Content-Type' => 'application/json'
                ])
                ->post('https://api.cloudpayments.ru/kkt/receipt');
            try{

                $receipt->cp_data = json_encode($response);
                $receipt->save();
            } catch (ErrorException $e) {
                $receipt->cp_data = $e->getMessage();
                $receipt->save();
            }

        } catch (ErrorException $e) {
            $receipt->cp_data = $e->getMessage();
            $receipt->save();
        }



        return $this->redirect(['my']);


    }

    public function actionMainTrainings($stage_id)
    {

        $stage = Stages::findOne($stage_id);
        $selectedTeam = Teams::findOne(['fk_author' => Yii::$app->user->id]);
        if (!$selectedTeam || is_null($selectedTeam) || empty($selectedTeam)) {
            $selectedTeam = Teams::findOne(['agent' => Yii::$app->user->id]);
        }
        if (!$selectedTeam || is_null($selectedTeam) || empty($selectedTeam)) {
                            $selectedTeam = Teams::findOne(['title' => User::findOne(Yii::$app->user->id)->username]);
                        }
        $teams = Teams::find()->where(['fk_author' => Yii::$app->user->id])->all();
        if (Yii::$app->request->post()) {
            if (Yii::$app->request->post()['Teams'] && Yii::$app->request->post()['Teams']['id']) {
                $selectedTeam = Teams::findOne(['id' => Yii::$app->request->post()['Teams']['id']]);
            } else $selectedTeam = new Teams();
        }
        $trainings = Training::find()->where(['stage_id' => $stage_id])->all();

        $func = function ($tr) {
            return array(
                'title' => Teams::findOne($tr->team_id)->title,
                'id' => $tr->id,
                'start' => $tr->start_datetime,
                'end' => $tr->end_datetime,
                'resourceId' => $this->mapping[$tr->boat_number],
                'payed' => $tr->cost > 0
            );
        };


        $formattedTrainings = array_map($func, $trainings);

        $grouped = array();
        foreach ($trainings as $training) {
            $grouped[$training->start_datetime][] = $training;
        }

        return $this->render('main_trainings', [
            'model' => $selectedTeam,
            'teams' => $teams,
            'events' => json_encode($formattedTrainings),
            'grouped' => json_encode($grouped),
            'myEvents' => json_encode([]),
            'action' => 'create',
            'receipt' => null,
            'stage' => $stage
        ]);
    }

    public function actionAdditionalTrainings($stage_id)
    {

        $stage = Stages::findOne($stage_id);
        $selectedTeam = Teams::findOne(['fk_author' => Yii::$app->user->id]);
        if (!$selectedTeam || is_null($selectedTeam) || empty($selectedTeam)) {
            $selectedTeam = Teams::findOne(['agent' => Yii::$app->user->id]);
        }
        if (!$selectedTeam || is_null($selectedTeam) || empty($selectedTeam)) {
                            $selectedTeam = Teams::findOne(['title' => User::findOne(Yii::$app->user->id)->username]);
                        }
        $teams = Teams::find()->where(['fk_author' => Yii::$app->user->id])->all();
        if (Yii::$app->request->post()) {
            if (Yii::$app->request->post()['Teams'] && Yii::$app->request->post()['Teams']['id']) {
                $selectedTeam = Teams::findOne(['id' => Yii::$app->request->post()['Teams']['id']]);
            } else $selectedTeam = new Teams();
        }
        $trainings = Training::find()->where(['stage_id' => $stage_id])->all();

        $func = function ($tr) {
            return array(
                'title' => Teams::findOne($tr->team_id)->title,
                'id' => $tr->id,
                'start' => $tr->start_datetime,
                'end' => $tr->end_datetime,
                'resourceId' => $this->mapping[$tr->boat_number],
                'color' => $tr->cost > 0 ? '' : 'gray'
            );
        };


        $formattedTrainings = array_map($func, $trainings);

        $grouped = array();
        foreach ($trainings as $training) {
            $grouped[$training->start_datetime][] = $training;
        }

        $availableDays = (new \yii\db\Query())->from(AdditionalTrainingsPeriod::tableName())->where(['stage_id' => $stage_id])->all();

        return $this->render('additional_trainings', [
            'model' => $selectedTeam,
            'teams' => $teams,
            'events' => json_encode($formattedTrainings),
            'grouped' => json_encode($grouped),
            'myEvents' => json_encode([]),
            'action' => 'create',
            'receipt' => null,
            'stage' => $stage,
            'availableDays' => json_encode($availableDays)
        ]);
    }

    public function actionRegisterFreeTraining($stage_id)
    {
        $tr = Training::findOne([
            'team_id' => (int)\Yii::$app->request->post('trainings')[0]['team_id'],
            'cost' => 0,
            'stage_id' => $stage_id
        ]);
        if (empty($tr->stage_id) || is_null($tr) || is_null($tr->stage_id)) {
            $receipt = new Receipt();
            $receipt->team_id = (int)\Yii::$app->request->post('trainings')[0]['team_id'];
            $receipt->confirmed = true;
            $receipt->product_type = 'training';
            $receipt->created_by = Yii::$app->user->id;
            $receipt->save();

            foreach (\Yii::$app->request->post('trainings') as $training) {
                $trainingObj = new Training();
                $trainingObj->team_id = (int)$training['team_id'];
                $trainingObj->cost = 0;
                $trainingObj->start_datetime = $training['start'];
                $trainingObj->end_datetime = $training['end'];
                $trainingObj->boat_number = $training['boat_number'];
                $trainingObj->created_by = Yii::$app->user->id;
                $trainingObj->stage_id = $stage_id;
                $trainingObj->save();

                $relReceiptTraining = new RelReceiptTraining();
                $relReceiptTraining->receipt_id = $receipt->id;
                $relReceiptTraining->training_id = $trainingObj->id;
                $relReceiptTraining->save();
            }
        } else {
            $tr->start_datetime = \Yii::$app->request->post('trainings')[0]['start'];
            $tr->end_datetime = \Yii::$app->request->post('trainings')[0]['end'];
            $tr->boat_number = \Yii::$app->request->post('trainings')[0]['boat_number'];
            $tr->save();
        }
        return $this->redirect(['done']);
    }

    public function actionRegisterPaidTraining($stage_id)
    {

        $trainings = Yii::$app->request->post('trainings');
        $updatedTrainings = Yii::$app->request->post('updatedTrs');
        $updatedTrainingsIds = null;
        if ($updatedTrainings && !empty($updatedTrainings) && !is_null($updatedTrainings)) {
            $updatedTrainingsIds = array_map(function ($t) {
                return $t['id'];
            }, $updatedTrainings);
        }


        foreach ($trainings as $training) {
            $tempTraining = null;
            if (
                !is_null($updatedTrainingsIds) &&
                is_array($updatedTrainingsIds) &&
                in_array($training['id'], $updatedTrainingsIds)
            ) {
                $tempTraining = Training::findOne($training['id']);
            }
            if (!empty($tempTraining->stage_id) && !is_null($tempTraining) && !is_null($tempTraining->stage_id)) {
                $tempTraining->start_datetime = $training['start'];
                $tempTraining->end_datetime = $training['end'];
                $tempTraining->boat_number = $training['boat_number'];
                $tempTraining->save();
            } else {
                $receipt = new Receipt();
                $receipt->team_id = $training['team_id'];
                $receipt->confirmed = false;
                $receipt->product_type = 'training';
                $receipt->created_by = Yii::$app->user->id;
                $receipt->save();

                $trainingObj = new Training();
                $trainingObj->team_id = $training['team_id'];
                $trainingObj->cost = 12000;
                $trainingObj->start_datetime = $training['start'];
                $trainingObj->end_datetime = $training['end'];
                $trainingObj->boat_number = $training['boat_number'];
                $trainingObj->created_by = Yii::$app->user->id;
                $trainingObj->stage_id = $stage_id;
                $trainingObj->save();

                $relReceiptTraining = new RelReceiptTraining();
                $relReceiptTraining->receipt_id = $receipt->id;
                $relReceiptTraining->training_id = $trainingObj->id;
                $relReceiptTraining->save();
            }
        }

        return $this->redirect(['done']);
    }

    public function actionPayTraining($receipt_id)
    {
        $receipt = Receipt::findOne($receipt_id);
        $relRecTraining = RelReceiptTraining::findOne(['receipt_id' => $receipt->id]);
        $training = Training::findOne($relRecTraining->training_id);
        $stage = Stages::findOne($training->stage_id);
        $formattedPaymentData = array(
            'stage_title' => $stage->title,
            'start_time' => $training->start_datetime,
            'total_cost' => $training->cost,
            'training' => $training,
            'receipt_id' => $receipt_id,
            'team_title' => $receipt->getFkTeam()->all()[0]->title,
            'user_email' => User::findOne(Yii::$app->user->id)->email,
        );
        $freePayedTrainings = PayedTraining::find()->where(['team_id' => $training->team_id, 'free' => true])->all();

        return $this->render('pay', [
            'paymentData' => json_encode($formattedPaymentData),
            'freePayedTrainings' => $freePayedTrainings,
            'receipt_id' => $receipt_id
        ]);
    }

    public function actionRedirectToCredits()
    {
        return $this->redirect('/personal/training/credits');
    }

    public function actionCredits()
    {
        return $this->render('credits');

    }

    public function actionPayByFreeTrainings($receipt_id)
    {
        $receipt = Receipt::findOne($receipt_id);
        $training = RelReceiptTraining::findOne(['receipt_id' => $receipt_id]);
        $availableFreeTraining = PayedTraining::findOne(['team_id' => $receipt->team_id, 'free' => true]);
        if (!empty($availableFreeTraining) && !is_null($availableFreeTraining) && $availableFreeTraining) {
            $availableFreeTraining->training_id = $training->id;
            $availableFreeTraining->free = false;
            $availableFreeTraining->save();
            $receipt->confirmed = true;
            $receipt->save();
            return $this->redirect('/personal/training/my');
        } else {
            return $this->redirect('/personal/training/error');
        }
    }

    public function actionError()
    {
        return $this->render('error');
    }

}
