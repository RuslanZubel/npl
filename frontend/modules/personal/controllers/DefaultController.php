<?php

namespace frontend\modules\personal\controllers;

use common\models\PayedTraining;
use common\models\Teams;
use frontend\modules\content\components\BaseContentController;
use frontend\modules\personal\components\BasePersonalController;

class DefaultController extends BasePersonalController
{
    public function actionIndex()
    {
        $team = Teams::findOne(['fk_author' => \Yii::$app->user->id]);
        $freePayedTrainings = [];
        if (!empty($team) && $team && !is_null($team)) {
            $freePayedTrainings = PayedTraining::find()->where(['team_id' => $team->id, 'free' => true])->all();
        }

        return $this->render('index', [
            'freePayedTrainings' => $freePayedTrainings
        ]);

    }
}