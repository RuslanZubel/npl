<?php
/**
 * Created by PhpStorm.
 * User: codepug
 * Date: 26/03/2018
 * Time: 20:52
 */

namespace frontend\modules\personal\components;


use frontend\components\BaseController;
use Yii;
use yii\web\BadRequestHttpException;

class BasePersonalController extends BaseController
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest){
            return $this->redirect('/site/login');

        }

        try {
            return parent::beforeAction($action);
        } catch (BadRequestHttpException $e) {
        }
    }
}