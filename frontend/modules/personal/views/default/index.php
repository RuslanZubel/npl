<?php

$this->title = 'Личный кабинет';
?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title">
                Личный кабинет
                <br>
                    <?= \common\models\User::findOne(Yii::$app->user->id)->username?>

                <div class="page-description__subtitle">
                     <?= count($freePayedTrainings) > 0 ? 'Количество оплаченных незарезервированных тренировок: ' . count($freePayedTrainings) : ''?>
                </div>
            </div>
        </div>
        <div class="page-item font-weight-bold">
            <a href="/personal/stages/intro">
                Инструкция по пользованию кабинета команды
            </a>
        </div>
        <div class="page-item font-weight-bold">
            <a href="/personal/stages">
                Зарегистрироваться на этап
            </a>
        </div>
        <div class="page-item font-weight-bold">
            <a href="/personal/stages/my">
                Мои этапы
            </a>
        </div>
        <div class="page-item font-weight-bold">
            <a href="/personal/training/my">
                Мои тренировки
            </a>
        </div>
</main>