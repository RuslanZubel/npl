<?php

use common\models\User;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use kartik\helpers\Html;
use yii\widgets\Pjax;


$this->title = 'Платные тренировки';
$stageTitles = array();

?>

<main>
    <div class="tab-content">
        <div class="page-wrapper">
            <div>
                <?php Pjax::begin(); ?>
                <?= \richardfan\sortable\SortableGridView::widget([
                    'dataProvider' => $receipts,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n{pager}",
                    'pager' => [
                        'firstPageLabel' => '« «',
                        'lastPageLabel' => '» »',
                    ],
//                    'rowOptions' => function ($receipts, $key, $index, $grid) {
//                        return ['id' => $receipts, 'onclick' => 'pay(this.id)'];
//
//                    },

                    'sortUrl' => Url::to(['sortItem']),
                    'columns' => [
                        [
                            'header' => 'Команда',
                            'attribute' => 'team_id'
                        ],
                        [
                            'header' => 'ID чека',
                            'attribute' => 'receipt_id'
                        ],
                        [
                            'header' => 'Стоимость, ₽',
                            'attribute' => 'total_cost'
                        ],
                        [
                            'header' => 'Оплата подтвержена',
                            'attribute' => 'confirmed'
                        ],
                        [
                            'class' => yii\grid\ActionColumn::className(),
                            'template' => '{view} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<span class="fa fa-eye"></span>', $url, [
                                        'title' => Yii::t('app', 'Просмотр'),
                                    ]);
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a('<span class="fa fa-trash"></span>', $url, [
                                        'title' => Yii::t('app', 'lead-delete'),
                                    ]);
                                }
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return true;
                                },
                                'delete' => function ($model, $key, $index) {
                                    return $model['confirmed'] === 1 ? false : true;
                                }
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                                return Url::to(['./training/' . $action . '?id=' . $model['receipt_id']]);
                            }
                        ],
                        /*[
                            'attribute' => 'team_id',
                            'format' => 'HTML',
                            'value' => function($data){
                                return '<img src="' . $data->getFileUrl('cover_image') . '" style="width: 50px">';
                            },
                        ],*/

                        // 'updated_at',
                        // 'fk_author',
                        // 'image',
                        // 'cover_image',

                    ],
                ]); ?>


                <?php Pjax::end(); ?>
            </div>


        </div>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script lang="javascript">



</script>
<style>
    #registerTraining {
        margin: 15px 0 15px;
    }

    .fc-license-message {
        display: none;
    }


</style>