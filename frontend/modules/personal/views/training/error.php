<?php

use yii\helpers\Url;

$this->title = 'Ошибка';

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title" style="position: relative">
                <a href="/personal" class="back_to_personal_link"> <b>< </b>Вернуться в личный кабинет</a>
                <a>ОШИБКА</a>
                <div class="page-description__subtitle">Приносим извинения. В ходе выполнения операции произошла ошибка</div>
            </div>
            <div style="text-align: center">

                <a href="<?= Url::toRoute('/personal') ?>">Вернуться в личный кабинет</a>

            </div>
        </div>
</main>

