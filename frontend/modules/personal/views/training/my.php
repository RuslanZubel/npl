<?php


use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;
use backend\components\SortableGridView;
use yii\helpers\Url;


$this->title = 'Тренировки';

$this->registerJs("
$('#showButton').on('click', '.toggle-active', function(id) {
                console.log('show');
            });
            pay = function (jsonCred) {
            console.log(jsonCred);
            const cred = JSON.parse(jsonCred);
            console.log(jsonCred);
    var widget = new cp.CloudPayments();
    console.log(widget);
    widget.charge({ // options
            publicId: 'pk_b1c7a44da029f822831b71e041955',  //id из личного кабинета
            description: 'Пример оплаты (деньги сниматься не будут)', //назначение
            amount: Math.ceil(cred.total_cost/0.97), //сумма
            currency: 'RUB', //валюта
            invoiceId: cred.receipt_id, //номер заказа  (необязательно)
            email: cred.user_email, //идентификатор плательщика (необязательно)
            require_email:true,
            data: {
                myProp: 'myProp value' //произвольный набор параметров
            }
        },
        function (options) { // success
            console.log(options);
            $.post('" . Url::toRoute('/personal/training/confirm') . "',
            {'id': cred.receipt_id},
            function( resp ){
                console.log(resp);
            });
        },
        function (reason, options) { // fail
            //действие при неуспешной оплате
        });
};    
            ");

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title" style="position: relative">
                <a href="/personal" class="back_to_personal_link"> <b>< </b>Вернуться в личный кабинет</a>
                <a>Мои тренировки</a>
                <div class="page-description__subtitle">Здесь представлены Ваши тренировки</div>
            </div>
            <div>
                <?php Pjax::begin(); ?>
                <?= \richardfan\sortable\SortableGridView::widget([
                    'dataProvider' => $receipts,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n{pager}",
                    'pager' => [
                        'firstPageLabel' => '« «',
                        'lastPageLabel' => '» »',
                    ],
//                    'rowOptions' => function ($receipts, $key, $index, $grid) {
//                        return ['id' => $receipts, 'onclick' => 'pay(this.id)'];
//
//                    },

                    'sortUrl' => Url::to(['sortItem']),
                    'columns' => [
                        [
                            'header' => 'Этап',
                            'attribute' => 'stage_title'
                        ],
                        [
                            'header' => 'ID чека',
                            'attribute' => 'receipt_id'
                        ],
                        [
                            'header' => 'Стоимость, ₽',
                            'attribute' => 'cost'
                        ],
                        [
                            'attribute' => 'Номер лодки',
                            'value' => function ($data) {
                                return !empty($data['boat_number']) ? $data['boat_number'] : 'Не опеределен';
                            },
                        ],
                        [
                            'attribute' => 'Время начала',
                            'value' => function ($data) {
                                return !empty($data['start_datetime']) ? $data['start_datetime'] : 'Не опеределен';
                            },
                        ],
                        [
                            'header' => 'Оплата подтвержена',
                            'attribute' => 'confirmed'
                        ],
                        [
                            'class' => yii\grid\ActionColumn::className(),
                            'template' => '{pay-training} {delete}',
                            'buttons' => [
                                'pay-training' => function ($url, $model) {
                                    return Html::a('<span class="fa fa-ruble" style="display: inline-grid; text-align: center">Оплатить</span>', $url, [
                                        'title' => Yii::t('app', 'Оплатить'),
                                    ]);
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a('<span class="fa fa-trash " style="display: inline-grid; text-align: center">Отменить</span>', $url, [
                                        'title' => Yii::t('app', 'lead-delete'),
                                    ]);
                                }
                            ],
                            'visibleButtons' => [
                                'pay-training' => function ($model, $key, $index) {

                                    return $model['confirmed'] ? false : true;
                                },
                                'delete' => function ($model, $key, $index) {
                                    if (strtotime($model['start_datetime']) < strtotime($model['cost'] > 0 ? '24 hours' : '96 hours')) {
                                        return false;
                                    }
                                    return $model['confirmed'] === 1 ? false : true;
                                }
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                                if ($action === 'view') {
                                    return Url::to(['/personal/training/show?stage_id=' . $model['stage_id']]);
                                }
                                return Url::to(['./training/' . $action . '?receipt_id=' . $model['receipt_id']]);
                            }
                        ],
                        /*[
                            'attribute' => 'team_id',
                            'format' => 'HTML',
                            'value' => function($data){
                                return '<img src="' . $data->getFileUrl('cover_image') . '" style="width: 50px">';
                            },
                        ],*/

                        // 'updated_at',
                        // 'fk_author',
                        // 'image',
                        // 'cover_image',

                    ],
                ]); ?>


                <?php Pjax::end(); ?>
            </div>

        </div>
    </div>
</main>

<script>
</script>


