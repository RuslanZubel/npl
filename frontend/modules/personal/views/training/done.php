<?php

use yii\helpers\Url;

$this->title = 'Тренировки';

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title" style="position: relative">
                <a href="/personal" class="back_to_personal_link"> <b>< </b>Вернуться в личный кабинет</a>
                <a>СОХРАНЕНО</a>
                <div class="page-description__subtitle">Регистрация тренировки прошла успешно</div>
            </div>
            <div style="text-align: center">

                <p>Для подтверждения регистрации не забудьте оплатить тренировки в разделе <a href="<?= Url::toRoute('/personal/training/my') ?>">Мои тренировки</a></p>

                <a href="<?= Url::toRoute('/personal') ?>">Вернуться в личный кабинет</a>

            </div>
        </div>
</main>

