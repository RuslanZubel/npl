<?php

use common\models\User;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use kartik\helpers\Html;


$this->title = 'Бесплатная тренировка';
$stageTitles = array();

$this->registerJs("
console.log($events);
console.log($grouped);
const recs = [
            {number: 1, id: 'a'},
            {number: 2, id: 'b'},
            {number: 3, id: 'c'},
            {number: 4, id: 'd'},
            {number: 5, id: 'e'},
            {number: 6, id: 'f'},
            {number: 7, id: 'g'},
            {number: 8, id: 'h'},
            {number: 9, id: 'i'},
        ];
const formatDate = ()=>{
            return new Date(Number('" . $stage->main_trainings_date . "') * 1000);

        }

 events = [];
    $('#registerTraining').click(()=>{
        $.post('" . Url::toRoute('/personal/training/register-free-training?stage_id=') . $stage->id . "',
            {'trainings': events},
            function( resp ){
                console.log(resp);
            });
        console.log(events);
    })
    $('#selectTeam').click(() => {
        console.log('click');
    });
    $('#calendar').fullCalendar({
//        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        header: {   
            'center' : 'title',
            'left': '',
            'right': ''
                },
        defaultView: 'timelineDay',
        defaultDate: formatDate(),
        editable: true,
        eventAllow: (dropInfo, draggedEvent) => {
            console.log(dropInfo);
            if ('$model->title' === draggedEvent.title) {
                events = events.filter(event => event.id !== draggedEvent.id);
                events.push({
                    id: draggedEvent.id,
                    start: dropInfo.start.format(),
                    end: dropInfo.end.format(),
                    title: '$model->title',
                    team_id: '$model->id',
                    resourceId: dropInfo.resourceId,
                    boat_number: recs.find(rec => rec.id === dropInfo.resourceId).number

                 });
                return true;
            }
            return false;
        },
        eventOverlap: false,
        allDaySlot: false,
        'events': $events,
        dragScroll: true,
        'selectable': true,
        'selectOverlap': false,
        'aspectRatio': 2.2,
        'minTime': '10:00',
        'maxTime': '18:00',
        'slotDuration': '02:00:00',
        'scrollTime': '10:00',
        hiddenDays: [  ],
        slotLabelFormat: [
          'HH:mm', // top level of text
        ],
        
         resourceColumns: [
            {
              labelText: 'Номер лодки',
              field: 'number'
            },
          ],
        resources: recs,
        selectAllow: function (moment) {
        console.log($events.find(elem => elem.title === '$model->title'));
        console.log($events, $myEvents);
            return events.length < 1; 
        },
        select: $events.find(elem => elem.title === '$model->title') ? null : function (startDate, endDate, s, d, e,f) {
            console.log(startDate, endDate,s,d,e,f);
            const id = new Date().getTime();
            $('#calendar').fullCalendar(
                'renderEvent',
                {
                    id,
                    start: startDate.format(),
                    end: endDate.format(),
                    title: '$model->title',
                    resourceId: e.id

                },
                true
            );
            events.push({
                id,
                start: startDate.format(),
                end: endDate.format(),
                title: '$model->title',
                team_id: '$model->id',
                resourceId: e.id,
                boat_number: e.number


            });
            console.log(events);
        },
    });




$('#payTraining').click(() => {
        const evs = $('#calendar').fullCalendar('getEventSources')[0].rawEventDefs;
        let total = 0;
        evs.forEach(ev => {
            total += (ev.cost / 0.97).toFixed(2);
        });
        pay2({
            total_cost: total,
            receipt_id: " . (!is_null($receipt) ? $receipt->id : 'xxx') . ",
            user_email: '" . User::findOne(Yii::$app->user->id)->email . "'
        });

    });
    
    
    
    
    
    
    
    pay2 = function (cred) {
        console.log($myEvents);
        var widget = new cp.CloudPayments();
        console.log(widget);
        widget.charge({ // options
                publicId: 'pk_b1c7a44da029f822831b71e041955',  //id из личного кабинета
                description: 'Оплата тренировок', //назначение
                amount: cred.total_cost, //сумма
                currency: 'RUB', //валюта
                invoiceId: cred.receipt_id, //номер заказа  (необязательно)
                email: cred.user_email, //идентификатор плательщика (необязательно)
                require_email: false,
                data: {
                    myProp: 'myProp value' //произвольный набор параметров
                }
            },
            function (options) { // success

                $.post('" . Url::toRoute('/personal/training/pay') . "',
                    {'id': cred.receipt_id},
                    function (resp) {
                        console.log(resp);
                    }
            )
            },
            function (reason, options) { // fail
                //действие при неуспешной оплате
            });
    };
    
    
    
    
    
    

",
    View::POS_READY)

?>

<main>
    <div class="tab-content">
        <div class="page-wrapper">
            <?php $form = ActiveForm::begin(); ?>
            <div class="panel panel-default jur-panel" style="<?= $model->id ? 'display: none' : '' ?>">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">

                            <?= $form->field($model, 'id')
                                ->dropDownList(\yii\helpers\ArrayHelper::map($teams, 'id', 'title'), ['prompt' => ''])
                                ->label('Команда') ?>

                            <a class="btn btn-default btn-xs"
                               href="/personal/teams"
                               style="margin-bottom: 15px">
                                Добавить команду
                            </a>


                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group" style="<?= $model->id ? 'display: none' : '' ?>">
                <button class="btn btn-primary" id="selectTeam">
                    Выбрать команду
                </button>
            </div>
            <?php ActiveForm::end(); ?>

            <div id="mycalendar">
                <?= $model->id ?
                    \edofre\fullcalendarscheduler\FullcalendarScheduler::widget([
                        'header' => [
                            'center' => 'title',
                        ],

                    ])
                    :
                    null
                ?>
            </div>
            <?php
            if ($model->id) {
                switch ($action) {
                    case 'update':
                        echo
                        '<button id="registerTraining" class="btn btn-primary" style="margin-bottom: 15px"> Сохранить</button>';
                        break;
                    default:
                        echo
                        '<button id="registerTraining" class="btn btn-primary" style="margin-bottom: 15px"> Сохранить </button>';
                }
            }
            ?>
            <button id="payTraining" class="btn btn-primary"
                    style="margin-bottom: 15px; <?= (is_null($receipt) ? 'display: none' : $receipt->confirmed === 1 ? 'display: none' : '') ?>">
                Оплатить тренировку
            </button>


        </div>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script lang="javascript">



</script>
<style>
    #registerTraining {
        margin: 15px 0 15px;
    }

    .fc-license-message {
        display: none;
    }


</style>