<?php

use yii\bootstrap\Tabs;

$this->title = 'Тренировки';

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title" style="position: relative">
                <a href="/personal" class="back_to_personal_link"> <b>< </b>Вернуться в личный кабинет</a>
                <a>Тренировки</a>
                <div class="page-description__subtitle">Тренировки для "<?= $stage->title?>"</div>
            </div>
            <div class="container">
                <?=
                Tabs::widget([
                    'items' => [
                        [
                            'label'     =>  '',
                            'content'   =>  $this->render('free', [
                                'model' => $model,
                                'teams' => $teams,
                                'events' => $events,
                                'grouped' => $grouped,
                                'myEvents' => json_encode([]),
                                'action' => 'create',
                                'receipt' => null,
                                'stage'=>$stage
                            ]),
                            'active'    =>  true
                        ],
                    ]
                ]);
                ?>
            </div>
        </div>
</main>

