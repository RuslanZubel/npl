<?php


use yii\bootstrap\Html;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = 'Оплата этапа';


$this->registerJs("

$('#getCredentials').click( () => {
    $.get('" . Url::toRoute('/personal/training/redirect-to-credits') . "',
            function( resp ){
                console.log(resp);
            });
});

$('#payByFreeTrainings').click( () => {
    $.get('" . Url::toRoute('/personal/training/pay-by-free-trainings?receipt_id=' . $receipt_id) . "',
            function( resp ){
                console.log(resp);
            });
});

$('#payStage').click(function(id) {
                console.log('show');
                pay($paymentData);
            });
            pay = function (jsonCred) {
            console.log(jsonCred);
            const cred = jsonCred;
    var widget = new cp.CloudPayments();
    widget.charge({ // options
            publicId: 'pk_b1c7a44da029f822831b71e041955',  //id из личного кабинета
            description: 'Оплата тренировки «' + cred.stage_title + '» на время ' + cred.start_time  + ' командной «' + cred.team_title + '»', //назначение
            amount: parseFloat((cred.total_cost/0.97).toFixed(2)), //сумма
            currency: 'RUB', //валюта
            invoiceId: cred.receipt_id, //номер заказа  (необязательно)
            email: cred.user_email, //идентификатор плательщика (необязательно)
            require_email: false,
            data: {
                myProp: 'myProp value' //произвольный набор параметров
            }
        },
        function (options) { // success
            console.log(options);
            $.post('" . Url::toRoute('/personal/training/pay') . "',
            {'id': cred.receipt_id},
            function( resp ){
                console.log(resp);
            });
        },
        function (reason, options) { // fail
            //действие при неуспешной оплате
        });
};    
            ", View::POS_READY);
?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title">
                <a>Оплата тренировки</a>
                <div class="page-description__subtitle">Выберите способ оплаты</div>
                <div class="page-description__subtitle">
                    <?= count($freePayedTrainings) > 0 ? 'Количество оплаченных незарезервированных тренировок: ' . count($freePayedTrainings) : ''?>
                </div>
            </div>
            <div class="container">
                <div class="row justify-content-md-center">
                    <div id="payStage" class="card mr-5" style="width: 18rem; cursor: pointer">
                        <img class="card-img-top p-3" src="img/online-payment.svg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title text-center">Онлайн оплата</h5>
                            <p class="card-text text-center">Данный вид оплаты проходит в несколько кликов и для него необходимо только наличие данных кредитной карты под рукой</p>
                        </div>
                    </div>
                    <div id="getCredentials" class="card mr-5" style="width: 18rem; cursor: pointer">
                        <img class="card-img-top p-3" src="img/bank-account.svg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title text-center">Оплата по банковским реквизитам</h5>
                            <p class="card-text text-center">При использовании данного вида оплаты Вам предоставляются банковские реквизиты, по которым нужно провести оплату и предоставить чек организатору для подтверждения</p>
                        </div>
                    </div>
                    <?=
                    count($freePayedTrainings) > 0 ?
                        '<div id="payByFreeTrainings" class="card mr-5" style="width: 18rem; cursor: pointer">
                        <img class="card-img-top p-3" src="img/check.svg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title text-center">Использование незарезервированной тренировки</h5>
                            <p class="card-text text-center">При использовании данного вида оплаты будет списана одна тренировка в вашего счёта оплаченных незарезервированных тренировок</p>
                        </div>
                    </div>' : ''
                    ?>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
<style>
    #payStage{
        color: #337ab7
    }
    #payStage:hover{
        color: #16326d
    }

    #payBank{
        color: #337ab7
    }
    #payBank:hover{
        color: #16326d
    }
</style>
