<?php

use yii\helpers\Html;
use backend\components\SortableGridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\TeamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Команды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teams-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-6 buttons-right">
            <?= Html::a('Создать команду', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">



        </div>
    </div>
</div>