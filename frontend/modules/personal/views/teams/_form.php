<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\widgets\single_image_field\SingleImageField;

/* @var $this yii\web\View */
/* @var $model backend\models\Teams */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teams-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'short_title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'club')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <?= SingleImageField::widget(['model' => $model, 'thumbName' => true, 'form' => $form, 'attribute' => 'image']) ?>
                    <?= SingleImageField::widget(['model' => $model, 'thumbName' => true, 'form' => $form, 'attribute' => 'cover_image']) ?>
                </div>
            </div>

        </div>
    </div>
    <div class="form-group">
        <?= \backend\components\widgets\crud_buttons\CrudButtons::widget(['model' => $model]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
