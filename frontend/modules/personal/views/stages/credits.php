<?php

use yii\helpers\Url;


$this->title = 'Реквизиты';

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title">
                <a>Реквизиты для оплаты</a>
                <div class="page-description__subtitle">Внимание! Если за четыре рабочих дня до тренировочного дня оплата стартового взноса не будет подтверждена, то тренировка будет автоматически аннулирована</div>
            </div>
            <div class="container">
                <div class="row justify-content-md-center">
                    <div  class="card">
                        <div class="card-header">
                            Платёжные реквизиты
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Расчётный счёт №</td>
                                    <td>40702810502200007827</td>
                                </tr>
                                <tr>
                                    <td>Наименование банка</td>
                                    <td>AO "АЛЬФА-БАНК" г. Москва</td>
                                </tr>
                                <tr>
                                    <td>БИК</td>
                                    <td>044525593</td>
                                </tr>
                                <tr>
                                    <td>Корреспондентский счет</td>
                                    <td>30101810200000000593</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer ">
                            <div class="row justify-content-md-end">
                                <a class="btn btn-primary" href="<?= Url::toRoute('/personal/training/main-trainings?stage_id=' . $id)?>">Далее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</main>

