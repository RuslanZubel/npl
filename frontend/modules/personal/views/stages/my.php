<?php


use yii\bootstrap\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = 'Этапы';


$this->registerJs("
$('#payStage').on('click', '.toggle-active', function(id) {
                console.log('show');
            });
            pay = function (jsonCred) {
            console.log(jsonCred);
            const cred = JSON.parse(jsonCred);
    var widget = new cp.CloudPayments();
    widget.charge({ // options
//            publicId: 'pk_d872e5b1a22e51582755ddb48b89b',  //id из личного кабинета nsl.dev
            publicId: 'pk_b1c7a44da029f822831b71e041955',  //id из личного кабинета nationalsailingleague.ru
            description: 'Оплата этапа «' + cred.stage_title + '» командной «' + cred.team_id + '»', //назначение
            amount: parseFloat((cred.total_cost/0.97).toFixed(2)), //сумма
            currency: 'RUB', //валюта
            invoiceId: cred.receipt_id, //номер заказа  (необязательно)
            email: cred.user_email, //идентификатор плательщика (необязательно)
            require_email: false,
            data: {
                myProp: 'myProp value' //произвольный набор параметров
            }
        },
        function (options) { // success
            console.log(options);
            $.post('" . Url::toRoute('/personal/stages/confirm') . "',
            {'id': cred.receipt_id},
            function( resp ){
                console.log(resp);
            });
        },
        function (reason, options) { // fail
            //действие при неуспешной оплате
        });
};    
            ");
?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title" style="position: relative">
                <a href="/personal" class="back_to_personal_link"> <b>< </b>Вернуться в личный кабинет</a>
                <a>Мои этапы</a>
                <div class="page-description__subtitle">Здесь представлены этапы, на которые вы зарегистрированы</div>
            </div>
            <div>
                <?php Pjax::begin(); ?>
                <?= \richardfan\sortable\SortableGridView::widget([
                    'dataProvider' => $receipts,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n{pager}",
                    'pager' => [
                        'firstPageLabel' => '« «',
                        'lastPageLabel' => '» »',
                    ],
                    'rowOptions' => function ($receipts, $key, $index, $grid) {
                        return ['id' => $receipts, 'onclick' => 'pay(this.id)'];

                    },

                    'sortUrl' => Url::to(['sortItem']),
                    'columns' => [
                        [
                            'header' => 'Этап',
                            'attribute' => 'stage_title'
                        ],
                        [
                            'header' => 'Команда',
                            'attribute' => 'team_id'
                        ],
                        [
                            'header' => 'ID чека',
                            'attribute' => 'receipt_id'
                        ],
                        [
                            'header' => 'Стоимость, ₽',
                            'attribute' => 'total_cost'
                        ],
                        [
                            'header' => 'Оплата подтверждена',
                            'attribute' => 'confirmed'
                        ],
                        [
                            'class' => yii\grid\ActionColumn::className(),
                            'template' => '{main-trainings} {additional-trainings} {pay} {delete}',
                            'buttons' => [
                                'main-trainings' => function ($url, $model) {
                                    return Html::a('<span class="fa" style="width: 80px; text-align: center"> Основные тренировки </span>', $url, [
                                        'title' => Yii::t('app', 'Открыть основные тренировки этапа'),
                                    ]);
                                },
                                'additional-trainings' => function ($url, $model) {
                                    return Html::a('<span class="fa" style="width: 120px; text-align: center"> Дополнительные тренировки </span>', $url, [
                                        'title' => Yii::t('app', 'Открыть дополнительные тренировки этапа'),
                                    ]);
                                },
                                'pay' => function ($url, $model) {
                                    return '<span id="payStage" class="fa fa-credit-card" title="Оплатить онлайн" ></span>';
                                },
                                'bank' => function ($url, $model) {
                                    return '<span id="payBank" class="fa fa-ruble" title="Оплатить по счёту"></span>';
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a('<span class="fa fa-trash"></span>', $url, [
                                        'title' => Yii::t('app', 'Отменить предварительную регистрацию'),
                                    ]);
                                }
                            ],
                            'visibleButtons' => [
                                'delete' => function ($model, $key, $index) {
                                    return $model['confirmed'] === 1 ? false : true;
                                }
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                                if ($action === 'delete') {
                                    return Url::to(['./stages/'.$action.'?id='.$model['receipt_id']]);
                                }
                                return Url::to(['./stages/'.$action.'?id='.$model['stage_id']]);
                            }
                        ],
                        /*[
                            'attribute' => 'cover_image',
                            'format' => 'HTML',
                            'value' => function($data){
                                return '<img src="' . $data->getFileUrl('cover_image') . '" style="width: 50px">';
                            },
                        ],*/

                        // 'updated_at',
                        // 'fk_author',
                        // 'image',
                        // 'cover_image',

                    ],
                ]); ?>


                <?php Pjax::end(); ?>
            </div>
        </div>
</main>
<style>
    #payStage{
        color: #337ab7
    }
    #payStage:hover{
        color: #16326d
    }

    #payBank{
        color: #337ab7
    }
    #payBank:hover{
        color: #16326d
    }
</style>
