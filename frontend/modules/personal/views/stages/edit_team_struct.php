<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;

$this->registerJs("

//TODO: не осилил обновление модального диалога редактирования списка партнёров для команды в рамках сезона - сделал как смог...
function reload_team_structure() {
    var popup = $('#team-edit-modal');
    popup.modal('show');
    $.post(
        '" . Url::toRoute('/personal/stages/edit-command') . "', 
            {'fkRelTeamsRelSeasonsLeagues':  popup.attr('rel'), 'fkTeam': popup.attr('team')}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
}

function add_team_member() {
    var memberUnit = $('#member_unit').val();
    var memberRole = $('#member_role').val();
    $.post('" . Url::toRoute('/personal/stages/add-team-member') . "', 
    {
        'fkRelTeamsRelSeasonsLeagues': " . $fkRelTeamsRelSeasonsLeagues . ",
        'memberUnit' : memberUnit,
        'memberRole' : memberRole},
    function( resp ){
        if (resp.error != '') {
            alert(resp.error);
        } else {
            console.log(resp);
        }    
    });
};



function remove_team_member(id) {
    krajeeDialogCust.confirm('Удалить данного члена команды?', function(out){
            if(out) {
                $.post('" . Url::toRoute('/personal/stages/remove-team-member') . "', 
                {
                    'fkRelTeamsRelSeasonsLeagues': " . $fkRelTeamsRelSeasonsLeagues . ",
                    'teamStructureId' : id},
                    function( resp ){
                        if (resp.error != '') {
                            alert(resp.error);
                        } else {
                            reload_team_structure();
                        }    
                    });
        }});
};


", yii\web\View::POS_HEAD);


?>

<main>
    <div class="modal-content mr-3 h-100" role="document">
        <div class="modal-header">
            <h1>Редактирование состава команды: <?= $teamTitle; ?>
                <br>
        </div>
        <div class="container ">
            <div class="row">
                <div class="col-xs-5 col-sm-5">
                    <?php
                    echo Select2::widget([
                        'name' => 'member_unit',
                        'id' => 'member_unit',
                        'data' => \common\models\TeamsStructures::getList(),
                        'options' => [
                            'multiple' => false,
                        ],
                        'pluginOptions' => [
                            'closeOnSelect' => false,
                        ],
                    ]);

                    ?>
                </div>
                <div class="col-xs-5 col-sm-5">
                    <?php
                    echo Select2::widget([
                        'name' => 'member_role',
                        'id' => 'member_role',
                        'data' => \common\models\Roles::getList(),
                        'options' => [
                            'multiple' => false,
                        ],
                        'pluginOptions' => [
                            'closeOnSelect' => false,
                        ],
                    ]);

                    ?>
                </div>
                <div class="col-xs-2 col-sm-2">
                    <div class="form-group">
                        <p><?= Html::a('Добавить', null, ['class' => 'btn btn-primary', 'onclick' => 'add_team_member();']) ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <?php Pjax::begin(['id' => 'teams-structure-pjax', 'enablePushState' => false]); ?>
                    <?= GridView::widget([
                        'id' => \common\components\UniqueString::getUniqueString(6),
                        'dataProvider' => $dataProvider,
                        'tableOptions' => ['class' => 'table table-striped table-hover'],
                        'layout' => "{items}\n{pager}",
                        'pager' => [
                            'firstPageLabel' => '« «',
                            'lastPageLabel' => '» »',
                        ],
                        //'sortUrl' => Url::to(['sortItem']),
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'title',
                            [
                                'label' => 'Имя',
                                'value' => function ($item) {
                                    return $item->getFkMembers()->one()->firstname . ' ' . $item->getFkMembers()->one()->lastname;
                                }
                            ],
                            [
                                'label' => 'Должность',
                                'value' => function ($item) {
                                    return $item->getFkRoles()->one()->title;
                                }
                            ],
                            /*[
                                'attribute' => 'is_active',
                            ],*/
                            //'sort',
                            [
                                'options' => ['width' => '60px'],
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{delete}',
                                'buttons' => [
                                    'delete' => function ($url, $item) {
                                        return Html::a(
                                            '<span class="fa fa-trash pr5 cp"></span>',
                                            $url,
                                            [
                                                'title' => Yii::t('yii', 'Удалить'),
                                            ]
                                        );
                                    },
                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    return Url::to(['/personal/stages/remove-team-member?id=' . $model->id]);
                                }
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                    <?= Html::a('Далее', ['/personal/stages/my'], ['class'=>'btn btn-primary float-right']) ?>

                </div>
            </div>
        </div>
    </div>
</main>
