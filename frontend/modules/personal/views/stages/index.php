<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Этапы';
$stageTitles = array();
?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title" style="position: relative">
                <a href="/personal" class="back_to_personal_link"> <b>< </b>Вернуться в личный кабинет</a>
                Регистрация на этап
                <div class="page-description__subtitle"></div>
            </div>
            <div>

                <?php $form = ActiveForm::begin(); ?>
                <div class="panel panel-default jur-panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">



                                <?= $form->field($relReceiptStage, 'stage_id')
                                    ->dropDownList(\yii\helpers\ArrayHelper::map($stages, 'id','title'), ['prompt'=>''])
                                    ->label('Этап')?>
                                <?= $selectedStage->title?>

                                <?= $form->field($selectedTeam, 'title')->textInput(['disabled' => true])
                                    ->label('Команда')?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <?=
                    Html::submitButton(
                        'Сохранить',
                        ['class' => 'btn btn-primary']
                    );
                    ?>
                    <?=
                    Html::a(
                        'Отменить' ,
                        \Yii::$app->request->referrer ?? Yii::$app->homeUrl,
                        [ 'class' =>  'btn btn-default' ]
                    );
                    ?>
                </div>



                <?php ActiveForm::end(); ?>

        </div>
    </div>
</main>