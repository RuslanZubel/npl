<?php

use frontend\components\widgets\galleries\image\ImageGalleryWidget;

$this->title = 'Фотогалереи';

?>

<main>
    <?= ImageGalleryWidget::widget(['viewType' => ImageGalleryWidget::VIEW_TYPE_LIST  ]) ?>
</main>
