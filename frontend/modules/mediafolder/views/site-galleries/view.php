<?php

use frontend\components\widgets\galleries\image\ImageGalleryWidget;

$this->title = $gallery->title . ' | ' . 'Фотогалереи';

?>

<main>
	<div class="gallery-in dark-background">
		<div class="container">
			<div class="row">
				<div class="text-center"><div class="gallery-date"><?= $gallery->custom_date; ?></div></div>
				<div class="gallery-text">
                        <div class="gallery-title"><?= $gallery->title; ?></div>
					<div class="gallery-caption">
						<p><?= $gallery->subtitle; ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="dark-background">
		<div class="container">
			<div class="gallery-items">
				<div class="row">

                    <?php
                        foreach($toGridGallery as $item) {
                            echo '<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="' .
                                $item->getFileUrl('image') . '"><img src="' .
                                $item->getFileUrl('image') . '" alt=""></a></div>';
                        }
                    ?>
				</div>
			</div>
		</div>
        <?= frontend\components\widgets\content\socialnetworks\SocialNetworksWidget::widget() ?>
	</div>
</main>





