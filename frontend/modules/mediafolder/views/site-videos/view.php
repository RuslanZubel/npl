<?php

use frontend\components\widgets\galleries\image\ImageGalleryWidget;

$this->title = $video->title . ' | ' . 'Видео';

?>



<main>
    <div class="video-in dark-background">
        <div class="page-wrapper">
            <div class="container">
                <div class="video-inner--info">
                    <div class="video-inner--date"><?= $video->custom_date; ?></div>
                    <div class="video-inner--title"><?= $video->title; ?></div>
                </div>
                <div class="half-wrapper">
                    <?= $video->video_id; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="dark-background">
        <div class="container">
            <div class="half-wrapper">
                <p><?= $video->description; ?></p>
            </div>
        </div>
        <?= frontend\components\widgets\content\socialnetworks\SocialNetworksWidget::widget() ?>
    </div>
</main>




