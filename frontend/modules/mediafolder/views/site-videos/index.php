<?php

use frontend\components\widgets\galleries\video\VideoGalleryWidget;

$this->title = 'Видео';

?>

<main>
    <?= VideoGalleryWidget::widget(['viewType' => VideoGalleryWidget::VIEW_TYPE_LIST  ]) ?>
</main>
