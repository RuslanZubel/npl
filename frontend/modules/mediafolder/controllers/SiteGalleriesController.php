<?php

namespace frontend\modules\mediafolder\controllers;

use frontend\models\SiteGalleries;
use frontend\models\SiteGalleriesImages;
use frontend\modules\mediafolder\components\BaseMediaFolderController;


/**
 * Default controller for the `mediafolder` module
 */
class SiteGalleriesController extends BaseMediaFolderController
{
    public function actionIndex()
	   {
		   return $this->render('index');
	   }
	
	public function actionView($id)
	{
	    $gallery = SiteGalleries::findOne($id);

		$toGridGallery = SiteGalleriesImages::find()
			->where(['fk_site_galleries' => $id])
			//->orderBy
			->all();
		
		return $this->render('view', [
		    'id' => $id,
            'toGridGallery' => $toGridGallery,
            'gallery' => $gallery,
        ]);
	}
}
