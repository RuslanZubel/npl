<?php

namespace frontend\modules\mediafolder\controllers;

use frontend\models\SiteVideos;
use frontend\modules\mediafolder\components\BaseMediaFolderController;



/**
 * Default controller for the `mediafolder` module
 */
class SiteVideosController extends BaseMediaFolderController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionView($id)
    {
        $video = SiteVideos::findOne($id);

        return $this->render('view', [
            'id' => $id,
            'video' => $video,
        ]);
    }
}
