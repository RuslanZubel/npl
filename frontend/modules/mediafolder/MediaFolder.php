<?php

namespace frontend\modules\mediafolder;

/**
 * mediafolder module definition class
 */
class MediaFolder extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\mediafolder\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
