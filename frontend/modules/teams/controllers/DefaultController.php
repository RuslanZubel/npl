<?php

namespace frontend\modules\teams\controllers;

use common\models\Roles;
use frontend\models\RelSeasonsLeagues;
use frontend\models\Teams;
use frontend\models\TeamsStructures;
use frontend\modules\teams\components\BaseTeamsController;
use frontend\models\RelTeamsRelSeasonsLeagues;
use frontend\models\Seasons;
use frontend\models\Leagues;
use frontend\models\RelTeamsTeamsPartners;

/**
 * Default controller for the `teams` module
 */
class DefaultController extends BaseTeamsController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex( $season = null, $league = null )
    {
        if (is_null($season)) {
            $seasonItem = Seasons::find()->orderBy(['sort'=>SORT_DESC])->select(['id', 'title'])->one();
        } else {
            $seasonItem = Seasons::find()->where(['id' => $season])->select(['id', 'title'])->one();
        }

        $leagues = Leagues::find()
            ->joinWith('relSeasonsLeagues')
            ->where( [ RelSeasonsLeagues::tableName().'.fk_seasons' => $seasonItem->id ])
            ->select([Leagues::tableName().'.id', Leagues::tableName().'.title'])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $leagueItem = null;

        if (!is_null($league)) {
            $leagueItem = Leagues::findOne($league);
        } else if (count($leagues) > 0) {
            $leagueItem = reset($leagues);
        }

        $teams = array();
        if ($leagueItem && $seasonItem) {
            $idRelSeasonsLeague = RelSeasonsLeagues::find()->where(['fk_leagues' => $leagueItem->id, 'fk_seasons' => $seasonItem->id])->select('id')->scalar();
            $teams = RelTeamsRelSeasonsLeagues::find()
                ->where(['fk_rel_seasons_leagues' => $idRelSeasonsLeague])
                ->joinWith('fkTeams as teams')
                ->onCondition(['teams.is_active' => 'yes'])
                ->orderBy(Teams::tableName() . '.title')
                ->all();
        }

        $seasons = Seasons::find()->orderBy(['sort'=>SORT_DESC])->select(['id', 'title'])->all();

        return $this->render('index', compact('teams', 'leagueItem', 'seasonItem', 'leagues', 'seasons' ));
    }

    public function actionView($id){

        $team = RelTeamsRelSeasonsLeagues::find()
            ->where([ RelTeamsRelSeasonsLeagues::tableName().'.id' => $id ])
            ->joinWith('fkTeams as teams')
            ->onCondition(['teams.is_active' => 'yes'])
            ->one();

        if (!$team) {
            throw new \Exception("Команда не найдена");
        }

        $teamStructure = TeamsStructures::find()
            ->where(['fk_rel_teams_rel_seasons_league' => $id])
            ->joinWith([ 'fkMembers as members', 'fkRoles as roles'])
            ->onCondition(['members.is_active' => 'yes', 'roles.is_active' => 'yes'])
            ->orderBy(['fk_roles' => SORT_ASC])
            ->all();

        $partnersRel = $team->fkRelTeamsTeamsPartners;

        return $this->render('view', compact('team', 'teamStructure', 'partnersRel'));

    }
}
