<?php

namespace frontend\modules\teams\controllers;

use common\models\Roles;
use frontend\models\Members;
use frontend\models\RelSeasonsLeagues;
use frontend\models\Teams;
use frontend\models\TeamsStructures;
use frontend\modules\teams\components\BaseTeamsController;
use frontend\models\RelTeamsRelSeasonsLeagues;
use frontend\models\Seasons;
use frontend\models\Leagues;
use frontend\models\RelTeamsTeamsPartners;

class MembersController extends BaseTeamsController
{
    public function actionView($id){

        $member = Members::findOne($id);
        if (!$member) {
            throw new \Exception("Участник не найден");
        }

        return $this->render('view', compact('member'));

    }
}
