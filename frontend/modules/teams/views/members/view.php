<?php
use yii\helpers\Url;

$this->title = 'Участник ' . $member->firstname . ' ' . $member->lastname;
?>

<main>
    <div class="member-top">
        <div class="container">
            <div class="row">
                <div class="member-top_left col-md-3">
                    <img src="<?= $member->getThumbFileUrl('image', 'thumb', 'img/no-user-photo.png') ?>" alt="">
                </div>
                <div class="member-top_right col-md-9">
                    <div class="member-top--name"><?= $member->firstname ?> <?= $member->lastname ?></div>
                    <div class="member-top--subname"><!--Повелитель паруса - Европа, Leviathan--></div>
                </div>
            </div>
        </div>
    </div>
    <div class="member-content">
        <div class="container">
            <div class="row">
                <div class="align-self-center">
                    <?= $member->description ?>
                </div>
            </div>
        </div>
    </div>
    <div class="soc-bottom">
        <a class="social-links__facebook" href="https://www.facebook.com/russiansailingleague/" target="_blank" rel="noopener">
            <i class="fa fa-facebook" aria-hidden="true"></i>
        </a>
        <a class="social-links__instagram" href="https://www.instagram.com/national_sailing_league/" target="_blank" rel="noopener">
            <i class="fa fa-instagram" aria-hidden="true"></i>
        </a>
        <a class="social-links__youtube" href="https://www.youtube.com/channel/UCHGQZUCjdMTm25zU3C78Huw" target="_blank" rel="noopener">
            <i class="fa fa-youtube-play" aria-hidden="true"></i>
        </a>
        <a class="social-links__youtube" href="#" target="_blank" rel="noopener">
            <i class="fa fa-twitter" aria-hidden="true"></i>
        </a>
        <a class="social-links__youtube" href="#" target="_blank" rel="noopener">
            <i class="fa fa-whatsapp" aria-hidden="true"></i>
        </a>
    </div>
</main>
