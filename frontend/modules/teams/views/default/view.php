<?php
use yii\helpers\Url;

$this->title = 'Команда ' . $team->fkTeams->title;

/*if ($team) {
    $this->title .= ' ' . $seasonItem->title;
}

if ($leagueItem) {
    $this->title .= ' ' . $leagueItem->title;
}*/

$this->registerCss("
div.partner_item {
    vertical-align: top;
    display: inline-block;
    text-align: center;
    width: 120px;
}
img.partner_item_img {
    width: 100px;
    height: 100px;
    background-color: grey;
}
.partner_item_caption {
    display: block;
}
");
?>
<main>
    <div class="article-background">
        <div class="team__cover" style="background-image: url('<?= $team->fkTeams->getFileUrl('cover_image'); ?>');">
            <div class="container">
                <div class="team__data">
                    <div class="team__logo">
                        <img class="" src="<?= $team->fkTeams->getThumbFileUrl('image', 'thumb', 'img/no-team-logo_NSL_trans_light.png'); ?>">
                    </div>
                    <div class="team__title">
                        <h1 class="team__name"><span><?= $team->fkTeams->title; ?></span></h1>
                        <div class="team__info">
                            <span class="team__region"><?= $team->fkTeams->city; ?></span>
                            <span class="team__yachtClub"><?= $team->fkTeams->club; ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="dark-background">
        <div class="container">
            <div class="team-list page-wrapper">
                <div class="row">
                    <?php foreach ($teamStructure as $t) : ?>
                        <div class="team-item col-md-2 col-sm-6">
                            <!--<a href="<?= Url::toRoute(['/teams/members/view', 'id' => $t->fkMembers->id ]) ?>">-->
                            <div class="team-img"><img src="<?= $t->fkMembers->getThumbFileUrl('image', 'thumb', 'img/no-user-photo.png') ?>" alt=""></div>
                            <!--</a>-->
                            <div class="team-name"><?= $t->fkMembers->firstname . ' ' .$t->fkMembers->lastname; ?></div>
                            <div class="team-name_subname"><?= $t->fkRoles->title ?></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="team-info">
        <div class="container">
            <div class="page-wrapper">
                <div class="half-wrapper">
                    <?= $team->fkTeams->description; ?>
                    <div class="page-wrapper">
                        <div class="page-title">
                            <a href="#">Партнеры</a>
                        </div>

                        <div class="frontpage__partners">
                            <div class="container">
                                <div class="frontpage__partners-list">
                                    <div class="partners__item">
                                        <div class="partners__wrap">
                                            <?php foreach($partnersRel as $item): ?>
                                                <div class="partners__wrap-item">
                                                    <div class="partners__img"><img src="<?= $item->fkTeamsPartners->getFileUrl() ?>" alt=""></div>
                                                </div>
                                            <?php endforeach ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= frontend\components\widgets\content\socialnetworks\SocialNetworksWidget::widget() ?>

                </div>
            </div>
        </div>
    </div>
</main>