<?php
use yii\helpers\Url;

$this->title = 'Команды';

if ($seasonItem) {
    $this->title .= ' ' . $seasonItem->title;
}

if ($leagueItem) {
    $this->title .= ' ' . $leagueItem->title;
}

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title">
                <a>Команды</a>
                <div class="page-description__subtitle"><?= $seasonItem->title?></div>
            </div>

            <?php if (count($leagues) > 0) : ?>
            <div class="groups-switcher__wrapper">
                <nav class="groups-switcher">
                    <?php foreach ($leagues as $league) : ?>
                        <a class="<?= ($league->id == $leagueItem->id) ? 'active' : ''; ?>"
                           href="<?= Url::toRoute(['/teams', 'season' => $seasonItem->id, 'league' => $league->id ]) ?>">
                            <?= $league->title ?>
                        </a>
                    <?php endforeach; ?>
                </nav>
            </div>
            <?php endif ?>
            <div class="teams teams-cards">
                <div class="row">
                    <?php foreach ($teams as $team) : ?>
                        <div class="team-card col-md-3 col-sm-6">
                            <a class="" href="<?= Url::toRoute(['/teams/default/view', 'id' => $team->id]) ?>">
                                <img class="team-card__logo" src="<?= $team->fkTeams->getThumbFileUrl('image', 'thumb', 'img/no-team-logo_NSL_trans_light.png') ?>">
                                <div class="title-wrapper">
                                    <div class="title"><span><?= $team->fkTeams->title ?></span></div>
                                    <div class="team-card__region"><?= $team->fkTeams->city ?></div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="seasons-archive">
        <span class="seasons-archive__title">Архив:</span>
        <?php foreach ($seasons as $season) : ?>
            <a class="season-archive__card" href="/teams?season=<?= $season->id ?>" ><?= $season->title ?></a>
        <?php endforeach; ?>
    </div>
</main>