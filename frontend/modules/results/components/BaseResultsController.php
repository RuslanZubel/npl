<?php

namespace frontend\modules\results\components;

use frontend\components\BaseController;
use yii\web\Controller;

/**
 * Default controller for the `results` module
 */
class BaseResultsController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
