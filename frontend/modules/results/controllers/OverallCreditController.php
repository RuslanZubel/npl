<?php

namespace frontend\modules\results\controllers;

use common\components\ScoreCounter;
use frontend\models\Seasons;
use frontend\models\Leagues;
use frontend\models\Races;
use frontend\models\RelTeamsRelSeasonsLeagues;
use frontend\models\Stages;
use frontend\models\Stickers;
use frontend\models\RelSeasonsLeagues;
use Yii;
use frontend\models\Scores;
use frontend\modules\results\models\ScoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use richardfan\sortable\SortableAction;
use frontend\modules\results\components\BaseResultsController;

/**
 * ScoresController implements the CRUD actions for Scores model.
 */
class OverallCreditController extends BaseResultsController
{

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => Scores::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Scores models.
     * @return mixed
     */
    public function actionIndex($season = NULL)
    {
        $special15Season = (object) [
            'id' => 'special15',
            'title' => 'Победители сезона 2023',
        ];

        $seasons = Seasons::find()->orderBy(['sort'=>SORT_DESC])->select(['id', 'title'])->all();

        array_unshift($seasons, $special15Season);

        if ($season === 'special15' || is_null($season)) {
            $currentStage = Stages::findOne(102);

            if (is_null($currentStage)) {
                throw new \Exception("Этап не найден");
            }

            $stages[] = $currentStage;

            $stageTeams = RelTeamsRelSeasonsLeagues::find()
                ->where(['fk_rel_seasons_leagues' => $currentStage->fk_rel_seasons_leagues])
                ->joinWith('fkTeams as teams')
                ->onCondition(['teams.is_active' => 'yes'])
                ->all();

            \common\components\ScoreCounter::getScoreData($stageTeams, $stages, Races::className(), Scores::className(), $stageTeamRacesScores, $stageTeamScores, $teamWholeScore, $teamAllScore, $raceRates);

            asort($teamWholeScore);

            return $this->render('special15', [
                'seasons' => $seasons,
                'seasonItem' => $special15Season,
                'teamWholeScore' => $teamWholeScore,
                'stageTeams' => $stageTeams,
            ]);
        }

        if (is_null($season)) {
            $seasonItem = Seasons::find()->orderBy(['sort'=>SORT_DESC])->select(['id', 'title'])->one();
        } else {
            $seasonItem = Seasons::find()->where(['id' => $season])->select(['id', 'title'])->one();
        }

        $teamSeasonWholeScore = ScoreCounter::getSeasonData(
            $seasonItem->id,
            RelSeasonsLeagues::className(),
            RelTeamsRelSeasonsLeagues::className(),
            Stages::className(),
            Races::className(),
            Scores::className(), true);

        return $this->render('index', [
            'seasons' => $seasons,
            'seasonItem' => $seasonItem,
            'teamSeasonWholeScore' => $teamSeasonWholeScore,
        ]);
    }

    /**
     * Displays a single Scores model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
}
