<?php

namespace frontend\modules\results\controllers;

use common\models\Roles;
use frontend\components\BaseController;
use frontend\models\RelSeasonsLeagues;
use frontend\models\Stages;
use frontend\models\Teams;
use frontend\models\TeamsStructures;
use frontend\modules\results\components\BaseResultsController;
use frontend\modules\teams\components\BaseTeamsController;
use frontend\models\RelTeamsRelSeasonsLeagues;
use frontend\models\Seasons;
use frontend\models\Leagues;
use frontend\models\RelTeamsTeamsPartners;
use frontend\models\Races;

/**
 * Default controller for the `teams` module
 */
class TrackingController extends BaseResultsController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex( $season = null, $league = null )
    {
        if (is_null($season)) {
            $seasonItem = Seasons::find()->orderBy(['sort'=>SORT_DESC])->select(['id', 'title'])->one();
        } else {
            $seasonItem = Seasons::find()->where(['id' => $season])->select(['id', 'title'])->one();
        }

        $leagues = Leagues::find()
            ->joinWith('relSeasonsLeagues')
            ->where( [ RelSeasonsLeagues::tableName().'.fk_seasons' => $seasonItem->id ])
            ->select([Leagues::tableName().'.id', Leagues::tableName().'.title'])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $leagueItem = null;

        if (!is_null($league)) {
            $leagueItem = Leagues::findOne($league);
        } else if (count($leagues) > 0) {
            $leagueItem = reset($leagues);
        }

        $stages = array();

        if ($leagueItem) {
            $idRelSeasonsLeague = RelSeasonsLeagues::find()->where(['fk_leagues' => $leagueItem->id, 'fk_seasons' => $seasonItem->id])->select('id')->scalar();
            $stages = Stages::find()
                ->where(['fk_rel_seasons_leagues' => $idRelSeasonsLeague])
                ->orderBy(Stages::tableName() . '.start_date')
                ->all();
        }

        $seasons = Seasons::find()->orderBy(['sort'=>SORT_DESC])->select(['id', 'title'])->all();

        return $this->render('index', compact('seasonItem','seasons', 'stages', 'leagues', 'leagueItem'));
    }

//    public function actionView($id){
//
//        $team = RelTeamsRelSeasonsLeagues::find()
//            ->where([ RelTeamsRelSeasonsLeagues::tableName().'.id' => $id ])
//            ->joinWith('fkTeams')
//            ->one();
//
//        $teamStructure = TeamsStructures::find()
//            ->where(['fk_rel_teams_rel_seasons_league' => $id])
//            ->joinWith([ 'fkMembers', 'fkRoles'])
//            ->orderBy(['fk_roles' => SORT_ASC])
//            ->all();
//
//        $partnersRel = RelTeamsTeamsPartners::find()->where(['fk_rel_teams_rel_seasons_league' => $id])->all();
//
//        return $this->render('view', compact('team', 'teamStructure', 'partnersRel'));
//
//    }
}
