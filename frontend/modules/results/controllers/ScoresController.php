<?php

namespace frontend\modules\results\controllers;

use frontend\models\Seasons;
use frontend\models\Leagues;
use frontend\models\Races;
use frontend\models\RelTeamsRelSeasonsLeagues;
use frontend\models\Stages;
use frontend\models\Stickers;
use frontend\models\RelSeasonsLeagues;
use Yii;
use frontend\models\Scores;
use frontend\modules\results\models\ScoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use richardfan\sortable\SortableAction;
use frontend\modules\results\components\BaseResultsController;

/**
 * ScoresController implements the CRUD actions for Scores model.
 */
class ScoresController extends BaseResultsController
{

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => Scores::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Scores models.
     * @return mixed
     */
    public function actionIndex($season = NULL, $league = NULL)
    {
        if (is_null($season)) {
            $seasonItem = Seasons::find()->orderBy('sort')->select(['id', 'title'])->one();
        } else {
            $seasonItem = Seasons::find()->where(['id' => $season])->select(['id', 'title'])->one();
        }

        $leagues = Leagues::find()
            ->joinWith('relSeasonsLeagues')
            ->where( [ RelSeasonsLeagues::tableName().'.fk_seasons' => $seasonItem->id ])
            ->select([Leagues::tableName().'.id', Leagues::tableName().'.title'])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $leagueItem = null;

        if (!is_null($league)) {
            $leagueItem = Leagues::findOne($league);
        } else if (count($leagues) > 0) {
            $leagueItem = reset($leagues);
        }

        $stages = array();
        $seasonTeams = array();

        if ($leagueItem) {
            $idRelSeasonsLeague = RelSeasonsLeagues::find()->where(['fk_leagues' => $leagueItem->id, 'fk_seasons' => $seasonItem->id])->select('id')->scalar();
            $stages = Stages::find()
                ->where(['fk_rel_seasons_leagues' => $idRelSeasonsLeague])
                ->orderBy(Stages::tableName() . '.start_date')
                ->all();

            $seasonTeams = RelTeamsRelSeasonsLeagues::find()
                ->where(['fk_rel_seasons_leagues' => $idRelSeasonsLeague])
                ->joinWith('fkTeams as teams')
                ->onCondition(['teams.is_active' => 'yes'])
                ->all();
        }

        $seasons = Seasons::find()->select(['id', 'title'])->all();

        \common\components\ScoreCounter::getScoreData($seasonTeams, $stages, Races::className(), Scores::className(), $stageTeamRacesScores, $stageTeamScores, $teamWholeScore, $teamAllScore, $raceRates);

        asort($teamWholeScore);

        return $this->render('index', [
            'seasons' => $seasons,
            'leagues' => $leagues,
            'stages' => $stages,
            'seasonItem' => $seasonItem,
            'leagueItem' => $leagueItem,
            'seasonTeams' => $seasonTeams,
            'stageTeamRacesScores' => $stageTeamRacesScores,
            'stageTeamScores' => $stageTeamScores,
            'teamWholeScore' => $teamWholeScore,
        ]);
    }

    /**
     * Displays a single Scores model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
}
