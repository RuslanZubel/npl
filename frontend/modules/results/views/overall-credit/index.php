<?php

use yii\helpers\Url;

$this->title = 'Общий зачёт';

if ($seasonItem) {
    $this->title .= ' ' . $seasonItem->title;
}

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="container">
                <div class="page-title">
                    <a>Общий зачёт</a>
                    <div class="page-description__subtitle"><?= $seasonItem->title ?></div>
                </div>

                <?php if (count($seasons) > 0) : ?>
                    <div class="groups-switcher__wrapper">
                        <nav class="groups-switcher">
                            <?php foreach ($seasons as $season) : ?>
                                <a class="<?= ($season->id == $seasonItem->id) ? 'active' : ''; ?>"
                                   href="<?= Url::toRoute(['/results/overall-credit', 'season' => $season->id]) ?>">
                                    <?= $season->title ?>
                                </a>
                            <?php endforeach; ?>
                        </nav>
                    </div>
                <?php endif ?>
            </div>

            <div class="overall">
                <div class="container">
                    <div class="overall__table2">
                        <?php if ($teamSeasonWholeScore) : ?>
                            <?php foreach ($teamSeasonWholeScore as $relSeasonsLeagues => $teamSeasonScore) : ?>
                                <?php if ($teamSeasonScore['league']->fkLeagues->id !== 6) : ?>

                                    <div class="overall__group">
                                        <div class="overall__group-title"><a class="overall__group-title"
                                                                             href="<?= Url::toRoute(['/results/scores', 'season' => $seasonItem->id, 'league' => $teamSeasonScore['league']->fkLeagues->id]) ?>"><?= $teamSeasonScore['league']->fkLeagues->title ?></a>
                                        </div>
                                        <ul class="overall__teams">
                                            <?php $pos = 0; ?>
                                            <?php foreach ($teamSeasonScore['scores'] as $teamId => $score) : ?>
                                                <?php
                                                $curTeam = null;
                                                foreach ($teamSeasonScore['teams'] as $team) {
                                                    if ($team->id == $teamId) {
                                                        $curTeam = $team;
                                                        break;
                                                    }
                                                }

                                                $pos++;
                                                ?>
                                                <li class="overall-team">
                                                    <div class="overall-team__standing"><span><?= $pos ?></span></div>
                                                    <div class="overall-team__logo"><img width="64px"
                                                                                         src="<?= $curTeam->fkTeams->getThumbFileUrl('image', 'thumb', 'img/no-team-logo_NSL_trans_light.png') ?>"
                                                                                         alt="$curTeam->fkTeams->title">
                                                    </div>
                                                    <div class="overall-team__name"><a
                                                                href="<?= Url::toRoute(['/teams/default/view', 'id' => $curTeam->id]); ?>"><?= $curTeam->fkTeams->title ?></a>
                                                    </div>
                                                    <div class="overall-team__score">
                                                        <span><?= number_format($score, 3) ?></span></div>
                                                </li>
                                            <?php endforeach ?>
                                        </ul>
                                    </div>

                                <?php endif; ?>

                            <?php endforeach ?>
                        <?php endif ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>

