<?php

use yii\helpers\Url;

$this->title = 'Общий зачёт';

if ($seasonItem) {
    $this->title .= ' ' . $seasonItem->title;
}

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="container">
                <div class="page-title">
                    <a>Общий зачёт</a>
                    <div class="page-description__subtitle"><?= $seasonItem->title ?></div>
                </div>

                <?php if (count($seasons) > 0) : ?>
                    <div class="groups-switcher__wrapper">
                        <nav class="groups-switcher">
                            <?php foreach ($seasons as $season) : ?>
                                <a class="<?= ($season->id == $seasonItem->id) ? 'active' : ''; ?>"
                                   href="<?= Url::toRoute(['/results/overall-credit', 'season' => $season->id]) ?>">
                                    <?= $season->title ?>
                                </a>
                            <?php endforeach; ?>
                        </nav>
                    </div>
                <?php endif ?>
            </div>

            <div class="overall">
                <div class="container">
                    <div class="overall__table2">
                        <?php if ($teamWholeScore) : ?>
                            <div class="overall__group">
                                <div class="overall__group-title"><a class="overall__group-title"
                                                                     href="<?= Url::toRoute(['/results/scores', 'season' => 15, 'league' => 1]) ?>">Высший
                                        дивизион</a>
                                </div>
                                <ul class="overall__teams">
                                    <?php $pos = 0; ?>
                                    <?php foreach ($teamWholeScore as $teamId => $scoreSum) : ?>
                                        <?php
                                        $curTeam = null;
                                        foreach ($stageTeams as $stageTeam) {
                                            if ($stageTeam->id == $teamId) {
                                                $curTeam = $stageTeam;
                                            }
                                        }

                                        $pos++;
                                        ?>
                                        <li class="overall-team">
                                            <div class="overall-team__standing"><span><?= $pos ?></span></div>
                                            <div class="overall-team__logo"><img width="64px"
                                                                                 src="<?= $curTeam->fkTeams->getThumbFileUrl('image', 'thumb', 'img/no-team-logo_NSL_trans_light.png') ?>"
                                                                                 alt="$curTeam->fkTeams->title">
                                            </div>
                                            <div class="overall-team__name"><a
                                                        href="<?= Url::toRoute(['/teams/default/view', 'id' => $curTeam->id]); ?>"><?= $curTeam->fkTeams->title ?></a>
                                            </div>
                                            <div class="overall-team__score">
                                                <span><?= number_format($teamWholeScore[$teamId], 3) ?></span></div>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>

