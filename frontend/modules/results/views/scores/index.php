<?php
use yii\helpers\Url;

$this->title = 'Результаты';

if ($seasonItem) {
    $this->title .= ' ' . $seasonItem->title;
}

if ($leagueItem) {
    $this->title .= ' ' . $leagueItem->title;
}

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="container">
                <div class="page-title">
                    <a>Результаты</a>
                    <div class="page-description__subtitle"><?= $seasonItem->title ?></div>
                </div>

                <?php if (count($leagues) > 0) : ?>
                    <div class="groups-switcher__wrapper">
                        <nav class="groups-switcher">
                            <?php foreach ($leagues as $league) : ?>
                                <a class="<?= ($league->id == $leagueItem->id) ? 'active' : ''; ?>"
                                   href="<?= Url::toRoute(['/results/scores', 'season' => $seasonItem->id, 'league' => $league->id ]) ?>" >
                                    <?= $league->title ?>
                                </a>
                            <?php endforeach; ?>
                        </nav>
                    </div>
                <?php endif ?>
            </div>

            <div class="tournament-table background-gray">
                <div class="container">
                    <div class="page-wrapper">
                        <div class="table-wrap">
                        <table class="table table-striped table-hover table-result">
                            <tr><td>Место</td><td>Команды</td>
                                <?php
                                foreach ($stages as $stage) {
                                    echo "<td>{$stage->title}. Кол-во очков/кол-во гонок</td>";

                                    $usePenaltyColumn = false;
                                    foreach ($seasonTeams as $seasonTeam) {
                                        if ($stageTeamScores[$stage->id][$seasonTeam->id]['penalty'] != 0) {
                                            $usePenaltyColumn = true;
                                            break;
                                        }
                                    }

                                    if ($usePenaltyColumn) {
                                        echo "<td>Штраф</td>";
                                    }

                                    echo "<td>Баллы за {$stage->title}</td>";
                                }

                                $stagesCount = count($stages);

                                echo "<td>Сумма баллов за {$stagesCount} этапа(-ов)</td>";

                                ?>
                            </tr>
                            <?php
                            $index = 0;
                            foreach($teamWholeScore as $teamId => $scoreSum) {
                                $team = null;
                                foreach($seasonTeams as $stageTeam) {
                                    if ($stageTeam->id == $teamId) {
                                        $team = $stageTeam;
                                    }
                                }
                                if ($team == null) {
                                    die("Ошибка формирования данных");
                                }
                                $index++;
                                echo "<tr><td>{$index}</td><td>{$team->getFkTeams()->one()->title}</td>";
                                foreach ($stages as $stage) {
                                    $scoreRaceSum = number_format($stageTeamScores[$stage->id][$team->id]['score'], 3);
                                    $scoreRaceWholeSum = number_format($stageTeamScores[$stage->id][$team->id]['whole_score'], 3);

                                    $scoreDiv = number_format($stageTeamScores[$stage->id][$team->id]['score_div'], 3);

                                    echo "<td style='background-color: #fffff'><div>{$stageTeamScores[$stage->id][$team->id]['score']}/{$stageTeamScores[$stage->id][$team->id]['race_count']} ({$scoreDiv})</div></td>";

                                    $usePenaltyColumn = false;
                                    foreach ($seasonTeams as $seasonTeam) {
                                        if ($stageTeamScores[$stage->id][$seasonTeam->id]['penalty'] != 0) {
                                            $usePenaltyColumn = true;
                                            break;
                                        }
                                    }

                                    if ($usePenaltyColumn) {
                                        echo "<td>";
                                        if ($stageTeamScores[$stage->id][$team->id]['penalty'] != 0) {
                                            echo $stageTeamScores[$stage->id][$team->id]['penalty'];
                                        }
                                        echo "</td>";
                                    }

                                    echo "<td style='background-color: #fffff'><div>{$scoreRaceWholeSum}</div></td>";
                                }

                                $scoreSum = number_format($scoreSum, 3);

                                echo "<td>{$scoreSum}</td> </tr>";
                            }
                            ?>


                        </table>

                    </div>
                 </div>
                </div>
            </div>
        </div>
    </div>
    <div class="seasons-archive">
        <span class="seasons-archive__title">Архив:</span>
        <?php foreach ($seasons as $season): ?>
            <a class="season-archive__card" href="<?= Url::toRoute(['/results/scores', 'season' => $season->id ]) ?>"><?= $season->title ?></a>
        <?php endforeach ?>

    </div>
</main>

