<?php

use yii\helpers\Html;
use yii\helpers\Url;
use richardfan\sortable\SortableGridView;
use yii\widgets\Pjax;
use frontend\models\Races;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\RacesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Трекинг';

if ($seasonItem) {
    $this->title .= ' ' . $seasonItem->title;
}

if ($leagueItem) {
    $this->title .= ' ' . $leagueItem->title;
}
?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title">
                <a>Трекинг</a>
                <div class="page-description__subtitle"><?= $seasonItem->title ?></div>
            </div>

            <?php if (count($leagues) > 0) : ?>
                <div class="groups-switcher__wrapper">
                    <nav class="groups-switcher">
                        <?php foreach ($leagues as $league) : ?>
                            <?php if ($league->id !== 6) : ?>
                                <a class="<?= ($league->id == $leagueItem->id) ? 'active' : ''; ?>"
                                   href="<?= Url::toRoute(['/results/tracking', 'season' => $seasonItem->id, 'league' => $league->id ]) ?>" >
                                    <?= $league->title ?>
                                </a>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </nav>
                </div>
            <?php endif ?>

            <?php foreach ($stages as $stage) : ?>

                <div class="tracking-block" style="background: url('<?= $stage->getFileUrl('image') ?>');">
                    <!-- TODO: адрес дефолтной картинки url('img/frontpage-cover-5.jpg');"   -->
                    <div class="tracking-block--left">
                        <div class="tracking-block--title"><?= $stage->title ?></div>
                        <div class="tracking-block--subtitle"><?= $stage->place ?>
                            , <?= $stage->getStartEndDate() ?></div>
                    </div>
                    <div class="tracking-block--right">
                        <div class="row">
                            <?php
                            $races = Races::find()->where(['fk_stages' => $stage->id])->andWhere("track_url IS NOT NULL AND TRIM(track_url) <> ''")->all();
                            //for ($var = 0; $var < 10; $var++) {
                            foreach ($races as $race) {
                                echo "<div class=\"col-md-20\">
                                <a class=\"tracking-block--link\" href=\"{$race->track_url}\" >
                                    <div class=\"tracking-block--link-name\">{$race->title}</div>
                                    <div class=\"tracking-block--link-info\">
                                        <span  class=\"tracking-block--link-video\"><img src=\"img/link-video.jpg\" alt=\"\"></span>
                                    </div>
                                </a>
                        </div>
                    ";
                            }
                            //}
                            ?></div>
                    </div>
                </div>
            <?php endforeach ?>

        </div>
    </div>
    <div class="seasons-archive">
        <span class="seasons-archive__title">Архив:</span>
        <?php foreach ($seasons as $season) : ?>
            <a class="season-archive__card"
               href="<?= Url::toRoute(['/results/tracking', 'season' => $season->id]) ?>"><?= $season->title ?></a>
        <?php endforeach; ?>
    </div>
</main>