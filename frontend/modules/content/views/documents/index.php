<?php
$this->title = 'Документы';
?>

<main>
	<div class="container">
		<div class="page-wrapper">
			<div class="half-wrapper">
                <?php
                    if ($pageDocuments && count($pageDocuments) > 0) {
                        foreach ($pageDocuments as $pageDocument) {
                            echo "<div class='text-title'>$pageDocument->title</div>";
                            echo $pageDocument->text;
                            echo "<img src='{$pageDocument->getFileUrl('image')}'>";
                        }
                    }
                ?>
			</div>
		</div>
	</div>
    <?= frontend\components\widgets\content\socialnetworks\SocialNetworksWidget::widget() ?>
</main>

