<?php

use frontend\components\widgets\galleries\video\VideoGalleryWidget;

$this->title = 'Этап ' . $currentStage->title;

$this->title .= ' ' . $currentStage->fkRelSeasonsLeagues->fkSeasons->title;
$this->title .= ' ' . $currentStage->fkRelSeasonsLeagues->fkLeagues->title;

$this->registerJS("
    console.log($encoded)
");

?>

<main>
    <div class="event-top" style="background: url('<?= $currentStage->getFileUrl('image') ?>') 50% 50% no-repeat;">
        <div class="container">
            <div class="row">
                <div class="event-top--caption">
                    <div class="event-top--caption_small">
                        <span><?= $currentStage->fkRelSeasonsLeagues->fkSeasons->title ?>, <?= $currentStage->fkRelSeasonsLeagues->fkLeagues->title ?></span><br>
                        <span class="event-top--date"><?= $currentStage->getStartEndDate() ?></span>
                    </div>
                    <div class="event-top--caption_big">
                        <span><?= $currentStage->title ?></span><br>
                        <span class="event-top--place"><?= $currentStage->place ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="event-text">
        <div class="container">
            <div class="row">
                <p class="event-text--caption"><?= $currentStage->description ?></p>
            </div>
        </div>
    </div>
    <?php if ($currentStage->fkRelSeasonsLeagues->fkLeagues->id !== 6) : ?>

        <div class="result">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <span><a href="#">
                                <?php
                                // Specific customer requirements to translate this text for specific stages
                                if (in_array($currentStage->id, [90])) {
                                    echo "Results Table";
                                } else {
                                    echo "Таблица результатов";
                                }
                                ?>

                            </a></span>
                    </div>
                    <table class="table table-striped table-hover table-result">
                        <tr>
                            <?php
                            // Specific customer requirements to translate this columns for specific stages
                            if (in_array($currentStage->id, [90])) {
                                echo "<td>Position</td>";
                                echo "<td>Teams</td>";
                            } else {
                                echo "<td>Место</td>";
                                echo "<td>Команды</td>";
                            }
                            $correctiveRace = null;
                            foreach ($stageRaces as $race) {
                                echo "<td>{$race->title}</td>";
                            }

                            // Specific customer requirements to translate this columns for specific stages
                            if (in_array($currentStage->id, [90])) {
                                echo "<td>Sum</td>";
                                echo "<td>Number of races</td>";
                            } else {
                                echo "<td>Сумма</td>";
                                echo "<td>Кол-во гонок</td>";
                            }

                            // Specific customer requirements for hide this column for specific stages
                            if (!in_array($currentStage->id, [90, 102])) {
                                echo "<td>Балл регаты</td>";
                            }
                            if ($isLastStage && !in_array($currentStage->id, [81, 83, 102])) {
                                echo "<td>Балл регаты X2</td>";
                            }
                            ?>
                        </tr>
                        <?php
                        $index = 0;
                        foreach ($teamWholeScore as $teamId => $scoreSum) {
                            $team = null;
                            foreach ($stageTeams as $stageTeam) {
                                if ($stageTeam->id == $teamId) {
                                    $team = $stageTeam;
                                }
                            }
                            if ($team == null) {
                                die("Ошибка формирования данных");
                            }
                            $index++;
                            echo "<tr><td>{$index}</td><td>{$team->fkTeams->title}</td>";
                            foreach ($stageRaces as $race) {
                                echo "<td style='background-color: {$stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['sticker']}'><div>" . floor($stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['score']) . "</div></td>";
                            }

                            //echo "<td>{$stageTeamScores[$currentStage->id][$teamId]['score']}</td>";
                            $allScorePrint = number_format($teamAllScore[$teamId], 1);
                            echo "<td>{$allScorePrint}</td>";
                            echo "<td>{$stageTeamScores[$currentStage->id][$team->id]['real_race_count']}</td>";
                            $scorePrint = number_format($teamWholeScore[$teamId], 3);
                            $x2ScorePrint = number_format($teamWholeScore[$teamId]*2, 3);

                            // Specific customer requirements for hide this column for specific stages
                            if (!in_array($currentStage->id, [90, 102])) {
                                echo "<td>{$scorePrint}</td>";
                            }
                            if ($isLastStage && !in_array($currentStage->id, [81, 83, 102])) {
                                echo "<td>{$x2ScorePrint}</td>";
                            }

                        }
                        ?>


                    </table>
                </div>
            </div>
        </div>

    <?php endif; ?>

    <?= VideoGalleryWidget::widget(['viewType' => VideoGalleryWidget::VIEW_TYPE_ROW]) ?>

</main>
