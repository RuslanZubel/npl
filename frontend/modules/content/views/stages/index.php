<?php
use yii\helpers\Url;

$this->title = 'Этапы';

if ($seasonItem) {
    $this->title .= ' ' . $seasonItem->title;
}

if ($leagueItem) {
    $this->title .= ' ' . $leagueItem->title;
}
?>

<main>
	<div class="container">
		<div class="page-wrapper">
			<div class="container">
				<div class="page-title">
					<a>Этапы</a>
					<div class="page-description__subtitle"><?= $seasonItem->title ?></div>
				</div>

                <?php if (count($leagues) > 0) : ?>
                <div class="groups-switcher__wrapper">
                    <nav class="groups-switcher">
                        <?php foreach ($leagues as $league) : ?>
                            <a class="<?= ($league->id == $leagueItem->id) ? 'active' : ''; ?>"
                               href="<?= Url::toRoute(['/content/stages', 'season' => $seasonItem->id, 'league' => $league->id ]) ?>" >
                                <?= $league->title ?>
                            </a>
                        <?php endforeach; ?>
                    </nav>
                </div>
                <?php endif ?>
			</div>
			<div class="page-wrapper">
				<div class="container">
					<div class="row">
                        <?php foreach ($stages as $stage) : ?>
						<div class="col-md-4">
							<a href="<?= Url::toRoute(['/content/stages/view', 'id' => $stage->id ]) ?>" class="events-item">
								<span class="events-item--img" style="background: url('<?= $stage->getFileUrl('image') ?>') 50% 50% no-repeat;">
									<span class="events-item--img-caption">
										<span class="events-item--title"><?= $stage->title ?></span>
										<span class="events-item--subtitle"><?= $stage->place ?></span>
										<span class="events-item--date"><?= $stage->getStartEndDate() ?></span>
									</span>
								</span>
							</a>
						</div>
                        <?php endforeach ?>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="seasons-archive">
        <span class="seasons-archive__title">Архив:</span>
        <?php foreach ($seasons as $season): ?>
            <a class="season-archive__card" href="<?= Url::toRoute(['/content/stages', 'season' => $season->id ]) ?>"><?= $season->title ?></a>
        <?php endforeach ?>

    </div>
</main>

