<?php
use yii\helpers\Url;

$this->title = 'Партнёры';

if ($seasonItem) {
    $this->title .= ' ' . $seasonItem->title;
}

if ($leagueItem) {
    $this->title .= ' ' . $leagueItem->title;
}
?>

<main>
	<div class="container">
		<div class="page-wrapper">
			<div class="container">
				<div class="page-title">
					<a>Партнёры</a>
					<div class="page-description__subtitle"><?= $seasonItem->title ?></div>
				</div>

                <?php if (count($leagues) > 0) : ?>
                <div class="groups-switcher__wrapper">
                    <nav class="groups-switcher">
                        <?php foreach ($leagues as $league) : ?>
                            <a class="<?= ($league->id == $leagueItem->id) ? 'active' : ''; ?>"
                               href="<?= Url::toRoute(['/content/partners', 'season' => $seasonItem->id, 'league' => $league->id ]) ?>" >
                                <?= $league->title ?>
                            </a>
                        <?php endforeach; ?>
                    </nav>
                </div>
                <?php endif ?>
			</div>


            <div class="page-wrapper">
                <div class="container">
                    <?php
                    $lastPartnerStatus = 0;
                    ?>
                    <?php foreach($relPartnersPartnersStatuses as $relPartnersPartnersStatus): ?>
                        <?php if ($lastPartnerStatus == 0 || $lastPartnerStatus != $relPartnersPartnersStatus->fk_partners_statuses) : ?>
                            <?php if ($lastPartnerStatus != 0) : ?>
                    </div>
                            <?php endif ?>
                            <?php $lastPartnerStatus = $relPartnersPartnersStatus->fk_partners_statuses; ?>

                    <div class="page-title2"><?= $relPartnersPartnersStatus->fkPartnersStatuses->description ?></div>
                    <div class="row flex-center">

                    <?php endif ?>

                        <div class="col-md-6 align-self-center">
                            <div class="partner-item" onclick="location.href='<?= $relPartnersPartnersStatus->fkPartners->site_url ?>';" style="cursor: pointer;">
                                <div class="partner-item--img"><img src="<?= $relPartnersPartnersStatus->fkPartners->getMainFileUrl() ?>" alt=""></div>
                                <div class="partner-item--text">
                                        <div class="partner-item--text-name"><?= $relPartnersPartnersStatus->fkPartners->title ?></div>
                                    <p><?php
                                        //$text = substr($relPartnersPartnersStatus->fkPartners->text, 0);
                                        $text = $relPartnersPartnersStatus->fkPartners->text;
                                        echo $text;
                                        ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>


                </div>
            </div>
		</div>
	</div>
    <div class="seasons-archive">
        <span class="seasons-archive__title">Архив:</span>
        <?php foreach ($seasons as $season): ?>
            <a class="season-archive__card" href="<?= Url::toRoute(['/content/partners', 'season' => $season->id ]) ?>"><?= $season->title ?></a>
        <?php endforeach ?>

    </div>
</main>

