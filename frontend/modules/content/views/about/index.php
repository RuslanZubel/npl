<?php
$this->title = 'О проекте';
?>

<main>
	<div class="container">
		<div class="page-wrapper">
			<div class="half-wrapper">
                <?php if ($pageAbout) : ?>
				<div class="text-title"><?= $pageAbout->title ?></div>
				<?= $pageAbout->text ?>
                <?php endif ?>
			</div>
		</div>
	</div>
    <?= frontend\components\widgets\content\socialnetworks\SocialNetworksWidget::widget() ?>
</main>