<?php
/**
 * Created by PhpStorm.
 * User: Виктор
 * Date: 10.05.2018
 * Time: 20:18
 */
use yii\helpers\Url;

$this->title = $newsItem->title . ' | ' . 'Новости';
?>


<main>
    <div class="news-top">
        <div class="page-wrapper">
            <div class="container">
                <div class="news-inner--info">
                    <div class="news-inner--date"><?= $newsItem->display_date ?></div>
                    <div class="news-inner--title"><?= $newsItem->title ?></div>
                    <div>
                        <a href="<?=  Url::toRoute(['/content/news/']); ?>">смотреть все новости</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="news-content">
        <div class="container">
            <div class="news-inner--block">
                <div class="news-inner--text">
                    <?= $newsItem->text ?>

                </div>
            </div>
        </div>
        <div class="news-inner--info">

            <p><a href="<?=  Url::toRoute(['/content/news/']); ?>">вернуться к списку новостей</a></p>
        </div>

    <?= frontend\components\widgets\content\socialnetworks\SocialNetworksWidget::widget() ?>
</main>