<?php
use frontend\components\widgets\content\news\NewsListWidget;

$this->title = 'Новости';

?>

<main>
    <?= NewsListWidget::widget(['viewType' => NewsListWidget::VIEW_TYPE_LIST  ]) ?>
</main>
