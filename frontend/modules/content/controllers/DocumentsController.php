<?php
namespace frontend\modules\content\controllers;
use frontend\modules\content\components\BaseContentController;
use frontend\models\Pages;
use frontend\traits\BaseFrontendTrait;

class DocumentsController extends BaseContentController
{
	use BaseFrontendTrait;
//	public $enableTwoColumnLayout = true;
//	public $sidebarName = 'static_page_sidebar';
//	public $widgetsOptions = [
//		'reportsList' => ['limit' => 3],
//	];
//
	public function actionIndex()
	{
		//$pageDocuments = Pages::getPageByType(Pages::TYPE_PAGE_DOCUMENTS);
        $pageDocuments = Pages::find()->where(['type' => 'document_root'])->all();
		return $this->render('index', compact('pageDocuments'));
	}
}