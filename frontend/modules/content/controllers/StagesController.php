<?php

namespace frontend\modules\content\controllers;

use frontend\models\RelTeamsRelSeasonsLeagues;
use frontend\models\Stages;
use frontend\models\Races;
use frontend\models\Scores;
use frontend\models\Seasons;
use frontend\models\Leagues;
use frontend\modules\content\components\BaseContentController;
use frontend\models\RelSeasonsLeagues;
use yii\base\ErrorException;

class StagesController extends BaseContentController
{
    public function actionIndex($season = NULL, $league = NULL)
    {

        if (is_null($season)) {
            $seasonItem = Seasons::find()->orderBy(['sort' => SORT_DESC])->select(['id', 'title'])->one();
        } else {
            $seasonItem = Seasons::find()->where(['id' => $season])->select(['id', 'title'])->one();
        }

        if (is_null($seasonItem)) {
            throw new \Exception("Сезон не найден");
        }

        $leagues = Leagues::find()
            ->joinWith('relSeasonsLeagues')
            ->where([RelSeasonsLeagues::tableName() . '.fk_seasons' => $seasonItem->id])
            ->select([Leagues::tableName() . '.id', Leagues::tableName() . '.title'])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $leagueItem = null;

        if (!is_null($league)) {
            $leagueItem = Leagues::findOne($league);
        } else if (count($leagues) > 0) {
            $leagueItem = reset($leagues);
        }

        if (is_null($leagueItem)) {
            throw new \Exception("Лига не найдена");
        }

        $stages = array();

        if ($leagueItem) {
            $idRelSeasonsLeague = RelSeasonsLeagues::find()->where(['fk_leagues' => $leagueItem->id, 'fk_seasons' => $seasonItem->id])->select('id')->scalar();
            $stages = Stages::find()
                ->where(['fk_rel_seasons_leagues' => $idRelSeasonsLeague])
                ->orderBy(Stages::tableName() . '.start_date')
                ->all();
        }

        $seasons = Seasons::find()->select(['id', 'title'])->orderBy(['sort' => SORT_DESC])->all();

        return $this->render('index', compact('seasonItem', 'seasons', 'stages', 'leagues', 'leagueItem'));

    }

    public function actionView($id)
    {

        $currentStage = Stages::findOne($id);
        if (is_null($currentStage)) {
            throw new \Exception("Этап не найден");
        }

        $stages[] = $currentStage;

        $stageTeams = RelTeamsRelSeasonsLeagues::find()
            ->where(['fk_rel_seasons_leagues' => $currentStage->fk_rel_seasons_leagues])
            ->joinWith('fkTeams as teams')
            ->onCondition(['teams.is_active' => 'yes'])
            ->all();

        \common\components\ScoreCounter::getScoreData($stageTeams, $stages, Races::className(), Scores::className(), $stageTeamRacesScores, $stageTeamScores, $teamWholeScore, $teamAllScore, $raceRates);

        $stageRaces = Races::find()->where(['fk_stages' => $id])->orderBy(['sort' => SORT_ASC])->all();

        asort($teamWholeScore);

        $isLastStage = false;
        $allStages = Stages::find()
            ->where(['fk_rel_seasons_leagues' => $currentStage->fk_rel_seasons_leagues])
            ->orderBy(Stages::tableName() . '.start_date')
            ->all();
        if (
            end($allStages)->id === $currentStage->id
            && count($allStages) > 1
            && !($currentStage->fkRelSeasonsLeagues->fkSeasons->id === 10 && $currentStage->fkRelSeasonsLeagues->fkLeagues->id === 5)
        ) {
            $isLastStage = true;
        }
        return $this->render('view', [
            'currentStage' => $currentStage,
            'stageRaces' => $stageRaces,
            'stageTeams' => $stageTeams,
            'stageTeamRacesScores' => $stageTeamRacesScores,
            'stageTeamScores' => $stageTeamScores,
            'teamWholeScore' => $teamWholeScore,
            'teamAllScore' => $teamAllScore,
            'encoded' => json_encode($teamWholeScore),
            'isLastStage' => $isLastStage
        ]);
    }
}
