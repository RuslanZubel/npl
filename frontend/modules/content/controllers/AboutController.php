<?php
namespace frontend\modules\content\controllers;
use frontend\modules\content\components\BaseContentController;
use frontend\models\Pages;
use frontend\traits\BaseFrontendTrait;

class AboutController extends BaseContentController
{
	use BaseFrontendTrait;
//	public $enableTwoColumnLayout = true;
//	public $sidebarName = 'static_page_sidebar';
//	public $widgetsOptions = [
//		'reportsList' => ['limit' => 3],
//	];
//
	public function actionIndex()
	{
		//$pageAbout = Pages::getPageByType(Pages::TYPE_PAGE_ABOUT);
        $pageAbout = Pages::find()->where(['type' => 'about'])->one();
		return $this->render('index', compact('pageAbout'));
	}
}