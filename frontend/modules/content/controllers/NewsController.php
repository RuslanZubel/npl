<?php

namespace frontend\modules\content\controllers;

use frontend\models\News;
use frontend\modules\content\components\BaseContentController;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class NewsController extends BaseContentController
{
	public function actionIndex(){
		return $this->render('index');
	}
	
	public function actionView($id) {
		
	//	$this->enableTwoColumnLayout = true;
		
		$newsItem = News::find()->where( ['id' => $id] )->one();
		
		if (is_null($newsItem)) {
			throw new NotFoundHttpException();
		}
		
		return $this->render('view', compact('newsItem'));
	}
}
