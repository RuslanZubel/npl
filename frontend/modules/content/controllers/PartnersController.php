<?php

namespace frontend\modules\content\controllers;

use frontend\models\RelTeamsRelSeasonsLeagues;
use frontend\models\Stages;
use frontend\models\Races;
use frontend\models\Scores;
use frontend\models\Seasons;
use frontend\models\Leagues;
use frontend\modules\content\components\BaseContentController;
use frontend\models\RelSeasonsLeagues;

class PartnersController extends BaseContentController
{
    public function actionIndex($season=NULL, $league=NULL){

        if (is_null($season)) {
            $seasonItem = Seasons::find()->orderBy(['sort'=>SORT_DESC])->select(['id', 'title'])->one();
        } else {
            $seasonItem = Seasons::find()->where(['id' => $season])->select(['id', 'title'])->one();
        }

        if (is_null($seasonItem)) {
            throw new \Exception("Сезон не найден");
        }

        $leagues = Leagues::find()
            ->joinWith('relSeasonsLeagues')
            ->where( [ RelSeasonsLeagues::tableName().'.fk_seasons' => $seasonItem->id ])
            ->select([Leagues::tableName().'.id', Leagues::tableName().'.title'])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $leagueItem = null;

        if (!is_null($league)) {
            $leagueItem = Leagues::findOne($league);
        } else if (count($leagues) > 0) {
            $leagueItem = reset($leagues);
        }

        if (is_null($leagueItem)) {
            throw new \Exception("Лига не найдена");
        }

        $relPartnersPartnersStatuses = array();

        if ($seasonItem && $leagueItem) {
            //получение последней пары сезон-лига
            $relSeasonLeague = RelSeasonsLeagues::find()->where(['fk_leagues' => $leagueItem->id, 'fk_seasons' => $seasonItem->id])->one();
            if ($relSeasonLeague) {
                $relPartnersPartnersStatuses = $relSeasonLeague->fkPartnersPartnersStatuses;
            }
        }

        $seasons = Seasons::find()->orderBy(['sort'=>SORT_DESC])->select(['id', 'title'])->all();

        return $this->render('index', compact('seasonItem','seasons', 'relPartnersPartnersStatuses', 'leagues', 'leagueItem'));

    }
}
