<footer class="footer background--nsl-sq">
    <div class="container">
        <div class="footer__content">
            <div class="footer__logo"><img src="img/footer-logo.svg" title="Национальная парусная лига">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-right" style="font: 100 .9rem/1.2 Open Sans, sans-serif; color: rgba(232,233,237,0.99);">
                <p>Контактная информация</p>
                <strong>ООО "НАЦИОНАЛЬНАЯ ПАРУСНАЯ ЛИГА"</strong><br>
                <address style="margin-bottom: 0 !important;">
                    <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                        <span itemprop="addressLocality">г. Москва</span>,
                        <span itemprop="streetAddress">119072, Берсеневская набережная, д.8, стр.1</span><br>
                        <abbr title="phone">Контактный телефон:</abbr>
                        <span itemprop="telephone">8 (916) 780-61-52</span><br>
                    </span>
                    <abbr title="Email">Электронная почта:</abbr>
                    <a href="mailto:info@rusnsl.com" itemprop="email" style="color: rgba(86,202,237,0.99);">info@rusnsl.com</a>
                </address>
            </div>

            <div class="footer__social-links">
                <div class="footer__social-links--title"><span>NSL в соцсетях:</span></div>
                <a class="social-links__facebook" href="https://www.facebook.com/russiansailingleague/" target="_blank" rel="noopener">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                <a class="social-links__instagram" href="https://www.instagram.com/national_sailing_league/" target="_blank" rel="noopener">
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                </a>
                <a class="social-links__youtube" href="https://www.youtube.com/channel/UCHGQZUCjdMTm25zU3C78Huw" target="_blank" rel="noopener">
                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <div class="footer__copyrights">
            <div class="footer__org">
                <a href="//rusyf.ru">© Национальная парусная Лига, 2019</a>
            </div>
        </div>
    </div>
</footer>