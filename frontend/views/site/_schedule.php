<?php

use yii\helpers\Url;

$this->title = 'Этапы';

if ($seasonItem) {
    $this->title .= ' ' . $seasonItem->title;
}

?>

<main>
    <div class="container">
        <div class="page-wrapper" style="padding-bottom: 0 !important;">
            <div class="container">
                <div class="page-title">
                    <a>Расписание этапов</a>
                    <div class="page-description__subtitle"><?= $seasonItem->title ?></div>
                </div>
            </div>
            <div class="overall">
                <div class="container">
                    <div class="overall__table2" style="overflow-x: auto">
                        <?php foreach ($leagues as $league) : ?>
                            <div class="overall__group">
                                <div class="overall__group-title" style="min-height: 95px">
                                    <a  href="/content/stages?season=<?=$seasonItem->id?>&league=<?= $league->id?>"
                                            class="overall__group-title"
                                    ><?= $league->title ?></a>
                                </div>


                                <ul class="overall__teams">
                                    <?php $pos = 0; ?>
                                    <?php foreach ($stages as $stage) : ?>

                                        <?php if ($stage->getFkRelSeasonsLeagues()->all()[0]->fk_leagues === $league->id) : ?>
                                            <li class="overall-stage" >
                                                <div style="border-radius: 5px 5px 5px; margin: 3px; border: 1px solid #1d2851;">
                                                    <a style="height: 1rem !important; min-height: 1rem !important; padding-top: 1rem" class="overall-team__score" href="/content/stages/view?id=<?=$stage->id?>">
                                                        <span><?= $stage->title ?></span>
                                                    </a>
                                                    <div class="overall-team__name" style="text-align: center">
                                                        <a href="/content/stages/view?id=<?=$stage->id?>"><?= $stage->getStartEndDate() ?></a>
                                                    </div>
                                                    <div class="overall-team__score">
                                                        <span><?= $stage->place ?></span>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </ul>


                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>


        </div>
    </div>
</main>

