<?php

/* @var $this yii\web\View */

use frontend\components\widgets\content\schedule\ScheduleWidget;
use yii\helpers\Url;
use frontend\components\widgets\content\news\NewsListWidget;
use frontend\components\widgets\galleries\video\VideoGalleryWidget;
use frontend\components\widgets\galleries\image\ImageGalleryWidget;

$this->title = 'Главная страница';

$randomCoverId = rand(1, 7);
?>
<main>
    <div class="frontpage-cover__holder">
        <div class="frontpage-cover"
             style="background-image: url('img/cover/frontpage-cover-<?= $randomCoverId ?>.jpg');">
            <div class="frontpage-cover__block">
                <img class="frontpage-cover__sails" src="img/fp-sails.png">
                <div class="frontpage-cover__text">
                    <div class="frontpage-cover__nsl">Национальная парусная Лига</div>
                    <div class="frontpage-cover__desc">Самый крупный парусный проект России. 50 команд, более 600
                        участников, три дивизиона
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('./_schedule', [
        'seasonItem' => $seasonItem,
        'seasons' => $seasons,
        'stages' => $stages,
        'leagues' => $leagues,
    ]); ?>


    <?= NewsListWidget::widget(['viewType' => NewsListWidget::VIEW_TYPE_ROW]) ?>

    <div class="overall">
        <div class="container">
            <div class="page-title"><a
                        href="<?= Url::toRoute(['/results/overall-credit', 'season' => $seasonItem->id]) ?>">Общий зачет</a></div>
            <div class="overall__table ">
                <?php foreach ($teamSeasonWholeScore as $relSeasonsLeagues => $teamSeasonScore) : ?>
                    <?php if ($teamSeasonScore['league']->fkLeagues->id !== 6) : ?>
                        <div class="overall__group">
                            <div class="overall__group-title"><a class="overall__group-title"
                                                                 href="<?= Url::toRoute(['/results/scores', 'season' => $seasonItem->id, 'league' => $teamSeasonScore['league']->fkLeagues->id]) ?>"><?= $teamSeasonScore['league']->fkLeagues->title ?></a>
                            </div>
                            <ul class="overall__teams">
                                <?php $pos = 0; ?>
                                <?php foreach ($teamSeasonScore['scores'] as $teamId => $score) : ?>
                                    <?php
                                    $curTeam = null;
                                    foreach ($teamSeasonScore['teams'] as $team) {
                                        if ($team->id == $teamId) {
                                            $curTeam = $team;
                                            break;
                                        }
                                    }

                                    $pos++;
                                    ?>
                                    <li class="overall-team">
                                        <div class="overall-team__standing"><span><?= $pos ?></span></div>
                                        <div class="overall-team__logo"><img width="64px"
                                                                             src="<?= $curTeam->fkTeams->getThumbFileUrl('image', 'thumb', 'img/no-team-logo_NSL_trans_light.png') ?>"
                                                                             alt="$curTeam->fkTeams->title"></div>
                                        <div class="overall-team__name"><a
                                                    href="<?= Url::toRoute(['teams/default/view', 'id' => $curTeam->id]); ?>"><?= $curTeam->fkTeams->title ?></a>
                                        </div>
                                        <div class="overall-team__score"><span><?= number_format($score, 3) ?></span>
                                        </div>
                                    </li>
                                    <?php if ($pos > 3) {
                                        break;
                                    } ?>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                <?php endforeach ?>
            </div>
            <div class="overall__height-switcher"><a class="button"
                                                     href="<?= Url::toRoute(['/results/overall-credit', 'season' => $seasonItem->id]) ?>">Показать
                    все</a></div>
        </div>
    </div>


    <?= ImageGalleryWidget::widget(['viewType' => ImageGalleryWidget::VIEW_TYPE_ROW]) ?>

    <?= VideoGalleryWidget::widget(['viewType' => VideoGalleryWidget::VIEW_TYPE_ROW]) ?>

    <div class="frontpage__partners">
        <div class="container">
            <div class="frontpage__partners-list">
                <?php
                $lastPartnerStatus = 0;
                ?>
                <?php foreach ($relPartnersPartnersStatuses

                as $relPartnersPartnersStatus): ?>
                <?php if ($lastPartnerStatus == 0 || $lastPartnerStatus != $relPartnersPartnersStatus->fk_partners_statuses) : ?>
                <?php if ($lastPartnerStatus != 0) : ?>
            </div>
        </div>
        <?php endif ?>
        <?php $lastPartnerStatus = $relPartnersPartnersStatus->fk_partners_statuses; ?>

        <div class="partners__item"
        >
            <div class="page-title page-title__partners">
                <span><a><?= $relPartnersPartnersStatus->fkPartnersStatuses->description ?></a></span>
            </div>
            <div class="partners__wrap">
                <?php endif ?>

                <div class="partners__wrap-item">
                    <div class="partners__img"
                         onclick="location.href='<?= $relPartnersPartnersStatus->fkPartners->site_url ?>';"
                         style="cursor: pointer;"
                    ><img src="<?= $relPartnersPartnersStatus->fkPartners->getMainFileUrl() ?>" alt=""></div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</main>
