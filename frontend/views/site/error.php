<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Ошибка';
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($message) : ?>
        <div class="alert alert-danger">
            <?= nl2br(Html::encode($message)) ?>
        </div>
    <?php endif ?>

    <?php if ($exception) : ?>
        <p>Сообщение: <strong><?= $exception->getMessage() ?></strong></p>
    <?php endif ?>

    <?php if (defined('YII_DEBUG') && constant('YII_DEBUG')) : ?>
        <div class="alert alert-danger">
            <?= $exception->getMessage() ?>
            <?= nl2br(Html::encode($exception)) ?>
        </div>
    <?php endif ?>

</div>
