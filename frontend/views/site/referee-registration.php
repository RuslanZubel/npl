<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$formattedStages = ArrayHelper::map($stages, 'id', 'title');
?>

<div class="container center-block text-center mt-5">
    <div class="span12 align-content-center">
        <div class="site-signup align-content-center">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>Пожалуйста, заполните следующие поля для регистрации на этап в качестве судьи:</p>

            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                        <?php $form = ActiveForm::begin(['id' => 'referee-form']); ?>

                        <?= $form->field($model, 'name')?>
                        <?= $form->field($model, 'contact_info') ?>
                        <?= $form->field($model, 'stage_id')->checkboxList($formattedStages,[
                            'itemOptions' => [
                                'labelOptions' => ['class' => 'col-md-12']
                            ]
                        ]) ?>

                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

