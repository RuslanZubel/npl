<?php

namespace frontend\controllers;

use common\components\ScoreCounter;
use common\models\Referee;
use common\models\RelPartnersPartnersStatuses;
use frontend\components\BaseController;
use frontend\models\Leagues;
use frontend\models\Races;
use frontend\models\RelSeasonsLeagues;
use frontend\models\RelTeamsRelSeasonsLeagues;
use frontend\models\Scores;
use frontend\models\Seasons;
use frontend\models\Stages;
use Yii;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            /*'error' => [
                'class' => 'yii\web\ErrorAction',
            ],*/
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionError()
    {
        /*$app = Yii::$app;
        print_r($app->errorHandler->error);
        if( $error = $app->errorHandler->error->code )
        {
            if( $app->request->isAjaxRequest )
                echo $error['message'];
            else
                $this->render( 'error' . ( $this->getViewFile( 'error' . $error ) ? $error : '' ), $error );
        }*/

        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }

        return $this->render('error', [
            'message' => 'Что-то пошло не так',
            'name' => 'Ошибка',
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $relPartnersPartnersStatuses = array();

        $seasons = Seasons::find()->orderBy(['sort' => SORT_DESC])->select(['id', 'title'])->all();

        $seasonItem = $seasons[0];

        $leagues = Leagues::find()
            ->joinWith('relSeasonsLeagues')
            ->where([RelSeasonsLeagues::tableName() . '.fk_seasons' => $seasonItem->id])
            ->select([Leagues::tableName() . '.id', Leagues::tableName() . '.title'])
            ->orderBy(['sort' => SORT_DESC])
            ->all();

        $leagueItem = reset($leagues);

        if ($seasonItem && $leagueItem) {
            //получение последней пары сезон-лига
            $relSeasonLeague = RelSeasonsLeagues::find()->where(['fk_leagues' => 1, 'fk_seasons' => $seasonItem->id])->one();
//            throw new ErrorException(json_encode((array)$relSeasonLeague));
            if ($relSeasonLeague) {
                $relPartnersPartnersStatuses = $relSeasonLeague->fkPartnersPartnersStatuses;
            }
        }


        if (count($relPartnersPartnersStatuses) === 0) {
//            $leagueItem = $leagues[1];
            $seasonItem = $seasons[1];
            if ($seasonItem && $leagueItem) {
                //получение последней пары сезон-лига
                $relSeasonLeague = RelSeasonsLeagues::find()->where(['fk_leagues' => 1, 'fk_seasons' => $seasonItem->id])->one();
//            throw new ErrorException(json_encode((array)$relSeasonLeague));
                if ($relSeasonLeague) {
                    $relPartnersPartnersStatuses = $relSeasonLeague->fkPartnersPartnersStatuses;
                }
            }

        }

        $teamSeasonWholeScore = ScoreCounter::getSeasonData(
            $seasonItem->id,
            RelSeasonsLeagues::className(),
            RelTeamsRelSeasonsLeagues::className(),
            Stages::className(),
            Races::className(),
            Scores::className(), true);

        return $this->render('index', array_merge(
            [
                'relPartnersPartnersStatuses' => $relPartnersPartnersStatuses,
                'teamSeasonWholeScore' => $teamSeasonWholeScore,
                'seasonItem' => $seasonItem,
            ],
            $this->getSchedule()
        ));
    }

    private function getSchedule()
    {
        $seasonItem = Seasons::find()->orderBy(['sort' => SORT_DESC])->select(['id', 'title'])->one();

        if (is_null($seasonItem)) {
            throw new \Exception("Сезон не найден");
        }

        $leagues = Leagues::find()
            ->joinWith('relSeasonsLeagues')
            ->where([RelSeasonsLeagues::tableName() . '.fk_seasons' => $seasonItem->id])
            ->select([Leagues::tableName() . '.id', Leagues::tableName() . '.title'])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $stages = Stages::find()
            ->where(
                'fk_rel_seasons_leagues IN (' . implode(', ', array_map(function ($league) {
                    return $league->getRelSeasonsLeagues()->orderBy(['created_at' => SORT_DESC])->one()->id;
                }, $leagues)) . ')'
            )
            ->orderBy(Stages::tableName() . '.start_date')
            ->all();

        $seasons = Seasons::find()->select(['id', 'title'])->orderBy(['sort' => SORT_DESC])->all();

        return array(
            'seasonItem' => $seasonItem,
            'seasons' => $seasons,
            'stages' => $stages,
            'leagues' => $leagues,
        );
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/personal');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionRegisterReferee($leagueId, $seasonId) {
        $model = new Referee();

        if ($req = Yii::$app->request->post()) {
            $referee = $req['Referee'];
            foreach ($referee['stage_id'] as $stage_id) {
                $newModel = new Referee();
                $newModel->name = $referee['name'];
                $newModel->stage_id = $stage_id;
                $newModel->contact_info = $referee['contact_info'];
                $newModel->save();
            }
            return $this->redirect('/');
        }
        $league = Leagues::findOne($leagueId);
        $relSeasonsLeagues = RelSeasonsLeagues::find()->where([
            'fk_seasons'=>$seasonId,
            'fk_leagues'=>$leagueId
        ])
            ->one();
        $stages = Stages::find()->where([
            'fk_rel_seasons_leagues' => $relSeasonsLeagues->id
        ])->all();
        foreach ($stages as $stage) {
            $stage->title = $league->title . ' ' .
                $stage->title . ' (' . $stage->getStartEndDate() . ' )';
        }
        return $this->render('referee-registration',
            [
                'stages' => $stages,
                'model' => $model
            ]);
    }
}
