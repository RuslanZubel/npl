<?php

use yii\helpers\Html;
use yii\helpers\Url;

$menuItems = [];


$menuItems[] = [ 'label' => 'Этапы', 'url' => Url::toRoute('/content/stages')];
$menuItems[] = ['label' => 'Общий зачёт', 'url' => Url::toRoute('/results/overall-credit')];
$menuItems[] = ['label' => 'Трекинг', 'url' => Url::toRoute('/results/tracking')];
$menuItems[] = ['label' => 'Команды', 'url' => Url::to('/teams')];

//$menuItems[] = [ 'label' => 'Результаты', 'url' => Url::toRoute('/results/scores')];
$menuItems[] = ['label' => 'Новости', 'url' => Url::toRoute('/content/news')];
$menuItems[] = ['label' => 'Фото', 'url' => Url::toRoute('/mediafolder/site-galleries')];
$menuItems[] = ['label' => 'Видео', 'url' => Url::toRoute('/mediafolder/site-videos')];
$menuItems[] = ['label' => 'Партнёры', 'url' => Url::toRoute('/content/partners')];
$menuItems[] = ['label' => 'О проекте', 'url' => Url::toRoute('/content/about')];
//$menuItems[] = [ 'label' => 'Документы', 'url' => Url::toRoute('/content/documents')];

if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Кабинет команды', 'url' => Url::toRoute('/site/login')];
} else {
    $menuItems[] = ['label' => 'Кабинет Команды', 'url' => Url::toRoute('/personal')];
}


?>

<div id="header">
    <div class="container">
        <div class="row">
            <div class="logo col-md-2 col-sm-6 col-6"><a href="/"><img src="img/logo.svg" alt=""></a></div>
            <nav class="nav col-md-10 col-sm-6 col-6">
                <div class="header-menu row">

                    <?php
                    $curRoute = \Yii::$app->controller->route;
                    if ($curRoute[0] !== '/') {
                        $curRoute = '/' . $curRoute;
                    }
                    $curRoute = explode('/', $curRoute);
                    foreach ($menuItems as $menuItem) {
                        $menuRoute = explode('/', $menuItem['url']);
                        echo '<a class="nav-link';

                        $isActive = true;
                        for ($i = 0; $i < count($menuRoute); $i++) {
                            if (count($menuRoute) > $i && count($curRoute) > $i) {
                                if ($menuRoute[$i] != $curRoute[$i]) {
                                    $isActive = false;
                                }
                            }
                        }

                        if ($isActive) {
                            echo ' nav-link-active';
                        }
                        echo '" href="' . $menuItem['url'] . '">' . $menuItem['label'] . '</a>';
                    }
                    ?>
                    <?php
                        if (!Yii::$app->user->isGuest) {
                            echo '<div class="nav-link" style="width: 20px !important; position: relative">'.
                        Html::beginForm(['/site/logout'], 'post', ['class' => 'ps'])
                        . Html::submitButton(
                            '',
                            ['class' => 'fa fa-sign-out buttonSubmit']
                        )
                        . Html::endForm()
                        . '</div>';
                        }
                    ?>


                </div>
                <a href="javascript:void(0)" class="mob-menu" id="nav-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </nav>
        </div>
    </div>
</div>

<style type="text/css">

    .buttonSubmit {
        height: 18px;
        width: 18px;
        text-transform: capitalize;
        color: white;
        border: none;
        background: none;
    }

    .ps{
        position: absolute;
        left: 5px;
    }

    button:focus {
        outline: none;
    }

</style>