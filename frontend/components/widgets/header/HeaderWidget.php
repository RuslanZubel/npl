<?php

namespace frontend\components\widgets\header;

/**
 * Created by PhpStorm.
 * User: amio
 * Date: 28/06/2017
 * Time: 11:00
 */
class HeaderWidget extends \yii\base\Widget
{
    public function init(){
        return parent::init();
    }

    public function run() {
        return $this->render('header');
    }

}