<?php
use yii\helpers\Url;
use frontend\components\widgets\galleries\video\VideoGalleryWidget;
?>

<div class="media-card video-card">
    <div class="media-card__in">
        <div class="video-card__cover">
            <?php
                $video = $model->video_id;
                $video = VideoGalleryWidget::nslReplace($video, 'width=', '100%');
                $video = VideoGalleryWidget::nslReplace($video, 'height=', '100%');
                echo $video;
            ?>
        </div>
        <a href="<?=  Url::toRoute(['/mediafolder/site-videos/view', 'id' => $model->id]); ?>" class="title-wrapper">
            <span class="title"><?= $model->title ?></span>
        </a>
        <div class="date-wrapper "><span class="date"><?= $model->custom_date ?></span></div>
    </div>
</div>
