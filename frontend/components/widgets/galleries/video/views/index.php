<?php

use yii\widgets\ListView;

?>
<div class="videos frontpage-videos dark-background">
    <div class="container">
        <div class="page-title dark"><a href="/mediafolder/site-videos"><?= $viewTitle ?></a></div>
        <div class="media-cards">
            <?php echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'layout' => "\n{items}\n{pager}",
                'itemOptions' => [
                    'tag' => false,
                ],
                'id' => false,
                'options' => [
                    'tag' => false,
                ],
            ]); ?>
        </div>
    </div>
</div>