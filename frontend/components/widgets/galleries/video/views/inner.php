<?php

use yii\widgets\ListView;

?>
<div class="dark-background">
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title white"><a href="/mediafolder/site-videos"><?= $viewTitle ?></a></div>
        </div>
        <div class="media-cards inner-video">
            <?php echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'layout' => "\n<div class=\"media-cards-wrapper\">{items}</div>\n{pager}",
                'itemOptions' => [
                    'tag' => false,
                ],
                'id' => false,
                'options' => [
                    'tag' => false,
                ],
            ]); ?>
        </div>
    </div>
</div>