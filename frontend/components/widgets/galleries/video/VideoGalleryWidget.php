<?php

namespace frontend\components\widgets\galleries\video;
use frontend\models\SiteVideos;
use yii\data\ActiveDataProvider;

/**
 * Created by PhpStorm.
 * User: amio
 * Date: 21/07/2017
 * Time: 10:10
 */
class VideoGalleryWidget extends \yii\base\Widget
{
	const VIEW_TYPE_ROW = 'row';
	const VIEW_TYPE_LIST = 'list';
	
	public $rowItemsCount = 3;
	public $listItemsCount = 9;
	public $viewTitle = 'Видео';
	public $viewType = self::VIEW_TYPE_LIST;
	
	private $_dataProvider = NULL;
	
	public function init()
	{
		$query = SiteVideos::find();
		$query->orderBy(['custom_date' => SORT_DESC]);
		
		if ( $this->viewType == self::VIEW_TYPE_ROW ) {
			$query->andWhere(['show_on_index' => 'yes']);
			$query->limit($this->rowItemsCount);
		}
		
		$this->_dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' =>  $this->viewType == self::VIEW_TYPE_ROW ? false : [ 'pageSize' => $this->listItemsCount ],
		]);
		
		return parent::init();
		
	}
	
	public function run()
	{
        if ($this->viewType == self::VIEW_TYPE_ROW) {
            return $this->render('index', ['dataProvider' => $this->_dataProvider, 'viewTitle' => $this->viewTitle]);
        } else {
            return $this->render('inner', ['dataProvider' => $this->_dataProvider, 'viewTitle' => $this->viewTitle]);
        }
	}

	public static function nslReplace($str, $word, $inner) {
        $width_pos = strpos($str, $word);
        if ($width_pos !== false) {
            $width_pos += strlen($word);
            $start_pos = $width_pos;
            $first_char = $str[$width_pos];
            $width_pos++;
            while ($str[$width_pos] != $first_char) {
                $width_pos++;
            }
            $str = substr($str, 0, $start_pos) . $inner . substr($str, $width_pos + 1);
        }
        return $str;
    }
	
}