<?php

use yii\helpers\Url;
use common\models\queries\SiteGalleriesImagesQuery;

?>

<div class="media-card gallery-card">
					<a href="<?=  Url::toRoute(['/mediafolder/site-galleries/view', 'id' => $model->id]); ?>">   <!-- TODO: подцепить данные во вьюхе -->
                        <div class="gallery-card__cover" style="background-image: url('<?php
                        $image = SiteGalleriesImagesQuery::getCover($model->id);
                        if ($image) {
                            echo $image->getFileUrl('image');
                        } else {
                            $image = SiteGalleriesImagesQuery::getFirstImage($model->id);
                            if ($image) {
                                echo $image->getFileUrl('image');
                            }
                        }
                        ?>');">
                                <span class="gallery-card__counter"><?= SiteGalleriesImagesQuery::countImage($model->id) ?></span>
							<div class="gallery-card__photo">
                                <?php
                                if (!$image) {
                                    echo "<div class='icono-camera'></div><br>";
                                }
                                ?>
						    </div>
                        </div>
						<div class="title-wrapper"><span class="title"><?= $model->title ?></span></div>
						<div class="date-wrapper"><span class="date"><?= $model->custom_date ?></span></div>
					</a>
</div>