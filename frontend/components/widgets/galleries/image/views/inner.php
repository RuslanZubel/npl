<?php
	use yii\widgets\ListView;
?>
<div class="gallery dark-background">
	<div class="container">
		<div class="row">
			<div class="page-title dark">
				<span><a href="/mediafolder/site-galleries"><?= $viewTitle ?></a></span>
			</div>
			<div class="media-cards inner-image">
				<?php echo ListView::widget([
					'dataProvider' => $dataProvider,
					'itemView' => '_item',
					'layout' => "\n<div class=\"media-cards-wrapper\">{items}</div>\n{pager}",
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'id' => false,
                    'options' => [
                        'tag' => false,
                    ],
				]); ?>
			</div>
		</div>
	</div>
</div>
