<?php
use yii\widgets\ListView;
?>
<div class="gallery-items">
	<div class="row">
		<?php echo ListView::widget([
			'dataProvider' => $dataProvider,
			'itemView' => '_griditem',
			'layout' => "\n{items}\n{pager}",
		]); ?>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery1.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery2.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery3.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery1.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery2.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery3.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery1.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery2.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery3.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery1.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery2.jpg" alt=""></a></div>
		<div class="gallery-item col-md-2"><a data-fancybox="gallery" href="img/gallery1.jpg"><img src="img/gallery3.jpg" alt=""></a></div>
	</div>
</div>

