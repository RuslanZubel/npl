<?php
	use yii\widgets\ListView;
?>
<div class="galleries dark-background">
	<div class="page-wrapper">
		<div class="container">
			<div class="page-title dark">
				<span><a href="/mediafolder/site-galleries"><?= $viewTitle ?></a></span>
			</div>
			<div class="media-cards">
				<?php echo ListView::widget([
					'dataProvider' => $dataProvider,
					'itemView' => '_item',
					'layout' => "\n{items}\n{pager}",
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'id' => false,
                    'options' => [
                        'tag' => false,
                    ],
				]); ?>
			</div>
		</div>
	</div>
</div>
