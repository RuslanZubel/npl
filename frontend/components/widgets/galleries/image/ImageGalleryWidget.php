<?php

namespace frontend\components\widgets\galleries\image;
use frontend\models\SiteGalleries;
use frontend\models\SiteGalleriesImages;
use yii\data\ActiveDataProvider;

/**
 * Created by PhpStorm.
 * User: amio
 * Date: 21/07/2017
 * Time: 10:10
 */
class ImageGalleryWidget extends \yii\base\Widget
{
	const VIEW_TYPE_ROW = 'row';
	const VIEW_TYPE_LIST = 'list';
	const VIEW_TYPE_GRID = 'grid';
	
	public $rowItemsCount = 3;
	public $listItemsCount = 9;
	public $gridItemsRowCount = 9;
	public $viewTitle = 'Фотогалереи';
	public $viewType = self::VIEW_TYPE_LIST;
	
	private $_dataProvider = NULL;
	
	public function init()
	{
		$query =  SiteGalleries::find();
		$query -> orderBy(['custom_date' => SORT_DESC]);
		
		if ( $this->viewType == self::VIEW_TYPE_ROW ) {
			$query ->andWhere(['show_on_index' => 'yes']);
			$query ->limit($this->rowItemsCount);
		}
		
		$this->_dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' =>  $this->viewType == self::VIEW_TYPE_ROW ? false : [ 'pageSize' => $this->listItemsCount ],
		]);
		
	//	$gridQuery = SiteGalleriesImages::
		
		return parent::init();
		
	}
	
	public function run()
	{
        if ($this->viewType == self::VIEW_TYPE_ROW) {
            return $this->render('index', ['dataProvider' => $this->_dataProvider, 'viewTitle' => $this->viewTitle]);
        } else {
            return $this->render('inner', ['dataProvider' => $this->_dataProvider, 'viewTitle' => $this->viewTitle]);
        }
	}
}