<?php

namespace frontend\components\widgets\content\socialnetworks;
use frontend\models\ConfigOptions;
use frontend\models\News;
use yii\data\ActiveDataProvider;

/**
 * Created by PhpStorm.
 * User: amio
 * Date: 21/07/2017
 * Time: 10:10
 */
class SocialNetworksWidget extends \yii\base\Widget
{
    public $viewTitle = 'Новости';

    private $_data = NULL;

    public function init()
    {
        $this->_data = ConfigOptions::find()->all();

        return parent::init();
    }

    public function run()
    {
        return $this->render( 'index', [
            'container' => $this,
        ]);
    }

    public function getValue($param) {
        foreach ($this->_data as $configOption) {
            if ($configOption->setting == $param) {
                return $configOption->value;
            }
        }

        return '';
    }
}