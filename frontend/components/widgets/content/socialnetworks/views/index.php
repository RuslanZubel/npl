<?php

?>

<div class="soc-bottom">
    <a class="social-links__facebook" href="https://www.facebook.com/russiansailingleague/" target="_blank" rel="noopener">
        <i class="fa fa-facebook" aria-hidden="true"></i>
    </a>
    <a class="social-links__instagram" href="https://www.instagram.com/national_sailing_league/" target="_blank" rel="noopener">
        <i class="fa fa-instagram" aria-hidden="true"></i>
    </a>
    <a class="social-links__youtube" href="https://www.youtube.com/channel/UCHGQZUCjdMTm25zU3C78Huw" target="_blank" rel="noopener">
        <i class="fa fa-youtube-play" aria-hidden="true"></i>
    </a>
    <a class="social-links__youtube" href="<?= $container->getValue('lnk_tw') ?>" target="_blank" rel="noopener">
        <i class="fa fa-twitter" aria-hidden="true"></i>
    </a>
    <!--<a class="social-links__youtube" href="#" target="_blank" rel="noopener">
        <i class="fa fa-whatsapp" aria-hidden="true"></i>
    </a>-->
</div>
