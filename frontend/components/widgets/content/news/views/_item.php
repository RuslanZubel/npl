<?php
use yii\helpers\Url;
?>

<div class="col-md-4 col-sm-12">
    <div href="<?= $model->title ?>" class="news-item">
        <a href="<?=  Url::toRoute(['/content/news/view', 'id' => $model->id]); ?>">
            <div class="date-wrapper"><span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><?= $model->display_date ?></span></div>
            <div class="title-wrapper"><span class="title"><?= $model->short_title ?> </span></div>
            <div class="article-card__cover" style="background-image: url('<?= $model->getFileUrl('image') ?>');"></div>
        </a>
    </div>
</div>