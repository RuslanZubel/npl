<?php
use yii\widgets\ListView;
?>

<div class="page-wrapper">
    <div class="articles">
        <div class="container">
            <div class="page-title">
                <span><a href="/content/news">Новости</a></span>
            </div>
            <div class="row">
                <?php echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_item',
                    'layout' => "\n{items}\n{pager}",
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'id' => false,
                    'options' => [
                            'tag' => false,
                     ],
                ]); ?>
            </div>
        </div>
    </div>
</div>