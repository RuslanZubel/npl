<?php

namespace frontend\components\widgets\content\news;
use frontend\models\News;
use yii\data\ActiveDataProvider;

/**
 * Created by PhpStorm.
 * User: amio
 * Date: 21/07/2017
 * Time: 10:10
 */
class NewsListWidget extends \yii\base\Widget
{
	const VIEW_TYPE_ROW = 'row';  //Одной строкой для главной страницы
	const VIEW_TYPE_LIST = 'list'; //Полный список новостей

	public $rowItemsCount = 3;
	public $listItemsCount = 9;
	public $viewTitle = 'Новости';
	public $viewType = self::VIEW_TYPE_LIST;
	
	private $_dataProvider = NULL;
	
	public function init()
	{
		$query = News::find();
		$query->orderBy(['display_date' => SORT_DESC]);
		
		if ( $this->viewType == self::VIEW_TYPE_ROW ) {
			$query->andWhere(['show_on_index' => 'yes']);
			$query->limit($this->rowItemsCount);
		}
		
		$this->_dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' =>  $this->viewType == self::VIEW_TYPE_ROW ? false : [ 'pageSize' => $this->listItemsCount ],
		]);
		
		return parent::init();
		
	}
	
	public function run()
	{
        if ($this->viewType == self::VIEW_TYPE_ROW) {
            return $this->render('index', ['dataProvider' => $this->_dataProvider, 'viewTitle' => $this->viewTitle]);
        } else {
            return $this->render('inner', ['dataProvider' => $this->_dataProvider, 'viewTitle' => $this->viewTitle]);
        }
	}
}