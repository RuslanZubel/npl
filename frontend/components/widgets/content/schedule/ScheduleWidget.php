<?php

namespace frontend\components\widgets\content\schedule;


use frontend\models\News;
use frontend\models\Stages;
use yii\data\ActiveDataProvider;

class ScheduleWidget extends \yii\base\Widget
{
    const VIEW_TYPE_ROW = 'row';  //Одной строкой для главной страницы
    const VIEW_TYPE_LIST = 'list'; //Полный список новостей

    public $rowItemsCount = 3;
    public $listItemsCount = 9;
    public $viewTitle = 'Расписание';
    public $viewType = self::VIEW_TYPE_LIST;

    private $_dataProvider = NULL;

    public function init()
{
    $query = Stages::find();
    $query->where(['fk_rel_seasons_leagues' => 11]);
    $query->orderBy(['start_date' => SORT_ASC]);


    $this->_dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' =>  $this->viewType == self::VIEW_TYPE_ROW ? false : [ 'pageSize' => $this->listItemsCount ],
    ]);

    return parent::init();

}

    public function run()
{
    if ($this->viewType == self::VIEW_TYPE_ROW) {
        return $this->render('index', ['dataProvider' => $this->_dataProvider, 'viewTitle' => $this->viewTitle]);
    } else {
        return $this->render('inner', ['dataProvider' => $this->_dataProvider, 'viewTitle' => $this->viewTitle]);
    }
}
}