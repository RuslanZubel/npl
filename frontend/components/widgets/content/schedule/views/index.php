<?php
use yii\widgets\ListView;
?>

<div class="page-wrapper">
    <div class="articles">
        <div class="container">
            <div class="page-title">
                <span><a href="/content/stages">Расписание Зимней Серии</a></span>
            </div>
            <div class="row d-flex justify-content-around">
                <?php echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_item',
                    'layout' => "\n{items}\n{pager}",
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'id' => false,
                    'options' => [
                        'tag' => false,
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>