<?php

use yii\helpers\Url;

?>

<div class="col-md-3 mb-3">
    <a href="<?= Url::toRoute(['/content/stages/view', 'id' => $model->id]) ?>" class="events-item">
								<span class="events-item--img"
                                      style="background: url('<?= $model->getFileUrl('image') ?>') 50% 50% no-repeat;">
									<span class="events-item--img-caption">
										<span class="events-item--title"><?= $model->title ?></span>
										<span class="events-item--subtitle"><?= $model->place ?></span>
										<span class="events-item--date"><?= $model->getStartEndDate() ?></span>
									</span>
								</span>
    </a>
</div>