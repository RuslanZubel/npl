<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 13/03/2018
 * Time: 12:19
 */

namespace frontend\components;


use yii\web\Controller;

class BaseController extends Controller
{

    public $layout = '//layout';

}