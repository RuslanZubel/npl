<?php
/**
 * Created by PhpStorm.
 * User: codepug
 * Date: 28/03/2018
 * Time: 15:22
 */

namespace frontend\models;

use yii\helpers\ArrayHelper;
use \frontend\traits\BaseFrontendTrait;

class SiteGalleries extends \common\models\SiteGalleries
{
    use BaseFrontendTrait;

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'frontend\behaviors\DateFieldBehavior',
                'attribute' => 'custom_date',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }
}