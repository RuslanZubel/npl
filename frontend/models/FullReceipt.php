<?php
/**
 * Created by PhpStorm.
 * User: Xiaomi
 * Date: 08.02.2019
 * Time: 12:27
 */

namespace frontend\models;


use frontend\traits\BaseFrontendTrait;

class FullReceipt extends \common\models\Receipt
{
    use BaseFrontendTrait;
}