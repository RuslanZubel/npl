<?php
/**
 * Created by PhpStorm.
 * User: codepug
 * Date: 03/04/2018
 * Time: 00:45
 */

namespace frontend\models;

use \frontend\traits\BaseFrontendTrait;

class SiteGalleriesImages extends \common\models\SiteGalleriesImages
{
    use BaseFrontendTrait;
}