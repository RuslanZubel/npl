<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 13/03/2018
 * Time: 13:00
 */

namespace frontend\models;

use \frontend\traits\BaseFrontendTrait;

class Teams extends \common\models\Teams
{
    use BaseFrontendTrait;
}