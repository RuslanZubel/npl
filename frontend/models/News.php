<?php

namespace frontend\models;

use yii\helpers\ArrayHelper;
use frontend\traits\BaseFrontendTrait;

class News extends \common\models\News
{
	use BaseFrontendTrait;

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'frontend\behaviors\DateFieldBehavior',
                'attribute' => 'display_date',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }
}