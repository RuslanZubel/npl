<?php

namespace frontend\models;

use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use frontend\traits\BaseFrontendTrait;

class Stages extends \common\models\Stages
{
	use BaseFrontendTrait;


    public $main_trainings_date_copy = null;

	public function getSeason() {
		return $this->relSeasonsLeagues->fkRelSeasonsLeagues->fkSeasons;
	}

	public function getLeague() {
		return $this->relSeasonsLeagues->fkRelSeasonsLeagues->fkLeagues;
	}

    public function afterFind() {
        parent::afterFind();
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'frontend\behaviors\DateFieldBehavior',
                'attribute' => 'start_date',
            ],
            [
                'class' => 'frontend\behaviors\DateFieldBehavior',
                'attribute' => 'end_date',
            ],
            [
                'class' => 'frontend\behaviors\DateFieldBehavior',
                'attribute' => 'main_trainings_date',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

    /**
     * Получить период проведения гонки в формате j F - j F Y (например 12 мая - 13 июня 2018 года)
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */

}