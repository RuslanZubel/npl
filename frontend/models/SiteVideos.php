<?php
/**
 * Created by PhpStorm.
 * User: codepug
 * Date: 28/03/2018
 * Time: 15:22
 */

namespace frontend\models;

use frontend\traits\BaseFrontendTrait;
use yii\helpers\ArrayHelper;

class SiteVideos extends \common\models\SiteVideos
{
    use BaseFrontendTrait;

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'frontend\behaviors\DateFieldBehavior',
                'attribute' => 'custom_date',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }
}