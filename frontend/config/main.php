<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'homeUrl' => '/',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'assetManager' => [
            'assetMap' => [
                'bootstrap.css'     =>  '@web/bootstrap/css/bootstrap.min.css',
                'bootstrap.js'      =>  'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js',
            ]
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '/',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/',
        ],

    ],
    'modules' => [
        'teams' => [
            'class' => 'frontend\modules\teams\Module',
        ],
        'personal' => [
            'class' => 'frontend\modules\personal\PersonalFolder',
        ],
        'content' => [
            'class' => 'frontend\modules\content\Module',
            'components'  => [
                'errorHandler' => [
                    'errorAction' => 'site/error',
                    'class' => 'frontend\modules\teams\Module',
                ],
            ]
        ],
        'mediafolder' => [    // TODO: добавлен модуль mediafolder
            'class' => 'frontend\modules\mediafolder\MediaFolder',
        ],
        'results' => [    // TODO: добавлен модуль mediafolder
            'class' => 'frontend\modules\results\Results',
        ],
    ],
    'params' => $params,
];
