<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 17/04/2017
 * Time: 09:25
 */

namespace backend\traits;

use common\models\BaseModel;
use backend\models\User;
use yii\helpers\ArrayHelper;


trait BaseBackendTrait
{
    public function baseRules()
    {

        $baseRules = [
            [['created_at', 'updated_at'], 'safe'],
            [[ 'fk_author'], 'integer'],
            [['created_at', 'updated_at'], 'default', 'value' => time() ],
            [['fk_author'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_author' => 'id']],
        ];

        if ($this->hasProperty('is_active')) {
            $baseRules[] = ['is_active', 'in', 'range' => [BaseModel::ACTIVE_TRUE, BaseModel::ACTIVE_FALSE]];
            $baseRules[] = ['is_active', 'required'];
        };

        if ($this->hasProperty('meta_description')) {
            $baseRules[] = ['meta_description', 'string'];
        };

        if ($this->hasProperty('meta_title')) {
            $baseRules[] = ['meta_title', 'string'];
        };

        if ($this->hasProperty('meta_keywords')) {
            $baseRules[] = ['meta_keywords', 'string'];
        };

        if ($this->hasProperty('meta_canonical')) {
            $baseRules[] = ['meta_canonical', 'string'];
        };

        return $baseRules;

    }

    public function beforeSave($insert)
    {
        $this->fk_author = \Yii::$app->user->id;
        if ($insert) {
            $this->created_at = time();
            if ($this->hasProperty('sort')) {
                $this->sort = (self::find()->select('sort')->orderBy(['sort' => SORT_DESC])->limit(1)->scalar()) + 1;
            }
        }
        $this->updated_at = time();
        return parent::beforeSave($insert);
    }

    public function getYesNoDropdownList()
    {
        return [BaseModel::ACTIVE_TRUE => 'Отображается', BaseModel::ACTIVE_FALSE => 'Не отображается'];
    }

    /**
     * Общий метод получения human-readable значения is_active
     * @return string
     */
    public function getIsActiveName()
    {
        $names = $this->getYesNoDropdownList();
        if ($this->is_active == 'yes') {
            return $names[BaseModel::ACTIVE_TRUE];
        } else if ($this->is_active == 'no') {
            return $names[BaseModel::ACTIVE_FALSE];
        } else {
            return "-";
        }
    }

    public function getPromptLabel()
    {
        return 'Выберите значение';
    }

    public static function getList($query = null, $key = 'id', $value = 'title')
    {
        if ($query == null) {
            $model = self::find()->orderBy(['title' => SORT_ASC])->all();
        } else {
            $model = $query;
        }
        return ArrayHelper::map($model, $key, $value);
    }

}