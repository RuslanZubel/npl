<?php
namespace backend\components;

use yiidreamteam\upload\ImageUploadBehavior ;
use Gregwar\Image\Image ;
use yii\helpers\FileHelper ;
use yii\helpers\ArrayHelper ;

class Upload extends ImageUploadBehavior {

    public $zoomCrop = true;

    public $zoomCropFromTheTop = false;

    /**
     * Creates image thumbnails
     */
    public function createThumbs()
    {
        $path = $this->getUploadedFilePath($this->attribute);
        foreach ($this->thumbs as $profile => $config) {
            $thumbPath = static::getThumbFilePath($this->attribute, $profile);
            if (is_file($path) && !is_file($thumbPath)) {

                $img    =   Image::open( $path ) ;

                $info   =   getimagesize( $path ) ;

                if( $info[0] != $config['width'] && $info[1] != $config['height'] ){
                    $maxProp        =   $this->_getMaxProp($config['width'], $config['height']) ;
                    $minSizeData    =   $this->_getMinSize($info[0], $info[1], $maxProp) ;
                        if ($this->zoomCrop && !$this->zoomCropFromTheTop) {
                            $img->cropResize( $minSizeData[0], $minSizeData[1]) ;
                            $img->zoomCrop( $config['width'], $config['height'] ) ;
                        } else if ($this->zoomCropFromTheTop) {
                            $img->cropResize( $minSizeData[0], $minSizeData[1]) ;
                            $xPos = ($info[0] - $config['width']) / 2;
                            $img->zoomCrop( $config['width'], $config['height'], 0xffffff, $xPos,0 ) ;
                        } else {
                            $img->scaleResize($config['width'], $config['height']) ; //Background не указывается (3им параметром), по умолчанию он белый
                        }
                }

                FileHelper::createDirectory(pathinfo($thumbPath, PATHINFO_DIRNAME), 0775, true);

                $temp   = explode('.', $thumbPath) ;
                $temp[count($temp)-2]    .=   '_'.$profile ;
//                $temp[count($temp)-2]    .=   '_'.\Yii::$app->security->generateRandomString(6) ;


                $img->save( implode( '.', $temp ), 'guess', \Yii::$app->params['jpgQuality'] ) ;

            }
        }
    }

    private function _getMaxProp( $w, $h ){
        if( $w > $h ){ return $w; }
        elseif( $h > $w ){ return $h; }
        return $w ;
    }

    private function _getMinSize( $w, $h, $maxProp ){
        if( $w < $h ){ return [$maxProp, null]; }
        elseif( $h < $w ){ return [null, $maxProp]; }
        return [$maxProp, null] ;
    }

    /**
     * @param string $attribute
     * @param string $profile
     * @param string|null $emptyUrl
     * @return string|null
     */
    public function getThumbFileUrl($attribute, $profile = 'thumb', $emptyUrl = null)
    {

        $behavior = static::getInstance($this->owner, $attribute);
        if ($behavior->createThumbsOnRequest)
            $behavior->createThumbs();


        $temp   = explode('.', \Yii::getAlias( $behavior->thumbUrl )) ;
        $temp[count($temp)-2]    .=   '_'.$profile ;

        $file   =   str_replace(
            [
                '[[pk]]',
                '[[salt]]'
            ],
            [
                $this->owner->id,
                $this->owner->{$attribute}
            ],
            implode('.', $temp)
        ) ;
        if( file_exists( \Yii::getAlias('@frontend/web') . $file )  ){ return $file; }
        else{ return $emptyUrl; }
    }

    /**
     * Removes files associated with attribute
     */
    public function cleanFiles()
    {
        $path = $this->resolvePath($this->filePath);
        @unlink($path);

        foreach ($this->thumbs as $profile => $config) {
            $thumbPath = static::getThumbFilePath($this->attribute, $profile);
            if ( is_file($thumbPath) ) {
                @unlink( str_replace('.', '_'.$profile.'.', $thumbPath) );
            }
        }
    }

    public function getFileUrl( $attribute, $emptyUrl = null ){
        $result = $this->getUploadedFileUrl($attribute);
        if( file_exists( \Yii::getAlias('@frontend/web') . $result )  ){ return $result; }
        else{ return $emptyUrl; }
    }

    public function getUploadedFileUrl($attribute)
    {
//        if (!$this->owner->{$attribute})
//            return null;

        $behavior = static::getInstance($this->owner, $attribute);
        return $behavior->resolvePath($behavior->fileUrl);
    }

    public function getFilePath( $attribute ){
        return $this->getUploadedFilePath($attribute) ;
    }

    public function resolvePath($path)
    {
        $path = \Yii::getAlias($path);

        $pi = pathinfo($this->owner->{$this->attribute});
        $fileName = ArrayHelper::getValue($pi, 'filename');
        $extension = strtolower(ArrayHelper::getValue($pi, 'extension'));

        return preg_replace_callback('|\[\[([\w\_/]+)\]\]|', function ($matches) use ($fileName, $extension) {
            $name = $matches[1];
            switch ($name) {
                case 'salt':
                    return $this->owner->{$this->attribute};
                case 'extension':
                    return $extension;
                case 'filename':
                    return $fileName;
                case 'basename':
                    return  $fileName . '.' . $extension;
                case 'app_root':
                    return Yii::getAlias('@app');
                case 'web_root':
                    return Yii::getAlias('@webroot');
                case 'base_url':
                    return Yii::getAlias('@web');
                case 'model':
                    $r = new \ReflectionClass($this->owner->className());
                    return lcfirst($r->getShortName());
                case 'attribute':
                    return lcfirst($this->attribute);
                case 'id':
                case 'pk':
                    $pk = implode('_', $this->owner->getPrimaryKey(true));
                    return lcfirst($pk);
                case 'id_path':
                    return static::makeIdPath($this->owner->getPrimaryKey());
                case 'parent_id':
                    return $this->owner->{$this->parentRelationAttribute};
            }
            if (preg_match('|^attribute_(\w+)$|', $name, $am)) {
                $attribute = $am[1];
                return $this->owner->{$attribute};
            }
            if (preg_match('|^md5_attribute_(\w+)$|', $name, $am)) {
                $attribute = $am[1];
                return md5($this->owner->{$attribute});
            }
            return '[[' . $name . ']]';
        }, $path);
    }

}