<?php

namespace backend\components\actions;
use yii\web\HttpException;
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 20/09/2017
 * Time: 12:08
 */

class SortableAction extends \richardfan\sortable\SortableAction
{

    public function run(){

        $direction = \Yii::$app->request->get('direction', SORT_ASC);

        $page = \Yii::$app->request->get('page');

        $perPage = \Yii::$app->request->get('per-page');

        if(!\Yii::$app->request->isAjax){
            throw new HttpException(404);
        }

        $activeRecordClassName = $this->activeRecordClassName;

        if (!is_null( $perPage )) {
            $offset = $perPage * ( $page - 1 );
        } else {
            $offset = 0;
        }

        if ( $direction == SORT_DESC ) {
            $maxSort = $activeRecordClassName::find()->select('sort')->orderBy([ 'sort' => SORT_DESC ])->scalar();
        }

        if (isset($_POST['items']) && is_array($_POST['items'])) {
            $activeRecordClassName = $this->activeRecordClassName;
            foreach ($_POST['items'] as $i=>$item) {
                if ( $direction == SORT_DESC ) {
                    $currentSort = $maxSort - $offset - $i;
                } else {
                    $currentSort = $offset + $i;
                }
                $activeRecordClassName::updateAll([$this->orderColumn => $currentSort], ['id' => $item]);

            }
        }
    }

}