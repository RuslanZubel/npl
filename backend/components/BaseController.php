<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 17/04/2017
 * Time: 09:13
 */

namespace backend\components;

use backend\components\actions\SortableAction;
use backend\models\PartnersStatuses;
use common\models\BaseModel;
use yii\web\Controller;

class BaseController extends Controller
{

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => $this->_getSortableModel(),
                'orderColumn' => 'sort',
            ],
        ];
    }

    private function _getSortableModel()
    {
        $resultModel = null;
        switch (\Yii::$app->controller->id) {
            case 'offices':
                $resultModel = PartnersStatuses::className();
                break;

        }
        return $resultModel;
    }

    public function actionToggleActive(){
        $id = \Yii::$app->request->post('id');
        $modelClass = urldecode(\Yii::$app->request->post('modelClass'));
        $currentStatus = \Yii::$app->request->post('status');
        if ($currentStatus == 'no') {
            $modelClass::updateAll(['is_active' => BaseModel::ACTIVE_TRUE], [ 'id' => $id]);
        } else {
            $modelClass::updateAll(['is_active' => BaseModel::ACTIVE_FALSE], [ 'id' => $id]);
        }
        return $this->asJson(['status' => 'ok']);
    }


}