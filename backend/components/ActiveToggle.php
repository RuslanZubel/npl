<?php
/**
 * Created by PhpStorm.
 * User: Виктор
 * Date: 17.05.2018
 * Time: 11:01
 */

namespace backend\components;

use common\models\BaseModel;

class ActiveToggle
{
    public static function getView($item) {
        if ($item->is_active == BaseModel::ACTIVE_TRUE) {
            return '<span class="toggle-active cp" rel="yes"><span class="glyphicon glyphicon-eye-open" title="Скрыть"></span></span> Да';
        } else {
            return '<span class="toggle-active cp" rel="no"><span class="glyphicon glyphicon-eye-close" title="Сделать видимым"></span></span> Нет';
        }
    }
}