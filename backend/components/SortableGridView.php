<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 27/11/2017
 * Time: 17:56
 */

namespace backend\components;

use yii\helpers\Url;


class SortableGridView extends \richardfan\sortable\SortableGridView
{

    public function init()
    {
        parent::init();

        $modelClass = $this->dataProvider->query->modelClass;

        \Yii::$app->getView()->registerJs("

            $('#" . $this->id . "').on('click', '.toggle-active', function() {
                var th = $(this);
                var id = th.closest('tr').data('key')
                var status = th.attr('rel');
                var pjaxId = $('#" . $this->id . "').closest('[data-pjax-container]').attr('id');
                $.post('" . Url::toRoute('toggle-active') . "', { 
                        'id' :  id,
                        'modelClass' : '" . urlencode($modelClass) . "',
                        'status' : status,
                    }, function( data ) {
                    if (data.status == 'ok') {
                        $.pjax.reload('#'+pjaxId);
                    }
                });
            });
            
        ");

    }

}