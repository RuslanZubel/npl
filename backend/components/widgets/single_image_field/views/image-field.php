<?php

use yii\helpers\Url;
use kartik\dialog\Dialog;
use yii\widgets\Pjax;

$id = $this->context->getId();

echo Dialog::widget([
    'libName' => 'krajeeDialogCust', // a custom lib name
    'options' => [  // customized BootstrapDialog options
        'type' => Dialog::TYPE_DANGER, // bootstrap contextual color
        'title' => 'Внимание!',
        'message' => 'This is an entirely customized bootstrap dialog from scratch. Click buttons below to test this:',
        'btnOKClass' => 'btn-danger',
        'btnCancelLabel' => 'Отмена',
    ],
]);

?>

<?php Pjax::begin(['id' => $id]); ?>
<?= $this->context->form->field($this->context->model, $this->context->attribute)->fileInput() ?>
<?php if ($this->context->thumbName === false) : ?>
    <img src="<?= $this->context->model->getFileUrl($this->context->attribute) ?>" style="max-width: 100%"
         rel="img-<?= $this->context->attribute ?>"/>
<?php else : ?>
    <img src="<?= $this->context->model->getThumbFileUrl($this->context->attribute) ?>"
         style="max-width: 100%" rel="img-<?= $this->context->attribute ?>"/>
<?php endif; ?>

<p><a
        class="btn btn-xs btn-default unlink-button"
        data-pjax="0"
        rel="<?= $this->context->model->getFileUrl($this->context->attribute) ?>"
        thumb="<?= (!$this->context->thumbName === false) ? $this->context->model->getThumbFileUrl($this->context->attribute) : '' ?>"
>Удалить</a></p>
<?php Pjax::end(); ?>


<?php

$this->registerJs("

$('#" . $id . "').on('click', '.unlink-button', function() {
    th = $(this);
    path = th.attr('rel');
    thumbPath = th.attr('thumb');
    idPjax = '" . $id . "';
    krajeeDialogCust.confirm('Удалить выбранное изображение безвозвратно? ', function (result) {
        if (result) { 
            $.post('" . Url::toRoute('/site/delete-image') . "', 
                { 'path':path, 'thumbPath':thumbPath }, 
                function( data ) {
                    if (data.status == 'ok') {
                        $.pjax.reload('#' + idPjax);
                    }
                }
            );            
        }        
    });
});

");



?>
