<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 01/07/2017
 * Time: 12:45
 */

namespace backend\components\widgets\single_image_field;


use yii\base\Widget;

class SingleImageField extends Widget
{
    public $model;
    public $form;
    public $thumbName;
    public $attribute;

    public function init() {
        return parent::init();
    }

    public function run() {
        return $this->render('image-field');
    }
}