<?php

use yii\helpers\Html;

?>

<?=
Html::submitButton(
    $this->context->model->isNewRecord ? 'Создать' : 'Сохранить',
    ['class' => $this->context->model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
);
?>

<?=
Html::a(
    'Отменить' ,
     \Yii::$app->request->referrer ?? Yii::$app->homeUrl,
    [ 'class' =>  'btn btn-default' ]
);
?>
