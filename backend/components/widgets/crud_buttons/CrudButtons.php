<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 01/07/2017
 * Time: 12:45
 */

namespace backend\components\widgets\crud_buttons;


use yii\base\Widget;

class CrudButtons extends Widget
{

    public $model;

    public function init() {
        return parent::init();
    }

    public function run() {
        return $this->render('crud-buttons');
    }
}