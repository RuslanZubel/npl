<?php

namespace backend\controllers;

use backend\components\BaseController;
use Yii;
use zxbodya\yii2\elfinder\ConnectorAction;

class ElFinderController extends BaseController
{
    public function actions()
    {
        return [
            'connector' => [
                'class' => ConnectorAction::className(),
                'settings' => [
                    'root' => Yii::getAlias('@elfinder'),
                    'URL' => Yii::getAlias('@elfinderUrl/'),
                    'rootAlias' => 'Home',
                    'mimeDetect' => 'none',
                    'options' => [
                        'uploadAllow' => ['image'],
                    ],
                ],
            ],
            'connectorUploads' => [
                'class' => ConnectorAction::className(),
                'settings' => [
                    'root' => Yii::getAlias('@uploads'),
                    'URL' => Yii::getAlias('@uploadsUrl') . '/' ,
                    'rootAlias' => 'Home',
                    'mimeDetect' => 'none',
                    'options' => [
                        'uploadAllow' => ['image'],
                    ],
                ],
            ]
        ];

    }
}