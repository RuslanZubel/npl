<?php
namespace backend\controllers;

use common\models\User;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function beforeAction($action)
    {
        if ($this->route !== 'site/login'){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['login']);
            }
        }

        if (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->id)->roles === 'user'){
            return $this->redirect('/admin');

        }


        try {
            return parent::beforeAction($action);
        } catch (BadRequestHttpException $e) {
        }
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'actions' => ['login', 'error'],
//                        'allow' => true,
//                    ],
//                    [
//                        'actions' => ['logout', 'index'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
                'route' => $this->route
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionDeleteImage(){
        $path = \Yii::$app->request->post('path');
        $thumbPath = \Yii::$app->request->post('thumbPath');
        if ( unlink( Yii::getAlias('@frontend/web/') . $path ) )
        {
            if ( !empty($thumbPath) ) { unlink( Yii::getAlias('@frontend/web/') . $thumbPath ); }
            return $this->asJson(['status' => 'ok']);
        } else {
            return $this->asJson(['status' => 'fail']);
        }

    }
}
