<?php
namespace backend\behaviors\uploader ;

use yii\db\ActiveRecord ;
use yii\helpers\FileHelper ;
use yii\web\UploadedFile ;
use common\components\OnforImagine ;

class UploaderBehaviour extends \yii\base\Behavior{

    public $needToRemoveImages  =   '' ;

    const TYPE_IMAGE        =   'image';

    const TYPE_FILE         =   'file';

    public $attribute       =   null ;

    public $fileType        =   self::TYPE_IMAGE ;

    public $thumbs          =   [] ;

    public $defaultPhoto    =   null ;

    public $fileNamePattern =   null ;

    public $thumbNamePattern=   null ;

    public $filePath        =   null ;

    public $fileUrl         =   null ;

    public $thumbPath       =   null ;

    public $thumbUrl        =   null ;

    public $saltLength      =   6 ;

    private $_instance      =   null ;

    private $_salt          =   null ;

    private $_newFileName   =   null ;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT   =>  'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE   =>  'beforeSave',
            ActiveRecord::EVENT_AFTER_INSERT    =>  'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE    =>  'afterSave',
            ActiveRecord::EVENT_AFTER_DELETE    =>  'afterDelete',
        ];
    }

    public function init(){
        if( is_null( $this->attribute ) ){
            throw new \yii\base\InvalidParamException('Attribute должен быть указан.') ;
        }
        if( is_null( $this->filePath ) || is_null( $this->fileUrl ) ){
            throw new \yii\base\InvalidParamException('Нужно указать filePath и fileUrl.') ;
        }

        FileHelper::createDirectory( \Yii::getAlias( $this->filePath ) ) ;
        if( !is_null( $this->thumbPath ) && $this->fileType == self::TYPE_IMAGE ){
            FileHelper::createDirectory( \Yii::getAlias( $this->thumbPath ) ) ;
        }

        return parent::init();
    }

    public function beforeSave( $event ){

        $postForm       =   \Yii::$app->request->post( $this->owner->formName() ) ;
        $needToRemove   =   array_filter(
            $postForm['needToRemoveImages'] ?? [],
            function( $value ){
                return !empty( $value ) ;
            }
        )  ;

        if( empty( $needToRemove ) ){
            if( $this->owner->{$this->attribute} instanceof UploadedFile){
                $this->_instance    =   $this->owner->{$this->attribute} ;
            }
            $this->owner->{$this->attribute}    =   $this->owner->getOldAttribute( $this->attribute ) ;
            return ;
        }


        foreach ($needToRemove as $attribute) {
            $this->_cleanFiles( $attribute );
            $this->owner->{$attribute}    =   '' ;
        }
        return true ;
    }

    public function afterDelete( $event ){
        $this->_cleanFiles();
        return true ;
    }

    public function afterSave( $event ){
        if( is_null( $this->_instance ) ){
            $this->_instance    =   UploadedFile::getInstance( $this->owner, $this->attribute ) ;
        }

        if( is_null( $this->_instance ) ){ return true ; }

        if( !empty( $this->owner->{$this->attribute} ) ){ $this->_cleanFiles(); }

        $this->_newFileName =   $this->_makeFileName() ;

        if( $this->_instance->saveAs( $this->_getFullFilePath() ) ){
            if( $this->fileType == self::TYPE_IMAGE && !empty( $this->thumbs ) ){
                $this->_generateThumbs();
            }
        }

        $this->owner->updateAll(
            [ $this->attribute => $this->_newFileName ],
            [ 'id' => $this->owner->id ]
        );
        $this->owner->{$this->attribute}    =   $this->_newFileName ;
        return true ;
    }

    private function _cleanFiles( $attribute = null ){
        $trueAttribute  =   ( is_null($attribute) ? $this->attribute : $attribute) ;
        if( empty( $this->owner->{$trueAttribute} ) ){ return; }
        @unlink( \Yii::getAlias( $this->filePath . $this->owner->{$trueAttribute} ) ) ;
        if( $this->fileType != self::TYPE_IMAGE || empty( $this->thumbs ) ){ return ; }

        foreach ( $this->thumbs as $profile => $data  ){
            @unlink( \Yii::getAlias( $this->thumbPath . $this->_getThumbNameByProfile($profile, $trueAttribute) ) ) ;
        }
    }

    private function _getThumbNameByProfile( $profile, $attribute = null ){
        return str_replace('.', '_'.$profile.'.', $this->owner->{( is_null($attribute) ? $this->attribute : $attribute)}) ;
    }

    private function _getFullFilePath(){
        return \Yii::getAlias( $this->filePath . $this->_newFileName )  ;
    }

    private function _getFullThumbFilePath( $thumbName ){
        return \Yii::getAlias( $this->thumbPath . $thumbName )  ;
    }

    private function _generateThumbs(){
        return;
        //echo phpinfo();
        //die;
        foreach ($this->thumbs as $profile => $data) {
            $thumbName  =   $this->_makeThumbFileName( $profile ) ;
            OnforImagine::resizeZoomCrop(
                $this->_getFullFilePath(),
                $data['width'],
                $data['height'],
                $this->_getFullThumbFilePath( $thumbName )
            );
        }
    }

    private function _makeFileName(){
        $replaceArray   =   $this->_getReplaceArray() ;
        return str_replace(
            array_keys( $replaceArray ),
            array_values( $replaceArray ),
            $this->fileNamePattern
        ) ;
    }

    private function _makeThumbFileName( $thumbProfile ){
        $replaceArray   =   $this->_getReplaceArray( $thumbProfile ) ;
        return str_replace(
            array_keys( $replaceArray ),
            array_values( $replaceArray ),
            $this->thumbNamePattern
        ) ;
    }

    private function _getReplaceArray( $thumbName = null ){
        $pathInfo   =   pathinfo( $this->_instance->name ) ;
        return [
            '[[pk]]'    =>  $this->owner->id,
            '[[ext]]'   =>  $pathInfo['extension'],
            '[[salt]]'  =>  $this->_getSalt(),
            '[[thumb]]' =>  $thumbName
        ] ;
    }

    private function _getSalt(){
        if( is_null( $this->_salt ) ){
            $this->_salt    =   \Yii::$app->security->generateRandomString( $this->saltLength ) ;
        }
        return $this->_salt ;
    }

    public function getDefaultPhoto(){
        if( is_null( $this->defaultPhoto ) ){ return '' ; }

        return \Yii::getAlias( $this->defaultPhoto ) ;
    }

    public function isHasFile(){
        return !empty( $this->owner->{$this->attribute} ) ;
    }

    public function getImageUrl(){
        if( !$this->isHasFile($this->attribute) ){
            return $this->getDefaultPhoto() ;
        }

        return \Yii::getAlias( $this->fileUrl . $this->owner->{$this->attribute} ) ;
    }

    public function getFileUrl(){
        if( !$this->isHasFile() ){
            return false ;
        }

        return \Yii::getAlias( $this->fileUrl . $this->owner->{$this->attribute} ) ;
    }

    public function getThumbUrl( $profile ){
        if( !$this->isHasFile($this->attribute) ){
            return $this->getDefaultPhoto() ;
        }

        return \Yii::getAlias( $this->thumbUrl . $this->_getThumbNameByProfile($profile) ) ;
    }

    public function getThumbFileUrl($attribute, $profile = 'thumb', $emptyUrl = null)
    {
        //return $this->getBehavior($attribute)->getThumbUrl('thumb');
        return $this->getFileUrl();
    }

    public function getFilePath(){
        if( !$this->isHasFile() ){
            return false ;
        }

        return \Yii::getAlias( $this->filePath . $this->owner->{$this->attribute} ) ;
    }

}
