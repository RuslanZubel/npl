<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'homeUrl' => '/admin',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/admin',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/admin',
            'rules' => [

//                Ручная настройка маршрутов
//                'about' => 'site/about',
//                'contact' => 'site/contact',

                '<action:(about|contact|login)>' => 'site/<action>',

//                '<module>/<controller>/<action>/<id:\d+>' => '<module>/<controller>/<action>',
            ],
        ],
//        'assetManager' => [
//            'appendTimestamp' => true,
//            'assetMap' => [
//                'jquery.js' => '@web/js/jquery-2.2.5.js',
//            ]
//        ],
    ],
    'modules' => [
        'content' => [
            'class' => 'backend\modules\content\Content',
        ],
        'mediafolder' => [
            'class' => 'backend\modules\mediafolder\MediaFolder',
        ],
        'results' => [
            'class' => 'backend\modules\results\Results',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],

    ],
    'params' => $params,
];
