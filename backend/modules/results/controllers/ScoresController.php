<?php

namespace backend\modules\results\controllers;

use backend\models\Races;
use backend\models\RelTeamsRelSeasonsLeagues;
use backend\models\Stages;
use backend\models\Stickers;
use Yii;
use backend\models\Scores;
use backend\modules\results\models\ScoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use richardfan\sortable\SortableAction;

/**
 * ScoresController implements the CRUD actions for Scores model.
 */
class ScoresController extends Controller
{

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => Scores::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Scores models.
     * @return mixed
     */
    public function actionIndex($stage = NULL, $seasonLeague = NULL)
    {
        $currentStage = Stages::findOne($stage);
        $stages[] = $currentStage;
        $stageRaces = Races::find()->where(['fk_stages' => $stage])->orderBy(['sort'=>SORT_ASC])->all();
        $stageTeams = RelTeamsRelSeasonsLeagues::find()->where(['fk_rel_seasons_leagues' => $currentStage->fk_rel_seasons_leagues])->all();

        \common\components\ScoreCounter::getScoreData($stageTeams, $stages, Races::className(), Scores::className(), $stageTeamRacesScores, $stageTeamScores, $teamWholeScore, $teamAllScore, $raceRates);

        asort($teamWholeScore);

        return $this->render('index', [
            'currentStage' => $currentStage,
            'stageRaces' => $stageRaces,
            'stageTeams' => $stageTeams,
            'stageTeamRacesScores' => $stageTeamRacesScores,
            'stageTeamScores' => $stageTeamScores,
            'teamWholeScore' => $teamWholeScore,
        ]);
    }

    /**
     * Displays a single Scores model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Scores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Scores();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Scores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Scores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Scores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Scores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Scores::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEditRaceData(){
        $raceId = (int)\Yii::$app->request->post('raceId');
        $relTeamId = (int)\Yii::$app->request->post('relTeamId');

        $currentScore = Scores::find()->where(['fk_races' => $raceId, 'fk_teams_rel_seasons_leagues' => $relTeamId ])->One();

        $race = Races::findOne($raceId);
        $team = RelTeamsRelSeasonsLeagues::findOne($relTeamId);

        $stickers = Stickers::find()->where(['fk_leagues' => $team->fkRelSeasonsLeagues->fk_leagues] )->all();

        return $this->renderAjax('_edit_race_data', [
            'currentScore' => $currentScore,
            'race' => $race,
            'team' => $team,
            'stickers' => $stickers,
        ]);
    }

    public function actionSaveRaceData() {
        $raceId = (int)\Yii::$app->request->post('raceId');
        $relTeamId = (int)\Yii::$app->request->post('relTeamId');
        $scoreCount = (\Yii::$app->request->post('scoreCount'));
        $stickerType = (int)(\Yii::$app->request->post('stickerType'));
        $stickerComment = (\Yii::$app->request->post('stickerComment'));
        $stickerScoreModifier = (\Yii::$app->request->post('stickerScoreModifier'));
        //print_r($stickerScoreModifier);
        //die;

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        try {

            $score = Scores::find()->where(['fk_races' => $raceId, 'fk_teams_rel_seasons_leagues' => $relTeamId ])->One();
            if (!$score) {
                $score = new Scores();
                $score->fk_races = $raceId;
                $score->fk_teams_rel_seasons_leagues = $relTeamId;
                $score->is_active = 'yes';
            }

            $score->score_count = $scoreCount;
            if ($stickerType == 0) {
                $score->fk_stickers = null;
            } else {
                $score->fk_stickers = $stickerType;
            }
            $score->sticker_comment = $stickerComment;
            $score->sticker_score_modifier = $stickerScoreModifier;

            //print_r($score);
            //die;
            //$race->created_at = date('Y-m-d H:i:s');
            if ($score->save() === false) {
                $answerJSON['error'] = 'Ошибка сохранения данных';
                $answerJSON['error'] .= print_r($score->getErrors(), true);
            }
        } catch(\Exception $ex) {
            $answerJSON['error'] = 'Ошибка сохранения данных (exception)';
        }

        return $this->asJson($answerJSON);
    }
}
