<?php

namespace backend\modules\results\controllers;

use Yii;
use backend\models\Races;
use backend\models\Stages;
use backend\modules\results\models\RacesSearch;
use backend\modules\content\components\BaseContentController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use richardfan\sortable\SortableAction;
use yii\data\ActiveDataProvider;

/**
 * RacesController implements the CRUD actions for Races model.
 */
class RacesController extends BaseContentController
{

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => Races::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Races models.
     * @return mixed
     */
    public function actionIndex($stage)
    {
        $searchModel = new RacesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Races model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Races model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Races();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Races model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Races model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Races model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Races the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Races::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEditRaces(){
        $fkStages = (int)\Yii::$app->request->post('fkStages');

        $firstAct = true;
        if ($fkStages == 0) {
            $fkStages = Yii::$app->request->queryParams['fkStages'];
            $firstAct = false;
        }

        $stage = Stages::findOne($fkStages);

        $members = Races::find()
            ->where(['fk_stages' => $fkStages])
            ->asArray()
            ->all();
        ;

        $providerParams = array();
        $providerParams['query'] = Races::find()
            ->where(['fk_stages' => $fkStages]);
        $providerParams['sort'] = [
            'defaultOrder'=>[
                'sort' => SORT_ASC
            ]
            ];

        if ($firstAct) {
            $providerParams['pagination'] = [
                'route' => '/results/races/edit-races',
                'pageSize' => 10,
                'params'=>['fkStages'=>$fkStages]
                ];
        } else {
            $providerParams['pagination'] = [
                'route' => '/results/races/edit-races',
                'pageSize' => 10,
            ];
        }

        $providerParams['pagination'] = false;

        $dataProvider = new ActiveDataProvider($providerParams);

        return $this->renderAjax('_edit_races', [

            'dataProvider' => $dataProvider,
            'members' => $members,
            'fkStages'=> $fkStages,
            'stage' => $stage,
        ]);
    }

    public function actionAddRace() {
        $fkStages = (int)\Yii::$app->request->post('fkStages');
        $title = strval(\Yii::$app->request->post('title'));

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        try {
            $race = new Races();
            $race->fk_stages = $fkStages;
            $race->title = $title;
            $race->is_active = 'no';
            $race->created_at = date('Y-m-d H:i:s');
            if ($race->save() === false) {
                $answerJSON['error'] = 'Ошибка сохранения данных';
                $answerJSON['error'] .= print_r($race->getErrors(), true);
            }
        } catch(\Exception $ex) {
            $answerJSON['error'] = "Ошибка сохранения данных: {$ex->getMessage()}";
        }

        return $this->asJson($answerJSON);
    }

    public function actionRemoveRace() {
        $raceId = (int)\Yii::$app->request->post('raceId');

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        try {

            $race = Races::findOne($raceId);
            if ($race) {
                if ($race->delete() === false) {
                    $answerJSON['error'] = 'Ошибка удаления данных';
                }
            }
        } catch(\Exception $ex) {
            $answerJSON['error'] = 'Ошибка удаления данных (exception)';
        }

        return $this->asJson($answerJSON);
    }

    public function actionEditRaceModal(){

        $model = Races::find()->where(['id' => \Yii::$app->request->post('id')])->one();

        return $this->renderAjax('_race_form', compact('model'));
    }

    public function actionUpdateRace($id)
    {
        $model = Races::find()->where(['id'=> $id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson(['status' => 'ok']);
        }

        return $this->asJson(['status' => 'fail']);
    }

    public function actionMakeCorrective() {
        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        $stageId = (int)\Yii::$app->request->post('stageId');
        $raceId = (int)\Yii::$app->request->post('raceId');

        $stage = Stages::findOne($stageId);
        if (!$stage) {
            $answerJSON['error'] = 'Этап не найден';
            return $this->asJson($answerJSON);
        }

        $race = Races::findOne($raceId);
        if (!$race) {
            $answerJSON['error'] = 'Гонка не найдена';
            return $this->asJson($answerJSON);
        }

        $races = Races::find()->where(['fk_stages' => $stageId])->all();

        foreach ($races as $race) {
            if ($race->id == $raceId) {
                $race->is_corrective = "yes";
            } else {
                $race->is_corrective = "no";
            }
            if ($race->save() === false) {
                $answerJSON['error'] = 'Ошибка работы с БД';
                break;
            }
        }

        return $this->asJson($answerJSON);
    }

}
