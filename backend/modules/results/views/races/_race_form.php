<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use kartik\datetime\DateTimePicker;
use yii\helpers\Url;
use kato\DropZone;

$idForm = \common\components\UniqueString::getUniqueString(5);

$this->registerJs("

    $('#" . $idForm . "').on('beforeSubmit', function() {
        var form = $(this);
            $.post(
                '" . Url::toRoute([ '/results/races/update-race', 'id' => $model->id ])  . "', 
                form.serializeArray(),
                function( data )
                {
                    if (data.status == 'ok') {
                        $('#edit-stage-race-modal').modal('hide');
                        $.pjax.reload('#stages-pjax');
                        close_race_modal();
                    } else {
                        alert('Непредвиденная ошибка');
                    }
                } 
            ); 
        return false;
    })

")


?>

<div class="stages-form">

    <?php $form = ActiveForm::begin(['id' => $idForm]); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Название этапа') ?>
                    <?= $form->field($model, 'track_url')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
                        'id' => \common\components\UniqueString::getUniqueString(5),
                        'options' => [
                            'rows' => 16,
                        ],
                        'settings' => [
                            'object_resizing' => false,
                            'image_dimensions' => false,
                            'image_class_list' => [
                                ['title' => 'Responsive', 'value' => 'img-responsive']
                            ],
                        ],
                        'language' => 'ru',
                        'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
                        'fileManager' => [
                            'class' => TinyMceElFinder::className(),
                            'connectorRoute' => '/el-finder/connector',
                        ],
                    ]);
                    ?>

                </div>

                <div class="col-xs-12 col-sm-4">
                    <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
                    <?= $form->field($model, 'is_corrective')->dropDownList([ 'yes' => 'Yes', 'no' => 'No', ], ['prompt' => '']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
