<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use backend\components\SortableGridView;

$this->registerJs("

//TODO: не осилил обновление модального диалога редактирования списка партнёров для команды в рамках сезона - сделал как смог...
function reload_stages_races() {
    var popup = $('#stages-races-modal');
    $.post(
        '" . Url::toRoute('/results/races/edit-races') . "', 
            {'fkStages': popup.attr('rel')}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
}

function add_stage_race() {
var title = $('#race_title').val();
    $.post('" . Url::toRoute('/results/races/add-race') . "', 
    {'fkStages': " . $fkStages . ",
     'title' : title},
    function( resp ){
        if (resp.error == '') {
            reload_stages_races();
        } else {
            alert(data.error);
        }
    });
};

function remove_stage_race(id) {
    krajeeDialogCust.confirm('Удалить данную гонку и все связанную с ней информацию об очках?', function(out){
            if(out) {
                $.post('" . Url::toRoute('/results/races/remove-race') . "', 
                {'raceId' : id},
                function( resp ){
                    if (resp.error == '') {
                        reload_stages_races();
                    } else {
                        alert(data.error);
                    }   
                });
        }});
};

$(document).on('click', '.stage-race-edit-button',function( e ) {
    var th = $(this);
    var popup = $('#edit-stage-race-modal');
    popup.find('.modal-body').text('. . . . . .');
    popup.modal('show');
    $.post(
        '" . Url::toRoute('/results/races/edit-race-modal') . "', 
        {'id': th.attr('rel')}, 
        function( content ){
            popup.find('.modal-body').html( content );
        } 
    ); 
});

function close_race_modal() {
    var popup = $('#edit-stage-race-modal');
    popup.modal('hide');
    reload_stages_races();
}

function make_race_corrective(raceId) {
    $.post(
    '" . Url::toRoute('/results/races/make-corrective') . "', 
        {
            'stageId': {$stage->id},
            'raceId': raceId,
        },
        function( content ){
            reload_stages_races();
        }
    );
}

");

?>




<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h1 class="modal-title">Редактирование списка гонок этапа: <?= $stage->title ?>
        </div>
        <div class="modal-body">
            <div class="row">
                <?php $form = ActiveForm::begin(); ?>
                <div class="col-xs-6 col-sm-6">
                    <div><input type="text" value="" placeholder="Введите наименование гонки" name="race_title" id="race_title"></div>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div class="form-group">
                        <p><?= Html::a('Добавить', null, ['class' => 'btn btn-primary', 'onclick' => 'add_stage_race();']) ?></p>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div>


                <?php Pjax::begin(['id' => 'stages-races-pjax', 'enablePushState' => false]); ?>
                <?= SortableGridView::widget([
                    'id' => \common\components\UniqueString::getUniqueString(6),
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n{pager}",
                    'pager' => [
                        'firstPageLabel' => '« «',
                        'lastPageLabel' => '» »',
                    ],
                    'sortUrl' => Url::to(['sortItem']),
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'title',
                        [
                            'attribute' => 'is_corrective',
                        ],
                        [
                            'attribute' => 'is_active',
                            'value' => function($item) {
                                return \backend\components\ActiveToggle::getView($item);
                            },
                            'format' => 'raw',
                        ],
                        //'sort',
                         [
                             'options' => ['width' => '60px'],
                            'class' => 'yii\grid\ActionColumn',
                            'template'=>'{edit}{make_corrective}{delete}',
                            'buttons'=>[
                                'edit' => function ($url, $item){
                                    /*return Html::button(
                                        '<span class="glyphicon glyphicon-pencil pr5 cp stage-race-edit-button"></span>',
                                        [
                                            'title' => Yii::t('yii', 'Редактировать'),
                                            'rel' => $item->id,
                                        ]
                                    );*/
                                    return Html::button('Описание', ['class' => 'btn btn-xs btn-default stage-race-edit-button', 'rel' => $item->id]);
                                },
                                'make_corrective' => function ($url, $item) {
                                    if ($item->is_corrective == "no" || $item->is_corrective == null) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-ok-circle pr5 cp"></span>',
                                            null,
                                            [
                                                'title' => Yii::t('yii', 'Сделать корректирующей'),
                                                'onclick' => "make_race_corrective({$item->id}, {$item->id});"
                                            ]
                                        );
                                    }

                                    return "";
                                },
                                'delete' => function ($url, $item){
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-trash pr5 cp"></span>',
                                        null,
                                        [
                                            'title' => Yii::t('yii', 'Удалить'),
                                            'onclick' => "remove_stage_race({$item->id});"
                                        ]
                                    );
                                },
                            ]
                        ],
                    ],
                ]); ?>


                <?php Pjax::end(); ?>

            </div>

            <div style="padding-left: 20px">Здесь вы можете редактировать список гонок этапа</div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="edit-stage-race-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-label="Close"><span aria-hidden="true" onclick="close_race_modal();">&times;</span>
                </button>
                <h4 class="modal-title">Редактирование гонки этапа</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
