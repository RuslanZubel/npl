<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Races */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="races-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

               
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= \backend\components\widgets\crud_buttons\CrudButtons::widget(['model' => $model]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
