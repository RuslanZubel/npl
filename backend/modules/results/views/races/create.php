<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Races */

$this->title = 'Создание Races';
?>
<div class="races-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
