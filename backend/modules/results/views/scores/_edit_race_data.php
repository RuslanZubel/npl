<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use richardfan\sortable\SortableGridView;

$this->registerJs("

//TODO: не осилил обновление модального диалога редактирования списка партнёров для команды в рамках сезона - сделал как смог...
function reload_stages_races() {
    var popup = $('#stages-races-modal');
    $.post(
        '" . Url::toRoute('/results/races/edit-races') . "', 
            {'fkStages': popup.attr('rel')}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
}

function save_race_data() {
var scoreCount = $('#scoreCount').val();
var rates = document.getElementsByName('sticker');
var stickerType = 0;
for(var i = 0; i < rates.length; i++){
    if(rates[i].checked){
        stickerType = rates[i].value;
    }
}

var stickerComment = $('#stickerComment').val();
var stickerScoreModifier = $('#stickerScoreModifier').val();
//return;
    $.post('" . Url::toRoute('/results/scores/save-race-data') . "', 
    {
    'raceId': " . $race->id . ",
    'relTeamId': " . $team->id . ",
     'scoreCount' : scoreCount,
     'stickerType' : stickerType,
     'stickerComment' : stickerComment,
     'stickerScoreModifier' : stickerScoreModifier,
     },
    function( resp ){
        //var popup = $('#team-partners-modal');
        //popup.attr('reload')();
        reload_stages_races();
        close_race_data_modal();
    });
};

function close_race_data_modal() {
    var popup = $('#edit-race-data-modal');
    popup.modal('hide');
    $.pjax.reload('#stages-pjax');
}

");

?>




<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" onclick="close_race_data_modal()" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h1 class="modal-title">Редактирование результата гонки: <?= $race->title ?>, команды:  <?= $team->fkTeams->title ?></h1>
        </div>
        <div class="modal-body">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-xs-4 col-sm-4">
                    <div>
                        <label for='score_count'>Очки</label>
                        <input type="text" value="<?php
                            if ($currentScore) {
                                echo $currentScore->score_count;
                            }
                            ?>" placeholder="Введите количество очков" name="scoreCount" id="scoreCount">
                    </div>
                    <?php
                    echo "<div id='sticker_type'><br>";
                    echo "<input type='radio' id='sticker_type_0' name='sticker' value='0'>";
                    echo "<label for='sticker_type_0'>Нет стикера</label>";
                    foreach ($stickers as $sticker) {
                        echo "<br>";
                        echo "<input type='radio' id='sticker_type_{$sticker->id}' name='sticker' value='{$sticker->id}' ";
                        if ($currentScore && $sticker->id == $currentScore->fk_stickers) {
                            echo 'checked';
                        }
                        echo ">";
                        echo "<label for='sticker_type_{$sticker->id}'><font color='{$sticker->color}'>{$sticker->description}</font></label>";
                    }
                    echo "<br></div>";
                    ?>
                </div>
                <div class="col-xs-8 col-sm-8">
                    <label for='sticker_modifier'>Модификатор стикера</label>
                    <input type="text" id="stickerScoreModifier" value="<?php
                        if ($currentScore) {
                            echo $currentScore->sticker_score_modifier;
                        }
                        ?>" placeholder="Модификатор стикера" name="stickerScoreModifier"><br/>
                    <label for='sticker_comment'>Комментарий к стикеру</label>
                    <textarea id="stickerComment" placeholder="Комментарий к стикеру" name="stickerComment" rows="4" cols="30"><?php
                        if ($currentScore) {
                            echo $currentScore->sticker_comment;
                        }
                        ?></textarea>
                </div>
            </div>
            <div class="row">
                <br/>
                <div class="col-xs-12 col-sm-12" align="center">
                    <div class="form-group">
                        <p><?= Html::a('Сохранить', null, ['class' => 'btn btn-primary', 'onclick' => 'save_race_data();']) ?></p>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

            <div>

            </div>

            <div style="padding-left: 20px">Здесь вы можете указать количество очков, указать стикер и добавить комментарий</div>
        </div>
    </div>
</div>
