<?php
/**
 * Created by PhpStorm.
 * User: codepug
 * Date: 13/05/2018
 * Time: 16:01
 */


use yii\helpers\Html;
use yii\helpers\Url;
use richardfan\sortable\SortableGridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\RacesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->registerJs("

$(document).on('click', '.edit-race-data-button',function( e ) {
    var th = $(this);
    var popup = $('#edit-race-data-modal');
    popup.find('.modal-body').text('. . . . . .');
    popup.modal('show');
    $.post(
        '" . Url::toRoute('/results/scores/edit-race-data') . "', 
        {
        'raceId': th.attr('raceId'), 
        'relTeamId': th.attr('relTeamId')
        }, 
        function( content ){           
            popup.find('.modal-ajax').html( content );
        } 
    ); 
});

");


$this->title = 'Таблица результатов этапа: ' . $currentStage->title;

//взято из app.css во frontend
$this->registerCss("
.table-result {
    width: 100%;
    border-collapse: collapse;
}
.table-result tr:not(:last-child) {
    border-bottom: 1px solid #787e94;
}
.table-result tr td {
    padding: 5px;
    text-align: center;
    color: #fff;
}
.table-result tr td:not(:last-child) {
    border-right: 1px solid #787e94;
}
.table-result tr:nth-child(1) td {
    background: #1f294e;
}
.table-result tr td:nth-child(1) {
    width: 1%;
}
.table-result tr td:nth-child(2) {
    width: 20%;
    text-align: left;
}
.table-result tr td:nth-child(1), .table-result tr td:nth-child(2) {
    background: #1f294e !important;
    color: #fff !important;
}
.table-result tr:not(:first-child) td {
    background: #fff;
    color: #1f294e;
}
.table-result tr:nth-child(even) td {
    background: #dedee4;
}
.table-result--notice {
    width: 800px;
    margin: 30px auto;
    padding: 20px;
    background: #fff;
    color: #000;
    font-size: 20px;
    font-style: italic;
}
");

$seasonTitle = ($currentStage->fkRelSeasonsLeagues && $currentStage->fkRelSeasonsLeagues->fkSeasons) ? $currentStage->fkRelSeasonsLeagues->fkSeasons->title : "";
$leagueTitle = ($currentStage->fkRelSeasonsLeagues && $currentStage->fkRelSeasonsLeagues->fkLeagues) ? $currentStage->fkRelSeasonsLeagues->fkLeagues->title : "";
$relSeasonLeagueId = $currentStage->fkRelSeasonsLeagues ? $currentStage->fkRelSeasonsLeagues->id : 0;

$this->params['breadcrumbs'][] = ['label' => 'Сезоны лиг', 'url' => ['/content/rel-seasons-leagues']];
$this->params['breadcrumbs'][] = ['label' => 'Сезон: ' . $seasonTitle . ', лига: ' . $leagueTitle, 'url' => ['/content/rel-seasons-leagues/update', 'id' => $relSeasonLeagueId]];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">

    <?php \yii\widgets\Pjax::begin(['id' => 'stages-pjax']); ?>
    <table class="table table-striped table-hover table-result">
        <tr><td>Место</td><td>Команды</td>
        <?php
            $correctiveRace = null;
            foreach ($stageRaces as $race) {
                if ($race->is_corrective == "no" || $race->is_corrective == null) {
                    echo "<td>{$race->title}</td>";
                } else {
                    $correctiveRace = $race;
                }
            }

            echo "<td width='50px'></td>";
            echo "<td>Сумма</td>";

            if ($correctiveRace) {
                echo "<td>{$correctiveRace->title}</td>";
            }

        ?>
        </tr>
        <?php
        $correctiveRace = null;
        $index = 0;
        foreach($teamWholeScore as $teamId => $scoreSum) {
            $team = null;
            foreach($stageTeams as $stageTeam) {
                if ($stageTeam->id == $teamId) {
                    $team = $stageTeam;
                }
            }
            if ($team == null) {
                die("Ошибка формирования данных");
            }
            $index++;
            echo "<tr><td>{$index}</td><td>{$team->fkTeams->title}</td>";
            foreach ($stageRaces as $race) {
                if ($race->is_corrective == "no" || $race->is_corrective == null) {
                    echo "<td title='Щёлкните ЛКМ для редактирования' class='edit-race-data-button' raceId='{$race->id}' relTeamId='{$team->id}'  style='background-color: {$stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['sticker']}'><div>{$stageTeamRacesScores[$currentStage->id][$team->id][$race->id]['score']}</div></td>";
                } else {
                    $correctiveRace = $race;
                }
            }

            echo "<td width='50px'></td>";
            echo "<td>{$teamWholeScore[$team->id]}</td>";

            if ($correctiveRace) {
                echo "<td title='Щёлкните ЛКМ для редактирования' class='edit-race-data-button' raceId='{$correctiveRace->id}' relTeamId='{$team->id}' style='background-color: {$stageTeamRacesScores[$currentStage->id][$team->id][$correctiveRace->id]['sticker']}'><div>{$stageTeamRacesScores[$currentStage->id][$team->id][$correctiveRace->id]['score']}</div></td>";
            }
        }
        ?>


    </table>
    <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="edit-race-data-modal">
    <div class="modal-ajax">

    </div>
</div>

