<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model backend\models\Stickers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stickers-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">


    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'например, Штрафные баллы'] ) ?>

    <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
        'options' => ['placeholder' => 'Выберите цвет для легенды таблицы ...'],
    ]) ?>

    <?php echo $form->field($model, 'fk_leagues')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\backend\models\Leagues::find()->orderBy('title ASC')->all(), 'id', 'title'),
        'options' => ['placeholder' => 'Выберите лигу'],
        'pluginOptions' => [
            'allowClear' => false
        ],
    ])->label('Лига'); ?>





    <?= $form->field($model, 'is_active')->dropDownList([ 'yes' => 'Yes', 'no' => 'No', ], ['prompt' => '']) ?>



        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
