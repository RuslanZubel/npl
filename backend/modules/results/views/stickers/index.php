<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\StickersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Стикеры';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stickers-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-6 buttons-right">
            <?=             Html::a('Создать стикер'            , ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


    <?php Pjax::begin(); ?>            <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => '« «',
            'lastPageLabel' => '» »',
        ],
        'sortUrl' => Url::to(['sortItem']),
//        'filterModel' => $searchModel,
        'columns' => [
        //    ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'color',
                'contentOptions' => [''],
                'options' => ['width' => '7%'],
                'format' => 'html',
                'value' => function ($data) {

                    return '<div style="background-color: ' . $data->color . '; width: 20px; height: 20px;"></div>';
                }
                
            ],

            'description',

            [
                'attribute' => 'fk_leagues',
                'value' => function ($data) {
                    return !empty($data->fkLeagues) ? $data->fkLeagues->title : 'Не определен';
                },
            ],
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return \backend\components\ActiveToggle::getView($item);
                },
                'format' => 'raw',
            ],
//            'sort',
            // 'created_at',
            // 'updated_at',
            // 'is_active',
            // 'fk_author',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        ]); ?>
    

            <?php Pjax::end(); ?>        </div>
    </div>
</div>