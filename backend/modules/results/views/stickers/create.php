<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Stickers */

$this->title = 'Создание Стикера';

$this->params['breadcrumbs'][] = ['label' => 'Стикеры', 'url' => ['/results/stickers']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stickers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
