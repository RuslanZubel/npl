<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Stickers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Стикеры', 'url' => ['/results/stickers']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stickers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fk_leagues',
            'description',
            'color',
            'sort',
            'created_at',
            'updated_at',
            'is_active',
            'fk_author',
        ],
    ]) ?>

</div>
