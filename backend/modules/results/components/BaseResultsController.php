<?php

namespace backend\modules\results\components;

use backend\components\BaseController;
use yii\web\Controller;

/**
 * Default controller for the `results` module
 */
class BaseResultsController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
