<?php

namespace backend\modules\content\controllers;


use backend\components\actions\SortableAction;
use backend\models\Stages;
use backend\models\Teams;
use backend\modules\content\components\BaseContentController;
use backend\modules\content\components\BaseContentManagerController;
use common\models\AdditionalTrainingsPeriod;
use common\models\Receipt;
use common\models\RelReceiptStage;
use common\models\RelReceiptTraining;
use common\models\Training;
use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii2tech\spreadsheet\Spreadsheet;

class TrainingsController extends BaseContentManagerController
{

    private $mapping = array(
        1 => 'a',
        2 => 'b',
        3 => 'c',
        4 => 'd',
        5 => 'e',
        6 => 'f',
        7 => 'g',
        8 => 'h',
        9 => 'i',

    );

    public function beforeAction($action)
    {
        if ($this->route !== 'site/login') {
            if (Yii::$app->user->isGuest) {
                return $this->redirect('/admin/login');
            }
        }

        if (User::findOne(Yii::$app->user->id)->roles === 'user') {
            return $this->redirect('/admin');

        }

        try {
            return parent::beforeAction($action);
        } catch (BadRequestHttpException $e) {
        }
    }

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => Receipt::className(),
                'orderColumn' => 'sort',
            ],
            'sortItemPartners' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => Receipt::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $preSts = (new \yii\db\Query())
            ->select(['stages.title AS stage_title', 'fk_leagues', 'stages.id AS stage_id', 'start_date', 'fk_seasons'])
            ->from('stages')
            ->leftJoin('rel_seasons_leagues', 'rel_seasons_leagues.id = stages.fk_rel_seasons_leagues');

        $sts = (new \yii\db\Query())
            ->select(['stage_title', 'fk_leagues', 'leagues.title AS league_title', 'stage_id', 'u.start_date', 'fk_seasons'])
            ->from('leagues')
            ->rightJoin(['u' => $preSts], 'u.fk_leagues = leagues.id')
            ->orderBy(['start_date' => SORT_DESC]);

        $finalSts = (new \yii\db\Query())
            ->select(['stage_title', 'fk_leagues', 'league_title', 'stage_id', 's.start_date', 'seasons.title AS season_title'])
            ->from('seasons')
            ->rightJoin(['s' => $sts], 's.fk_seasons = seasons.id')
            ->orderBy(['start_date' => SORT_DESC])
            ->all();


        $stagesProvider = new ArrayDataProvider([
            'allModels' => $finalSts,
            'sort' => [
                'attributes' => ['id'],
            ],
            'pagination' => [
                'pageSize' => 15,
            ],
        ]);
        return $this->render('index', [
            'stages' => $stagesProvider,
        ]);
    }

    public function actionView($stage_id)
    {

        $stage = Stages::findOne($stage_id);
        $trainings = Training::find()->where(['stage_id' => $stage_id])->all();

        $func = function ($tr) {
            return array(
                'title' => Teams::findOne($tr->team_id)->title,
                'id' => $tr->id,
                'start' => $tr->start_datetime,
                'end' => $tr->end_datetime,
                'resourceId' => $this->mapping[$tr->boat_number]
            );
        };


        $formattedTrainings = array_map($func, $trainings);

        $grouped = array();
        foreach ($trainings as $training) {
            $grouped[$training->start_datetime][] = $training;
        }
        $stage->start_date = (int)strtotime($stage->start_date) * 1000;

        return $this->render('show', [
            'events' => json_encode($formattedTrainings),
            'grouped' => json_encode($grouped),
            'myEvents' => json_encode([]),
            'action' => 'create',
            'receipt' => null,
            'stage' => $stage
        ]);
    }

    public function actionAdditionalTrainings($stage_id)
    {

        $additionalTrainingsDays = AdditionalTrainingsPeriod::find()->where(['stage_id' => $stage_id]);
        $additionalTrainingsDaysProvider = new ActiveDataProvider([
            'query' => $additionalTrainingsDays,
            'pagination' => [
                'pageSize' => 15,
            ],
            'sort' => [
                'defaultOrder' => [
                    'start_date' => SORT_ASC
                ]
            ]
        ]);

        $stage = Stages::findOne($stage_id);
        $trainings = Training::find()
            ->where(['stage_id' => $stage_id])
//            ->where(['>', 'cost', 0])
            ->all();

        $func = function ($tr) {
            return array(
                'title' => Teams::findOne($tr->team_id)->title,
                'id' => $tr->id,
                'start' => $tr->start_datetime,
                'end' => $tr->end_datetime,
                'resourceId' => $this->mapping[$tr->boat_number]
            );
        };


        $formattedTrainings = array_map($func, $trainings);

        $grouped = array();
        foreach ($trainings as $training) {
            $grouped[$training->start_datetime][] = $training;
        }
        $stage->start_date = (int)strtotime($stage->start_date) * 1000;

        return $this->render('additional_trainings', [
            'events' => json_encode($formattedTrainings),
            'grouped' => json_encode($grouped),
            'myEvents' => json_encode([]),
            'action' => 'create',
            'receipt' => null,
            'stage' => $stage,
            'days' => $additionalTrainingsDaysProvider
        ]);
    }

    public function actionMainTrainings($stage_id)
    {

        $stage = Stages::findOne($stage_id);
        $trainings = Training::find()
            ->where(['stage_id' => $stage_id])
            ->where(['cost' => 0])
            ->all();

        $func = function ($tr) {
            return array(
                'title' => Teams::findOne($tr->team_id)->title,
                'id' => $tr->id,
                'start' => $tr->start_datetime,
                'end' => $tr->end_datetime,
                'resourceId' => $this->mapping[$tr->boat_number]
            );
        };


        $formattedTrainings = array_map($func, $trainings);

        $grouped = array();
        foreach ($trainings as $training) {
            $grouped[$training->start_datetime][] = $training;
        }
        $stage->start_date = (int)strtotime($stage->start_date) * 1000;

        return $this->render('free', [
            'events' => json_encode($formattedTrainings),
            'grouped' => json_encode($grouped),
            'myEvents' => json_encode([]),
            'action' => 'create',
            'receipt' => null,
            'stage' => $stage
        ]);
    }


    public function actionExportTrainings($stage_id, $date, $main)
    {
        $trainings = null;

        if ($main === false || $main === 'false') {
            $trainings = Training::find()
                ->where(['stage_id' => $stage_id])
                ->andWhere('cost > 0')
                ->andWhere('to_days(start_datetime) = to_days("' . $date . '")')
                ->all();
        } else {
            $trainings = Training::find()->where(['stage_id' => $stage_id, 'cost' => 0])->all();
        }

        $func = function ($tr) {
            return array(
                'title' => Teams::findOne($tr->team_id)->title,
                'start' => $tr->start_datetime,
                'boat_number' => $tr->boat_number,
                'hour' => 10
            );
        };


        $formattedTrainings = array_map($func, $trainings);
        $groupedByBoats = array();

        for ($i = 1; $i < 10; $i++) {
            $groupedByBoats[] = [
                'boat_number' => $i,
                'ten' => '',
                '10' => '',
                '12' => '',
                '14' => '',
                '16' => ''
            ];
        }

        $final = array();

        foreach ($groupedByBoats as $boatTrs) {
            $prepared = array_filter($formattedTrainings, function ($tr) use ($boatTrs) {
                return ((int)$tr['boat_number']) === ((int)$boatTrs['boat_number']);
            });
            foreach ($prepared as $pr) {
                $boatTrs[(string)date('H', strtotime($pr['start']))] = $pr['title'];
            }
            $final[] = $boatTrs;

        }

        $provider = new ArrayDataProvider([
            'allModels' => $final,
            'sort' => [
                'attributes' => ['boat_number'],
            ],
            'pagination' => [
                'pageSize' => 999,
            ],
        ]);

        $exporter = new Spreadsheet([
            'dataProvider' => $provider,
            'columns' => [
                [
                    'attribute' => 'boat_number',
                    'label' => 'Номер лодки',
                    'contentOptions' => [
                        'alignment' => [
                            'horizontal' => 'center',
                            'vertical' => 'center',
                        ],
                    ],
                ],
                [
                    'attribute' => '10',
                    'label' => '10:00',
                ],
                [
                    'attribute' => '12',
                    'label' => '12:00',
                ],
                [
                    'attribute' => '14',
                    'label' => '14:00',
                ],
                [
                    'attribute' => '16',
                    'label' => '16:00',
                ],
            ],
        ]);
        return $exporter->send('trainings_r.xls');
    }

    public function actionAddAdditionalTrainingPeriod($stage_id)
    {
        $additionalTrainings = (new \yii\db\Query())
            ->select(['id', 'start_date AS start', 'end_date AS end'])
            ->from(AdditionalTrainingsPeriod::tableName())
            ->where(['stage_id' => $stage_id])
            ->all();

        if (Yii::$app->request->post()) {
            if (array_key_exists('events', Yii::$app->request->post())) {
                foreach (Yii::$app->request->post()['events'] as $event) {
                    if (!AdditionalTrainingsPeriod::findOne(['start_date' => $event['start_date'], 'stage_id' => $stage_id])) {
                        $addTr = new AdditionalTrainingsPeriod();
                        $addTr->start_date = $event['start_date'];
                        $addTr->end_date = $event['end_date'];
                        $addTr->stage_id = $stage_id;
                        $addTr->save();
                    }
                }
            }
            if (array_key_exists('removedItemsIds', Yii::$app->request->post())) {
                foreach (Yii::$app->request->post()['removedItemsIds'] as $id) {
                    AdditionalTrainingsPeriod::deleteAll(['id' => $id]);
                }
            }
            return $this->redirect('/admin/content/trainings/additional-trainings?stage_id=' . $stage_id);
        }

        return $this->render('additional_days_selector', [
            'events' => json_encode($additionalTrainings),
            'stage_id' => $stage_id
        ]);
    }


}