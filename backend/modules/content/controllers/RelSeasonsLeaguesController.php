<?php

namespace backend\modules\content\controllers;

use backend\models\Partners;
use backend\models\RelPartnersPartnersStatuses;
use backend\models\RelTeamsRelSeasonsLeagues;
use backend\models\RelTeamsTeamsPartners;
use backend\models\Stages;
use Yii;
use backend\models\RelSeasonsLeagues;
use yii\data\ActiveDataProvider;
use backend\modules\content\components\BaseContentController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use richardfan\sortable\SortableAction;
use backend\models\Teams;


class RelSeasonsLeaguesController extends BaseContentController
{

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => RelSeasonsLeagues::className(),
                'orderColumn' => 'sort',
            ],
            'sortItemPartners' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => RelPartnersPartnersStatuses::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RelSeasonsLeagues::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate()
    {
        $model = new RelSeasonsLeagues();
        $modelId = 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelId' => $modelId,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelId = $model->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {

            $teams = RelTeamsRelSeasonsLeagues::find()
                ->where(['fk_rel_seasons_leagues' => $id])
                ->joinWith('fkTeams')
                ->orderBy(Teams::tableName(). '.title')
                ->asArray()
                ->all();

            $stages = Stages::find()
                ->where([ 'fk_rel_seasons_leagues' => $id ])
                ->asArray()
                ->all();

            $stagesDataProvider = new ActiveDataProvider([
                'query' => Stages::find()->where([ 'fk_rel_seasons_leagues' => $id]),
            ]);

            $partnersDataProvider = new ActiveDataProvider([
                'query' => RelPartnersPartnersStatuses::find()->where([ 'fk_rel_seasons_leagues' => $id]),
                'sort' => ['defaultOrder' => ['sort'=>SORT_ASC ]]

            ]);

            return $this->render('update', [
                'model' => $model,
                'modelId' => $modelId,
                'teams' => $teams,
                'stages' => $stages,
                'stagesDataProvider' => $stagesDataProvider,
                'partnersDataProvider' => $partnersDataProvider,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionAddPartner(){
        $fkRelSeasonsLeagues = \Yii::$app->request->post('fkRelSeasonsLeagues');
        return $this->renderAjax('_partner', [
            'fkRelSeasonsLeagues' => $fkRelSeasonsLeagues,
        ]);
    }

    public function actionAddPartnerUnit() {
        $fkRelSeasonsLeagues = (int)\Yii::$app->request->post('fkRelSeasonLeagues');
        $partnerId = (int)\Yii::$app->request->post('partnerId');
        $partnerStatusId = (int)\Yii::$app->request->post('partnerStatusId');

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        $relSeasonsLeagues = RelSeasonsLeagues::findOne($fkRelSeasonsLeagues);
        if (!$relSeasonsLeagues) {
            $answerJSON['error'] = 'Сезон-лига не найдена';
            return $this->asJson($answerJSON);
        }

        try {
            $relPartnersPartnersStatuses = new RelPartnersPartnersStatuses();
            $relPartnersPartnersStatuses->fk_partners = $partnerId;
            $relPartnersPartnersStatuses->fk_partners_statuses = $partnerStatusId;
            $relPartnersPartnersStatuses->fk_rel_seasons_leagues = $fkRelSeasonsLeagues;
            $relPartnersPartnersStatuses->is_active = 'yes';
            $relPartnersPartnersStatuses->created_at = date('Y-m-d H:i:s');
            if ($relPartnersPartnersStatuses->save() === false) {
                $answerJSON['error'] = 'Ошибка сохранения данных';
                $answerJSON['error'] .= print_r($relPartnersPartnersStatuses->getErrors(), true);
            }
        } catch(\Exception $ex) {
            $exMsg = $ex->getMessage();
            if (strpos($exMsg, 'Duplicate') !== false) {
                $answerJSON['error'] = "Такая пара спонсор-статус уже есть в данной паре сезон-лига";
            } else {
                $answerJSON['error'] = "Ошибка сохранения данных: {$exMsg}";
            }
        }

        return $this->asJson($answerJSON);
    }

    public function actionRemovePartnerUnit() {
        $id = (int)\Yii::$app->request->post('id');

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        try {

            $relPartnersPartnersStatuses = RelPartnersPartnersStatuses::findOne($id);
            if ($relPartnersPartnersStatuses) {
                if ($relPartnersPartnersStatuses->delete() === false) {
                    $answerJSON['error'] = 'Ошибка удаления данных';
                }
            }
        } catch(\Exception $ex) {
            $answerJSON['error'] = "Ошибка удаления данных: {$ex->getMessage()}";
        }

        return $this->asJson($answerJSON);
    }

    public function actionAddStage(){
        $model = new Stages();
        $model->fk_rel_seasons_leagues = \Yii::$app->request->post('fkRelSeasonsLeagues');
        if ($model->save(false)) {
            $t = Stages::find()->where(['id' => $model->id])->asArray()->one();
            return $this->renderAjax('_stage', compact ('t'));
        }
    }

    public function actionEditStageModal(){

        $model = Stages::find()->where(['id' => \Yii::$app->request->post('id')])->one();

        return $this->renderAjax('_stage_form', compact('model'));
    }


    public function actionUpdateStage($id)
    {
        $model = Stages::find()->where(['id'=> $id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson(['status' => 'ok']);
        }

        return $this->asJson(['status' => 'fail']);

    }

    public function actionDeleteStage()
    {
        $id = \Yii::$app->request->post('id');

        if ($model = Stages::find()->where(['id'=> $id])->one()->delete()) {

            return $this->asJson(['status' => 'ok']);

        }

        return $this->asJson(['status' => 'fail']);

    }


    protected function findModel($id)
    {
        if (($model = RelSeasonsLeagues::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
