<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 23/03/2018
 * Time: 16:31
 */

namespace backend\modules\content\controllers;


use Yii;
use backend\models\RelTeamsRelSeasonsLeagues;
use backend\models\Teams;
use backend\models\TeamsStructures;
use backend\modules\content\components\BaseContentController;
use yii\data\ActiveDataProvider;

class TeamsStructuresController extends BaseContentController
{

    public function actionEditStructure(){
        $fkRelTeamsRelSeasonsLeagues = (int)\Yii::$app->request->post('fkRelTeamsRelSeasonsLeagues');
        $teamId = (int)\Yii::$app->request->post('fkTeam');

        $firstAct = true;
        if ($fkRelTeamsRelSeasonsLeagues == 0) {
            $fkRelTeamsRelSeasonsLeagues = Yii::$app->request->queryParams['fkRelTeamsRelSeasonsLeagues'];
            $teamId = Yii::$app->request->queryParams['fkTeam'];
            $firstAct = false;
        }

        $members = TeamsStructures::find()
            ->where(['fk_rel_teams_rel_seasons_league' => $fkRelTeamsRelSeasonsLeagues])
            ->joinWith('fkMembers')
            ->joinWith('fkRoles');

        $providerParams = array();
        $providerParams['query'] = $members;
        /*$providerParams['sort'] = [
            'defaultOrder'=>[
                'sort' => SORT_ASC
            ]
        ];*/

        if ($firstAct) {
            $providerParams['pagination'] = [
                'route' => '/content/teams-structures/edit-structure',
                'pageSize' => 10,
                'params'=>['fkRelTeamsRelSeasonsLeagues'=>$fkRelTeamsRelSeasonsLeagues, 'fkTeam' => $teamId ]
            ];
        } else {
            $providerParams['pagination'] = [
                'route' => '/content/teams-structures/edit-structure',
                'pageSize' => 10,
            ];
        }

        $providerParams['pagination'] = false;

        $dataProvider = new ActiveDataProvider($providerParams);

        $teamTitle = Teams::getById($teamId);

        return $this->renderAjax('_update', [
            'members' => $members,
            'teamTitle' => $teamTitle,
            'fkRelTeamsRelSeasonsLeagues' => $fkRelTeamsRelSeasonsLeagues,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddTeamMember() {
        $fkRelTeamsRelSeasonsLeagues = (int)\Yii::$app->request->post('fkRelTeamsRelSeasonsLeagues');
        $memberUnit = strval(\Yii::$app->request->post('memberUnit'));
        $memberRole = strval(\Yii::$app->request->post('memberRole'));

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        $relTeamsRelSeasonsLeagues = RelTeamsRelSeasonsLeagues::findOne($fkRelTeamsRelSeasonsLeagues);
        if (!$relTeamsRelSeasonsLeagues) {
            $answerJSON['error'] = 'Сезон-лига не найдена';
            return $this->asJson($answerJSON);
        }

        try {
            $teamStructures = new TeamsStructures();
            $teamStructures->fk_rel_teams_rel_seasons_league = $fkRelTeamsRelSeasonsLeagues;
            $teamStructures->fk_members = $memberUnit;
            $teamStructures->fk_roles = $memberRole;
            $teamStructures->created_at = date('Y-m-d H:i:s');
            $teamStructures->is_active = 'yes';
            if ($teamStructures->save() === false) {
                $answerJSON['error'] = 'Ошибка сохранения данных';
                $answerJSON['error'] .= print_r($teamStructures->getErrors(), true);
            }
        } catch(\Exception $ex) {
            $exMsg = $ex->getMessage();
            if (strpos($exMsg, 'Duplicate') !== false) {
                $answerJSON['error'] = "Такая пара участник-должность уже есть в данной команде данной пары сезон-лига";
            } else {
                $answerJSON['error'] = "Ошибка сохранения данных: {$exMsg}";
            }
        }

        return $this->asJson($answerJSON);
    }

    public function actionRemoveTeamMember() {
        $teamStructureId = (int)\Yii::$app->request->post('teamStructureId');

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        try {

            $teamStructures = TeamsStructures::findOne($teamStructureId);
            if ($teamStructures) {
                if ($teamStructures->delete() === false) {
                    $answerJSON['error'] = 'Ошибка удаления данных';
                }
            } else {
                $answerJSON['error'] = "Запись о члене команды не найдена";
            }
        } catch(\Exception $ex) {
            $answerJSON['error'] = "Ошибка удаления данных: {$ex->getMessage()}";
        }

        return $this->asJson($answerJSON);
    }
}