<?php

namespace backend\modules\content\controllers;


use backend\modules\content\components\BaseContentController;
use backend\modules\content\models\CreateUserForm;
use backend\modules\content\models\UsersSearch;
use common\models\Teams;
use common\models\User;
use frontend\models\SignupForm;
use Yii;
use yii\data\ActiveDataProvider;

class UsersController extends BaseContentController
{

    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'=>$searchModel,
        ]);
    }

    public function actionDelete($id)
    {
        $user = User::findOne($id);
        $user->delete();
        return $this->redirect(['index']);
    }

    public function actionCreate()
    {
        $model = new CreateUserForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (!empty($user->id)) {
                    return $this->redirect(['index']);
                }
            }
        }
        $othersUsers = User::find()->where(['roles' => 'user'])->all();
        $usersIds = array_map(function (User $tempUser) {
            return $tempUser->id;
        }, $othersUsers);
        $teams = Teams::find()->where(['not in', 'fk_author', $usersIds])->all();

        return $this->render('create', [
            'model' => $model,
            'teams' => $teams
        ]);
    }

    public function actionEdit($id)
    {
        $model = User::findOne($id);
        if (Yii::$app->request->post()) {
            $model->email = Yii::$app->request->post()['User']['email'];
            $model->setPassword(Yii::$app->request->post()['User']['password']);
            $model->save();
            return $this->redirect(['index']);
        }
        return $this->render('edit', [
            'model' => $model,
        ]);
    }

}