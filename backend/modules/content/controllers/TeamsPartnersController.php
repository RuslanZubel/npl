<?php

namespace backend\modules\content\controllers;

use backend\models\RelTeamsTeamsPartners;
use Yii;
use backend\models\Teams;
use backend\models\TeamsPartners;
use backend\modules\content\models\TeamsPartnersSearch;
use yii\data\ActiveDataProvider;
use backend\modules\content\components\BaseContentController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use richardfan\sortable\SortableAction;
use Gregwar\Image\Image;

/**
 * TeamsController implements the CRUD actions for Teams model.
 */
class TeamsPartnersController extends BaseContentController
{
    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => TeamsPartners::className(),
                'orderColumn' => 'sort',
            ],
            'sortItemTeamsPartners' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => RelTeamsTeamsPartners::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Teams models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*$dataProvider = new ActiveDataProvider([
            'query' => RelTeamsTeamsPartners::find(),
        ]);*/


        $searchModel = new TeamsPartnersSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Teams model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Teams model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TeamsPartners();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Teams model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Teams model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Teams model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Teams the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TeamsPartners::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCropLogo($id) {
        $model = $this->findModel($id);

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }*/

        if (Yii::$app->request->post()) {
            $request = Yii::$app->request;

            $x1 = $request->post('x1');
            $x2 = $request->post('x2');
            $y1 = $request->post('y1');
            $y2 = $request->post('y2');

            $h = $request->post('h');
            $w = $request->post('w');

            $image_height = $request->post('image_height');
            $image_width = $request->post('image_width');

            if (empty($w)) {
                //nothing selected
                throw new \Exception("Выберите часть изображения");
            }
            $filePath = $model->getFilePath('image');
            $img = Image::open( $filePath ) ;
            $resized_width = ((int) $x2) - ((int) $x1);
            $resized_height = ((int) $y2) - ((int) $y1);
            $img->crop((int)$x1, (int)$y1, $resized_width, $resized_height);
            $img->scaleResize(345 ,345);
            $img->save($filePath, 'guess') ;

            return $this->redirect(['update', 'id' => $id]);
        }

        return $this->render('crop-logo', [
            'model' => $model,
        ]);
    }

    public function actionEditPartners(){

        $id = (int)\Yii::$app->request->post('fkRelTeamsRelSeasonsLeagues');
        $teamId = (int)\Yii::$app->request->post('fkTeam');

        $members = RelTeamsTeamsPartners::find()
            ->orderBy(RelTeamsTeamsPartners::tableName(). '.sort')
            ->where(['fk_rel_teams_rel_seasons_league' => $id])
            ->joinWith('fkTeamsPartners')
            //->joinWith('fkRoles')
            ->asArray()
            ->all();
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => RelTeamsTeamsPartners::find()
                ->orderBy(RelTeamsTeamsPartners::tableName(). '.sort')
                ->where(['fk_rel_teams_rel_seasons_league' => $id])
                ->joinWith('fkTeamsPartners'),
               'sort' => ['defaultOrder' => ['sort'=>SORT_ASC ]],
          //  'sort' => ['defaultOrder' => [RelTeamsTeamsPartners::tableName().'.sort'=>SORT_ASC ]]
        ]);

        /*echo "<pre>";
        print_r($members);
        die;*/

        $teamTitle = Teams::getById($teamId);

        return $this->renderAjax('_update', [
            'dataProvider' => $dataProvider,
            'members' => $members,
            'teamTitle' => $teamTitle,
            'fkRelTeamsRelSeasonsLeagues'=> $id,

        ]);
    }

    public function actionAddPartner() {
        $id = (int)\Yii::$app->request->post('fkRelTeamsRelSeasonsLeagues');
        $partnerId = (int)\Yii::$app->request->post('partnerId');

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        try {
            $partner = new RelTeamsTeamsPartners();
            $partner->fk_rel_teams_rel_seasons_league = $id;
            $partner->fk_teams_partners = $partnerId;
            if ($partner->save() === false) {
                $answerJSON['error'] = 'Ошибка сохранения данных';
            }
        } catch(\Exception $ex) {
            $exMsg = $ex->getMessage();
            if (strpos($exMsg, 'Duplicate') !== false) {
                $answerJSON['error'] = "Такой партнёр уже привязан к команде в сезон-лиге";
            } else {
                $answerJSON['error'] = "Ошибка сохранения данных: {$exMsg}";
            }
        }

        return $this->asJson($answerJSON);
    }

    public function actionRemovePartner() {
        $id = (int)\Yii::$app->request->post('fkRelTeamsRelSeasonsLeagues');
        $relId = (int)\Yii::$app->request->post('relId');

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        try {

            $partner = RelTeamsTeamsPartners::findOne($relId);
            if ($partner) {
                if ($partner->delete() === false) {
                    $answerJSON['error'] = 'Ошибка удаления данных';
                }
            }
        } catch(\Exception $ex) {
            $answerJSON['error'] = 'Ошибка удаления данных';
        }

        return $this->asJson($answerJSON);
    }
}
