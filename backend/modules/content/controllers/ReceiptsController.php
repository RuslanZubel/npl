<?php

namespace backend\modules\content\controllers;


use backend\components\actions\SortableAction;
use backend\models\StagesReceiptSearch;
use backend\models\Stages;
use backend\models\Teams;
use backend\models\TrainingsReceiptSearch;
use backend\modules\content\components\BaseContentController;
use backend\modules\content\components\BaseContentManagerController;
use common\models\PayedTraining;
use common\models\Receipt;
use common\models\RelReceiptStage;
use common\models\RelReceiptTraining;
use common\models\Training;
use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii2tech\spreadsheet\Spreadsheet;

class ReceiptsController extends BaseContentManagerController
{

    public function beforeAction($action)
    {
        if ($this->route !== 'site/login') {
            if (Yii::$app->user->isGuest) {
                return $this->redirect('/admin/login');
            }
        }

        if (User::findOne(Yii::$app->user->id)->roles === 'user') {
            return $this->redirect('/admin');

        }

        try {
            return parent::beforeAction($action);
        } catch (BadRequestHttpException $e) {
        }
    }

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => Receipt::className(),
                'orderColumn' => 'sort',
            ],
            'sortItemPartners' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => Receipt::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $searchModel = new StagesReceiptSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $trSearchModel = new TrainingsReceiptSearch();
        $trDataProvider = $trSearchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'trainings' => $trDataProvider,
            'stages' => $dataProvider,
            'searchModel'=>$searchModel,
            'trainingsSearchModel'=>$trSearchModel,

        ]);
    }

    public function actionView($id)
    {
        $receipt = Receipt::findOne($id);

        if ($receipt->product_type === 'training') {
            $relReceiptTrainings = RelReceiptTraining::find()->where(['receipt_id' => $receipt->id])->all();
            $trIds = array_map(function ($rel) {
                return $rel->training_id;
            }, $relReceiptTrainings);
            $trainingDataProvider = new ActiveDataProvider([
                'query' => Training::find()->where(['in', 'id', $trIds]),
            ]);
            return $this->render('training', [
                'dataProvider' => $trainingDataProvider,
            ]);

        }

        if ($receipt->product_type === 'stage') {
            $relReceiptStage = RelReceiptStage::find()->where(['receipt_id' => $receipt->id])->all();
            $stageIds = array_map(function ($rel) {
                return $rel->stage_id;
            }, $relReceiptStage);
            $stageDataProvider = new ActiveDataProvider([
                'query' => Stages::find()->where(['in', 'id', $stageIds]),
            ]);
            $stage = Stages::find()->where(['in', 'id', $stageIds])->all()[0];
            $formattedStage = array([
                'id' => $stage->id,
                'cost' => $stage->cost,
                'title' => $stage->title,
                'team' => Teams::findOne($receipt->team_id)->title,
            ]);
            $provider = new ArrayDataProvider([
                'allModels' => $formattedStage,
                'sort' => [
                    'attributes' => ['id'],
                ],
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            return $this->render('stage', [
                'dataProvider' => $provider,
            ]);

        }
    }

    public function actionCheck($id)
    {
        $receipt = Receipt::findOne($id);
        $receipt->confirmed = true;
        $receipt->save();
        return $this->redirect(['index']);
    }

    public function actionUncheck($id)
    {
        $receipt = Receipt::findOne($id);
        $receipt->confirmed = false;
        $receipt->save();
        return $this->redirect(['index']);
    }

    public function actionDelete($id)
    {
        $receipt = Receipt::findOne($id);
        $relRecTraining = RelReceiptTraining::findOne(['receipt_id' => $id]);
        $training = Training::find()
            ->where(['id' => $relRecTraining->training_id,])
            ->all();
        if (empty($training) || is_null($training) || !$training) {
            return $this->redirect('/personal/training/error');
        }
        else {
            $training = $training[0];
        }
        if ($training->cost > 0 && $receipt->confirmed) {
            $payedTraining = PayedTraining::findOne(['training_id' => $training->id]);
            if (empty($payedTraining) || !$payedTraining || is_null($payedTraining)) {
                $payedTraining = new PayedTraining();
            }
            $payedTraining->team_id = $receipt->team_id;
            $payedTraining->free = true;
            $payedTraining->save();
        }
        $receipt->delete();
        $relRecTraining->delete();
        $training->delete();


        return $this->redirect(['index']);
    }

    public function actionExportTrainings()
    {
        $trsSubQuery = (new \yii\db\Query())
            ->select(['training_id', 'confirmed', 'receipt.id AS receipt_id'])
            ->from('rel_receipt_trainings')
//            ->where(['product_type' => 'training'])
            ->rightJoin('receipt', 'receipt.id = rel_receipt_trainings.receipt_id');

        $trs = (new \yii\db\Query())
            ->select(['training.id', 'start_datetime', 'cost', 'team_id', 'title', 'created_by', 'confirmed', 'boat_number', 'receipt_id'])
            ->from('training')
            ->leftJoin('teams', 'teams.id = training.team_id')
            ->leftJoin(['u' => $trsSubQuery], 'u.training_id = training.id');

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $trs,
            ]),
            'columns' => [
                [
                    'attribute' => 'receipt_id',
                    'label' => 'ID чека',
                    'contentOptions' => [
                        'alignment' => [
                            'horizontal' => 'center',
                            'vertical' => 'center',
                        ],
                    ],
                ],
                [
                    'attribute' => 'title',
                    'label' => 'Команда',
                ],
                [
                    'attribute' => 'cost',
                    'label' => 'Стоимость',
                ],
                [
                    'attribute' => 'start_datetime',
                    'label' => 'Время начала',
                ],
                [
                    'attribute' => 'boat_number',
                    'label' => 'Номер лодки',
                ],
                [
                    'attribute' => 'confirmed',
                    'label' => 'Оплата подтверждена',
                ],
            ],
        ]);
        return $exporter->send('trainings.xls');
    }

    public function actionExportStages()
    {
        $stsSubQuery = (new \yii\db\Query())
            ->select(['confirmed', 'team_id', 'stage_id', 'title','receipt.id AS receipt_id'])
            ->from('receipt')
            ->where(['product_type' => 'stage'])
            ->leftJoin('rel_receipt_stages', 'rel_receipt_stages.receipt_id = receipt.id')
            ->leftJoin('teams', 'teams.id = receipt.team_id');

        $preSts = (new \yii\db\Query())
            ->select(['stages.title AS stage_title', 'u.title AS team_title', 'cost', 'team_id', 'confirmed', 'fk_leagues', 'receipt_id'])
            ->from('stages')
            ->rightJoin(['u' => $stsSubQuery], 'u.stage_id = stages.id')
            ->leftJoin('rel_seasons_leagues', 'rel_seasons_leagues.id = stages.fk_rel_seasons_leagues');

        $sts = (new \yii\db\Query())
            ->select(['stage_title', 'team_title', 'cost', 'team_id', 'confirmed', 'fk_leagues', 'leagues.title AS league_title', 'receipt_id'])
            ->from('leagues')
            ->rightJoin(['u' => $preSts], 'u.fk_leagues = leagues.id');

        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $sts,
            ]),
            'columns' => [
                [
                    'attribute' => 'team_title',
                    'label' => 'Команда',
                    'contentOptions' => [
                        'size'=> 'auto'
                    ],
                ],
                [
                    'attribute' => 'league_title',
                    'label' => 'Лига',
                ],
                [
                    'attribute' => 'stage_title',
                    'label' => 'Этап',
                ],
                [
                    'attribute' => 'confirmed',
                    'label' => 'Оплата подтверждена',
                ],
            ],
        ]);
        return $exporter->send('stages.xls');
    }

}