<?php

namespace backend\modules\content\controllers;

use backend\models\RelTeamsRelSeasonsLeagues;
use backend\models\Stages;
use backend\modules\content\models\RelTeamsRelSeasonsLeaguesSearch;
use Yii;
use backend\models\RelSeasonsLeagues;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use richardfan\sortable\SortableAction;
use backend\models\Teams;


class RelTeamsRelSeasonsLeaguesController extends Controller
{

    public function actions()
    {
        return [
            /*'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => RelTeamsRelSeasonsLeagues::className(),
                'orderColumn' => 'sort',
            ],*/
        ];
    }


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RelSeasonsLeagues::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate()
    {
        $model = new RelSeasonsLeagues();
        $modelId = 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelId' => $modelId,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelId = $model->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {

            $teams = RelTeamsRelSeasonsLeagues::find()
                ->where(['fk_rel_seasons_leagues' => $id])
                ->joinWith('fkTeams')
                ->orderBy(Teams::tableName(). '.title')
                ->asArray()
                ->all();

            $stages = Stages::find()
                ->where([ 'fk_rel_seasons_leagues' => $id ])
                ->asArray()
                ->all();

            $stagesDataProvider = new ActiveDataProvider([
                'query' => Stages::find()->where([ 'fk_rel_seasons_leagues' => $id]),
            ]);

            return $this->render('update', [
                'model' => $model,
                'modelId' => $modelId,
                'teams' => $teams,
                'stages' => $stages,
                'stagesDataProvider' => $stagesDataProvider,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionAddPartner(){
        return $this->renderAjax('_partner');
    }


    public function actionAddStage(){
        $model = new Stages();
        $model->fk_rel_seasons_leagues = \Yii::$app->request->post('fkRelSeasonsLeagues');
        if ($model->save(false)) {
            $t = Stages::find()->where(['id' => $model->id])->asArray()->one();
            return $this->renderAjax('_stage', compact ('t'));
        }
    }


    public function actionEditStageModal(){

        $model = Stages::find()->where(['id' => \Yii::$app->request->post('id')])->one();

        return $this->renderAjax('_stage_form', compact('model'));
    }


    public function actionUpdateStage($id)
    {
        $model = Stages::find()->where(['id'=> $id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson(['status' => 'ok']);
        }

        return $this->asJson(['status' => 'fail']);

    }

    public function actionDeleteStage()
    {
        $id = \Yii::$app->request->post('id');

        if ($model = Stages::find()->where(['id'=> $id])->one()->delete()) {

            return $this->asJson(['status' => 'ok']);

        }

        return $this->asJson(['status' => 'fail']);

    }


    protected function findModel($id)
    {
        if (($model = RelSeasonsLeagues::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEditTeams(){

        $fkRelSeasonsLeagues = (int)\Yii::$app->request->post('relSLId');

        $firstAct = true;
        if ($fkRelSeasonsLeagues == 0) {
            $fkRelSeasonsLeagues = Yii::$app->request->queryParams['relSLId'];
            $firstAct = false;
        }

        $params = Yii::$app->request->queryParams;
        unset($params['relSLId']);

        //print_r(Yii::$app->request->queryParams['fkRelSeasonsLeagues']);
        //die;
        //print_r($fkRelSeasonsLeagues);
        //die;

        $relSeasonsLeagues = RelSeasonsLeagues::findOne($fkRelSeasonsLeagues);
        $searchModel = new RelTeamsRelSeasonsLeaguesSearch();
        $searchModel->fk_rel_seasons_leagues = $fkRelSeasonsLeagues;
        $dataProvider = $searchModel->search($params, $firstAct);

        $teams = Teams::find()->all();

        return $this->renderAjax('_edit_teams', [
            'relSeasonsLeagues' => $relSeasonsLeagues,
            'teams' => $teams,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionAddTeam() {
        $fkRelSeasonsLeagues = (int)\Yii::$app->request->post('fkRelSeasonLeagues');
        $teamId = strval(\Yii::$app->request->post('teamId'));

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        $relSeasonsLeagues = RelSeasonsLeagues::findOne($fkRelSeasonsLeagues);
        if (!$relSeasonsLeagues) {
            $answerJSON['error'] = 'Сезон-лига не найдена';
            return $this->asJson($answerJSON);
        }

        $team = Teams::findOne($teamId);
        if (!$team) {
            $answerJSON['error'] = 'Команда не найдена';
            return $this->asJson($answerJSON);
        }

        try {
            $relTeamRelSeasonsLeagues = new RelTeamsRelSeasonsLeagues();
            $relTeamRelSeasonsLeagues->fk_teams = $teamId;
            $relTeamRelSeasonsLeagues->fk_rel_seasons_leagues = $fkRelSeasonsLeagues;
            $relTeamRelSeasonsLeagues->is_active = 'yes';
            $relTeamRelSeasonsLeagues->created_at = date('Y-m-d H:i:s');
            if ($relTeamRelSeasonsLeagues->save() === false) {
                $answerJSON['error'] = 'Ошибка сохранения данных';
                $answerJSON['error'] .= print_r($relTeamRelSeasonsLeagues->getErrors(), true);
            }
        } catch(\Exception $ex) {
            $exMsg = $ex->getMessage();
            if (strpos($exMsg, 'Duplicate') !== false) {
                $answerJSON['error'] = "Такая команда уже есть в данной паре сезон-лига";
            } else {
                $answerJSON['error'] = "Ошибка сохранения данных: {$exMsg}";
            }
        }

        return $this->asJson($answerJSON);
    }

    public function actionRemoveTeam() {
        $teamId = (int)\Yii::$app->request->post('teamId');

        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        try {

            $relSeasonsLeagues = RelTeamsRelSeasonsLeagues::findOne($teamId);
            if ($relSeasonsLeagues) {
                if ($relSeasonsLeagues->delete() === false) {
                    $answerJSON['error'] = 'Ошибка удаления данных';
                }
            }
        } catch(\Exception $ex) {
            $answerJSON['error'] = "Ошибка удаления данных: {$ex->getMessage()}";
        }

        return $this->asJson($answerJSON);
    }

}
