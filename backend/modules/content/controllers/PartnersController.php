<?php

namespace backend\modules\content\controllers;

use Yii;
use backend\models\Partners;
use backend\modules\content\models\PartnersSearch;
use backend\modules\content\components\BaseContentController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use richardfan\sortable\SortableAction;
use Gregwar\Image\Image;

/**
 * PartnersController implements the CRUD actions for Partners model.
 */
class PartnersController extends BaseContentController
{

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => Partners::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Partners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartnersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partners model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCropLogoPartner($id) {
        $model = $this->findModel($id);

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }*/

        if (Yii::$app->request->post()) {
            $request = Yii::$app->request;

            $x1 = $request->post('x1');
            $x2 = $request->post('x2');
            $y1 = $request->post('y1');
            $y2 = $request->post('y2');

            $h = $request->post('h');
            $w = $request->post('w');

            $image_height = $request->post('image_height');
            $image_width = $request->post('image_width');

            if (empty($w)) {
                //nothing selected
                throw new \Exception("Выберите часть изображения");
            }

            $filePath = $model->getFilePath('image');
            $img = Image::open( $filePath ) ;
            $resized_width = ((int) $x2) - ((int) $x1);
            $resized_height = ((int) $y2) - ((int) $y1);
            $img->crop((int)$x1, (int)$y1, $resized_width, $resized_height);
            $img->scaleResize(345 ,345);
            $img->save($filePath, 'guess') ;

            return $this->redirect(['update', 'id' => $id]);
        }

        return $this->render('crop-logo-partner', [
            'model' => $model,
        ]);
    }

    public function actionCropLogoMain($id) {
        $model = $this->findModel($id);

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }*/

        if (Yii::$app->request->post()) {
            $request = Yii::$app->request;

            $x1 = $request->post('x1');
            $x2 = $request->post('x2');
            $y1 = $request->post('y1');
            $y2 = $request->post('y2');

            $h = $request->post('h');
            $w = $request->post('w');

            $image_height = $request->post('image_height');
            $image_width = $request->post('image_width');

            if (empty($w)) {
                //nothing selected
                throw new \Exception("Выберите часть изображения");
            }

            $filePath = $model->getFilePath('image_main');
            $img = Image::open( $filePath ) ;
            $resized_width = ((int) $x2) - ((int) $x1);
            $resized_height = ((int) $y2) - ((int) $y1);
            $img->crop((int)$x1, (int)$y1, $resized_width, $resized_height);
            $img->scaleResize(345 ,345);
            $img->save($filePath, 'guess') ;

            return $this->redirect(['update', 'id' => $id]);
        }

        return $this->render('crop-logo-main', [
            'model' => $model,
        ]);
    }


    /**
     * Creates a new Partners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Partners();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Partners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Partners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Partners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Partners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Partners::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
