<?php

namespace backend\modules\content\components;

use backend\components\BaseController;
use common\models\User;
use Yii;
use yii\web\BadRequestHttpException;

class BaseContentManagerController extends BaseController
{

    public function beforeAction($action)
    {
        if ($this->route !== 'site/login'){
            if (Yii::$app->user->isGuest){
                return $this->redirect('/admin/login');
            }
        }

        if (User::findOne(Yii::$app->user->id)->roles === 'user'){
            return $this->redirect('/admin');

        }

        try {
            return parent::beforeAction($action);
        } catch (BadRequestHttpException $e) {
        }
    }

}