<?php

namespace backend\modules\content\models;


use common\models\Teams;
use common\models\User;
use yii\base\Model;

class CreateUserForm extends Model
{
    public $username;
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данный логин уже занят.'],
            ['username', 'exist', 'targetClass' => '\common\models\Teams', 'targetAttribute'=>'title', 'message' => 'Введите имя существующей команды.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данный email адрес уже занят.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        if ($result = $user->save()) {
            $team = Teams::findOne(['title'=>$user->username]);
            $team->agent = $user->id;
            $team->save();
        }
        return $result ? $user : null;
    }
}
