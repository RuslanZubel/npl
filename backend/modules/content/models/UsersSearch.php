<?php


namespace backend\modules\content\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class UsersSearch extends Model
{
    public $username;
    public $email;
    public $roles;
    public $team_title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[['username', 'email', 'roles', 'team_title'], 'safe'],];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new \yii\db\Query())
            ->select(['username', 'email', 'roles', 'users.id AS user_id', 'teams.title AS team_title'])
            ->from('users')
            ->leftJoin('teams', 'teams.fk_author = users.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'teams.title', $this->team_title]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'roles', $this->roles]);

        return $dataProvider;
    }
}