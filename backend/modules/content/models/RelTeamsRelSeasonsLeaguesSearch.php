<?php

namespace backend\modules\content\models;

use backend\components\ActiveToggle;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RelTeamsRelSeasonsLeagues;

/**
 * PartnersSearch represents the model behind the search form about `backend\models\Partners`.
 */
class RelTeamsRelSeasonsLeaguesSearch extends RelTeamsRelSeasonsLeagues
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'fk_author', 'fk_rel_seasons_leagues'], 'integer'],
            [['is_active', 'fk_rel_seasons_leagues'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $firstAct = true)
    {
        /**
         * TODO: непонятно, почему пагинатор, получая впервые relSLId и не получая его при пагинации работает, а если
         * relSLId передавать всегда, а не только в первый раз, то пагинатор тупит...
         */


        $query = RelTeamsRelSeasonsLeagues::find()
            //->where(['fk_rel_seasons_leagues' => $this->fk_rel_seasons_leagues])
            ->joinWith('fkTeams')
            ->joinWith('fkRelSeasonsLeagues');

        // add conditions that should always apply here

        $providerParams = array();
        $providerParams['query'] = $query;

        if ($firstAct) {
            $providerParams['pagination'] = [
                    'route' => '/content/rel-teams-rel-seasons-leagues/edit-teams',
                    'pageSize' => 10,
                    'params'=>['relSLId'=>$this->fk_rel_seasons_leagues],
                ];
        } else {

            $providerParams['pagination'] = [
                    'route' => '/content/rel-teams-rel-seasons-leagues/edit-teams',
                    'pageSize' => 10,
                ];
        }

        $providerParams['pagination'] = false;

        $dataProvider = new ActiveDataProvider($providerParams);

        //urldecode()
        //
        //Url
        //  grid view pagination params

        //print_r($params);

        if (!$this->load($params)) {
            //print_r($this->getErrors());
            //die('unable to load');
        }

        if (!$this->validate()) {
            //die('unable to validate');
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fk_rel_seasons_leagues' => $this->fk_rel_seasons_leagues,
        ]);

        //print_r($query->createCommand()->rawSql);
        //die;

        //print_r($query);

        //$this->addCondition

        return $dataProvider;
    }
}
