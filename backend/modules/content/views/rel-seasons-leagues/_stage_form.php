<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use kartik\datetime\DateTimePicker;
use yii\helpers\Url;
use kato\DropZone;

$idForm = \common\components\UniqueString::getUniqueString(5);

$this->registerJs("

    $('#" . $idForm . "').on('beforeSubmit', function() {
        var form = $(this);
            $.post(
                '" . Url::toRoute([ '/content/rel-seasons-leagues/update-stage', 'id' => $model->id ])  . "', 
                form.serializeArray(),
                function( data )
                {
                    if (data.status == 'ok') {
                        $('#edit-stage-modal').modal('hide');
                        $.pjax.reload('#stages-pjax');
                    } else {
                        alert('Непредвиденная ошибка');
                    }
                } 
            ); 
        return false;
    })

")


?>

<div class="stages-form">

    <?php $form = ActiveForm::begin(['id' => $idForm]); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Название этапа') ?>
                    <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
                        'id' => \common\components\UniqueString::getUniqueString(5),
                        'options' => [
                            'rows' => 16,
                        ],
                        'settings' => [
                            'object_resizing' => false,
                            'image_dimensions' => false,
                            'image_class_list' => [
                                ['title' => 'Responsive', 'value' => 'img-responsive']
                            ],
                        ],
                        'language' => 'ru',
                        'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
                        'fileManager' => [
                            'class' => TinyMceElFinder::className(),
                            'connectorRoute' => '/el-finder/connector',
                        ],
                    ]);
                    ?>

                </div>

                <div class="col-xs-12 col-sm-4">
                    <?= $form->field($model, 'start_date')->widget(\kartik\datetime\DateTimePicker::classname(), [
                        'name' => 'datetime_19',
                        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                        'layout' => '{picker}{input}{remove}',
                        'convertFormat' => 'true',
                        'pluginOptions' => [
                            'format' => 'dd-MM-yyyy HH:i',
                            'language' => 'en',
                            'autoclose' => true,]
                    ]) ?>
                    <?= $form->field($model, 'end_date')->widget(\kartik\datetime\DateTimePicker::classname(), [
                        'name' => 'datetime_19',
                        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                        'layout' => '{picker}{input}{remove}',
                        'convertFormat' => 'true',
                        'pluginOptions' => [
                            'format' => 'dd-MM-yyyy HH:i',
                            'language' => 'en',
                            'autoclose' => true,]
                    ]) ?>
                    <?= $form->field($model, 'main_trainings_date')->widget(\kartik\datetime\DateTimePicker::classname(), [
                        'name' => 'datetime_20',
                        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                        'layout' => '{picker}{input}{remove}',
                        'convertFormat' => 'true',
                        'pluginOptions' => [
                            'format' => 'dd-MM-yyyy HH:i',
                            'language' => 'en',
                            'autoclose' => true,]
                    ]) ?>
                    <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
                    <?= $form->field($model, 'calculation_races')->input('number') ?>
                    <?= $form->field($model, 'cost')->input('number') ?>
                    <?php echo DropZone::widget([
                        'options' => [
                            'maxFilesize' => '20',
                            'url' => '/admin/content/stages/upload?id=' . $model->id,
                            'addRemoveLinks' => true,
                            'createImageThumbnails' => true,
                            'dictDefaultMessage' => 'Нажмите здесь или перетащите изображение этапа',
                            'dictRemoveFile' => 'Удалить',
                        ],
                        'clientEvents' => [
                            'complete' => "function(file){console.log(file)}",
                            'removedfile' => "function(file){alert(file.name + ' is removed')}"
                        ],
                    ]);
                    ?>
                    <img src="<?= $model->getThumbFileUrl('image','thumb'); ?>" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
