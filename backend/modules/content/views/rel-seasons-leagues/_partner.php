<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs("

function add_seasons_leagues_partner() {
    var partnerId = $('#add_partner_type').val();
    var partnerStatusId = $('#add_partner_status').val();
    $.post('" . Url::toRoute('/content/rel-seasons-leagues/add-partner-unit') . "', 
    {'fkRelSeasonLeagues': {$fkRelSeasonsLeagues},
     'partnerId' : partnerId,
     'partnerStatusId' : partnerStatusId,
     },
    function( resp ){
        if (resp.error != '') {
            alert(resp.error);
        } else {
            close_partners_modal();
        }
    });
};

function close_partners_modal() {
    var popup = $('#add-league-partner');
    popup.modal('hide');
    $.pjax.reload('#league-partners-pjax');    
}

");

?>




<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close"><span aria-hidden="true" onclick="close_partners_modal();">x</span>
            </button>
            <h1 class="modal-title">Добавление спонсора лиги</h1>
        </div>
        <div class="modal-body">
            <div class="row">
                <?php $form = ActiveForm::begin(); ?>
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <?php

                                echo '<label class="control-label">Спонсор</label>';
                                echo Select2::widget([
                                    'name' => 'add_partner_type',
                                    'id' => 'add_partner_type',
                                    'data' => ArrayHelper::map(\backend\models\Partners::find()->orderBy('title ASC')->all(), 'id', 'title'),
                                    'options' => [
                                        'options' => ['placeholder' => 'Выберите спонсора'],
                                        'multiple' => false,
                                        'allowClear' => false
                                    ],
                                ]);

                                ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <?php

                                echo '<label class="control-label">Статус партнера</label>';
                                echo Select2::widget([
                                    'name' => 'add_partner_status',
                                    'id' => 'add_partner_status',
                                    'data' => ArrayHelper::map(\backend\models\PartnersStatuses::find()->orderBy('description ASC')->all(), 'id', 'description'),
                                    'options' => [
                                        'options' => ['placeholder' => 'ВЫберите статус'],
                                        'multiple' => false,
                                        'allowClear' => false
                                    ],
                                ]);

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div class="form-group">
                        <p><?= Html::a('Добавить', null, ['class' => 'btn btn-primary', 'onclick' => 'add_seasons_leagues_partner();']) ?></p>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
</div>
