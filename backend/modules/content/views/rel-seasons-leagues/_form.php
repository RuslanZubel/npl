<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;
use backend\components\SortableGridView;
use kartik\dialog\Dialog;
use yii\grid\GridView;


$this->registerJs("


$(document).on('click', '.add-league-partner', function() {
    var th = $(this);
    var popup = $('#add-league-partner');
    popup.find('.modal-body').text('. . . . . .');
    popup.modal('show');
    $.post(
        '" . Url::toRoute('/content/rel-seasons-leagues/add-partner') . "', 
            {'fkRelSeasonsLeagues': $modelId}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
});

/*
$('#add-partner').on('click', function () {
    $.post('" . Url::toRoute('/content/rel-seasons-leagues/add-partner') . "',
     {'fkRelSeasonsLeagues': " . $modelId . "},
     function( resp ){
        $('#partners-container').append( resp );
    });

}); */

$('#add-stage').on('click', function () {

    $.post('" . Url::toRoute('/content/rel-seasons-leagues/add-stage') . "', 
    {'fkRelSeasonsLeagues': " . $modelId . "},
    function( resp ){
        $('#stages-container').append( resp );
    });

});



$(document).on('click', '.edit-structure', function() {
    var th = $(this);
    var popup = $('#team-edit-modal');
    popup.attr('rel', th.attr('rel'));
    popup.attr('team', th.attr('team'));
    popup.find('.modal-body').text('. . . . . .');
    popup.modal('show');
    $.post(
        '" . Url::toRoute('/content/teams-structures/edit-structure') . "', 
            {'fkRelTeamsRelSeasonsLeagues': th.attr('rel'), 'fkTeam': th.attr('team')}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
});

function reload_edit_partners() {
    var popup = $('#team-partners-modal');
    $.post(
        '" . Url::toRoute('/content/teams-partners/edit-partners') . "', 
            {'fkRelTeamsRelSeasonsLeagues': popup.attr('rel'), 'fkTeam': popup.attr('team')}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
}

$(document).on('click', '.edit-partners', function() {
    var th = $(this);
    var popup = $('#team-partners-modal');
    popup.attr('rel', th.attr('rel'));
    popup.attr('team', th.attr('team'));
    popup.find('.modal-body').text('. . . . . .');
    popup.modal('show');
    reload_edit_partners();
});

$(document).on('click', '#update-comment', function() {
    var popup = $('#comment-edit-modal');
    $.post('" . Url::toRoute('/tasks-comments/update') . "', { 
            'idComment' :  $('#taskscomments-id').val(),
            'comment' : $('#taskscomments-text').val(),
        }, function( data ) {
        if (data.status == 'ok') {
            popup.modal('hide');
            $.pjax.reload('#comments');
            $(document).on('pjax:end', function(){ 
                makeCommentsLinks();
                makeSmallLinks();
            });
        }
    });
}); 


$(document).on('click', '.stage-edit-button',function( e ) {
    var th = $(this);
    var popup = $('#edit-stage-modal');
    popup.find('.modal-body').text('. . . . . .');
    popup.modal('show');
    $.post(
        '" . Url::toRoute('/content/rel-seasons-leagues/edit-stage-modal') . "', 
        {'id': th.attr('rel')}, 
        function( content ){
            popup.find('.modal-body').html( content );
        } 
    ); 
});


$(document).on('click', '.stage-delete-button',function( e ) {
    var th = $(this);
    krajeeDialogCust.confirm('Удалить данный этап и все связанные с ним данные?', function(out){
        if(out) {
            $.post(
                '" . Url::toRoute('/content/rel-seasons-leagues/delete-stage') . "', 
                {'id': th.attr('rel')}, 
                function( data ){
                    if (data.status == 'ok') {
                        $.pjax.reload('#stages-pjax');
                    } else {
                        alert('Непредвиденная ошибка');
                    }
                } 
            ); 
        }
    });
    
});

function reload_stages_races() {
    var popup = $('#stages-races-modal');
    $.post(
        '" . Url::toRoute('/results/races/edit-races') . "', 
            {'fkStages': popup.attr('rel')}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
}

$(document).on('click', '.edit-races', function() {
    var th = $(this);
    var popup = $('#stages-races-modal');
    popup.attr('rel', th.attr('rel'));
    popup.attr('team', th.attr('team'));
    popup.find('.modal-body').text('. . . . . .');
    popup.modal('show');
    reload_stages_races();
    
});

function reload_teams() {
    var popup = $('#edit-teams-modal');
    $.post(
        '" . Url::toRoute('/content/races/edit-races') . "', 
            {'fkStages': popup.attr('rel')}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
}

$(document).on('click', '.edit-teams', function() {
    var th = $(this);
    var popup = $('#edit-teams-modal');
    popup.attr('rel', th.attr('rel'));
    popup.attr('team', th.attr('team'));
    popup.find('.modal-body').text('. . . . . .');
    popup.modal('show');
    reload_teams();
});

$(document).on('click', '.league-partner-delete-button',function( e ) {
    var th = $(this);
    krajeeDialogCust.confirm('Удалить спонсора?', function(out){
        if(out) {
            $.post(
                '" . Url::toRoute('/content/rel-seasons-leagues/remove-partner-unit') . "', 
                {'id': th.attr('rel')}, 
                function( data ){
                    if (data.error == '') {
                        $.pjax.reload('#league-partners-pjax');
                    } else {
                        alert(data.error);
                    }
                } 
            ); 
        }
    });
    
});

")


?>

<div class="rel-seasons-leagues-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <?php echo $form->field($model, 'fk_seasons')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\backend\models\Seasons::find()->orderBy('title ASC')->all(), 'id', 'title'),
                        'options' => ['placeholder' => 'Выберите сезон'],
                        'pluginOptions' => [
                            'allowClear' => false
                        ],
                    ])->label('Сезон'); ?>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <?php echo $form->field($model, 'fk_leagues')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\backend\models\Leagues::find()->orderBy('title ASC')->all(), 'id', 'title'),
                        'options' => ['placeholder' => 'Выберите лигу'],
                        'pluginOptions' => [
                            'allowClear' => false
                        ],
                    ])->label('Лига'); ?>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
                </div>

            </div>
        </div>
    </div>
    <div class="form-group">
        <?= \backend\components\widgets\crud_buttons\CrudButtons::widget(['model' => $model]); ?>
    </div>

    <?php ActiveForm::end(); ?>


    <?php if ($model->id !== null ): ?>
    <?= $this->render('_teams', compact('teams', 'modelId')); ?>
    <?php endif ?>

    <?php if ($model->id !== null ): ?>
    <?php \yii\widgets\Pjax::begin(['id' => 'stages-pjax']); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
            <div id="stages-container">
                <div class="form-group">
                <span class="h2">
                    <span class="pr10">Этапы</span>
                    <?php echo Html::tag('span', 'Добавить этап', ['class' => 'btn btn-default btn-xs', 'id' => 'add-stage']) ?>
                </span>
                </div>
                <?= GridView::widget([
                    'dataProvider' => $stagesDataProvider,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n{pager}",
                    'pager' => [
                        'firstPageLabel' => '« «',
                        'lastPageLabel' => '» »',
                    ],
                    //'sortUrl' => Url::to(['sortItemStages']),
                    'columns' => [
                        'title',
                        [
                            'attribute' => '',
                            'value' => function ($data) use ($model) {
                                return
                                    Html::button('Описание', ['class' => 'btn btn-xs btn-default stage-edit-button', 'rel' => $data->id]) . ' ' .
                                    Html::button('Гонки', ['class' => 'btn btn-xs btn-default edit-races', 'rel' => $data->id]) . ' ' .
                                    Html::button('Очки', ['class' => 'btn btn-xs btn-default', 'onclick'=>"window.location.href = '" .
                                        \Yii::$app->urlManager->createUrl(['/results/scores','stage'=>$data->id]) . "';",]) . ' ' .
                                    Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash stage-delete-button pr5 cp', 'rel' => $data->id]);
                            },
                            'format' => 'raw',
                            'options' => ['width' => '15%']
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <?php \yii\widgets\Pjax::end(); ?>
    <?php endif ?>

    <?php if ($model->id != 0 ): ?>
    <?php \yii\widgets\Pjax::begin(['id' => 'league-partners-pjax']); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
            <div id="partners-container">
                <div class="form-group">
                <span class="h2">
                    <span class="pr10">Спонсоры</span>
                    <?php echo Html::tag('span', 'Добавить спонсора', ['class' => 'btn btn-default btn-xs add-league-partner']) ?>
                </span>
                </div>
                <?= SortableGridView::widget([
                    'dataProvider' => $partnersDataProvider,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n{pager}",
                    'pager' => [
                        'firstPageLabel' => '« «',
                        'lastPageLabel' => '» »',
                    ],
                    'sortUrl' => Url::to(['sortItemPartners']),
                    'columns' => [
                        [
                            'label' => 'Спонсор',
                            'value' => function ($item) {
                                return $item->getFkPartners()->one()->title;
                            }
                        ],
                        [
                            'label' => 'Статус',
                            'value' => function ($item) {
                                return $item->getFkPartnersStatuses()->one()->description;
                            }
                        ],
                        /*[
                            'label' => 'Статус',
                            'format' => 'html',
                            'value' => function($item) {
                                $partnersStatuses = \backend\models\PartnersStatuses::find()->all();
                                //$combo =
                                return \yii\bootstrap\Dropdown::widget([
                                        'items' => [
                                            ['label' => 'DropdownA', 'url' => '/'],
                                            ['label' => 'DropdownB', 'url' => '#'],
                                        ],
                                    ]);
                            }
                            //'filter' => Html::activeDropDownList(null, 'fk_partners_statuses', ArrayHelper::map(\backend\models\PartnersStatuses::find()->asArray()->all(), 'id', 'description'),['class'=>'form-control','prompt' => 'Select Category']),
                        ],*/
                        [
                            'attribute' => '',
                            'value' => function ($data) use ($model) {
                                return
                                    Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash league-partner-delete-button pr5 cp', 'rel' => $data->id]);
                            },
                            'format' => 'raw',
                            'options' => ['width' => '60px']
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <?php \yii\widgets\Pjax::end(); ?>
    <?php endif ?>

</div>


<div class="modal fade" tabindex="-1" role="dialog" id="edit-stage-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Редактирование этапа</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="team-edit-modal">
    <div class="modal-ajax">

    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="team-partners-modal">
    <div class="modal-ajax">

    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="stages-races-modal">
    <div class="modal-ajax">

    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="edit-teams-modal">
    <div class="modal-ajax">

    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="add-league-partner">
    <div class="modal-ajax">

    </div>
</div>


<?php

echo Dialog::widget([
    'libName' => 'krajeeDialogCust', // optional if not set will default to `krajeeDialog`

    'options' => [
        'draggable' => true, 'closable' => true,
        'title' => 'Подтверждение',
        'type' => 'type-danger',
        'btnOKLabel' => '<span class="glyphicon glyphicon-ok pr10"></span>Да',
        'btnOKClass' => 'btn-danger',
        'btnCancelLabel' => '<span class="glyphicon glyphicon-ban-circle pr10"></span>Отмена'
    ], // custom options
]);

?>
