<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\RelSeasonsLeagues */

$this->title = $model->id;

$this->params['breadcrumbs'][] = ['label' => 'Сезоны лиг', 'url' => ['/content/rel-seasons-leagues']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="rel-seasons-leagues-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'fk_seasons',
                'value' => function ($item) {
                    return $item->fkSeasons->title;
                }
            ],
            [
                'attribute' => 'fk_leagues',
                'value' => function ($item) {
                    return $item->fkLeagues->title;
                }
            ],
            'created_at',
            'updated_at',
            'fk_author',
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return $item->getIsActiveName();
                }
            ],
        ],
    ]) ?>

</div>
