<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сезоны лиг';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-seasons-leagues-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-6 buttons-right">
            <?=             Html::a('Создать сезон лиги'            , ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


    <?php Pjax::begin(); ?>            <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => '« «',
            'lastPageLabel' => '» »',
        ],
        'sortUrl' => Url::to(['sortItem']),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Сезон',
                'value' => function ($data) {
                    return !empty($data->fkSeasons) ? $data->fkSeasons->title : 'Не определен';
                },
            ],
            [
                'attribute' => 'Лига',
                'value' => function ($data) {
                    return !empty($data->fkLeagues) ? $data->fkLeagues->title : 'Не определен';
                },
            ],
            'created_at',
            'updated_at',
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return \backend\components\ActiveToggle::getView($item);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
        ]); ?>
    

            <?php Pjax::end(); ?>        </div>
    </div>
</div>