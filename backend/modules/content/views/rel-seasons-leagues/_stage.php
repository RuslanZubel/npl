<?php


use yii\helpers\Html;
use yii\helpers\Url;



?>
<div class="row">
    <div class="col-xs-12 col-sm-9">
        <div class="form-group">
        <?php

        echo '<label class="control-label">Название</label>';
        echo Html::input('', 'stage-name', (!empty($t['title'])) ? $t['title'] : 'Заполните описание' , ['class' => 'form-control']);
        ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-2">
        <div class="input-group">
            <div class="input-group-btn">
                <button class="btn btn-default stage-edit-button" type="button" rel="<?=  $t['id']; ?>">
                    <span aria-hidden="true">
                        <i class="glyphicon glyphicon-info-sign pr5"></i>
                        Заполнить описание
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-1">
        <div class="input-group">
            <div class="input-group-btn">
                <button class="btn btn-default remove-button" type="button">
                    <span class="glyphicon glyphicon-remove-circle remove-glyph remove-glyph-gray" aria-hidden="true">
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>

