<?php


use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs("

function reload_teams() {
    var popup = $('#edit-teams-modal');
    $.post(
        '" . Url::toRoute('/content/rel-teams-rel-seasons-leagues/edit-teams') . "', 
            {'relSLId': {$modelId}}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
}

$(document).on('click', '.edit-teams', function() {
    var th = $(this);
    var popup = $('#edit-teams-modal');
    popup.attr('rel', th.attr('rel'));
    popup.attr('team', th.attr('team'));
    popup.find('.modal-body').text('. . . . . .');
    popup.modal('show');
    reload_teams();
});

")

?>

<div class="panel panel-default jur-panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12">

                <div class="form-group">
                    <span class="h2">
                    <span class="pr10">Команды</span>
                        <?php echo Html::tag('span', 'Настроить список команд', ['class' => 'btn btn-default btn-xs edit-teams', 'id' => 'edit-teams']) ?>
                </span>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="form-group">
                    <?php \yii\widgets\Pjax::begin(['id' => 'teams-pjax']); ?>
                        <?php if ($teams !== null): ?>
                        <?php foreach ($teams as $t) : ?>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                            <span class="edit-structure" rel="<?= $t['id'] ?>" team="<?= $t['fk_teams'] ?>">
                                <span class="glyphicon glyphicon-user blue"></span>
                                <span class="glyphicon glyphicon-user pr10 blue"></span>
                            </span>
                            <span class="edit-partners" rel="<?= $t['id'] ?>" team="<?= $t['fk_teams'] ?>">
                                <span class="glyphicon glyphicon-blank"></span>
                                <span class="team-link">
                                    <span class="glyphicon glyphicon-briefcase pr10"></span>
                                </span>
                            </span>
                            <span class="edit-structure" rel="<?= $t['id'] ?>" team="<?= $t['fk_teams'] ?>">
                                <span class="team-link">
                                    <?= $t['fkTeams']['title'] ?>
                                </span>

                            </span>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        <?php endif ?>
                    <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>