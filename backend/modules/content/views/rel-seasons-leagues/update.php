<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\RelSeasonsLeagues */

$this->title = 'Редактирование сезона: ' . $model->fkSeasons->title . ', лига: ' . $model->fkLeagues->title;

$this->params['breadcrumbs'][] = ['label' => 'Сезоны лиг', 'url' => ['/content/rel-seasons-leagues']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="rel-seasons-leagues-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelId' => $modelId,
        'teams' => $teams,
        'stages' => $stages,
        'stagesDataProvider' => $stagesDataProvider,
        'partnersDataProvider' => $partnersDataProvider,
    ]) ?>

</div>
