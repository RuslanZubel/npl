<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\RelSeasonsLeagues */

$this->title = 'Создание сезона лиги';

$this->params['breadcrumbs'][] = ['label' => 'Сезоны лиг', 'url' => ['/content/rel-seasons-leagues']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-seasons-leagues-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelId' => $modelId,
    ]) ?>

</div>
