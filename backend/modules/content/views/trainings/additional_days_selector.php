<?php

use yii\helpers\Url;
use yii\web\View;


$this->title = 'Дополнительные тренировки';
$stageTitles = array();

$this->registerJs("
const recs = [
            {number: 1, id: 'a'},
            {number: 2, id: 'b'},
            {number: 3, id: 'c'},
            {number: 4, id: 'd'},
            {number: 5, id: 'e'},
            {number: 6, id: 'f'},
            {number: 7, id: 'g'},
            {number: 8, id: 'h'},
            {number: 9, id: 'i'},
        ];
removedItemsIds = [];
 events = [];
    $('#calendar').fullCalendar({
    plugins: [ 'dayGrid' ],
    timeZone: 'UTC',
    selectable: true,
    events: $events,
    dayRender: (dayRenderInfo, is) => { 
    if(!dayRenderInfo.add('day',1).isAfter(moment())) {
        is[0].style.background = 'gray'
    }
    },
    selectAllow: (m) => {
        return m.start.add('day',1).isAfter(moment())
    },
    select: function (startDate, endDate, s, d, e,f) {
            const id = new Date().getTime();
            $('#calendar').fullCalendar(
                'renderEvent',
                {
                    start: startDate.startOf('day').format(),
                    end: endDate.endOf('day').format(),
                    rendering: 'background',

                },
                true
            );
            events.push({
                start: startDate.startOf('day').format(),
                end: endDate.endOf('day').format(),


            });
        },
    eventClick: (info) => {
        $('#calendar').fullCalendar('removeEvents', [info.id]);
        removedItemsIds.push(info.id);
    },
  });


$('#saveButton').on('click', () => {
        const events = $('#calendar').fullCalendar('clientEvents').map(event => ({
            start_date: event.start.format('YYYY-MM-DD HH:mm:ss'), 
            end_date: event.end.format('YYYY-MM-DD HH:mm:ss')
        }));
        $.post(
            '" . Url::toRoute('/content/trainings/add-additional-training-period?stage_id=') . $stage_id."',
            { events , removedItemsIds},
            function (content) {
            }
        );
    });

",
    View::POS_READY)


?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title" style="position: relative">
                <div class="page-description__subtitle">Дополнительные тренировки"</div>
            </div>
            <div class="container">
                <div id="mycalendar">
                    <?=
                    \edofre\fullcalendarscheduler\FullcalendarScheduler::widget([
                        'header' => [
                            'center' => 'title',
                        ],

                    ])

                    ?>
                </div>
                <button id="saveButton" type="button">Сохранить</button>
            </div>
        </div>
    </div>
</main>

<style>
    #registerTraining {
        margin: 15px 0 15px;
    }

    .fc-license-message {
        display: none;
    }


</style>

<script>

</script>