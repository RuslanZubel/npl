<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\Url;
use\richardfan\sortable\SortableGridView;


$this->title = 'Дополнительные тренировки';

$this->registerJs("
const recs = [
            {number: 1, id: 'a'},
            {number: 2, id: 'b'},
            {number: 3, id: 'c'},
            {number: 4, id: 'd'},
            {number: 5, id: 'e'},
            {number: 6, id: 'f'},
            {number: 7, id: 'g'},
            {number: 8, id: 'h'},
            {number: 9, id: 'i'},
        ];
const formatDate = ()=>{
            return new Date(Number('" . strtotime($stage->main_trainings_date) . "') * 1000);
        }

 events = [];
    $('#mycalendar').children().fullCalendar({
//        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        defaultView: 'timelineDay',
        defaultDate: formatDate(),
        editable: false,
        eventOverlap: false,
        allDaySlot: false,
        'events': $events,
        dragScroll: false,
        'selectable': false,
        'selectOverlap': false,
        'aspectRatio': 2.2,
        'minTime': '10:00',
        'maxTime': '18:00',
        'slotDuration': '02:00:00',
        'scrollTime': '10:00',
        slotLabelFormat: [
          'HH:mm', // top level of text
        ],
        
         resourceColumns: [
            {
              labelText: 'Номер лодки',
              field: 'number'
            },
          ],
        resources: recs,
    });





",
    View::POS_READY)

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title" style="position: relative">
                <div class="page-description__subtitle">Дополнительные тренировки для "<?= $stage->title ?>"</div>
            </div>

            <div class="container">

                <div class="page-wrapper">
                    <h1>Дни тренировок</h1>

                    <?= SortableGridView::widget([
                        'dataProvider' => $days,
                        'tableOptions' => ['class' => 'table table-striped table-hover'],
                        'layout' => "{items}\n{pager}",
                        'pager' => [
                            'firstPageLabel' => '« «',
                            'lastPageLabel' => '» »',
                        ],
                        'sortUrl' => Url::to(['sortItem']),
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'attribute' => 'Доступные для дополнительных тренировок дни',
                                'value' => function ($data) {
                                    return date_format(date_create($data->start_date), 'Y-m-d');
                                },
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{export-trainings}',
                                'buttons' => [
                                    'export-trainings' => function ($url, $model) {
                                        return Html::a('<span> Экспортировать в Excel</span>', $url, [
                                            'title' => Yii::t('app', 'Экспортировать в Excel'),
                                        ]);
                                    },
                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    return Url::to(['./trainings/' . $action . '?stage_id=' . $model['stage_id'] . '&date=' . $model->start_date . '&main=false']);
                                }
                            ],
                        ],
                    ]); ?>


                    <div class="container page-link">
                        <a href="/admin/content/trainings/add-additional-training-period?stage_id=<?= $stage->id ?>">Добавить/Изменить
                            время дополнительных тренировок</a>
                    </div>
                </div>

                <div class="page-wrapper">
                    <h1>Тренировки</h1>
                    <div id="mycalendar">
                        <?=
                        \edofre\fullcalendarscheduler\FullcalendarScheduler::widget([
                            'header' => [
                                'center' => 'title',
                            ],

                        ])

                        ?>
                    </div>
                </div>
            </div>
        </div>
</main>

<style>
    #registerTraining {
        margin: 15px 0 15px;
    }

    .fc-license-message {
        display: none;
    }


</style>