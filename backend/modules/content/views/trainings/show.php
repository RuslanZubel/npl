<?php

use yii\bootstrap\Tabs;

$this->title = 'Тренировки';
$this->params['breadcrumbs'][] = $this->title;

?>

<main>
    <div class="container">
        <div class="page-wrapper">
            <div class="page-title" style="position: relative">
                <div class="page-description__subtitle">Тренировки для "<?= $stage->title?>"</div>
            </div>
            <div class="container">
                <?=
                Tabs::widget([
                    'items' => [
                        [
                            'label'     =>  'Основные тренировки',
                            'content'   =>  $this->render('free', [
                                'events' => $events,
                                'grouped' => $grouped,
                                'myEvents' => json_encode([]),
                                'action' => 'create',
                                'receipt' => null,
                                'stage'=>$stage
                            ]),
                            'active'    =>  true
                        ],
                        [
                            'label'     =>  'Дополнительные тренировки',
                            'content'   =>  $this->render('add_period', [
                                    'stage'=>$stage
                            ]),
                            'active'    =>  false
                        ],
                    ]
                ]);
                ?>
            </div>
        </div>
</main>

