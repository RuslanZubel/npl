<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use\richardfan\sortable\SortableGridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тренировки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-seasons-leagues-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


            <div class="panel panel-default jur-panel">
                <div class="panel-body">


                    <?php Pjax::begin(); ?>            <?= SortableGridView::widget([
                        'dataProvider' => $stages,
                        'tableOptions' => ['class' => 'table table-striped table-hover'],
                        'layout' => "{items}\n{pager}",
                        'pager' => [
                            'firstPageLabel' => '« «',
                            'lastPageLabel' => '» »',
                        ],
                        'sortUrl' => Url::to(['sortItem']),
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'attribute' => 'Сезон',
                                'value' => function ($data) {
                                    return !empty($data['season_title']) ? $data['season_title'] : 'Не опеределен';
                                },
                            ],[
                                'attribute' => 'Лига',
                                'value' => function ($data) {
                                    return !empty($data['league_title']) ? $data['league_title'] : 'Не опеределен';
                                },
                            ],
                            [
                                'attribute' => 'Этап',
                                'value' => function ($data) {
                                    return !empty($data['stage_title']) ? $data['stage_title'] : 'Не опеределен';
                                },
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{main-trainings} {additional-trainings}',
                                'buttons' => [
                                    'main-trainings' => function ($url, $model) {
                                        return Html::a('<span> Основные тренировки</span>', $url, [
                                            'title' => Yii::t('app', 'Перейти к основным тренировкам'),
                                        ]);
                                    },
                                    'additional-trainings' => function ($url, $model) {
                                        return Html::a('<span> Дополнительные тренировки</span>', $url, [
                                            'title' => Yii::t('app', 'Перейти к дополнительным тренировкам'),
                                        ]);
                                    },
                                ],
                                'visibleButtons' => [
                                    'check' => function ($model, $key, $index) {
                                        return empty($model['confirmed']);
                                    },
                                    'uncheck' => function ($model, $key, $index) {
                                        return !empty($model['confirmed']);
                                    }
                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    return Url::to(['./trainings/'.$action.'?stage_id='.$model['stage_id']]);
                                }
                            ],
                        ],
                    ]); ?>


                    <?php Pjax::end(); ?>


                </div>
            </div>




        </div>
    </div>
</div>