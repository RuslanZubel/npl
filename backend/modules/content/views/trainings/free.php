<?php

use yii\helpers\Url;
use yii\web\View;


$this->title = 'Основные тренировки';
$stageTitles = array();

$this->registerJs("
const recs = [
            {number: 1, id: 'a'},
            {number: 2, id: 'b'},
            {number: 3, id: 'c'},
            {number: 4, id: 'd'},
            {number: 5, id: 'e'},
            {number: 6, id: 'f'},
            {number: 7, id: 'g'},
            {number: 8, id: 'h'},
            {number: 9, id: 'i'},
        ];
const formatDate = () => {
            return new Date(Number('" . strtotime($stage->main_trainings_date) . "') * 1000);
        }

 events = [];
    $('#mycalendar').children().fullCalendar({
//        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        header: {   
            'center' : 'title',
            'left': '',
            'right': ''
                },
        defaultView: 'timelineDay',
        defaultDate: formatDate(),
        editable: false,
        eventOverlap: false,
        allDaySlot: false,
        'events': $events,
        dragScroll: false,
        'selectable': false,
        'selectOverlap': false,
        'aspectRatio': 2.2,
        'minTime': '10:00',
        'maxTime': '18:00',
        'slotDuration': '02:00:00',
        'scrollTime': '10:00',
        hiddenDays: [ ],
        slotLabelFormat: [
          'HH:mm', // top level of text
        ],
        
         resourceColumns: [
            {
              labelText: 'Номер лодки',
              field: 'number'
            },
          ],
        resources: recs,
    });





",
    View::POS_READY)


?>

<main>
    <div class="tab-content">
        <div class="align-self-end mr-3" style="position: relative">
            <a href="/admin/content/trainings/export-trainings?stage_id=<?= $stage->id ?>&date=null&main=true"
               style="position: absolute; right: 10px">Экспортировать в Excel</a>
        </div>
        <div class="page-wrapper">
            <div id="mycalendar">
                <?=
                \edofre\fullcalendarscheduler\FullcalendarScheduler::widget([
                    'header' => [
                        'center' => 'title',
                    ],

                ])

                ?>
            </div>
        </div>
</main>

<style>
    #registerTraining {
        margin: 15px 0 15px;
    }

    .fc-license-message {
        display: none;
    }


</style>