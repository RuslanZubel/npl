<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
//use backend\components\SortableGridView;
use yii\grid\GridView;
$this->registerJs("

//TODO: не осилил обновление модального диалога редактирования списка партнёров для команды в рамках сезона - сделал как смог...
function reload_teams() {
    var popup = $('#edit-teams-modal');
    $.post(
        '" . Url::toRoute('/content/rel-teams-rel-seasons-leagues/edit-teams') . "', 
            {'relSLId': {$relSeasonsLeagues->id}}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
}

function add_seasonleagues_team() {
    var teamId = $('#teams_select').val();
    $.post('" . Url::toRoute('/content/rel-teams-rel-seasons-leagues/add-team') . "', 
    {'fkRelSeasonLeagues': {$relSeasonsLeagues->id},
     'teamId' : teamId},
    function( resp ){
        if (resp.error != '') {
            alert(resp.error);
        } else {
            reload_teams();
        }
    });
};

function remove_seasonleagues_team(id) {
    krajeeDialogCust.confirm('Удалить данную команду?', function(out){
            if(out) {
                $.post('" . Url::toRoute('/content/rel-teams-rel-seasons-leagues/remove-team') . "', 
                {'teamId' : id},
                function( resp ){
                    if (resp.error != '') {
                        alert(resp.error);
                    } else {
                        reload_teams();
                    }
                });
        }});
};

function close_teams_modal() {
    var popup = $('#edit-teams-modal');
    popup.modal('hide');
    $.pjax.reload('#teams-pjax');    
}

");

?>




<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close"><span aria-hidden="true" onclick="close_teams_modal();">x</span>
            </button>
            <h1 class="modal-title">Редактирование списка команд лиги</h1>
        </div>
        <div class="modal-body">
            <div class="row">
                <?php $form = ActiveForm::begin(); ?>
                <div class="col-xs-6 col-sm-6">
                    <?php
                    echo Select2::widget([
                        'name' => 'teams_select',
                        'id' => 'teams_select',
                        'data' => \common\models\Teams::getList(),
                        'options' => [
                            'multiple' => false,
                        ],
                        'pluginOptions' => [
                            'closeOnSelect' => false,
                        ],
                    ]);

                    ?>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div class="form-group">
                        <p><?= Html::a('Добавить', null, ['class' => 'btn btn-primary', 'onclick' => 'add_seasonleagues_team();']) ?></p>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>


                <?php Pjax::begin(['id' => 'teams-edit-pjax', 'enablePushState' => false]); ?>
                <?= GridView::widget([
                    'id' => \common\components\UniqueString::getUniqueString(6),
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n{pager}",
                    'pager' => [
                        'firstPageLabel' => '« «',
                        'lastPageLabel' => '» »',
                    ],
                    //'sortUrl' => Url::to(['sortItem']),
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'title',
                        [
                            'label' => 'Название',
                            'value' => function($item) {
                                return $item->fkTeams->title;
                            }
                        ],
                        /*[
                            'attribute' => 'is_active',
                        ],*/
                        //'sort',
                         [
                             'options' => ['width' => '60px'],
                            'class' => 'yii\grid\ActionColumn',
                            'template'=>'{delete}',
                            'buttons'=>[
                                'delete' => function ($url, $item){
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-trash pr5 cp"></span>',
                                        null,
                                        [
                                            'title' => Yii::t('yii', 'Удалить'),
                                            'onclick' => "remove_seasonleagues_team({$item->id});"
                                        ]
                                    );
                                },
                            ]
                        ],
                    ],
                ]); ?>


                <?php Pjax::end(); ?>

            </div>

            <div style="padding-left: 20px">Здесь вы можете редактировать список команд сезона-лиги</div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="edit-teams-modal-dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-label="Close"><span aria-hidden="true" onclick="close_teams_modal();">x</span>
                </button>
                <h4 class="modal-title">Редактирование гонки этапа</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
