<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("

$('#config-options-grid').on('click', '.editable-config-title', function() {
    $('.config-feed-title:not(.hidden)').trigger('blur');
    $(this).toggleClass('hidden');
    $(this).siblings('input').toggleClass('hidden');
});


$('#config-options-grid').on('blur', '.config-feed-title', function() {
    var th = $(this);
    $.post('" . Url::toRoute('edit-title') . "', { 'id' : th.closest('tr').data('key'), 'value' : th.val() });
    th.toggleClass('hidden');
    th.siblings('span').toggleClass('hidden');
    if ($(this).val() == '') {
       th.siblings('span').html('<span class=\"empty-config-title\">Нажмите для заполнения</span>');
    } else {
        th.siblings('span').text($(this).val());
    }
});

");
?>
<div class="config-options-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(['id' => 'config-options-grid']); ?>

    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'sortUrl' => Url::to(['sortItem']),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'setting',
            [
                'attribute' => 'value',
                'options' => [
                    'width' => '50%'],
                'value' => function ($data) {
                    $str = Html::tag('span', empty ($data->value) ? '<span class="empty-config-title">Нажмите для заполнения</span>' : $data->value, ['class' => 'editable-config-title']);
                    $str .= Html::input('text', '', $data->value, ['class' => 'form-control hidden config-feed-title']);
                    return $str;
                },
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'editable-title-td'
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
