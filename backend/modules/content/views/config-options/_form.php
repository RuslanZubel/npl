<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ConfigOptions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="config-options-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'setting')->dropDownList([ 'banner_h1' => 'Banner h1', 'banner_img' => 'Banner img', 'banner_txt' => 'Banner txt', 'banner_lnk' => 'Banner lnk', 'lnk_instagram' => 'Lnk instagram', 'lnk_youtube' => 'Lnk youtube', 'lnk_fb' => 'Lnk fb', 'lnk_vk' => 'Lnk vk', 'lnk_tw' => 'Lnk tw', 'lnk_ok' => 'Lnk ok', 'map_code' => 'Map code', 'notify_email' => 'Notify email', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
