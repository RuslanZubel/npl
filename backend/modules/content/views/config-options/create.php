<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ConfigOptions */

$this->title = 'Create Config Options';
$this->params['breadcrumbs'][] = ['label' => 'Config Options', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-options-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
