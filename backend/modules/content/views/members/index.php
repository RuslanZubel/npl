<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\MembersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Участники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="members-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-6 buttons-right">
            <?=             Html::a('Создать участника'            , ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


    <?php Pjax::begin(); ?>
    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => '« «',
            'lastPageLabel' => '» »',
        ],
        'sortUrl' => Url::to(['sortItem']),
        'filterModel' => $searchModel,
        'columns' => [
             ['class' => 'yii\grid\ActionColumn'],
             [
                'label' => 'Фотография',
                'format' => 'HTML',
                'value' => function($data){
                    return '<img src="' . $data->getThumbFileUrl('image') . '" style="width: 50px">';
                },
            ],
            'lastname',
            'firstname',
            'vfps_id',
            'updated_at',
            'created_at',
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return \backend\components\ActiveToggle::getView($item);
                },
                'format' => 'raw',
            ],
          'id' //  => [
//                     'contentOptions' => [ 'style' => 'width: 300']
//            ]

        ],
        ]); ?>
     <?php Pjax::end(); ?>
        </div>
    </div>
</div>