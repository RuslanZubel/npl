<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Members */

$this->title = 'Редактирование участника: ' . $model->id;

$this->params['breadcrumbs'][] = ['label' => 'Участники', 'url' => ['/content/members']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="members-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
