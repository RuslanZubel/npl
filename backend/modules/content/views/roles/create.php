<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Roles */

$this->title = 'Создание должности';

$this->params['breadcrumbs'][] = ['label' => 'Должности', 'url' => ['/content/roles']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="roles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
