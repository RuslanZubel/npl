<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Roles */

$this->title = 'Редактирование должности: ' . $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Должности', 'url' => ['/content/roles']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="roles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
