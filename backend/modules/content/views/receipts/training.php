<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Чек';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-seasons-leagues-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                Информация по чеку оплаты за тренировки
            </h1>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


            <?php Pjax::begin(); ?>            <?= SortableGridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-striped table-hover'],
                'layout' => "{items}\n{pager}",
                'pager' => [
                    'firstPageLabel' => '« «',
                    'lastPageLabel' => '» »',
                ],
                'sortUrl' => Url::to(['sortItem']),
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => '№',
                        'value' => function ($data) {
                            return !empty($data->id) ? $data->id : 'Не определен';
                        },
                    ],
                    [
                        'attribute' => 'Стоимость',
                        'value' => function ($data) {
                            return !empty($data->cost) ? $data->cost : 'Не опеределен';
                        },
                    ],
                    [
                        'attribute' => 'Создано пользователем',
                        'value' => function ($data) {
                            return !empty($data->created_by) ? \common\models\User::findOne($data->created_by)->username : 'Не определен';
                        },
                    ],
                    [
                        'attribute' => 'Команда',
                        'value' => function ($data) {
                            return !empty($data->team_id) ? \common\models\Teams::findOne($data->team_id)->title : 'Не опеределен';
                        },
                    ],
                    [
                        'attribute' => 'Начало тренировки',
                        'value' => function ($data) {
                            return !empty($data->start_datetime) ? $data->start_datetime : 'Не определен';
                        },
                    ],
                    [
                        'attribute' => 'Конец тренировки',
                        'value' => function ($data) {
                            return !empty($data->end_datetime) ? $data->end_datetime : 'Не определен';
                        },
                    ],
                    'created_at'
                ],
            ]); ?>


            <?php Pjax::end(); ?>        </div>
    </div>
</div>