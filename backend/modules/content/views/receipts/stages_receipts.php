<?php

use common\models\User;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\helpers\Url;

use yii\widgets\Pjax;
use\richardfan\sortable\SortableGridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<main>
    <div class="tab-content">
        <div class="align-self-end mr-3" style="position: relative">
            <a href="/admin/content/receipts/export-stages" style="position: absolute; right: 10px">Экспортировать в
                Excel</a>
        </div>
        <div class="panel panel-default jur-panel">
            <div class="panel-body">


                <?php Pjax::begin(); ?>            <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n{pager}",
                    'filterModel' => $searchModel,
                    'pager' => [
                        'firstPageLabel' => '« «',
                        'lastPageLabel' => '» »',
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'label' => 'Команда',
                            'attribute' => 'team_title',
                            'value' => function ($data) {
                                return !empty($data['team_title']) ? $data['team_title'] : 'Не опеределен';
                            },
                            'enableSorting' => true,
                            'filter' => null
                        ],
                        [
                            'label' => 'Лига',
                            'attribute' => 'league_title',
                            'value' => function ($data) {
                                return !empty($data['league_title']) ? $data['league_title'] : 'Не опеределен';
                            },
                            'filter' => null
                        ],
                        [
                            'label' => 'Этап',
                            'attribute' => 'stage_title',
                            'value' => function ($data) {
                                return !empty($data['stage_title']) ? $data['stage_title'] : 'Не опеределен';
                            },
                            'filter' => null
                        ],
                        [
                            'label' => 'Оплата',
                            'attribute' => 'confirmed',
                            'value' => function ($data) {
                                return !empty($data['confirmed']) ? 'Подтверждена' : 'Не подтверждена';
                            },
                            'filter' => [
                                    0 => 'Не подтверждена',
                                    1 => 'Подтверждена',
                            ]
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{check} {uncheck}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('app', 'Просмотр'),
                                    ]);
                                },
                                'check' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', $url, [
                                        'title' => Yii::t('app', 'Подтвердить оплату'),
                                    ]);
                                },
                                'uncheck' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', $url, [
                                        'title' => Yii::t('app', 'Отменить оплату'),
                                    ]);
                                }
                            ],
                            'visibleButtons' => [
                                'check' => function ($model, $key, $index) {
                                    return User::findOne(Yii::$app->user->id)->roles !== 'trainer' && empty($model['confirmed']);
                                },
                                'uncheck' => function ($model, $key, $index) {
                                    return User::findOne(Yii::$app->user->id)->roles !== 'trainer' && !empty($model['confirmed']);
                                }
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                                return Url::to(['./receipts/'.$action.'?id='.$model['receipt_id']]);
                            }
                        ],
                    ],
                ]); ?>


                <?php Pjax::end(); ?>


            </div>
        </div>
</main>
