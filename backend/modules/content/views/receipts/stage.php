<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Чек';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-seasons-leagues-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                Информация по чеку оплаты за этап
            </h1>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


            <?php Pjax::begin(); ?>            <?= \richardfan\sortable\SortableGridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-striped table-hover'],
                'layout' => "{items}\n{pager}",
                'pager' => [
                    'firstPageLabel' => '« «',
                    'lastPageLabel' => '» »',
                ],
                'sortUrl' => Url::to(['sortItem']),
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => '№',
                        'value' => function ($data) {
                            return !empty($data['id']) ? $data['id'] : 'Не определен';
                        },
                    ],
                    [
                        'attribute' => 'Стоимость',
                        'value' => function ($data) {
                            return !empty($data['cost']) ? $data['cost'] : 'Не опеределен';
                        },
                    ],
                    [
                        'attribute' => 'Название этапа',
                        'value' => function ($data) {
                            return !empty($data['title']) ? $data['title'] : 'Не определен';
                        },
                    ],
                    [
                        'attribute' => 'Команда',
                        'value' => function ($data) {
                            return !empty($data['team']) ? $data['team'] : 'Не определен';
                        },
                    ],
                ],
            ]); ?>


            <?php Pjax::end(); ?>        </div>
    </div>
</div>