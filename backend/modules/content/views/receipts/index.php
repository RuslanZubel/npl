<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Чеки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-seasons-leagues-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


            <?=
            Tabs::widget([
                'items' => [
                    [
                        'label'     =>  'Тренировки',
                        'content'   =>  $this->render('trainings_receipts', [
                            'dataProvider' => $trainings,
                            'searchModel'=>$trainingsSearchModel
                        ]),
                        'active'    =>  true
                    ],
                    [
                        'label'     =>  'Этапы',
                        'content'   =>  $this->render('stages_receipts', [
                            'dataProvider' => $stages,
                            'searchModel'=>$searchModel
                        ]),
                        'active'    =>  false
                    ],
                ],
            ]);
            ?>


        </div>
    </div>
</div>