<?php

use common\models\User;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\helpers\Url;

use yii\widgets\Pjax;
use\richardfan\sortable\SortableGridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<main>
    <div class="tab-content">
        <div class="align-self-end mr-3" style="position: relative">
            <a href="/admin/content/receipts/export-trainings" style="position: absolute; right: 10px">Экспортировать в
                Excel</a>
        </div>
        <div class="panel panel-default jur-panel">
            <div class="panel-body">


                <?php Pjax::begin(); ?>            <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n{pager}",
                    'filterModel' => $searchModel,
                    'pager' => [
                        'firstPageLabel' => '« «',
                        'lastPageLabel' => '» »',
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'label' => 'ID Чека',
                            'attribute' => 'receipt_id',
                            'value' => function ($data) {
                                return !empty($data['receipt_id']) ? $data['receipt_id'] : 'Не определен';
                            },
                        ],
                        [
                            'label' => 'Команда',
                            'attribute' => 'title',
                            'value' => function ($data) {
                                return !empty($data['title']) ? $data['title'] : 'Не опеределен';
                            },
                            'filter' => null
                        ],


                        [
                            'label' => 'Стоимость',
                            'attribute' => 'cost',
                            'value' => function ($data) {
                                return !is_null($data['cost']) ? $data['cost'] : 'Не опеределен';
                            },
                            'filter' => null
                        ],
                        [
                            'label' => 'Время начала',
                            'attribute' => 'start_datetime',
                            'value' => function ($data) {
                                return !empty($data['start_datetime']) ? $data['start_datetime'] : 'Не опеределен';
                            },
                        ],
                        [
                            'attribute' => 'Длительность',
                            'value' => function ($data) {
                                return '2 часа';
                            },
                        ],
                        [
                            'label' => 'Номер лодки',
                            'attribute' => 'boat_number',
                            'value' => function ($data) {
                                return !empty($data['boat_number']) ? $data['boat_number'] : 'Не опеределен';
                            },
                        ],
                        [
                            'label' => 'Оплата',
                            'attribute' => 'confirmed',
                            'value' => function ($data) {
                                return !empty($data['confirmed']) ? 'Подтверждена' : 'Не подтверждена';
                            },
                            'filter' => [
                                0 => 'Не подтверждена',
                                1 => 'Подтверждена',
                            ]
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{check} {uncheck} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('app', 'Просмотр'),
                                    ]);
                                },
                                'check' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', $url, [
                                        'title' => Yii::t('app', 'Подтвердить оплату'),
                                    ]);
                                },
                                'uncheck' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', $url, [
                                        'title' => Yii::t('app', 'Отменить оплату'),
                                    ]);
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                        'title' => Yii::t('app', 'Удалить чек и тренировку'),
                                    ]);
                                }
                            ],
                            'visibleButtons' => [
                                'check' => function ($model, $key, $index) {
                                    return User::findOne(Yii::$app->user->id)->roles !== 'trainer' && empty($model['confirmed']);
                                },
                                'uncheck' => function ($model, $key, $index) {
                                    return User::findOne(Yii::$app->user->id)->roles !== 'trainer' && !empty($model['confirmed']);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return User::findOne(Yii::$app->user->id)->roles !== 'trainer';
                                }
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                                return Url::to(['./receipts/' . $action . '?id=' . $model['receipt_id']]);
                            }
                        ],
                    ],
                ]); ?>


                <?php Pjax::end(); ?>


            </div>
        </div>
</main>
