<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Partners */

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Партнёры', 'url' => ['/content/partners/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="partners-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sort',
            'title',
            'text:ntext',
            'site_url:url',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function() use ($model) { return "<a href='" .  Url::toRoute(['/content/partners/crop-logo-partner', 'id' => $model->id]) . "'><img src='{$model->getFileUrl()}' /></a>";},
            ],
            [
                'attribute' => 'image_main',
                'format' => 'html',
                'value' => function() use ($model) { return "<a href='" .  Url::toRoute(['/content/partners/crop-logo-main', 'id' => $model->id]) . "'><img src='{$model->getMainFileUrl()}' /></a>";},
            ],
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return $item->getIsActiveName();
                }
            ],
            'created_at',
            'updated_at',
            'fk_author',
        ],
    ]) ?>

</div>
