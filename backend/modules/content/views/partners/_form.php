<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\PartnersStatuses;
use kartik\select2\Select2;
use backend\components\widgets\single_image_field\SingleImageField;


/* @var $this yii\web\View */
/* @var $model backend\models\Partners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partners-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'shorttitle')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'site_url')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'shorttext')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <?= SingleImageField::widget(['model' => $model, 'thumbName' => true, 'form' => $form, 'attribute' => 'image_main']) ?>
                    <?php
                    if ($model->image_main !== null && $model->image_main != "") {
                        echo Html::a('Редактировать логотип', ['crop-logo-main', 'id' => $model->id], ['class' => 'btn btn-primary']);
                    }
                    ?>

                    <?= SingleImageField::widget(['model' => $model, 'thumbName' => true, 'form' => $form, 'attribute' => 'image']) ?>
                    <?php
                    if ($model->image !== null && $model->image != "") {
                        echo Html::a('Редактировать логотип', ['crop-logo-partner', 'id' => $model->id], ['class' => 'btn btn-primary']);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= \backend\components\widgets\crud_buttons\CrudButtons::widget(['model' => $model]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
