<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use kartik\datetime\DateTimePicker;
use backend\components\widgets\single_image_field\SingleImageField;

/* @var $this yii\web\View */
/* @var $model backend\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
			<div class="row">
				<div class="col-xs-12 col-sm-9">
					<div class="row">
						<div class="col-xs-12 col-sm-8">
								<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
						</div>
						<div class="col-xs-12 col-sm-4">
								<?= $form->field($model, 'shorttitle')->textInput(['maxlength' => true]) ?>
						</div>
					</div>
					<?= $form->field($model, 'text')->widget(TinyMce::className(), [
						'options' => [
							'rows' => 16,
						],
						'settings' => [
							'object_resizing' => false,
							'image_dimensions' => false,
							'image_class_list' => [
								['title' => 'Responsive', 'value' => 'img-responsive']
							],
						],
						'language' => 'ru',
						'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
						'fileManager' => [
							'class' => TinyMceElFinder::className(),
							'connectorRoute' => '/el-finder/connector',
						],
					]);
					?>
				</div>
				<div class="col-xs-12 col-sm-3">
                    <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
				</div>
                <div class="col-xs-12 col-sm-6">
                    <?= SingleImageField::widget(['model' => $model, 'thumbName' => true, 'form' => $form, 'attribute' => 'image']) ?>
                </div>
			</div>
				
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
