<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Pages */

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['/content/pages']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="pages-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'title',
            'shorttitle',
            'text:ntext',
            'shorttext:ntext',
            'sort',
            'fk_author',
            'created_at',
            'updated_at',
            'is_active',
        ],
    ]) ?>

</div>
