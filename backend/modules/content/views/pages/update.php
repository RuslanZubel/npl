<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Pages */

$this->title = 'Редактирование страницы: ' . $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['/content/pages']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="pages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
