<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-6 buttons-right">
      <!--      <?=             Html::a('Создать документ'            , ['create'], ['class' => 'btn btn-success']) ?> -->
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


    <?php Pjax::begin(); ?>            <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => '« «',
            'lastPageLabel' => '» »',
        ],
        'sortUrl' => Url::to(['sortItem']),
      //  'filterModel' => $searchModel,
        'columns' => [
         ['class' => 'yii\grid\ActionColumn', 'template' => '{update}', 'options' => ['style' => 'width:10px'], ],
            //       ['class' => 'yii\grid\SerialColumn'],
     //       'id',
			'shorttitle',
			'title',
			'type',
            [
                'label' => 'Изображение',
                'format' => 'HTML',
                'value' => function($data){
                    return '<img src="' . $data->getFileUrl('image') . '" style="width: 50px">';
                },
            ],
            //'text:ntext',
            // 'shorttext:ntext',
            // 'sort',
            // 'fk_author',
            // 'created_at',
            // 'updated_at',
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return \backend\components\ActiveToggle::getView($item);
                },
                'format' => 'raw',
            ],


        ],
        ]); ?>
    

            <?php Pjax::end(); ?>        </div>
    </div>
</div>