<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Teams */

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['/content/teams']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="teams-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'short_title',
            'description:ntext',
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return $item->getIsActiveName();
                }
            ],
            'club',
            'created_at',
            'updated_at',
            'fk_author',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function() use ($model) { return "<img src='{$model->getFileUrl('image')}' />";},
            ],
            [
                'attribute' => 'cover_image',
                'format' => 'html',
                'value' => function() use ($model) { return "<img src='{$model->getFileUrl('cover_image')}' />";},
            ],
        ],
    ]) ?>

</div>
