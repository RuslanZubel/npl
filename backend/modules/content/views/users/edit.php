<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Изменение пользователя';
$this->params['breadcrumbs'][] = $this->title;
$model->password = Yii::$app->getSecurity()->generateRandomString(8);
?>
<div class="container center-block text-center mt-5 h-100">
    <div class="span12 align-content-center">
        <div class="site-signup align-content-center">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>Здесь вы можете изменить данные пользователя:</p>

            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                        <?= $form->field($model, 'username')->textInput(['disabled' => true])
                            ->label('Логин')?>

                        <?= $form->field($model, 'email')->label('Email адрес') ?>

                        <?= $form->field($model, 'password')->label('Новый пароль') ?>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

    </div>
</div>