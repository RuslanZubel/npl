<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-seasons-leagues-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-6 buttons-right">
            <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


            <?php Pjax::begin(); ?> <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-striped table-hover'],
                'layout' => "{items}\n{pager}",
                'filterModel' => $searchModel,
                'pager' => [
                    'firstPageLabel' => '« «',
                    'lastPageLabel' => '» »',
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'label' => 'Имя пользователя',
                        'attribute' => 'username',
                        'enableSorting' => true,
                        'filter' => null,
                        'value' => function ($data) {
                            return !empty($data['username']) ? $data['username'] : 'Не определен';
                        },
                    ],
                    [
                        'label' => 'Email адрес',
                        'attribute' => 'email',
                        'enableSorting' => true,
                        'filter' => null,
                        'value' => function ($data) {
                            return !empty($data['email']) ? $data['email'] : 'Не определен';
                        },
                    ],
                    [
                        'label' => 'Роль',
                        'attribute' => 'roles',
                        'enableSorting' => true,
                        'filter' => null,
                        'value' => function ($data) {
                            return !empty($data['roles']) ? $data['roles'] : 'Не определен';
                        },
                    ],
                    [
                        'label' => 'Команда',
                        'attribute' => 'team_title',
                        'enableSorting' => true,
                        'filter' => null,
                        'value' => function ($data) {
                            return !empty($data['team_title']) ? $data['team_title'] : 'Не определен';
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{edit} {delete}',
                        'buttons' => [
                            'edit' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, [
                                    'title' => Yii::t('app', 'Изменить'),
                                ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'Удалить пользователя'),
                                ]);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return Url::to(['./users/' . $action . '?id=' . $model['user_id']]);
                        }
                    ],
                ],
            ]); ?>


            <?php Pjax::end(); ?>        </div>
    </div>
</div>