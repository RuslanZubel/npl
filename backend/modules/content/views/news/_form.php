<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\News;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\News */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="news-form">
	
	<?php $form = ActiveForm::begin(); ?>
	
	<div class="panel panel-default jur-panel">
		<div class="panel-body">

			<div class="row">
				<div class="col-xs-12 col-sm-8">
					<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
					<?= $form->field($model, 'short_title')->textInput(['maxlength' => true]) ?>
					<?= $form->field($model, 'short_text')->widget(TinyMce::className(), [
						'options' => [
							'rows' => 10,
						],
						'settings' => [
							'object_resizing' => false,
							'image_dimensions' => false,
							'image_class_list' => [
								['title' => 'Responsive', 'value' => 'img-responsive']
							],
						],
						'language' => 'ru',
						'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
						'fileManager' => [
							'class' => TinyMceElFinder::className(),
							'connectorRoute' => '/el-finder/connector',
						], ]);
					?>
					<?= $form->field($model, 'text')->widget(TinyMce::className(), [
						'options' => [
							'rows' => 16,
						],
						'settings' => [
							'object_resizing' => false,
							'image_dimensions' => false,
							'image_class_list' => [
								['title' => 'Responsive', 'value' => 'img-responsive']
							],
						],
						'language' => 'ru',
						'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
						'fileManager' => [
							'class' => TinyMceElFinder::className(),
							'connectorRoute' => '/el-finder/connector',
						],
					]);
					?>
				</div>
				<div class="col-xs-12 col-sm-4">
                    <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
                    <?= $form->field($model, 'show_on_index')->dropDownList($model->getShowOnMainDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
					<?= \backend\components\widgets\single_image_field\SingleImageField::widget(['model' => $model, 'thumbName' => true, 'form' => $form, 'attribute' => 'image']) ?>
					<?= $form->field($model, 'display_date')->widget(\kartik\datetime\DateTimePicker::classname(), [
						'name' => 'datetime_19',
						'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
						'layout' => '{picker}{input}{remove}',
						'convertFormat' => 'true',
						'pluginOptions' => [
							'format' => 'dd-MM-yyyy HH:i',
							'language' => 'en',
							'autoclose' => true,]
					]) ?>
					

				</div>
			</div>
<!--			-->
<!--			<div class="row">-->
<!--				<div class="col-xs-12 col-sm-8">-->
<!--				-->
<!--				</div>-->
<!--				<div class="col-xs-12 col-sm-4">-->
<!--				</div>-->
<!--			</div>-->
<!--			-->
<!--			-->
<!--			<div class="row">-->
<!--				<div class="col-xs-12 col-sm-6">-->
<!--				-->
<!--				</div>-->
<!--				<div class="col-xs-12 col-sm-6">-->
<!--				-->
<!--				</div>-->
<!--            </div>-->
<!--			<div class="row">-->
<!--				<div class="col-xs-12 col-sm-6">-->
<!--       		 -->
<!--       			</div>-->
<!--					<div class="col-xs-12 col-sm-6">-->
<!--					-->
<!--					</div>-->
<!--            </div>-->
			
        </div>
    </div>
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	<?php ActiveForm::end(); ?>

</div>
