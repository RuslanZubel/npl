<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\News */

$this->title = 'Редактирование новости: ' . $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['/content/news']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="news-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
