<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\News */

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['/content/news']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'short_title',
            'text:ntext',
            'short_text:ntext',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function() use ($model) { return "<img src='{$model->getFileUrl('image')}' />"; },
            ],
            [
                'attribute' => 'show_on_index',
                'value' => function($item) {
                    return $item->getIsShowOnMainName();
                }
            ],
            'sort',
            'display_date',
            'fk_author',
            'created_at',
            'updated_at',
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return $item->getIsActiveName();
                }
            ],
        ],
    ]) ?>

</div>
