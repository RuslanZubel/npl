<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Leagues */

$this->title = 'Создание лиги';

$this->params['breadcrumbs'][] = ['label' => 'Лиги', 'url' => ['/content/leagues']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leagues-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
