<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Leagues */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="leagues-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>

                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= \backend\components\widgets\crud_buttons\CrudButtons::widget(['model' => $model]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
