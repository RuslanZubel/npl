<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PartnersStatuses */

$this->title = 'Редактирование статуса партнера: ' . $model->description;

$this->params['breadcrumbs'][] = ['label' => 'Статусы партнёров', 'url' => ['/content/partners-statuses']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="partners-statuses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
