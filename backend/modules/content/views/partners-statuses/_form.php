<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PartnersStatuses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partners-statuses-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>

        </div>
    </div>
    <div class="form-group">
        <?= \backend\components\widgets\crud_buttons\CrudButtons::widget(['model' => $model]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
