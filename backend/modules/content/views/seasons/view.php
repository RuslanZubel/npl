<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Seasons */

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Сезоны', 'url' => ['/content/seasons']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seasons-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            'created_at',
            'updated_at',
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return $item->getIsActiveName();
                }
            ],
            [
                'attribute' => 'archived',
                'value' => function($item) {
                    return $item->getIsArchivedName();
                }
            ],
            'fk_author',
        ],
    ]) ?>

</div>
