<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Seasons */

$this->title = 'Редактирование сезона: ' . $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Сезоны', 'url' => ['/content/seasons']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="seasons-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
