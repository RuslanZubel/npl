<?php

use yii\helpers\Html;
use yii\helpers\Url;
use richardfan\sortable\SortableGridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\StagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Этапы';
?>
<div class="stages-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-6 buttons-right">
            <?=             Html::a('Create Stages'            , ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


    <?php Pjax::begin(); ?>
    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => '« «',
            'lastPageLabel' => '» »',
        ],
        'sortUrl' => Url::to(['sortItem']),
        'columns' => [
            'title',
            [
                'attribute' => 'Сезон',
                'value' => function ($data) {
                    return !empty($data->fkSeasons) ? $data->fkSeasons->title : 'Не определен';
                },
            ],
            'description',
            ['class' => 'yii\grid\ActionColumn'],
        ],
        ]); ?>
    

            <?php Pjax::end(); ?>        </div>
    </div>
</div>