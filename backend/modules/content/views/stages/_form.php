<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Stages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stages-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>



                    <div class="form-group field-stages-description">
                        <label class="control-label" for="subcat-id">Лига</label>
                        <?= \yii\helpers\Html::dropDownList(
                            'seasons',
                            null,
                            \backend\models\Leagues::getList(),
                            [ 'id' => 'leagues_dep_drop', 'class' => 'form-control']
                        ); ?>
                        <div class="help-block"></div>
                    </div>
                    <?= $form->field($model, 'fk_seasons')->widget(DepDrop::classname(), [
                        'options' => ['id' => 'subcat-id'],
                        'pluginOptions' => [
                            'depends' => ['leagues_dep_drop'],
                            'placeholder' => 'Select...',
                            'url' => Url::to(['/content/seasons/subcat'])
                        ]
                    ]); ?>

                    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= \backend\components\widgets\crud_buttons\CrudButtons::widget(['model' => $model]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
