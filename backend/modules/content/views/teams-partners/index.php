<?php

use yii\helpers\Html;
use backend\components\SortableGridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\TeamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Партнёры команд';

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="teams-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-6 buttons-right">
            <?=             Html::a('Создать партнёра команды'            , ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


    <?php Pjax::begin(); ?>            <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => '« «',
            'lastPageLabel' => '» »',
        ],
        'sortUrl' => Url::to(['sortItem']),
        'columns' => [
            'title',
            [
                'label' => 'Логотип',
                'format' => 'HTML',
                'value' => function($data){
                    return '<img src="' . $data->getFileUrl('image') . '" style="width: 50px">';
                },
            ],
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return \backend\components\ActiveToggle::getView($item);
                },
                'format' => 'raw',
            ],
            'created_at',
            // 'updated_at',
            // 'fk_author',
            // 'image',
            // 'cover_image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        ]); ?>
    

            <?php Pjax::end(); ?>        </div>
    </div>
</div>