<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use backend\components\SortableGridView;

$this->registerJs("

//TODO: не осилил обновление модального диалога редактирования списка партнёров для команды в рамках сезона - сделал как смог...
function reload_edit_partners() {
    var popup = $('#team-partners-modal');
    $.post(
        '" . Url::toRoute('/content/teams-partners/edit-partners') . "', 
            {'fkRelTeamsRelSeasonsLeagues': popup.attr('rel'), 'fkTeam': popup.attr('team')}, 
            function( content ){
            popup.find('.modal-ajax').html( content );
        } 
    ); 
}

function add_team_partner() {
    var id = $('#teams_partners_select').val();
    $.post('" . Url::toRoute('/content/teams-partners/add-partner') . "', 
    {'fkRelTeamsRelSeasonsLeagues': " . $fkRelTeamsRelSeasonsLeagues . ",
      'partnerId' : id},
    function( resp ){
        if (resp.error != '') {
            alert(resp.error);
        } else {
            reload_edit_partners();
        }    
    });
};

function remove_team_partner(id) {
    krajeeDialogCust.confirm('Удалить данного партнёра команды?', function(out){
            if(out) {
                $.post('" . Url::toRoute('/content/teams-partners/remove-partner') . "', 
                {'fkRelTeamsRelSeasonsLeagues': " . $fkRelTeamsRelSeasonsLeagues . ",
                  'relId' : id},
                function( resp ){
                    if (resp.error != '') {
                        alert(resp.error);
                    } else {
                        reload_edit_partners();
                    }    
                });
        }});
};


");

?>




<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h1 class="modal-title">Редактирование списка партнёров команды: <?= $teamTitle; ?>
        </div>
        <div class="modal-body">

            <div class="row">
                <?php $form = ActiveForm::begin(); ?>
                <div class="col-xs-6 col-sm-6">
                    <?php

                    echo Select2::widget([
                        'name' => 'teams_partners_select',
                        'id' => 'teams_partners_select',
                        'data' => \common\models\TeamsPartners::getList(),
                        'options' => [
                            'multiple' => false,
                        ],
                        'pluginOptions' => [
                            'closeOnSelect' => false,
                        ],
                    ]);

                    ?>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div class="form-group">
                        <p><?= Html::a('Добавить', null, ['class' => 'btn btn-primary', 'onclick' => 'add_team_partner();']) ?></p>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div>
                <?php Pjax::begin(['id' => 'teams-partners-pjax']); ?>
                <?= SortableGridView::widget([
                    'id' => \common\components\UniqueString::getUniqueString(6),
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n{pager}",
                    'pager' => [
                        'firstPageLabel' => '« «',
                        'lastPageLabel' => '» »',
                    ],
                    'sortUrl' => Url::to(['sortItemTeamsPartners']),
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'fk_teams_partners',
                            'label' => 'Партнёр',
                            'format' => 'raw',
                            'value' => function($item) { return $item->fkTeamsPartners->title;},
                        ],
                        [
                            'label' => 'Логотип',
                            'format' => 'raw',
                            'value' => function($item) { return '<img src="' . $item->fkTeamsPartners->getFileUrl() . '" style="width: 50px">'; },
                        ],
                        //'sort',
                         [
                            'class' => 'yii\grid\ActionColumn',
                            'template'=>'{delete}',
                            'buttons'=>[
                                'delete' => function ($url, $item){
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-trash pr5 cp"></span>',
                                        null,
                                        [
                                            'title' => Yii::t('yii', 'Удалить'),
                                            'onclick' => "remove_team_partner({$item->id});"
                                        ]
                                    );
                                },
                            ]
                        ],
                    ],
                ]); ?>


                <?php Pjax::end(); ?>

            </div>
            <div style="padding-left: 20px">Здесь вы можете редактировать список партнёров команды</div>
        </div>
    </div>
</div>