<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Partners */

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Партнёры команд', 'url' => ['/content/teams-partners/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="partners-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sort',
            'title',
            'site_url:url',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function() use ($model) { return "<a href='" .  Url::toRoute(['/content/teams-partners/crop-logo', 'id' => $model->id]) . "'><img src='{$model->getFileUrl()}' /></a>";},
            ],
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return $item->getIsActiveName();
                }
            ],
            'created_at',
            'updated_at',
            'fk_author',
        ],
    ]) ?>

</div>
