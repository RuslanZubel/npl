<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Photo */

$this->title = 'Изменить логотип для главной страницы';
$this->params['breadcrumbs'][] = ['label' => 'Партнёры', 'url' => ['/content/teams-partners/index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => Url::toRoute(['/content/teams-partners/view', 'id' => $model->id])];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="photo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= \cozumel\cropper\ImageCropper::widget(['id' => 'user_profile_photo', 'minWidth' => 345]); ?>

    <img width="<?= $model->getImgWidth('image'); ?>" height="<?= $model->getImgHeight('image'); ?>" max-width="<?= 600;?>" class="border" id="user_profile_photo" alt="<?= 'image';?>" src="<?= $model->getFileUrl('image'); ?>">

    <div style="display:none;" id="js_photo_preview">
        <strong>Предпросмотр</strong>
        <div class="p_2">
            <div id="js_profile_photo_preview_container" style="position:relative; overflow:hidden; width:75px; height:75px; border:1px #000 solid;">
                <img width="<?= $model->getImgWidth('image'); ?>" height="<?= $model->getImgHeight('image'); ?>" class="border" id="js_profile_photo_preview" alt="<?= 'image';?>" src="<?= $model->getFileUrl('image'); ?>" style="">
            </div>
        </div>
    </div>

    <?php
    $form = ActiveForm::begin(['options' => ['id' => 'crop_form'],
    ]);
    ?>
    <div><input type="hidden" id="x1" value="" name="x1"></div>
    <div><input type="hidden" id="y1" value="" name="y1"></div>
    <div><input type="hidden" id="x2" value="" name="x2"></div>
    <div><input type="hidden" id="y2" value="" name="y2"></div>
    <div><input type="hidden" id="w" value="" name="w"></div>
    <div><input type="hidden" id="h" value="" name="h"></div>
    <div><input type="hidden" value="<?= $model->getImgWidth('image'); ?>" name="image_width"></div>
    <div><input type="hidden" value="<?= $model->getImgHeight('image'); ?>" name="image_height"></div>
    <div style="margin-top:10px;">
        <input type="submit" class="button" id="js_save_profile_photo" value="Сохранить изменения">
    </div>

    <?php ActiveForm::end(); ?>


</div>
