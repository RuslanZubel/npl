<?php

namespace backend\modules\mediafolder\controllers;

use backend\models\SiteGalleriesImages;
use backend\modules\mediafolder\components\BaseMediaFolderController;
use backend\modules\mediafolder\models\SiteGalleriesImagesSearch;
use Yii;
use backend\models\SiteGalleries;
use backend\modules\mediafolder\models\SiteGalleriesSearch;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use richardfan\sortable\SortableAction;

/**
 * SiteGalleriesController implements the CRUD actions for SiteGalleries model.
 */


class SiteGalleriesController extends BaseMediaFolderController
{

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => SiteGalleriesImages::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteGalleries models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteGalleriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteGalleries model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SiteGalleries model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SiteGalleries();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save() === false) {
                //VarDumper::dump($model->getErrors());
                //die;
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SiteGalleries model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
	
		$searchModel = new SiteGalleriesImagesSearch();
		$dataProvider = $searchModel->search( $id, Yii::$app->request->queryParams);
	
	
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {

            return $this->render('update', [
                'model' => $model,
				'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing SiteGalleries model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SiteGalleries model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SiteGalleries the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiteGalleries::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionEditTitle()
	{
		SiteGalleriesImages::updateAll(
			[
				'description' => \Yii::$app->request->post('description')
			],
			[
				'id' => \Yii::$app->request->post('id')
			]
		);
		
	}
	
	public function actionUpload($id)
	{
		$fileName = 'file';

		if (isset($_FILES[$fileName])) {
			$file      =   \yii\web\UploadedFile::getInstanceByName($fileName);
			$model     =   new SiteGalleriesImages();
			$model     ->  image = $file;
			$model     ->  is_active = SiteGalleriesImages::ACTIVE_TRUE;
			$model->fk_site_galleries = $id;
			if ($model->save()) {
     //       v([$model->validate(), $model->getErrors()], true, true);
				
				return $this->asJson(['status' => 'ok']);
				
			}
		}
		
		return $this->asJson(['status' => 'fail']);
	}
	
	
	
}
