<?php

namespace backend\modules\mediafolder\controllers;

use Yii;
use backend\models\SiteGalleries;
use backend\models\SiteGalleriesImages;
use backend\modules\mediafolder\models\SiteGalleriesImagesSearch;
use backend\modules\mediafolder\components\BaseMediaFolderController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use richardfan\sortable\SortableAction;



/**
 * SiteGalleriesImagesController implements the CRUD actions for SiteGalleriesImages model.
 */
class SiteGalleriesImagesController extends BaseMediaFolderController
{

    public function actions()
    {
        return [
            'sortItem' => [
                'class' => SortableAction::className(),
                'activeRecordClassName' => SiteGalleriesImages::className(),
                'orderColumn' => 'sort',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteGalleriesImages models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new SiteGalleriesImagesSearch();
        $dataProvider = $searchModel->search( $id, Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteGalleriesImages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SiteGalleriesImages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SiteGalleriesImages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SiteGalleriesImages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SiteGalleriesImages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        $id = (int)\Yii::$app->request->post('id');

        $model = SiteGalleriesImages::findOne($id);
        if (!$model) {
            $answerJSON['error'] = 'Фотография не найдена';
            return $this->asJson($answerJSON);
        }

        if ($model->delete() === false) {
            $answerJSON['error'] = 'Ошибка работы с БД';
            return $this->asJson($answerJSON);
        }

        return $this->asJson($answerJSON);
    }

    public function actionMakeCover()
    {
        $answerJSON = [
            'data' => [],
            'error' => ''
        ];

        $galleryId = (int)\Yii::$app->request->post('galleryId');
        $id = (int)\Yii::$app->request->post('id');

        $gallery = SiteGalleries::findOne($galleryId);
        if (!$gallery) {
            $answerJSON['error'] = 'Галлерея не найдена';
            return $this->asJson($answerJSON);
        }

        $model = SiteGalleriesImages::findOne($id);
        if (!$model) {
            $answerJSON['error'] = 'Фотография не найдена';
            return $this->asJson($answerJSON);
        }

        $photos = SiteGalleriesImages::find()->where(['fk_site_galleries' => $galleryId])->all();

        foreach ($photos as $photo) {
            if ($photo->id == $id) {
                $photo->is_cover = "yes";
            } else {
                $photo->is_cover = "no";
            }
            if ($photo->save() === false) {
                $answerJSON['error'] = 'Ошибка работы с БД';
                break;
            }
        }

        return $this->asJson($answerJSON);
    }

    /**
     * Finds the SiteGalleriesImages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SiteGalleriesImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiteGalleriesImages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	public function actionEditTitle()
	{
		SiteGalleriesImages::updateAll(
			[
				'description' => \Yii::$app->request->post('description')
			],
			[
				'id' => \Yii::$app->request->post('id')
			]
		);
		
	}
	
	public function actionUpload()
	{
		$fileName = 'file';
		
		
		if (isset($_FILES[$fileName])) {
			$file      =   \yii\web\UploadedFile::getInstanceByName($fileName);
			$model     =   new SiteGalleriesImages();
			$model     ->  image = $file;
			$model     ->  is_active = SiteGalleriesImages::ACTIVE_TRUE;
			if ($model->save()) {
//          v([$model->validate(), $model->getErrors()], true, true);
			return $this->asJson(['status' => 'ok']);
				
			}
		}
		
		return $this->asJson(['status' => 'fail']);
	}
	
	
	
}
