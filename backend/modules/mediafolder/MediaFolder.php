<?php

namespace backend\modules\mediafolder;

/**
 * mediafolder module definition class
 */
class MediaFolder extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\mediafolder\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
