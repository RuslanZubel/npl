<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\mediafolder\models\SiteVideosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Видеогалереи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-videos-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-6 buttons-right">
            <?=             Html::a('Создать видеогалерею'            , ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


    <?php Pjax::begin(); ?>            <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => '« «',
            'lastPageLabel' => '» »',
        ],
        'sortUrl' => Url::to(['sortItem']),
      //  'filterModel' => $searchModel,
        'options'=>['style'=>'white-space: normal;'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

      //    'id',
            'title',
      //      'description',
      //      'source_type',
            'video_id',
            // 'cover_image',
            // 'sort',
            // 'show_on_index',
			'created_at',
			'custom_date',
            //'fk_author',
            // 'updated_at',
            // 'is_active',
            [
                'attribute' => 'show_on_index',
                'value' => function($item) {
                    return $item->getIsShowOnMainName();
                }
            ],
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return \backend\components\ActiveToggle::getView($item);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
        ]); ?>
    

            <?php Pjax::end(); ?>        </div>
    </div>
</div>