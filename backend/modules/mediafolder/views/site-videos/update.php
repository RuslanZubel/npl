<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SiteVideos */

$this->title = 'Редактирование Видеогалереи: ' . $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Видеогалереи', 'url' => ['/mediafolder/site-videos']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-videos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
