<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\SiteVideos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-videos-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source_type')->dropDownList([ 'youtube' => 'Youtube', 'other' => 'Other', ], ['prompt' => 'Выберите источник']) ?>

    <?= $form->field($model, 'video_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'show_on_index')->dropDownList($model->getShowOnMainDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
    <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>

        <?= $form->field($model, 'custom_date')->widget(\kartik\datetime\DateTimePicker::classname(), [
            'name' => 'datetime_19',
            'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
            'layout' => '{picker}{input}{remove}',
            'convertFormat' => 'true',
            'pluginOptions' => [
                'format' => 'dd-MM-yyyy HH:i',
                'language' => 'en',
                'autoclose' => true,]
        ]) ?>

        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
