<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\SiteVideos */

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Видеогалереи', 'url' => ['/mediafolder/site-videos']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-videos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            'source_type',
            'video_id',
            'cover_image',
            'sort',
            [
                'attribute' => 'show_on_index',
                'value' => function($item) {
                    return $item->getIsShowOnMainName();
                }
            ],
            'custom_date',
            'fk_author',
            'created_at',
            'updated_at',
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return $item->getIsActiveName();
                }
            ],
        ],
    ]) ?>

</div>
