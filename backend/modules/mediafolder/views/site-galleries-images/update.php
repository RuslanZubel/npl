<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SiteGalleriesImages */

$this->title = 'Редактирование Site Galleries Images: ' . $model->id;
?>
<div class="site-galleries-images-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
