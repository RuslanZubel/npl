<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\SiteGalleries */

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Фотогалереи', 'url' => ['/mediafolder/site-galleries']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-galleries-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'subtitle',
            'sort',
            [
                'attribute' => 'show_on_index',
                'value' => function($item) {
                    return $item->getIsShowOnMainName();
                }
            ],
            'custom_date',
            'fk_author',
            'updated_at',
            'created_at',
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return $item->getIsActiveName();
                }
            ],
        ],
    ]) ?>

</div>
