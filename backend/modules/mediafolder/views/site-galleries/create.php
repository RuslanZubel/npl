<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SiteGalleries */

$this->title = 'Создание фотогалереи';

$this->params['breadcrumbs'][] = ['label' => 'Фотогалереи', 'url' => ['/mediafolder/site-galleries']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-galleries-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
