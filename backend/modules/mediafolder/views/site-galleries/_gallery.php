<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;
use kato\DropZone;
use newerton\fancybox\FancyBox;
use common\models\SiteGalleriesImages;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\mediafolder\models\SiteGalleriesImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Загрузка изображений';

$this->registerJs("

$('#site-galleries-images-grid').on('click', '.editable-photo-title', function() {
    $('.photo-feed-title:not(.hidden)').trigger('blur');
    $(this).toggleClass('hidden');
    $(this).siblings('input').toggleClass('hidden');
});


$('#site-galleries-images-grid').on('blur', '.photo-feed-title', function() {
    var th = $(this);
    $.post('" . Url::toRoute('edit-title') . "', { 'id' : th.closest('tr').data('key'), 'description' : th.val() });
    th.toggleClass('hidden');
    th.siblings('span').toggleClass('hidden');
    if ($(this).val() == '') {
       th.siblings('span').html('<span class=\"empty-photo-title\">Нажмите для заполнения</span>');
    } else {
        th.siblings('span').text($(this).val());
    }
});

");
?>

<div class="site-galleries-images-index">
	<h1><?= Html::encode($this->title) ?></h1>
	<div class="row">
		<div class="col-xs-12 col-sm-8 ">
			<div class="pb20">
				<?php echo DropZone::widget([
					'options' => [
						'maxFilesize' => '200',
						'url' => '/admin/mediafolder/site-galleries/upload?id=' . $model->id,
						'addRemoveLinks' => true,
						'createImageThumbnails' => true,
						'dictDefaultMessage' => 'Нажмите здесь или перетащите сюда файлы в формате JPG, JPEG',
						'dictRemoveFile' => 'Удалить',
					],
					'clientEvents' => [
						'complete' => "function(file){console.log(file); $.pjax.reload('#site-galleries-images-grid');}",
						'removedfile' => "function(file){alert(file.name + ' is removed')}"
					],
					'id' => \common\components\UniqueString::getUniqueString(5),
				]);
				?>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4 ">
			<br>
			<p style="font-style: italic; color:gray;">*  Перетащите изображения в область загрузки для добавления в галерею </p>
			<p style="font-style: italic; color:gray;">** Для сортировки фотографий - используйте перетаскивание с зажатой ЛКМ </p>
			<br>
		</div>
	</div>
	<?php Pjax::begin(['id' => 'site-galleries-images-grid']); ?>
   <br>

  <div class="panel panel-default jur-panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">

	
        <?= SortableGridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => ['class' => 'table table-striped table-hover'],
			'layout' => "{items}\n{pager}",
			'pager' => [
				'firstPageLabel' => '« «',
				'lastPageLabel' => '» »',
			],
			'sortUrl' => Url::to([
				'sortItem',
				'direction' => SORT_DESC,
				'page' => \Yii::$app->request->get('page', 1),
				'per-page' => ( $dataProvider->pagination != false ) ? $dataProvider->pagination->pageSize : null,
			]),
			'rowOptions' => function ($model, $key, $index, $grid) {
				$model->rowClass = '';
				if ($model->is_active == common\models\BaseModel::ACTIVE_FALSE) {
					$model->rowClass = 'disabled-tr ';
				}
		
				return ['class' => $model->rowClass];
			},
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				[
					'label' => 'Фото',
					'format' => 'raw',
					'value' => function ($data) {
						return Html::a(
							Html::img($data->getThumbFileUrl('image', 'thumb'), ['width' => '100px']),
							$data->getFileUrl('image'),
							['rel' => 'fancy', 'data-pjax' => 0]
						);
					},
					'options' => [
						'width' => '100px',
					]
				],
				[
					'attribute' => 'description',
					'options' => [
						'width' => '50%'],
					'value' => function ($data) {
						$str = Html::tag('span', empty ($data->description) ? '<span class="empty-photo-title">Нажмите для заполнения</span>' : $data->description, ['class' => 'editable-photo-title']);
						$str .= Html::input('text', '', $data->description, ['class' => 'form-control hidden photo-feed-title']);
						return $str;
					},
					'format' => 'raw',
					'contentOptions' => [
						'class' => 'editable-title-td'
					]
				],
				'custom_date',
				[
					'attribute' => 'is_active',
					'value' => function ($data) {
						if ($data->is_active == \backend\models\SiteGalleriesImages::ACTIVE_TRUE) {
							return '<span class="toggle-active cp" rel="yes"><span class="glyphicon glyphicon-minus-sign"></span> Скрыть</span>';
						} else {
							return '<span class="toggle-active cp" rel="no"><span class="glyphicon glyphicon-plus-sign"></span> Показать</span>';
						}
					},
					'format' => 'raw',
					'options' => [
						'width' => '50px',
					]
				],
                [
                        'attribute' => 'is_cover',
                        'format' => 'raw',
                        'value' => function($item) {
                            if ($item->is_cover == "yes") {
                                return "Да";
                            }
                        }
                ],
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{delete}{make_cover}',
					'options' => [
						'width' => '40px',
					],
                    'buttons'=>[
                        'delete' => function ($url, $item){
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash pr5 cp"></span>',
                                null,
                                [
                                    'title' => Yii::t('yii', 'Удалить'),
                                    'onclick' => "remove_gallery_image({$item->id});"
                                ]
                            );
                        },
                        'make_cover' => function ($url, $item) use ($model) {
                            if ($item->is_cover == "no") {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-ok-circle pr5 cp"></span>',
                                    null,
                                    [
                                        'title' => Yii::t('yii', 'Сделать основным'),
                                        'onclick' => "make_gallery_image_main({$model->id}, {$item->id});"
                                    ]
                                );
                            }

                            return "";
                        },
                    ]
				],
			],


		]); ?>

		<?= FancyBox::widget([
			'target' => 'a[rel=fancy]',
			'helpers' => true,
			'mouse' => true,
			'config' => [
				'maxWidth' => '90%',
				'maxHeight' => '90%',
				'playSpeed' => 7000,
				'padding' => 0,
				'fitToView' => false,
				'width' => '70%',
				'height' => '70%',
				'autoSize' => false,
				'closeClick' => false,
				'openEffect' => 'elastic',
				'closeEffect' => 'elastic',
				'prevEffect' => 'elastic',
				'nextEffect' => 'elastic',
				'closeBtn' => false,
				'openOpacity' => true,
				'helpers' => [
					'title' => ['type' => 'float'],
					'buttons' => [],
					'thumbs' => ['width' => 68, 'height' => 50],
					'overlay' => [
						'css' => [
							'background' => 'rgba(0, 49, 70, 0.8)'
						]
					]
				],
			]
		]);

		?>
		<?php Pjax::end(); ?>        </div>
    </div>
</div>

<?php
$this->registerJsFile('/js/site-gallery.js', ['position' => \yii\web\View::POS_END]);
?>