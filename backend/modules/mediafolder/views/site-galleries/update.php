<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SiteGalleries */

$this->title = 'Редактирование галереи: ' . $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Фотогалереи', 'url' => ['/mediafolder/site-galleries']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-galleries-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

	<?= $this->render('_gallery', [
		'dataProvider' => $dataProvider,
		'model' => $model,
	]) ?>
	
</div>

