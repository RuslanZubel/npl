<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

/* @var $this yii\web\View */
/* @var $model backend\models\SiteGalleries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-galleries-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default jur-panel">
        <div class="panel-body">
			<div class="row">
					<div class="col-xs-12 col-sm-5">
  						<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-xs-12 col-sm-3">
						<?= $form->field($model, 'custom_date')->widget(\kartik\datetime\DateTimePicker::classname(), [
							'name' => 'datetime_19',
							'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
							'layout' => '{picker}{input}{remove}',
							'convertFormat' => 'true',
							'pluginOptions' => [
								'format' => 'dd-MM-yyyy HH:i',
								'language' => 'en',
								'autoclose' => true,]
						]) ?>
					</div>

                    <div class="col-xs-12 col-sm-2">
                        <?= $form->field($model, 'show_on_index')->dropDownList($model->getShowOnMainDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
                    </div>
                    <div class="col-xs-12 col-sm-2">
                        <?= $form->field($model, 'is_active')->dropDownList($model->getYesNoDropdownList(), ['prompt' => $model->getPromptLabel()]) ?>
                    </div>
					<div class="form-group col-xs-12 col-sm-2" style="margin-top: 25px;">
      					  <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
   					</div>
			</div>
			<div class="row">
			<div class="col-xs-12 col-sm-12">
				<?= $form->field($model, 'subtitle')->widget(TinyMce::className(), [
					'options' => [
						'rows' => 10,
					],
					'settings' => [
						'object_resizing' => false,
						'image_dimensions' => false,
						'image_class_list' => [
							['title' => 'Responsive', 'value' => 'img-responsive']
						],
					],
					'language' => 'ru',
					'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
					'fileManager' => [
						'class' => TinyMceElFinder::className(),
						'connectorRoute' => '/el-finder/connector',
					],
				]);
				?>
			</div>
			</div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
