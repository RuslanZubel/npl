<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\SortableGridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\mediafolder\models\SiteGalleriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фотогалереи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-galleries-index">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-6 buttons-right">
            <?=             Html::a('Создать фотогалерею'            , ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <div class="panel panel-default jur-panel">
        <div class="panel-body">


    <?php Pjax::begin(); ?>            <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => '« «',
            'lastPageLabel' => '» »',
        ],
        'sortUrl' => Url::to(['sortItem']),
     //   'filterModel' => $searchModel,
        'columns' => [
      //      ['class' => 'yii\grid\SerialColumn'],
      //      'id',
            'title',
            //'subtitle',

            [
                'attribute' => 'show_on_index',
                'value' => function($item) {
                    return $item->getIsShowOnMainName();
                }
            ],

            'custom_date',
            // 'fk_author',
            // 'updated_at',
            // 'created_at',
            [
                'attribute' => 'is_active',
                'value' => function($item) {
                    return \backend\components\ActiveToggle::getView($item);
                },
                'format' => 'raw',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
        ]); ?>
    

            <?php Pjax::end(); ?>        </div>
    </div>
</div>