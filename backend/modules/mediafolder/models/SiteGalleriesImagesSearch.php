<?php

namespace backend\modules\mediafolder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SiteGalleriesImages;

/**
 * SiteGalleriesImagesSearch represents the model behind the search form about `backend\models\SiteGalleriesImages`.
 */
class SiteGalleriesImagesSearch extends SiteGalleriesImages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fk_site_galleries', 'custom_date', 'sort', 'fk_author', 'created_at', 'updated_at'], 'integer'],
            [['image', 'description', 'is_cover', 'is_active'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($id, $params)
    {
        $query = SiteGalleriesImages::find();
		$query->andWhere(['fk_site_galleries' => $id]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort'=>[
                'defaultOrder'=>[
                    'sort' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'custom_date' => $this->custom_date,
            'sort' => $this->sort,
            'fk_author' => $this->fk_author,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'is_cover', $this->is_cover])
            ->andFilterWhere(['like', 'is_active', $this->is_active]);

        return $dataProvider;
    }
}
