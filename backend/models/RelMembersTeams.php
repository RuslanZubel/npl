<?php

namespace backend\models;
use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

//НА УДАЛЕНИЕ
//
//
//
//
//
//
//
//
//
//                 ДАННУЮ МОДЕЛЬ МЫ МЕНЯЕМ НА ДРУГУЮ http://joxi.ru/52a6D4WtG1yl0A
//
//
//
//
//
//
//
//
//
//
//
//
//
//НА УДАЛЕНИЕ
/**
 * This is the model class for table "rel_members_teams".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property int $fk_members
 * @property int $fk_teams
 *
 * @property Users $fkAuthor
 * @property Members $fkMembers
 * @property Teams $fkTeams
 */
class RelMembersTeams extends \common\models\RelMembersTeams
{

    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['fk_members', 'fk_teams'], 'integer'],
            [['fk_members'], 'exist', 'skipOnError' => true, 'targetClass' => Members::className(), 'targetAttribute' => ['fk_members' => 'id']],
            [['fk_teams'], 'exist', 'skipOnError' => true, 'targetClass' => Teams::className(), 'targetAttribute' => ['fk_teams' => 'id']],
        ];

        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'fk_author' => 'Fk Author',
            'fk_members' => 'Fk Members',
            'fk_teams' => 'Fk Teams',
        ];
    }
}


