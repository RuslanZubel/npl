<?php

namespace backend\models;

use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "teams_structures".
 *
 * @property int $id
 * @property int $fk_members
 * @property int $fk_rel_teams_rel_seasons_league
 * @property int $fk_roles
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property string $is_active
 */
class TeamsStructures extends \common\models\TeamsStructures
{

    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teams_structures';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['fk_members', 'fk_rel_teams_rel_seasons_league', 'fk_roles'], 'integer'],
        ];
        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_members' => 'Fk Members',
            'fk_rel_teams_rel_seasons_league' => 'Fk Rel Teams Rel Seasons League',
            'fk_roles' => 'Fk Roles',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'fk_author' => 'Fk Author',
            'is_active' => 'Is Active',
        ];
    }

}
