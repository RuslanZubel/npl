<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "rel_stages_rel_seasons_leagues".
 *
 * @property int $id
 * @property int $fk_stages
 * @property int $fk_rel_seasons_leagues
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property string $is_active
 *
 * @property Races[] $races
 * @property Stages $fkStages
 * @property RelSeasonsLeagues $fkRelSeasonsLeagues
 */
class RelStagesRelSeasonsLeagues extends \common\models\RelStagesRelSeasonsLeagues
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_stages', 'fk_rel_seasons_leagues'], 'integer'],
            [['fk_stages'], 'exist', 'skipOnError' => true, 'targetClass' => Stages::className(), 'targetAttribute' => ['fk_stages' => 'id']],
            [['fk_rel_seasons_leagues'], 'exist', 'skipOnError' => true, 'targetClass' => RelSeasonsLeagues::className(), 'targetAttribute' => ['fk_rel_seasons_leagues' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_stages' => 'Fk Stages',
            'fk_rel_seasons_leagues' => 'Fk Rel Seasons Leagues',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'fk_author' => 'Fk Author',
            'is_active' => 'Is Active',
        ];
    }

}
