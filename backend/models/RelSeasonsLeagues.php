<?php

namespace backend\models;


use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rel_seasons_leagues".
 *
 * @property int $id
 * @property int $fk_seasons
 * @property int $fk_leagues
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property string $is_active
 *
 * @property Users $fkAuthor
 * @property Leagues $fkLeagues
 * @property Seasons $fkSeasons
 * @property RelStagesRelSeasonsLeagues[] $relStagesRelSeasonsLeagues
 */
class RelSeasonsLeagues extends \common\models\RelSeasonsLeagues
{

    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['fk_seasons', 'fk_leagues'], 'integer'],
            [['fk_seasons', 'fk_leagues'], 'required'],
            [['is_active'], 'string'],
            [['fk_leagues'], 'exist', 'skipOnError' => true, 'targetClass' => Leagues::className(), 'targetAttribute' => ['fk_leagues' => 'id']],
            [['fk_seasons'], 'exist', 'skipOnError' => true, 'targetClass' => Seasons::className(), 'targetAttribute' => ['fk_seasons' => 'id']],
        ];
        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_seasons' => 'Сезон',
            'fk_leagues' => 'Лига',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'fk_author' => 'Автор',
            'is_active' => 'Видимость',
        ];
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

}
