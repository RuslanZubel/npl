<?php

namespace backend\models;

use yii\helpers\ArrayHelper;
use backend\traits\BaseBackendTrait;

/**
 * This is the model class for table "site_galleries_images".
 *
 * @property int $id id
 * @property int $fk_site_galleries Галерея
 * @property string $image изображение
 * @property string $description Описание
 * @property int $custom_date Использовать свою дату
 * @property int $sort Сортировка
 * @property string $is_cover являе>тся обложкой
 * @property int $id_author id автора
 * @property int $created_at дата создания
 * @property int $updated_at дата редактирования
 * @property string $is_active видимость
 *
 * @property SiteGalleries $fkSiteGalleries
 */
class SiteGalleriesImages extends \common\models\SiteGalleriesImages
{

    use BaseBackendTrait;

    public static function tableName()
    {
        return 'site_galleries_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        $selfRules = [
            [['fk_site_galleries', 'custom_date', 'sort'], 'integer'],
            [['is_cover'], 'string'],
			[['image'] , 'file'],
            [[ 'description'], 'string', 'max' => 255],
            [['fk_site_galleries'], 'exist', 'skipOnError' => true, 'targetClass' => SiteGalleries::className(), 'targetAttribute' => ['fk_site_galleries' => 'id']],
        ];

        return ArrayHelper::merge($selfRules, $this->baseRules());


    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'fk_site_galleries' => 'Галерея',
            'image' => 'изображение',
            'description' => 'Описание',
            'custom_date' => 'Использовать свою дату',
            'sort' => 'Сортировка',
            'is_cover' => 'является обложкой',
            'fk_author' => 'id автора',
            'created_at' => 'дата создания',
            'updated_at' => 'дата редактирования',
            'is_active' => 'видимость',
        ];
    }

}
