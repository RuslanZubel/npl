<?php

namespace backend\models;
use yii\helpers\ArrayHelper;

use backend\traits\BaseBackendTrait;

class Seasons extends \common\models\Seasons
{
    use BaseBackendTrait;

    const ARCHIVED_TRUE = 'yes';
    const ARCHIVED_FALSE = 'no';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['sort'], 'integer'],
            [['title', 'description'], 'string', 'max' => 255],
            [['title'], 'required'],
            [['archived'], 'string'],
            [['archived'], 'required'],
           ];

        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Комментарий',
            'sort' => 'Sort', //Добавлена сортировка
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'fk_author' => 'Автор',
            'is_active' => 'Видимость',
            'archived' => 'Архивность',
        ];
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

    public static function getList($params = [])
    {
        $query = self::find();

        if (isset($params['condition'])) {
            $query->where($params['condition']);
        }

        $query->orderBy([
            'created_at' => SORT_ASC,
            'title' => SORT_ASC
        ]);

        return ArrayHelper::map(
            $query->all(),
            'id',
            'title'
        );
    }

    public function getArchivedDropdownList()
    {
        return [self::ARCHIVED_TRUE => 'Архивный', self::ARCHIVED_FALSE => 'Неархивный'];
    }

    public function getIsArchivedName()
    {
        $names = $this->getArchivedDropdownList();
        if ($this->archived == 'yes') {
            return $names[self::ARCHIVED_TRUE];
        } else if ($this->archived == 'no') {
            return $names[self::ARCHIVED_FALSE];
        } else {
            return "-";
        }
    }
}
