<?php

namespace backend\models;

use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

//Проработано Илья 19 марта

class Partners extends \common\models\Partners

{

    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['title'], 'required'],
            [['text', 'shorttext'], 'string'],
            [['sort'], 'integer'],
            [['title', 'site_url'], 'string', 'max' => 255],
            [['image'], 'file', 'maxSize' => 10 * 1024 * 1024, 'skipOnEmpty' => true,
                'except' => 'ajax', 'extensions' => 'jpg, png'],
            [['image_main'], 'file', 'maxSize' => 10 * 1024 * 1024, 'skipOnEmpty' => true,
                'except' => 'ajax', 'extensions' => 'jpg, png'],
            [['shorttitle'], 'string', 'max' => 130],
          ];

        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Полное название спонсора',
            'shorttitle' => 'Краткое название',
            'text' => 'Описание спонсора',
            'shorttext' => 'Краткое описание',
            'site_url' => 'Ссылка на сайт',
            'image' => 'Логотип. Партнёры',
            'image_main' => 'Логотип. Главная',
            'sort' => 'Сортировка',
            'fk_author' => 'Автор',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'is_active' => 'Видимость',
        ];
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }


}
