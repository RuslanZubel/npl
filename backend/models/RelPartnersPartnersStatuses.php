<?php

namespace backend\models;

use yii\helpers\ArrayHelper;
use backend\traits\BaseBackendTrait;

/**
 * This is the model class for table "rel_partners_partners_statuses".
 *
 * @property int $id
 * @property int $fk_partners Партнер
 * @property int $fk_partners_statuses Статус
 * @property int $fk_author
 * @property int $created_at
 * @property int $updated_at
 * @property string $is_active
 *
 * @property Partners $fkPartners
 * @property PartnersStatuses $fkPartnersStatuses
 * @property Users $fkAuthor
 */
class RelPartnersPartnersStatuses extends \common\models\RelPartnersPartnersStatuses
{
    use BaseBackendTrait;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['fk_partners', 'fk_partners_statuses'], 'integer'],
            [['fk_partners'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['fk_partners' => 'id']],
            [['fk_partners_statuses'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersStatuses::className(), 'targetAttribute' => ['fk_partners_statuses' => 'id']],
            [['sort'], 'safe']
        ];
        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_partners' => 'Партнер',
            'fk_partners_statuses' => 'Статус',
            'fk_author' => 'Fk Author',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'is_active' => 'Is Active',
            'sort' => 'sort',
        ];
    }

}
