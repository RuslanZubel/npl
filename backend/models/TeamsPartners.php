<?php

namespace backend\models;

use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

class TeamsPartners extends \common\models\TeamsPartners
{

    use BaseBackendTrait;

    public function rules()
    {
        $selfRules = [
            [['description', 'is_active'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'required'],
            [['sort'],'integer' ],
            [['image'], 'file', 'maxSize' => 10 * 1024 * 1024, 'skipOnEmpty' => true,
                'except' => 'ajax', 'extensions' => 'jpg, jpeg, png'],
            [['sort'],'safe' ]
        ];
        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название спонсора',
            'description' => 'Описание спонсора',
            'site_url' => 'Ссылка на сайт',
            'image' => 'Логотип',
            'sort' => 'Сортировка',
            'fk_author' => 'Автор',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'is_active' => 'Видимость',
        ];
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

}
