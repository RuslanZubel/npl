<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "rel_teams_rel_seasons_leagues".
 *
 * @property int $id
 * @property int $fk_teams
 * @property int $fk_rel_seasons_leagues
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property string $is_active
 *
 * @property Teams $fkTeams
 * @property RelSeasonsLeagues $fkRelSeasonsLeagues
 */
class RelTeamsRelSeasonsLeagues extends \common\models\RelTeamsRelSeasonsLeagues
{
    public function rules()
    {
        return [
            [['fk_teams', 'fk_rel_seasons_leagues'], 'integer'],
            [['fk_teams', 'fk_rel_seasons_leagues'], 'required'],
            [['fk_teams'], 'exist', 'skipOnError' => true, 'targetClass' => Teams::className(), 'targetAttribute' => ['fk_teams' => 'id']],
            [['fk_rel_seasons_leagues'], 'exist', 'skipOnError' => true, 'targetClass' => RelSeasonsLeagues::className(), 'targetAttribute' => ['fk_rel_seasons_leagues' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_teams' => 'Fk Teams',
            'fk_rel_seasons_leagues' => 'Fk Rel Seasons Leagues',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'fk_author' => 'Fk Author',
            'is_active' => 'Is Active',
        ];
    }

}
