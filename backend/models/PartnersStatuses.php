<?php
/**
 * Created by PhpStorm.
 * User: amio
 * Date: 16/01/2018
 * Time: 12:53
 */

namespace backend\models;
use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

//Проработано Илья 19 марта

class PartnersStatuses extends \common\models\PartnersStatuses
{

    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [[ 'sort'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['description'], 'required'],
            [['fk_seasons'], 'exist', 'skipOnError' => true, 'targetClass' => Seasons::className(), 'targetAttribute' => ['fk_seasons' => 'id']],

        ];

        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Описание',
            'fk_rel_seasons_leagues' => 'Принадлженость сезону',
            'sort' => 'Сортировка',
            'fk_author' => 'Автор',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'is_active' => 'Видимость',

        ];
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

}