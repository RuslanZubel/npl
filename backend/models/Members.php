<?php

namespace backend\models;
use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

//19 марта Илья


class Members extends \common\models\Members
{
    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['description', ], 'string'],
            [['firstname', 'lastname', 'vfps_id'], 'string', 'max' => 255],
            [['firstname', 'lastname'], 'required'],
            [['image'], 'file', 'maxSize' => 10 * 1024 * 1024, 'skipOnEmpty' => true,
                'except' => 'ajax', 'extensions' => 'jpg, jpeg, png'],
        ];
//
//        return [
//            [['description', 'is_active'], 'string'],
//            [['created_at', 'updated_at', 'fk_author', 'sort'], 'integer'],
//
//            [['fk_author'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['fk_author' => 'id']],
//        ];

        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'vfps_id' => 'Vfps ID',
            'image' => 'Фотография',
            'description' => 'Комментарий',
            'is_active' => 'Видимость',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'fk_author' => 'Автор',
            'sort' => 'Sort', //Добавлено
        ];
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

}
