<?php

namespace backend\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class StagesReceiptSearch extends Model
{
    public $team_title;
    public $stage_title;
    public $confirmed;
    public $league_title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[['team_title', 'stage_title', 'league_title', 'confirmed'], 'safe'],];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $stsSubQuery = (new \yii\db\Query())
            ->select(['confirmed', 'team_id', 'stage_id', 'title', 'receipt.id AS receipt_id'])
            ->from('receipt')
            ->where(['product_type' => 'stage'])
            ->leftJoin('rel_receipt_stages', 'rel_receipt_stages.receipt_id = receipt.id')
            ->leftJoin('teams', 'teams.id = receipt.team_id');

        $preSts = (new \yii\db\Query())
            ->select(['stages.title AS stage_title', 'u.title AS team_title', 'cost', 'team_id', 'confirmed', 'fk_leagues', 'receipt_id'])
            ->from('stages')
            ->rightJoin(['u' => $stsSubQuery], 'u.stage_id = stages.id')
            ->leftJoin('rel_seasons_leagues', 'rel_seasons_leagues.id = stages.fk_rel_seasons_leagues');

        $query = (new \yii\db\Query())
            ->select(['stage_title', 'team_title', 'cost', 'team_id', 'confirmed', 'fk_leagues', 'leagues.title AS league_title', 'receipt_id'])
            ->from('leagues')
            ->rightJoin(['u' => $preSts], 'u.fk_leagues = leagues.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'team_title', $this->team_title]);
        $query->andFilterWhere(['like', 'stage_title', $this->stage_title]);
        $query->andFilterWhere(['like', 'leagues.title', $this->league_title]);
        $query->andFilterWhere(['like', 'confirmed', $this->confirmed]);

        return $dataProvider;
    }
}