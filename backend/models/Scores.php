<?php

namespace backend\models;

use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "scores".
 *
 * @property int $id
 * @property int $fk_races
 * @property int $fk_teams
 * @property int $fk_stickers
 * @property double $score_count
 * @property string $sticker_comment
 * @property double $sticker_score_modifier
 * @property int $created_at
 * @property int $updated_at
 * @property int $id_author
 * @property string $is_active
 *
 * @property Races $fkRaces
 * @property Stickers $fkStickers
 * @property Teams $fkTeams
 * @property Users $author
 */

class Scores extends \common\models\Scores
{
    use BaseBackendTrait;
    /**
     * @inheritdoc
     */

    public function rules()
    {
        $selfRules = [
            [['fk_races', 'fk_teams_rel_seasons_leagues', 'fk_stickers', 'created_at', 'updated_at', 'fk_author'], 'integer'],
            [['score_count', 'sticker_score_modifier'], 'number'],
            [['sticker_comment'], 'string', 'max' => 100],
            [['fk_races'], 'exist', 'skipOnError' => true, 'targetClass' => Races::className(), 'targetAttribute' => ['fk_races' => 'id']],
            [['fk_stickers'], 'exist', 'skipOnError' => true, 'targetClass' => Stickers::className(), 'targetAttribute' => ['fk_stickers' => 'id']],
            [['fk_teams_rel_seasons_leagues'], 'exist', 'skipOnError' => true, 'targetClass' => RelTeamsRelSeasonsLeagues::className(), 'targetAttribute' => ['fk_teams_rel_seasons_leagues' => 'id']],
            [['fk_author'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_author' => 'id']],
        ];

        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_races' => 'Гонка',
            'fk_teams_rel_seasons_leagues' => 'Команда',
            'fk_stickers' => 'Отметка',
            'score_count' => 'Очки',
            'sticker_comment' => 'Коментарий',
            'sticker_score_modifier' => 'Модификатор отметки',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'fk_author' => 'Id Author',
            'is_active' => 'Is Active',
        ];
    }

}
