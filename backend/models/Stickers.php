<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use backend\traits\BaseBackendTrait;

/**
 * This is the model class for table "stickers".
 *
 * @property int $id
 * @property int $fk_leagues
 * @property string $description
 * @property string $color
 * @property int $sort
 * @property int $created_at
 * @property int $updated_at
 * @property string $is_active
 * @property int $fk_author
 *
 * @property Scores[] $scores
 * @property Users $fkAuthor
 * @property Leagues $fkLeagues
 */
class Stickers extends \common\models\Stickers
{

    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['fk_leagues', 'sort', 'created_at', 'updated_at', 'fk_author'], 'integer'],
            [['is_active'], 'string'],
            [['description'], 'string', 'max' => 120],
            [['color'], 'string', 'max' => 7],
            [['fk_author'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_author' => 'id']],
            [['fk_leagues'], 'exist', 'skipOnError' => true, 'targetClass' => Leagues::className(), 'targetAttribute' => ['fk_leagues' => 'id']],
        ];


        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_leagues' => 'Лига',
            'description' => 'Описание',
            'color' => 'Цвет',
            'sort' => 'Сортировка',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'is_active' => 'Видимость',
            'fk_author' => 'Автор',
        ];
    }

}
