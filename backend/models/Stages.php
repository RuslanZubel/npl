<?php

namespace backend\models;

use backend\traits\BaseBackendTrait;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;

class Stages extends \common\models\Stages
{

    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['title', 'start_date', 'end_date', 'main_trainings_date', 'fk_rel_seasons_leagues'], 'required'],
            [['description'], 'string'],
            [['calculation_races', 'cost'], 'integer'],
            [['title', 'place'], 'string', 'max' => 255],
            [['image'], 'file', 'maxSize' => 10 * 1024 * 1024, 'skipOnEmpty' => true,
                'except' => 'ajax', 'extensions' => 'jpg, png'],
            [['start_date', 'end_date', 'main_trainings_date'], 'match', 'pattern' => '/[0-9]{2}-(\d{2}|[a-zа-я]{3})-\d{4} \d{2}:\d{2}/'],
            [['fk_rel_seasons_leagues'], 'exist', 'skipOnError' => true, 'targetClass' => RelSeasonsLeagues::className(), 'targetAttribute' => ['fk_rel_seasons_leagues' => 'id']],
        ];
        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '',
            'start_date' => 'Дата начала эатапа',
            'end_date' => 'Дата окончания этапа',
            'trainings_date' => 'Дата основных тренировок',
            'place' => 'Место проведения этапа',
            'description' => 'Описание этапа',
            'image' => 'Фоновое изображение на странице этапа',
            'fk_author' => 'Автор',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'is_active' => 'Видимость',
            'calculation_races' => 'Количество зачётных гонок',
            'cost' => 'Стоимость этапа'
        ];
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

    public function beforeSave($insert)
    {
        $this->start_date = strtotime($this->start_date);
        $this->end_date = strtotime($this->end_date);
        $this->main_trainings_date = ((int)strtotime($this->main_trainings_date));
        //    v($this->attributes);
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        if (!empty($this->main_trainings_date)) {
            $this->main_trainings_date = date('d-m-Y H:i', $this->main_trainings_date);
        } else {
            $startDateTime = new \DateTime();
            date_timestamp_set($startDateTime, $this->start_date);
            $startDayOfWeek = (int)date('N', $startDateTime->getTimestamp());
            if ($startDayOfWeek === 4) {
                $this->main_trainings_date = date('d-m-Y H:i', $startDateTime->getTimestamp());
            }
            if ($startDayOfWeek < 4) {
                $this->main_trainings_date = date('d-m-Y H:i', strtotime(date('Y-m-d H:i', $startDateTime->getTimestamp()) . ' + ' . (4 - $startDayOfWeek) . ' days'));
            }
            if ($startDayOfWeek > 4) {
                $this->main_trainings_date = date('d-m-Y H:i', strtotime(date('Y-m-d H:i', $startDateTime->getTimestamp()) . ' - ' . (6 - $startDayOfWeek) . ' days'));
            }

        }
        $this->start_date = date('d-m-Y H:i', $this->start_date);
        $this->end_date = date('d-m-Y H:i', $this->end_date);

        parent::afterFind();
    }

}
