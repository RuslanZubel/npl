<?php

namespace backend\models;

use Yii;
use backend\traits\BaseBackendTrait;

/**
 * This is the model class for table "rel_teams_rel_seasons_leagues".
 *
 * @property int $id
 * @property int $fk_teams
 * @property int $fk_rel_seasons_leagues
 * @property int $created_at
 * @property int $updated_at
 * @property int $fk_author
 * @property string $is_active
 *
 * @property Teams $fkTeams
 * @property RelSeasonsLeagues $fkRelSeasonsLeagues
 */
class RelTeamsTeamsPartners extends \common\models\RelTeamsTeamsPartners
{
    use BaseBackendTrait;

    public function rules()
    {
        return [
            [['fk_rel_teams_rel_seasons_league', 'fk_teams_partners'], 'integer'],
            [['fk_rel_teams_rel_seasons_league', 'fk_teams_partners'], 'required'],
            [['sort'], 'integer'],
            [['fk_teams_partners'], 'exist', 'skipOnError' => true, 'targetClass' => TeamsPartners::className(), 'targetAttribute' => ['fk_teams_partners' => 'id']],
            [['sort'], 'safe']
            //[['fk_teams_partners'], 'exist', 'skipOnError' => true, 'targetClass' => TeamsPartners::className(), 'targetAttribute' => ['fk_teams_partners' => 'id']],
            //[['fk_teams_partners'], 'exist', 'skipOnError' => true, 'targetClass' => RelSeasonsLeagues::className(), 'targetAttribute' => ['fk_teams_partners' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_teams' => 'Команд',
            'fk_rel_teams_rel_seasons_league' => 'Сеозн-лига',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'fk_author' => 'Автор',
            'is_active' => 'Видимость',
            'sort' => 'sort'
        ];
    }

}
