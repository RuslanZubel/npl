<?php

namespace backend\models;

use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

class Teams extends \common\models\Teams
{

    use BaseBackendTrait;

    public function rules()
    {
        $selfRules = [
            [['description', 'is_active'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'required'],
            [['short_title'], 'string', 'max' => 255],
            [['short_title'], 'required'],
            [['club'], 'string', 'max' => 255],
            [['club'], 'required'],
            [['image', 'cover_image' ], 'file', 'maxSize' => 10 * 1024 * 1024, 'skipOnEmpty' => true,
                'except' => 'ajax', 'extensions' => 'jpg, jpeg, png'],
        ];
        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'short_title' => 'Краткое наименование',
            'description' => 'Описание',
            'city' => 'Город',
            'club' => 'Клуб',
            'image' => 'Логотип',
            'cover_image' => 'Обложка',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'fk_author' => 'Автор',
            'is_active' => 'Видимость',
        ];
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

}
