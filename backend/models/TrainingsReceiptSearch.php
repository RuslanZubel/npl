<?php

namespace backend\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class TrainingsReceiptSearch extends Model
{
    public $title;
    public $confirmed;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[['title', 'confirmed'], 'safe'],];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $trsSubQuery = (new \yii\db\Query())
            ->select(['training_id', 'confirmed', 'receipt.id AS receipt_id'])
            ->from('rel_receipt_trainings')
//            ->where(['product_type' => 'training'])
            ->rightJoin('receipt', 'receipt.id = rel_receipt_trainings.receipt_id');

        $query = (new \yii\db\Query())
            ->select(['training.id', 'start_datetime', 'cost', 'team_id', 'title', 'created_by', 'confirmed', 'boat_number', 'receipt_id'])
            ->from('training')
            ->leftJoin('teams', 'teams.id = training.team_id')
            ->leftJoin(['u' => $trsSubQuery], 'u.training_id = training.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'fk_author' => $this->fk_author,
//        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'confirmed', $this->confirmed]);

        return $dataProvider;
    }
}