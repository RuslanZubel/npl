<?php

namespace backend\models;
use backend\traits\BaseBackendTrait;
use Yii;
use yii\helpers\ArrayHelper;




/**
 * This is the model class for table "pages".
 *
 * @property int $id id страницы
 * @property string $type тип страницы
 * @property string $title Заголовок
 * @property string $shorttitle Короткий заголовок
 * @property string $text Содержимое
 * @property string $shorttext Анонс
 * @property int $sort сортировка
 * @property int $fk_author автор
 * @property int $created_at Дата создания
 * @property int $updated_at Дата редактирования
 * @property string $is_active Видимость
 *
 * @property Users $fkAuthor
 */
class Pages extends \common\models\Pages
{
    use BaseBackendTrait;
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['type', 'text', 'shorttext'], 'string'],
            [['sort'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['shorttitle'], 'string', 'max' => 130],
            [['type'], 'unique'],
            [['image'], 'file', 'maxSize' => 10 * 1024 * 1024, 'skipOnEmpty' => true,
                'except' => 'ajax', 'extensions' => 'jpg, jpeg, png'],
        ];

        return ArrayHelper::merge($selfRules, $this->baseRules());

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id страницы',
            'type' => 'Тип страницы',
            'title' => 'Заголовок',
            'shorttitle' => 'Пункт в меню',
            'text' => 'Содержимое страницы',
            'shorttext' => 'Краткое описание',
            'sort' => 'Сортировка',
            'fk_author' => 'Автор',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'is_active' => 'Видимость',
            'image' => 'Изображение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_author']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\PagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\PagesQuery(get_called_class());
    }
}
