<?php

namespace backend\models;

use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;



class Leagues extends \common\models\Leagues
{
    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules =  [
            [['title', 'description'], 'string', 'max' => 255],
            [['title'], 'required' ],
            [['sort'], 'integer'],

        ];
//        return [
//            [['created_at', 'updated_at', 'fk_author', 'sort'], 'integer'],
//            [['is_active'], 'string'],
//            [['title', 'description'], 'string', 'max' => 255],
//            [['fk_author'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['fk_author' => 'id']],
//        ];
        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'fk_author' => 'Автор',
            'sort' => 'Sort',   //+ Добавлено
            'is_active' => 'Видимость',   //+ Добавлено
        ];
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

    public static function getList($params = [])
    {
        $query = self::find();

        if (isset($params['condition'])) {
            $query->where($params['condition']);
        }

        $query->orderBy([
            'created_at' => SORT_ASC,
            'title' => SORT_ASC
        ]);

        return ArrayHelper::map(
            $query->all(),
            'id',
            'title'
        );
    }

}
