<?php

namespace backend\models;

use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

//Прорбаотано, без изменений

class Roles extends \common\models\Roles
{

    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            ['title', 'required'],
            ['title', 'unique'],
            [['title', 'description'], 'string', 'max' => 255],
        ];

        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название должности',
            'description' => 'Комментарий',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'fk_author' => 'Автор',
            'is_active' => 'Видимость',
        ];
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

}
