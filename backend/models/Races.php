<?php

namespace backend\models;

use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;


//Проработано Илья 19 марта 2017

class Races extends \common\models\Races
{
    use BaseBackendTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['fk_stages', 'created_at', 'updated_at', 'fk_author', 'sort'], 'integer'],
            [['is_calculating'], 'string'],
            [['title', 'description'], 'string', 'max' => 255],
            [['track_url'], 'string', 'max' => 2048],
            [['fk_stages'], 'exist', 'skipOnError' => true, 'targetClass' => Stages::className(), 'targetAttribute' => ['fk_stages' => 'id']],
            [['sort'],'safe' ],
            [['sort'],'integer' ],
        ];

        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_stages' => 'Этап гонки',  //+
            'title' => 'Название',
            'description' => 'Комментарий',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'fk_author' => 'Fk Author',
            'sort' => 'Sort', //+
            'is_active' => 'Видимость',
            'is_calculating' => 'Is Calculating', //+ Когда проставлены оценки у более чем 1й команды но не у всех,
            'track_url'=> 'Трекинг ссылка',
            'is_corrective'=> 'Корректирующая',
           // 'fk_stages' => 'Fk Stages',  Удалил
        ];
//        return [
//            'id' => 'ID',
//            'fk_rel_stages_rel_seasons_leagues' => 'Fk Rel Stages Rel Seasons Leagues',
//            'title' => 'Title',
//            'description' => 'Description',
//            'created_at' => 'Created At',
//            'updated_at' => 'Updated At',
//            'fk_author' => 'Fk Author',
//            'sort' => 'Sort',
//            'is_calculating' => 'Is Calculating',
//            'is_active' => 'Is Active',
//        ];

    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }

}
