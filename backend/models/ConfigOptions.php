<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "config_options".
 *
 * @property int $id
 * @property string $setting
 * @property string $value
 */
class ConfigOptions extends \common\models\ConfigOptions
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['setting'], 'string'],
            [['value'], 'string', 'max' => 2048],
        ];
    }
}
