<?php

namespace backend\models;
use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "site_galleries".
 * @property int $id id
 * @property string $title название галереи
 * @property string $subtitle подзаголовок
 * @property int $sort сортировка
 * @property string $show_on_index отображать на главной
 * @property int $custom_date своя дата у галереи
 * @property int $id_author id автора
 * @property int $updated_at дата редактирования
 * @property int $created_at дата создания
 * @property string $is_active видимость
 * @property Users $author
 * @property SiteGalleriesImages[] $si
 * teGalleriesImages
 */
class SiteGalleries extends \common\models\SiteGalleries
{
    use BaseBackendTrait;

    const SHOW_ON_MAIN_TRUE = 'yes';
    const SHOW_ON_MAIN_FALSE = 'no';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['sort' ], 			'integer'],
			[['custom_date'], 		'safe'],
            [['show_on_index'], 	'string'],
            [['show_on_index'], 	'required'],
            [['title', 'subtitle'], 'string'],
            [['title'], 	'required'],
        ];
        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'title' => 'Навание галереи',
            'subtitle' => 'Описание на странице галереи',
            'sort' => 'Сортировка',
            'show_on_index' => 'Выводить на главной',
            'custom_date' => 'Отображаемая дата',
            'fk_author' => 'Автор',
            'updated_at' => 'Дата редактирования',
            'created_at' => 'Дата создания',
            'is_active' => 'Видимость',
        ];

    }
	
	public function beforeSave($insert)
	{
		$this->custom_date = strtotime( $this->custom_date );
		//    v($this->attributes);
		return parent::beforeSave($insert);
	}
	
	public function afterFind()
	{
		$this->custom_date = date(  'd-m-Y H:i' ,  $this->custom_date  );
		parent::afterFind();
	}

    public function getShowOnMainDropdownList()
    {
        return [self::SHOW_ON_MAIN_TRUE => 'Да', self::SHOW_ON_MAIN_FALSE => 'Нет'];
    }

    public function getIsShowOnMainName()
    {
        $names = $this->getShowOnMainDropdownList();
        if ($this->show_on_index == 'yes') {
            return $names[self::SHOW_ON_MAIN_TRUE];
        } else if ($this->show_on_index == 'no') {
            return $names[self::SHOW_ON_MAIN_FALSE];
        } else {
            return "-";
        }
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }
}
