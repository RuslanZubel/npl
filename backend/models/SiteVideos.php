<?php

namespace backend\models;

use Yii;
use backend\traits\BaseBackendTrait;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "site_videos".
 *
 * @property int $id id
 * @property string $title Название
 * @property string $description Описание
 * @property string $source_type Источник id видео
 * @property string $video_id id видео
 * @property string $cover_image обложка видео
 * @property int $sort Сортировка
 * @property string $show_on_index является обложкой
 * @property int $custom_date Использовать свою дату
 * @property int $id_author id автора
 * @property int $created_at дата создания
 * @property int $updated_at дата редактирования
 * @property string $is_active видимость
 */
class SiteVideos extends \common\models\SiteVideos
{
    use BaseBackendTrait;

    const SHOW_ON_MAIN_TRUE = 'yes';
    const SHOW_ON_MAIN_FALSE = 'no';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_videos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $selfRules = [
            [['source_type' ], 'string'],
            [['sort' ], 'integer'],
			[['custom_date' ], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['title'], 	'required'],
            [['description', 'video_id', 'cover_image'], 'string', 'max' => 255],
            [['show_on_index'], 'string'],
            [['show_on_index'], 'required'],

        ];
        return ArrayHelper::merge($selfRules, $this->baseRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'title' => 'Название видеоролика',
            'description' => 'Описание',
            'source_type' => 'Источник id видео',
            'video_id' => 'Ссылка на ролик',
            'cover_image' => 'Обложка видео',
            'sort' => 'Сортировка',
            'show_on_index' => 'Выводить на главной',
            'custom_date' => 'Своя дата',
            'fk_author' => 'Автор',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'is_active' => 'Видимость',
        ];
    }

    
    /**
     * @inheritdoc
     * @return \common\models\queries\SiteVideosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SiteVideosQuery(get_called_class());
    }
	
	public function beforeSave($insert)
	{
		$this->custom_date = strtotime( $this->custom_date );
		//    v($this->attributes);
		return parent::beforeSave($insert);
	}
	
	public function afterFind()
	{
		$this->custom_date = date(  'd-m-Y H:i' ,  $this->custom_date  );
		parent::afterFind();
	}

    public function getShowOnMainDropdownList()
    {
        return [self::SHOW_ON_MAIN_TRUE => 'Да', self::SHOW_ON_MAIN_FALSE => 'Нет'];
    }

    public function getIsShowOnMainName()
    {
        $names = $this->getShowOnMainDropdownList();
        if ($this->show_on_index == 'yes') {
            return $names[self::SHOW_ON_MAIN_TRUE];
        } else if ($this->show_on_index == 'no') {
            return $names[self::SHOW_ON_MAIN_FALSE];
        } else {
            return "-";
        }
    }

    public function behaviors()
    {
        $selfBehaviors = [
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'created_at',
            ],
            [
                'class' => 'backend\behaviors\DateFieldBehavior',
                'attribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($selfBehaviors, parent::behaviors());
    }
}
