<?php

use common\models\User;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

/**
 * Created by PhpStorm.
 * User: amio
 * Date: 17/04/2017
 * Time: 09:05
 */

NavBar::begin([
    'brandLabel' => '<img src="/admin/img/logo.svg" class="pull-left"/><span class="office-in-logo">office</span>',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);

$menuItems = array();

if (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->id)->roles === 'trainer') {
    $menuItems = [

        ['label' => '<i class="glyphicon glyphicon-time pr5"></i>Тренировки', 'url' => ['/content/trainings/index']],
        ['label' => '<i class="glyphicon glyphicon-tower pr5"></i>Команды', 'url' => ['/content/teams/index']],
        ['label' => '<i class="glyphicon pr5"></i>Сезоны лиг', 'url' => ['/content/rel-seasons-leagues/index']],
        ['label' => '<i class="glyphicon glyphicon-ruble pr5"></i>Чеки', 'url' => ['/content/receipts/index']],
    ];
} elseif (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->id)->roles !== 'admin') {
    $menuItems = [

        ['label' => '<i class="glyphicon glyphicon-ruble pr5"></i>Чеки', 'url' => ['/content/receipts/index']],
    ];
} elseif (!Yii::$app->user->isGuest) {
    $menuItems = [
        //['label' => '<i class="glyphicon glyphicon-equalizer pr5"></i>Общий зачет', 'url' => ['/content/partners-statuses/index']],

        ['label' => '<i class="glyphicon glyphicon-user pr5"></i>Пользователи', 'url' => ['/content/users/index']],
        ['label' => '<i class="glyphicon glyphicon-ruble pr5"></i>Чеки', 'url' => ['/content/receipts/index']],
        //['label' => '<i class="glyphicon glyphicon-tower pr5"></i>Команды', 'url' => ['/content/teams/index']],
        ['label' => '<i class="glyphicon glyphicon-time pr5"></i>Сезоны лиг', 'items' => [
            ['label' => '<i class="glyphicon pr5"></i>Сезоны лиг', 'url' => ['/content/rel-seasons-leagues/index']],
            ['label' => '<i class="glyphicon pr5"></i>Тренировки', 'url' => ['/content/trainings/index']],
        ]],
        ['label' => '<i class="glyphicon glyphicon-tower pr5"></i>Команды', 'items' => [
            ['label' => '<i class="glyphicon glyphicon-ruble pr5"></i>Тренировки', 'url' => ['/content/trainings/index']],
            ['label' => '<i class="glyphicon glyphicon-tower pr5"></i>Команды', 'url' => ['/content/teams/index']],
            ['label' => '<i class="glyphicon glyphicon-briefcase pr10"></i>Партнёры команд', 'url' => ['/content/teams-partners/index']],
        ]],
        ['label' => '<i class="glyphicon glyphicon-user pr5"></i>Участники', 'url' => ['/content/members/index']],
        ['label' => '<i class="glyphicon glyphicon-briefcase pr10"></i>Партнеры', 'url' => ['/content/partners/index']],
        ['label' => '<i class="glyphicon glyphicon-globe pr5"></i>Справочники', 'items' => [
            ['label' => '<i class="glyphicon glyphicon-picture pr5"></i>Статусы партнеров', 'url' => ['/content/partners-statuses/index']],
            ['label' => '<i class="glyphicon glyphicon-picture pr5"></i>Должности в командах', 'url' => ['/content/roles/index']],
            ['label' => '<i class="glyphicon glyphicon-calendar pr5"></i>Лиги', 'url' => ['/content/leagues/index']],
            ['label' => '<i class="glyphicon glyphicon-star pr5"></i>Сезоны', 'url' => ['/content/seasons/index']],
            ['label' => '<i class="glyphicon glyphicon-picture pr5"></i>Стикеры', 'url' => ['/results/stickers']],

        ]],
        ['label' => '<i class="glyphicon glyphicon-globe pr5"></i>Сайт', 'items' => [
            ['label' => '<i class="glyphicon glyphicon-bullhorn pr5"></i>Новости', 'url' => ['/content/news']],
            ['label' => '<i class="glyphicon glyphicon-camera pr5"></i>Фото', 'url' => ['/mediafolder/site-galleries']],
            ['label' => '<i class="glyphicon glyphicon-facetime-video pr5"></i>Видео', 'url' => ['/mediafolder/site-videos']],
            ['label' => '<i class="glyphicon glyphicon-align-justify pr5"></i>Страницы', 'url' => ['/content/pages']],
            ['label' => '<i class="glyphicon glyphicon-align-justify pr5"></i>Настройки', 'url' => ['/content/config-options']],


        ]],
    ];
}


if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
} else {
    $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Выход (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'encodeLabels' => false,
    'items' => $menuItems,
]);
NavBar::end();
?>