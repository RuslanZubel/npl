<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Административная панель | National Sailing League';

$this->registerCss("
.menu_link {
    font-size: 16px;
    font-color: blue;
    font-weight: bold;
}
");

$this->registerJs("


$(document).on('click', '.help-team-members-button', function() {
    var th = $(this);
    var popup = $('#help-team-members-modal');
    popup.modal('show');
});
");

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Здравствуйте!</h1>

        <p class="lead">Вы находитесь в административной панели сайта <strong>National Sailing League</strong></p>
        <p align="center" style="font-size:12px;"><?= Html::button('?', ['class' => 'btn btn-xs btn-primary help-team-members-button', 'style' => 'font-size:16px; padding:2px 8px;', 'title' => 'Справка']) ?></p>
    </div>

    <div class="body-content">
        <p><a class="menu_link" href="/admin/content/rel-seasons-leagues/index">Сезоны лиг</a> - создавайте пары Сезон-Лига. Для того, чтобы создать пару Сезон-лига,
            вам необходимо предварительно создать <a class="menu_link" href="/admin/content/seasons/index">Сезон</a> и <a class="menu_link" href="/admin/content/leagues/index">Лигу</a>.</p>

        <h2>Команды</h2>
        <ul>
            <li><a class="menu_link" href="/admin/content/teams/index">Команды</a> - сформируйте список команд, которые вы позже сможете добавлять к сезонам-лигам</li>
            <li><a class="menu_link" href="/admin/content/teams-partners/index">Партнёры команд</a> - сформируйте список партнёров команд</li>
        </ul>
        <br/>

        <p><a class="menu_link" href="/admin/content/members/index">Участники</a> - сформируйте список участников соревнований</p>
        <p><a class="menu_link" href="/admin/content/members/index">Партнёры</a> - составьте список партнёров сезонов-лиг</p>

        <h2>Справочники</h2>
        <ul>
            <li><a class="menu_link" href="/admin/content/partners-statuses/index">Статусы партнеров</a> - создайте список статусов партнёров сезонов-лиг</li>
            <li><a class="menu_link" href="/admin/content/roles/index">Должности в командах</a> - создайте список должностей членов команды</li>
            <li><a class="menu_link" href="/admin/content/leagues/index">Лиги</a> - создайте список лиг</li>
            <li><a class="menu_link" href="/admin/content/seasons/index">Сезоны</a> - создайте список лиг</li>
            <li><a class="menu_link" href="/admin/results/stickers">Стикеры</a> - создайте список стикеров с привязкой к сезонам-лигам</li>
        </ul>

        <h2>Дополнительные разделы сайта</h2>
        <ul>
            <li><a class="menu_link" href="/admin/content/news">Новости</a> - добавляйте новостные статьи, настраивайте их отображение на страницах сайта</li>
            <li><a class="menu_link" href="/admin/mediafolder/site-galleries">Фото</a> - создавайте фотогалереи, наполняйте их фотографиями</li>
            <li><a class="menu_link" href="/admin/mediafolder/site-videos">Видео</a> - формируйте список видеороликов, доступных посетителям сайта</li>
            <li><a class="menu_link" href="/admin/content/pages">Страницы</a> - создавайте дополнительные страницы сайта при помощи <strong><em>WYSIWIG</em></strong></li>
        </ul>

        <br/>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="help-team-members-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Справка</h4>
            </div>
            <div class="modal-body">
                <ol>
                    <li>Редактирование <a href="http://nationalsailingleague.com/admin/content/rel-seasons-leagues">Составов команд, Выбор спонсоров команд Участников, Очков и Гонок</a>
                        - Нажать редактирование, напротив связки Сезон-Лига</li>
                    <li>Для того что бы сделать спонсоров у команды:
                        - <a href="http://nationalsailingleague.com/admin/content/teams-partners/index">Создайте спонсоров команд</a>
                        - Далее нужно пройти в Сезон-Лигу, и выбрать команду, <a href="http://joxi.ru/Vm6B10jUxyWoym">
                            <span class="glyphicon glyphicon-eye-open"></span></a></li>
                    <li>Редактирование состава команды в Сезоне-Лиге : <a href="http://joxi.ru/RmzGMWjfWpLyx2"><span class="glyphicon glyphicon-eye-open"></span></a></li>
                </ol>
            </div>
        </div>
    </div>
</div>
