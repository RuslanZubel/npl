<?php


namespace console\controllers;


use backend\models\Members;
use backend\models\RelTeamsRelSeasonsLeagues;
use backend\models\Teams;
use backend\models\TeamsStructures;
use yii\helpers\VarDumper;
use yii\helpers\Json;

class TempController extends \yii\console\Controller
{
    public function actionMake()
    {

        $data = array(
            array('liga' => 'Премьер', 'team' => 'CARAMBA!', 'city' => 'Москва', 'fio' => 'Носов Петр', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'CARAMBA!', 'city' => 'Москва', 'fio' => 'Пузанов Игорь', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'CARAMBA!', 'city' => 'Москва', 'fio' => 'Пушкарева Елена', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'CARAMBA!', 'city' => 'Москва', 'fio' => 'Знаменский Алексей', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'ПЭК: Спорт', 'city' => 'Москва', 'fio' => 'Иевлев Дмитрий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ПЭК: Спорт', 'city' => 'Москва', 'fio' => 'Филиппов Виктор', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ПЭК: Спорт', 'city' => 'Москва', 'fio' => 'Филатов Вадим', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'PARUSNIK74', 'city' => 'Челябинск', 'fio' => 'Кузнецов Андрей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'PARUSNIK74', 'city' => 'Челябинск', 'fio' => 'Сухов Сергей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'PARUSNIK74', 'city' => 'Челябинск', 'fio' => 'Гартман Дмитрий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'PARUSNIK74', 'city' => 'Челябинск', 'fio' => 'Гоц Евгений', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'PARUSNIK74', 'city' => 'Челябинск', 'fio' => 'Улькин Владимир', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'PARUSNIK74', 'city' => 'Челябинск', 'fio' => 'Баранов Виталий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'PARUSNIK74', 'city' => 'Челябинск', 'fio' => 'Баранов Максим', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'PARUSNIK74', 'city' => 'Челябинск', 'fio' => 'Подшивалов Эдуард', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'ZID Art Sailing Team ', 'city' => 'Москва', 'fio' => 'Игнатенко Виталий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ZID Art Sailing Team ', 'city' => 'Москва', 'fio' => 'Волчков Сергей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ZID Art Sailing Team ', 'city' => 'Москва', 'fio' => 'Нароженко Дмитрий ', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ZID Art Sailing Team ', 'city' => 'Москва', 'fio' => 'Паунович Зоран', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'UGAR CREW', 'city' => 'Москва', 'fio' => 'Ямников Владимир', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'UGAR CREW', 'city' => 'Москва', 'fio' => 'Хоперский Олег', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'UGAR CREW', 'city' => 'Москва', 'fio' => 'Ганженко Александр', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'UGAR CREW', 'city' => 'Москва', 'fio' => 'Хаметов Михаил', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'UGAR CREW', 'city' => 'Москва', 'fio' => 'Якупов Руслан', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'Winners Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Михайлов Александр', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Winners Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Никитина Милена', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Winners Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Шлеенков Евгений ', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Winners Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Михайлов Василий', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => '7 яхт', 'city' => 'Москва ', 'fio' => 'Филатов Михаил', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => '7 яхт', 'city' => 'Москва ', 'fio' => 'Пузенко Андрей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => '7 яхт', 'city' => 'Москва ', 'fio' => 'Сивенков Вячеслав', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => '7 яхт', 'city' => 'Москва ', 'fio' => 'Тельянц Карина ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'B-Team', 'city' => 'Санкт-Петербург ', 'fio' => 'Басалкин Артем', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'B-Team', 'city' => 'Санкт-Петербург ', 'fio' => 'Шадрина Юлия', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'B-Team', 'city' => 'Санкт-Петербург ', 'fio' => 'Савенко Павел ', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'B-Team', 'city' => 'Санкт-Петербург ', 'fio' => 'Лесников Алексей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'B-Team', 'city' => 'Санкт-Петербург ', 'fio' => 'Морозова Анастасия ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'MATRYOSHKA', 'city' => 'Екатеринбург ', 'fio' => 'Лаверычева Милена ', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'MATRYOSHKA', 'city' => 'Екатеринбург ', 'fio' => 'Хмурович Борис        ', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'MATRYOSHKA', 'city' => 'Екатеринбург ', 'fio' => 'Зудов Сергей      ', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'MATRYOSHKA', 'city' => 'Екатеринбург ', 'fio' => 'Кравец Наталья', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'MY MARUSIA', 'city' => 'Санкт-Петербург ', 'fio' => 'Чугунов Олег', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'MY MARUSIA', 'city' => 'Санкт-Петербург ', 'fio' => 'Сысоев Иван ', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'MY MARUSIA', 'city' => 'Санкт-Петербург ', 'fio' => 'Таран Марина', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'MY MARUSIA', 'city' => 'Санкт-Петербург ', 'fio' => 'Латкин Борис ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'SAIL&SEA', 'city' => 'Санкт-Петербург ', 'fio' => 'Бондаренко Владислав', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'SAIL&SEA', 'city' => 'Санкт-Петербург ', 'fio' => 'Банаян Даниил', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'SAIL&SEA', 'city' => 'Санкт-Петербург ', 'fio' => 'Наумов Алексей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'SAIL&SEA', 'city' => 'Санкт-Петербург ', 'fio' => 'Нечволодов Михаил', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'SAIL&SEA', 'city' => 'Санкт-Петербург ', 'fio' => 'Райков Андрей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'SAIL&SEA', 'city' => 'Санкт-Петербург ', 'fio' => 'Меньшикова Татьяна', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'SAIL&SEA', 'city' => 'Санкт-Петербург ', 'fio' => 'Харабардин Василий', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'TEAM BELKA', 'city' => 'Москва', 'fio' => 'Шумарин Василий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'TEAM BELKA', 'city' => 'Москва', 'fio' => 'Кузнецов Эдуард', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'TEAM BELKA', 'city' => 'Москва', 'fio' => 'Шюлер Маркус', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'TEAM BELKA', 'city' => 'Москва', 'fio' => 'Елфимов Евгений', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'Гранит-Инвест ', 'city' => 'Екатеринбург ', 'fio' => 'Попов Юрий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Гранит-Инвест ', 'city' => 'Екатеринбург ', 'fio' => 'Тарасов Леонид', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Гранит-Инвест ', 'city' => 'Екатеринбург ', 'fio' => 'Тарасов Федор', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Гранит-Инвест ', 'city' => 'Екатеринбург ', 'fio' => 'Горкунов Альберт', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'Демидовский экспресс', 'city' => 'Екатеринбург', 'fio' => 'Мусихин Сергей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Демидовский экспресс', 'city' => 'Екатеринбург', 'fio' => 'Подбельцев Максим', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Демидовский экспресс', 'city' => 'Екатеринбург', 'fio' => 'Шпагин Андрей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Демидовский экспресс', 'city' => 'Екатеринбург', 'fio' => 'Морозов Владислав', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'Добрыня ', 'city' => 'Москва ', 'fio' => 'Гурьянов Иван', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Добрыня ', 'city' => 'Москва ', 'fio' => 'Жайворонок Дмитрий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Добрыня ', 'city' => 'Москва ', 'fio' => 'Игнатова Ольга', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Добрыня ', 'city' => 'Москва ', 'fio' => 'Игнатов Игорь ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'ПЭК Спорт 2', 'city' => 'Санкт-Петербург ', 'fio' => 'Бартенев Денис', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ПЭК Спорт 2', 'city' => 'Санкт-Петербург ', 'fio' => 'Матвеей Евгений', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ПЭК Спорт 2', 'city' => 'Санкт-Петербург ', 'fio' => 'Кузьмин Максим', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ПЭК Спорт 2', 'city' => 'Санкт-Петербург ', 'fio' => 'Коломеец Евгений', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Петухов Борис ', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Медведев Роман', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Уваров Максим', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Сергеев Александр', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Пикулин Дмитрий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Грицких Дмитрий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Каримов Альберт', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Кадырова Гузель', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Газизов Фарит', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Федоров Андрей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Федотова Екатерина', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Садыков Альберт', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Гужев Алексей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Семин Алексей ', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Химград-Казань ', 'city' => 'Казань', 'fio' => 'Трофимов Вадим ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'ПЭК Спорт Молодежка', 'city' => 'Москва ', 'fio' => 'Филатов Михаил', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ПЭК Спорт Молодежка', 'city' => 'Москва ', 'fio' => 'Пузенко Андрей', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ПЭК Спорт Молодежка', 'city' => 'Москва ', 'fio' => 'Сивенков Вячеслав', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'ПЭК Спорт Молодежка', 'city' => 'Москва ', 'fio' => 'Попков Дмитрий ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Премьер', 'team' => 'Ассоль home companions', 'city' => 'Москва ', 'fio' => 'Гаврилов Дмитрий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Ассоль home companions', 'city' => 'Москва ', 'fio' => 'Головко Герман', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Ассоль home companions', 'city' => 'Москва ', 'fio' => 'Кузюк Антон ', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Ассоль home companions', 'city' => 'Москва ', 'fio' => 'Каралюс Донатас', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Ассоль home companions', 'city' => 'Москва ', 'fio' => 'Ширинкин Дмитрий', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Ассоль home companions', 'city' => 'Москва ', 'fio' => 'Ляшенко Артем', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Ассоль home companions', 'city' => 'Москва ', 'fio' => 'Тимофеева Елена', 'dolzh' => ''),
            array('liga' => 'Премьер', 'team' => 'Ассоль home companions', 'city' => 'Москва ', 'fio' => 'Липень Игорь ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - АЗИЯ', 'city' => 'Екатеринбург ', 'fio' => 'Судаков Дмитрий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - АЗИЯ', 'city' => 'Екатеринбург ', 'fio' => 'Самоделкин Юрий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - АЗИЯ', 'city' => 'Екатеринбург ', 'fio' => 'Козорез Вячеслав', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - АЗИЯ', 'city' => 'Екатеринбург ', 'fio' => 'Трофимчук Павел ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - АЗИЯ', 'city' => 'Екатеринбург ', 'fio' => 'Корзников Михаил ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - АЗИЯ', 'city' => 'Екатеринбург ', 'fio' => 'Тюриков Денис ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - АЗИЯ', 'city' => 'Екатеринбург ', 'fio' => 'Мусихин Сергей ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'NAVIGATOR Sailing Team', 'city' => 'Москва ', 'fio' => 'Беспутин Константин ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'NAVIGATOR Sailing Team', 'city' => 'Москва ', 'fio' => 'Сергеев Антон ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'NAVIGATOR Sailing Team', 'city' => 'Москва ', 'fio' => 'Мартынов Вячеслав ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'NAVIGATOR Sailing Team', 'city' => 'Москва ', 'fio' => 'Рытов Игорь ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - ЕВРОПА', 'city' => 'Екатеринбург ', 'fio' => 'Неугодников Евгений ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - ЕВРОПА', 'city' => 'Екатеринбург ', 'fio' => 'Попов Юрий        ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - ЕВРОПА', 'city' => 'Екатеринбург ', 'fio' => 'Ермоленко Вячеслав   ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Повелитель Паруса - ЕВРОПА', 'city' => 'Екатеринбург ', 'fio' => 'Кузнецов Павел', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'ПИРогово', 'city' => 'Московская область', 'fio' => 'Одинцов Даниил ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ПИРогово', 'city' => 'Московская область', 'fio' => 'Екимов Александр ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ПИРогово', 'city' => 'Московская область', 'fio' => 'Ларионов Егор', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ПИРогово', 'city' => 'Московская область', 'fio' => 'Кузьмин Максим', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ПИРогово', 'city' => 'Московская область', 'fio' => 'Уваркин Валентин ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ПИРогово', 'city' => 'Московская область', 'fio' => 'Морозов Юрий', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'ПИРогово', 'city' => 'Московская область', 'fio' => 'Ежков Александр', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Конаково Ривер Клаб', 'city' => 'Тверская обл. ', 'fio' => 'Басалкин Артем ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Конаково Ривер Клаб', 'city' => 'Тверская обл. ', 'fio' => 'Марков Артем ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Конаково Ривер Клаб', 'city' => 'Тверская обл. ', 'fio' => 'Колинко Иван  ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Конаково Ривер Клаб', 'city' => 'Тверская обл. ', 'fio' => 'Страх Илья', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Конаково Ривер Клаб', 'city' => 'Тверская обл. ', 'fio' => 'Каганский Марк', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Конаково Ривер Клаб', 'city' => 'Тверская обл. ', 'fio' => 'Шунин Дмитрий', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'ArtTube RUS1', 'city' => 'Таганрог/Москва', 'fio' => 'Божко Александр', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ArtTube RUS1', 'city' => 'Таганрог/Москва', 'fio' => ' Лисовенко Игорь', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ArtTube RUS1', 'city' => 'Таганрог/Москва', 'fio' => 'Рожков Денис ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ArtTube RUS1', 'city' => 'Таганрог/Москва', 'fio' => 'Коваленко Валерия ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Академия парусного спорта Я\\К Санкт-Петербурга', 'city' => 'Санкт-Петербург ', 'fio' => 'Чех Ян', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Академия парусного спорта Я\\К Санкт-Петербурга', 'city' => 'Санкт-Петербург ', 'fio' => 'Чех Кристиан', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Академия парусного спорта Я\\К Санкт-Петербурга', 'city' => 'Санкт-Петербург ', 'fio' => 'Тарасов Виктор', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Академия парусного спорта Я\\К Санкт-Петербурга', 'city' => 'Санкт-Петербург ', 'fio' => 'Басалкина Анна', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Rocknrolla Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Галло Виталий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Rocknrolla Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Бердников Максим', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Rocknrolla Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Мордасов Максим', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Rocknrolla Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Кирилюк Андрей ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Rocknrolla Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Бальдевайн Дастин ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Rocknrolla Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Доценко Алина  ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Rocknrolla Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Самохин Дмитрий', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Rocknrolla Sailing Team ', 'city' => 'Санкт-Петербург ', 'fio' => 'Кирилюк Алиса', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Leviathan', 'city' => 'Санкт-Петербург ', 'fio' => 'Шереметьев Максим', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Leviathan', 'city' => 'Санкт-Петербург ', 'fio' => 'Шереметьев Михаил', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Leviathan', 'city' => 'Санкт-Петербург ', 'fio' => 'Ильков Сергей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Leviathan', 'city' => 'Санкт-Петербург ', 'fio' => 'Яхинсон Вадим', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Leviathan', 'city' => 'Санкт-Петербург ', 'fio' => 'Титаренко Максим', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'X-Fit', 'city' => 'Москва ', 'fio' => 'Матвиенко Игорь ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'X-Fit', 'city' => 'Москва ', 'fio' => 'Зацаринский Валерий ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'X-Fit', 'city' => 'Москва ', 'fio' => 'Коваленко Николай ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'X-Fit', 'city' => 'Москва ', 'fio' => 'Силкин Владимир', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'DC TEAM ', 'city' => 'Москва ', 'fio' => 'Им Георгий ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'DC TEAM ', 'city' => 'Москва ', 'fio' => 'Клепиков Леонид', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'DC TEAM ', 'city' => 'Москва ', 'fio' => 'Чаус Владимир', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'DC TEAM ', 'city' => 'Москва ', 'fio' => 'Череватенко Денис ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'ФПС Сочи - Юг Спорт', 'city' => 'Сочи ', 'fio' => 'Ганженко Наталья', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ФПС Сочи - Юг Спорт', 'city' => 'Сочи ', 'fio' => 'Ганженко Артем', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ФПС Сочи - Юг Спорт', 'city' => 'Сочи ', 'fio' => 'Бербеков Инал', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ФПС Сочи - Юг Спорт', 'city' => 'Сочи ', 'fio' => 'Франик Алиса', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ФПС Сочи - Юг Спорт', 'city' => 'Сочи ', 'fio' => 'Алавердов Георгий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ФПС Сочи - Юг Спорт', 'city' => 'Сочи ', 'fio' => 'Малявский Игорь', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ФПС Сочи - Юг Спорт', 'city' => 'Сочи ', 'fio' => 'Стаценко Вадим', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'ФПС Сочи - Юг Спорт', 'city' => 'Сочи ', 'fio' => 'Мазурин Александр', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'ФПС Сочи - Юг Спорт', 'city' => 'Сочи ', 'fio' => 'Варначкин Вячеслав', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'ФПС Сочи - Юг Спорт', 'city' => 'Сочи ', 'fio' => 'Саховский Михаил', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Регион - 23', 'city' => 'Краснодар', 'fio' => 'Фирсов Юрий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Регион - 23', 'city' => 'Краснодар', 'fio' => 'Асламов Эдуард', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Регион - 23', 'city' => 'Краснодар', 'fio' => 'Руднев Евгений', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Регион - 23', 'city' => 'Краснодар', 'fio' => 'Никифоров Евгений', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'NAVIGATOR Trem ', 'city' => 'Москва', 'fio' => 'Новиков Андрей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'NAVIGATOR Trem ', 'city' => 'Москва', 'fio' => 'Кирилюк Павел', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'NAVIGATOR Trem ', 'city' => 'Москва', 'fio' => 'Маянцев Владимир', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'NAVIGATOR Trem ', 'city' => 'Москва', 'fio' => 'Дмитриев Андрей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'NAVIGATOR Trem ', 'city' => 'Москва', 'fio' => 'Сорокина Ирина', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'NAVIGATOR Trem ', 'city' => 'Москва', 'fio' => 'Петерсон Александра', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Парма - Лукоморье', 'city' => 'Пермь', 'fio' => 'Подшивалов Эдуард', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Парма - Лукоморье', 'city' => 'Пермь', 'fio' => 'Санников Олег  ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Парма - Лукоморье', 'city' => 'Пермь', 'fio' => 'Сутормин Дмитрий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Парма - Лукоморье', 'city' => 'Пермь', 'fio' => 'Шило Михаил', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Парма - Лукоморье', 'city' => 'Пермь', 'fio' => 'Тараканов Виталий ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Николаева Елена', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Николаев Алексей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Воронков Дмитрий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Орлов Роман', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Рулев Виктор', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Наседкин Владимир', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Михайлов Александр', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Сауткин Иван', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Чембарцев Игорь', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Тихонова Анна', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Леднёв Никита', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Тихонов Дмитрий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Тихонов Олег', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Ресурскомплект', 'city' => 'Саратов', 'fio' => 'Николаев Андрей', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Skolkovo Sailing Team', 'city' => 'Москва ', 'fio' => 'Гайкевич Кондратий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Skolkovo Sailing Team', 'city' => 'Москва ', 'fio' => 'Бербеков Алим', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Skolkovo Sailing Team', 'city' => 'Москва ', 'fio' => 'Митин Юрий  ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Skolkovo Sailing Team', 'city' => 'Москва ', 'fio' => 'Давидюк Андрей', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'КОМАТЕК Губернский яхт-клуб Коматек', 'city' => 'Екатеринбург ', 'fio' => 'Крюченков Константин', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'КОМАТЕК Губернский яхт-клуб Коматек', 'city' => 'Екатеринбург ', 'fio' => 'Катаев Сергей ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'КОМАТЕК Губернский яхт-клуб Коматек', 'city' => 'Екатеринбург ', 'fio' => 'Милентьев Максим  ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'КОМАТЕК Губернский яхт-клуб Коматек', 'city' => 'Екатеринбург ', 'fio' => 'Селюков Алексей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'КОМАТЕК Губернский яхт-клуб Коматек', 'city' => 'Екатеринбург ', 'fio' => 'Кузнецов Андрей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'КОМАТЕК Губернский яхт-клуб Коматек', 'city' => 'Екатеринбург ', 'fio' => 'Тимаков Антон', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'КОМАТЕК Губернский яхт-клуб Коматек', 'city' => 'Екатеринбург ', 'fio' => 'Фролов Вячеслав', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'КОМАТЕК Губернский яхт-клуб Коматек', 'city' => 'Екатеринбург ', 'fio' => 'Ступка Алексей', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'КОМАТЕК Губернский яхт-клуб Коматек', 'city' => 'Екатеринбург ', 'fio' => 'Крюченков Юрий ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'RUS7', 'city' => 'Москва/Таганрог ', 'fio' => 'Шевцов Сергей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'RUS7', 'city' => 'Москва/Таганрог ', 'fio' => 'Рожков Виталий  ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'RUS7', 'city' => 'Москва/Таганрог ', 'fio' => '  Воробьева Юлия   ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'RUS7', 'city' => 'Москва/Таганрог ', 'fio' => 'Арнаутов Анатолий', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Зуев Андрей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Грачев Алексей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Вавилов Алексей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Сивков Сергей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Калинина Анастасия', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Бочаров Александр', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Ксенофонтов Дмитрий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Бацанова Анна', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Буньков Алексей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Иваненков Никита', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Лебедев Александр', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Имаева Гульнара', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Батраков Иван', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Восток - Запад', 'city' => 'Новосибирск/Москва', 'fio' => 'Анишев Евгений', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'Императорский Яхт-клуб', 'city' => 'Москва', 'fio' => 'Чернов Евгений', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Императорский Яхт-клуб', 'city' => 'Москва', 'fio' => 'Пешков Сергей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Императорский Яхт-клуб', 'city' => 'Москва', 'fio' => 'Гущин-Кузнецов Юрий', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Императорский Яхт-клуб', 'city' => 'Москва', 'fio' => 'Гущин-Кузнецов Николай', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'Императорский Яхт-клуб', 'city' => 'Москва', 'fio' => 'Кузнецов Артем', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'ЦСКА', 'city' => 'Москва', 'fio' => 'Березкин Денис', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ЦСКА', 'city' => 'Москва', 'fio' => 'Ганженко Александр', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ЦСКА', 'city' => 'Москва', 'fio' => 'Патрушев Александр', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ЦСКА', 'city' => 'Москва', 'fio' => 'Тарасов Алексей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'ЦСКА', 'city' => 'Москва', 'fio' => 'Кожевников Илья', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'ЦСКА', 'city' => 'Москва', 'fio' => 'Михайлик Алексендр ', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'QPRO Sailing Team', 'city' => 'Москва', 'fio' => 'Авдонин Сергей', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'QPRO Sailing Team', 'city' => 'Москва', 'fio' => 'Андрианов Александр ', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'QPRO Sailing Team', 'city' => 'Москва', 'fio' => 'Гришунин Александр', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'QPRO Sailing Team', 'city' => 'Москва', 'fio' => 'Паунович Зоран', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'BLACK SEA', 'city' => 'Сочи ', 'fio' => 'Олег Кузьмин', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'BLACK SEA', 'city' => 'Сочи ', 'fio' => 'Вячеслав Иванов', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'BLACK SEA', 'city' => 'Сочи ', 'fio' => 'Андрей Игнатенко', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'BLACK SEA', 'city' => 'Сочи ', 'fio' => 'Артем Аветисян', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'BLACK SEA', 'city' => 'Сочи ', 'fio' => 'Инал Бербеков', 'dolzh' => 'Рулевой'),
            array('liga' => 'Высшая', 'team' => 'CALIPSO', 'city' => 'Туапсе', 'fio' => 'Верзаков Вадим', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'CALIPSO', 'city' => 'Туапсе', 'fio' => 'Крючков Николай', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'CALIPSO', 'city' => 'Туапсе', 'fio' => 'Зуев Егор', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'CALIPSO', 'city' => 'Туапсе', 'fio' => 'Таранов Максим', 'dolzh' => ''),
            array('liga' => 'Высшая', 'team' => 'CALIPSO', 'city' => 'Туапсе', 'fio' => 'Шишкин Владимир', 'dolzh' => 'Рулевой')
        );

        echo count($data);

        $bigTransaction = \Yii::$app->db->beginTransaction();
        try {



            foreach ($data as $t) {

                ini_set('memory_limit', '1024M');

                echo 'add member: ' . $t['fio'] . PHP_EOL . PHP_EOL ;

                $transaction = \Yii::$app->db->beginTransaction();

                try {


                    $fio = explode(' ', $t['fio']);

                    $member = new Members();
                    $member->firstname = $fio[1];
                    $member->lastname = $fio[0];

                    if (!$member->save(false)) { throw new \Exception(Json::encode($member->getErrors())); }

                    if ($t['liga'] == "Премьер") {
                        $liga = 2;
                    } else {
                        $liga = 1;
                    }


                    $teams = Teams::getList();

                    if (!in_array($t['team'], $teams)) {
                        $team = new Teams();
                        $team->title = $t['team'];
                        $team->city = $t['city'];
                        if (!$team->save(false)) { throw new \Exception(Json::encode($member->getErrors())); }
                        $teamId = $team->id;
                    } else {
                        $teamId = Teams::find()->where(['title' => $t['team']])->select('id')->scalar();
                    }


                    $teamsInLigs = array_keys( RelTeamsRelSeasonsLeagues::find()->indexBy('fk_teams')->all() );


                    if (!in_array($teamId, $teamsInLigs)) {

                        $relTeamLigs = new RelTeamsRelSeasonsLeagues();
                        $relTeamLigs->fk_teams = $teamId;
                        $relTeamLigs->fk_rel_seasons_leagues = $liga;
                        if (!$relTeamLigs->save(false)) { throw new \Exception(Json::encode($member->getErrors())); }
                        $relTeamLigsId = $relTeamLigs->id;

                    } else {

                        $relTeamLigsId = RelTeamsRelSeasonsLeagues::find()->where([ 'fk_teams' => $teamId ])->select('id')->scalar();
                    }


                    if ($t['dolzh'] == 'Рулевой') {
                        $dolzhId = 1;
                    } else {
                        $dolzhId = 2;
                    }


                    $teamMember = new TeamsStructures();
                    $teamMember->fk_members = $member->id;
                    $teamMember->fk_rel_teams_rel_seasons_league = $relTeamLigsId;
                    $teamMember->fk_roles = $dolzhId;
                    if (!$teamMember->save(false)) { throw new \Exception(Json::encode($member->getErrors())); }

                    $transaction->commit();

                } catch (\Exception $e) {

                    $transaction->rollBack();
                    echo Json::encode([$e->getMessage(), $e->getTraceAsString()]);

                }

            }

            $bigTransaction->commit();

        } catch (\Exception $e) {

            $bigTransaction->rollBack();
            echo Json::encode([$e->getTrace()]);

        }

    }


    public function actionTest()
    {
        echo Json::encode(array_keys( RelTeamsRelSeasonsLeagues::find()->select('fk_teams')->indexBy('fk_teams')->all() ));
    }

}



































