<?php

use yii\db\Migration;

/**
 * Class m190416_205152_add_agent_field_to_teams
 */
class m190416_205152_add_agent_field_to_teams extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('teams', 'agent', $this->integer()->unsigned()->defaultValue(2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('teams', 'agent');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190416_205152_add_agent_field_to_teams cannot be reverted.\n";

        return false;
    }
    */
}
