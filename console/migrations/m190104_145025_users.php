<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190104_145025_users
 */
class m190104_145025_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey()->unsigned(),
            'username' => $this->string()->notNull(),
            'password_hash' => $this->string(),
            'password_reset_token' => $this->string(),
            'email' => $this->string(),
            'auth_key' => $this->string(),
            'status' => $this->integer(),
            'created_at' => $this->integer()->defaultValue(null),
            'updated_at' => $this->integer()->defaultValue(null),
            'password' => $this->string(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {

        $this->dropTable('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190104_145025_users cannot be reverted.\n";

        return false;
    }
    */
}
