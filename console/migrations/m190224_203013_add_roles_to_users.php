<?php

use yii\db\Migration;

/**
 * Class m190224_203013_add_roles_to_users
 */
class m190224_203013_add_roles_to_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'roles', "ENUM('user', 'admin', 'manager', 'trainer') NOT NULL DEFAULT 'user'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('users', 'roles');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190224_203013_add_roles_to_users cannot be reverted.\n";

        return false;
    }
    */
}
