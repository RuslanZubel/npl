<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rel_receipt_trainings`.
 */
class m190201_122954_create_rel_receipt_trainings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rel_receipt_trainings', [
            'id' => $this->primaryKey()->unsigned(),
            'receipt_id' => $this->integer()->unsigned(),
            'training_id' => $this->integer()->unsigned(),
        ]);
//        $this->createIndex(
//            'idx_receipt',
//            'rel_receipt_trainings',
//            'receipt_id',
//            false
//        );
        $this->addForeignKey(
            "fk_rel_training_receipt",
            "{{%rel_receipt_trainings}}",
            "receipt_id",
            "{{%receipt}}",
            "id",
            'CASCADE'
        );

//        $this->createIndex(
//            'uq_id_training',
//            'rel_receipt_trainings',
//            'training_id',
//            false
//        );
        $this->addForeignKey(
            "fk_rel_receipt_training",
            "{{%rel_receipt_trainings}}",
            "training_id",
            "{{%training}}",
            "id",
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_rel_receipt_training',
            'rel_receipt_trainings'
        );

//        $this->dropIndex(
//            'uq_id_training',
//            'rel_receipt_trainings'
//        );

        $this->dropForeignKey(
            'fk_rel_training_receipt',
            'rel_receipt_trainings'
        );

//        $this->dropIndex(
//            'idx_receipt',
//            'rel_receipt_trainings'
//        );

        $this->dropTable('rel_receipt_trainings');
    }
}
