<?php

use yii\db\Migration;

/**
 * Class m190301_013323_add_boat_num_to_training
 */
class m190301_013323_add_boat_num_to_training extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('training', 'boat_number', $this->integer());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('training', 'boat_number');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190301_013323_add_boat_num_to_training cannot be reverted.\n";

        return false;
    }
    */
}
