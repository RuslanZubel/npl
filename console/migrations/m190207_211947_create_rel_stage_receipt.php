<?php

use yii\db\Migration;

/**
 * Class m190207_211947_create_rel_stage_receipt
 */
class m190207_211947_create_rel_stage_receipt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rel_receipt_stages', [
            'id' => $this->primaryKey()->unsigned(),
            'receipt_id' => $this->integer()->unsigned(),
            'stage_id' => $this->integer()->unsigned(),
        ]);

//        $this->createIndex(
//            'idx_receipt_s',
//            'rel_receipt_stages',
//            'receipt_id',
//            false
//        );
        $this->addForeignKey(
            "fk_rel_receipt_stages_receipt",
            "{{%rel_receipt_stages}}",
            "receipt_id",
            "{{%receipt}}",
            "id",
            'CASCADE'
        );

//        $this->createIndex(
//            'uq_id_training_s',
//            'rel_receipt_stages',
//            'stage_id',
//            false
//        );
        $this->addForeignKey(
            "fk_rel_receipt_stages_stage",
            "{{%rel_receipt_stages}}",
            "stage_id",
            "{{%stages}}",
            "id",
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey(
            'fk_rel_receipt_stages_receipt',
            'rel_receipt_stages'
        );

//        $this->dropIndex(
//            'idx_receipt_s',
//            'rel_receipt_stages'
//        );

        $this->dropForeignKey(
            'fk_rel_receipt_stages_stage',
            'rel_receipt_stages'
        );

//        $this->dropIndex(
//            'uq_id_training_s',
//            'rel_receipt_stages'
//        );

        $this->dropTable('rel_receipt_stages');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190207_211947_create_rel_stage_receipt cannot be reverted.\n";

        return false;
    }
    */
}
