<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%additional_trainings}}`.
 */
class m190318_181657_create_additional_trainings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%additional_trainings_period}}', [
            'id' => $this->primaryKey()->unsigned(),
            'start_date' => $this->dateTime()->notNull(),
            'end_date' => $this->dateTime()->notNull(),
            'stage_id' =>$this->integer()->unsigned(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%additional_trainings_period}}');
    }
}
