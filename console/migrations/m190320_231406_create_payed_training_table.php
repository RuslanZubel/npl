<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payed_training}}`.
 */
class m190320_231406_create_payed_training_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payed_training}}', [
            'id' => $this->primaryKey(),
            'team_id' =>$this->integer()->unsigned(),
            'training_id' =>$this->integer()->unsigned(),
            'free' =>$this->boolean()->defaultValue(false),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payed_training}}');
    }
}
