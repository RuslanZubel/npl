<?php

use yii\db\Migration;

/**
 * Handles the creation of table `training`.
 */
class m190201_121540_create_training_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('training', [
            'id' => $this->primaryKey()->unsigned(),
            'team_id' => $this->integer(11)->notNull()->unsigned(),
            'cost' => $this->integer()->notNull()->defaultValue(12000),
            'start_datetime' => $this->dateTime()->notNull(),
            'end_datetime' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull()->unsigned(),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
//        $this->createIndex('uq_id_teams','training','team_id', false);
        $this->addForeignKey("fk_training_to_team", "{{%training}}", "team_id", "{{%teams}}", "id", 'CASCADE');

//        $this->createIndex('uq_created_by','training','created_by', false);
        $this->addForeignKey("fk_training_to_user", "{{%training}}", "created_by", "{{%users}}", "id", 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_training_to_team',
            'training'
        );

        // drops index for column `author_id`
//        $this->dropIndex(
//            'uq_id_teams',
//            'training'
//        );
//
        $this->dropForeignKey(
            'fk_training_to_user',
            'training'
        );

        // drops index for column `author_id`
//        $this->dropIndex(
//            'uq_created_by',
//            'training'
//        );

        $this->dropTable('training');
    }
}
