<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%referee}}`.
 */
class m191231_093419_create_referee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%referee}}', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'stage_id' => $this->integer(),
            'contact_info' => $this->text(),
            'updated_at' => $this->timestamp()
                ->defaultExpression('CURRENT_TIMESTAMP')
                ->append('ON UPDATE CURRENT_TIMESTAMP'),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%referee}}');
    }
}
