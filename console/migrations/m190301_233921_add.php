<?php

use yii\db\Migration;

/**
 * Class m190301_233921_add
 */
class m190301_233921_add extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('training', 'stage_id', $this->integer()->unsigned());
        $this->addForeignKey(
            "fk_rel_training_stage",
            "{{%training}}",
            "stage_id",
            "{{%stages}}",
            "id",
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_rel_training_stage',
            'training'
        );
        $this->dropColumn('training', 'stage_id');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190301_233921_add cannot be reverted.\n";

        return false;
    }
    */
}
