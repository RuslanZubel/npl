<?php

use yii\db\Migration;

/**
 * Class m190623_073148_add_races_count_to_stages_table
 */
class m190623_073148_add_races_count_to_stages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('stages', 'calculation_races', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('stages', 'calculation_races');

    }

}
