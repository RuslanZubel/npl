<?php

use yii\db\Migration;

/**
 * Class m190530_125340_add_datetimes_to_receipts
 */
class m190530_125340_add_datetimes_to_receipts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('receipt', 'updated_at', $this->timestamp()
            ->defaultExpression('CURRENT_TIMESTAMP')
            ->append('ON UPDATE CURRENT_TIMESTAMP'));
        $this->addColumn('receipt', 'created_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('teams', 'updated_at');
        $this->dropColumn('teams', 'created_at');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190530_125340_add_datetimes_to_receipts cannot be reverted.\n";

        return false;
    }
    */
}
