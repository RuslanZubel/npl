<?php

use yii\db\Migration;

/**
 * Class m190221_220356_add_cost_field_to_stages
 */
class m190221_220356_add_cost_field_to_stages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('stages', 'cost', $this->integer()->defaultValue(60000));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('stages', 'cost');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190221_220356_add_cost_field_to_stages cannot be reverted.\n";

        return false;
    }
    */
}
