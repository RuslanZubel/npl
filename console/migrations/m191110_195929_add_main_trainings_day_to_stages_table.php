<?php

use yii\db\Migration;

/**
 * Class m191110_195929_add_main_trainings_day_to_stages_table
 */
class m191110_195929_add_main_trainings_day_to_stages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('stages', 'main_trainings_date', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('stages', 'main_trainings_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191110_195929_add_main_trainings_day_to_stages_table cannot be reverted.\n";

        return false;
    }
    */
}
