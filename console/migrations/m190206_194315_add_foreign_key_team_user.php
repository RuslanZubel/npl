<?php

use yii\db\Migration;

/**
 * Class m190206_194315_add_foreign_key_team_user
 */
class m190206_194315_add_foreign_key_team_user extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
//        $this->createIndex('uq_fk_author_x','teams','fk_author', false);

        $this->addForeignKey("fk_team_to_user", "{{%teams}}", "fk_author", "{{%users}}", "id", 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey(
            'fk_team_to_user',
            'teams'
        );
//        $this->dropIndex(
//            'uq_fk_author_x',
//            'teams'
//        );


    }
}
