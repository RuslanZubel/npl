<?php

use yii\db\Migration;

/**
 * Class m190425_065756_create_cp_receipt_data_field_in_receipts
 */
class m190425_065756_create_cp_receipt_data_field_in_receipts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('receipt', 'cp_data', $this->text());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('receipt', 'cp_data');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190425_065756_create_cp_receipt_data_field_in_receipts cannot be reverted.\n";

        return false;
    }
    */
}
