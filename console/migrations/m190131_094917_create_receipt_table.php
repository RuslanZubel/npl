<?php

use yii\db\Migration;

/**
 * Handles the creation of table `receipt`.
 */
class m190131_094917_create_receipt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('receipt', [
            'id' => $this->primaryKey()->unsigned(),
            'team_id' => $this->integer(11)->notNull()->unsigned(),
            'created_by' => $this->integer()->notNull()->unsigned(),
            'product_type' => "ENUM('training', 'stage') NOT NULL",
            'confirmed' => $this->boolean()->defaultValue(false)
        ]);
//        $this->createIndex('uq_id_teams','receipt','team_id', false);
        $this->addForeignKey("fk_teams", "{{%receipt}}", "team_id", "{{%teams}}", "id", 'CASCADE');


//        $this->createIndex('uq_created_by_rec','receipt','created_by', false);
        $this->addForeignKey("fk_receipt_to_user", "{{%receipt}}", "created_by", "{{%users}}", "id", 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_receipt_to_user',
            'receipt'
        );

        // drops index for column `author_id`
//        $this->dropIndex(
//            'uq_created_by_rec',
//            'receipt'
//        );

        $this->dropForeignKey(
            'fk_teams',
            'receipt'
        );

//        $this->dropIndex(
//            'uq_id_teams',
//            'receipt'
//        );
        $this->dropTable('receipt');
    }
}
